{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 24,
  "bbox_top": 0,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 25,
  "height": 23,
  "textureGroupId": {
    "name": "IcelandTexture",
    "path": "texturegroups/IcelandTexture",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"06f0960b-654e-4cbc-aeb8-ea380b223752","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06f0960b-654e-4cbc-aeb8-ea380b223752","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"25a651f2-1356-41dc-ba75-1a9fb0920270","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"06f0960b-654e-4cbc-aeb8-ea380b223752","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"e7b18980-8d97-400e-8804-622ce104ed3f","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"06f0960b-654e-4cbc-aeb8-ea380b223752","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e0fb3e55-17ca-4e5e-ac81-7c6e326fcd47","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e0fb3e55-17ca-4e5e-ac81-7c6e326fcd47","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"25a651f2-1356-41dc-ba75-1a9fb0920270","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e0fb3e55-17ca-4e5e-ac81-7c6e326fcd47","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"e7b18980-8d97-400e-8804-622ce104ed3f","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"e0fb3e55-17ca-4e5e-ac81-7c6e326fcd47","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a04994b7-67ff-498c-9f10-70459b37fafb","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a04994b7-67ff-498c-9f10-70459b37fafb","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"25a651f2-1356-41dc-ba75-1a9fb0920270","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"a04994b7-67ff-498c-9f10-70459b37fafb","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"e7b18980-8d97-400e-8804-622ce104ed3f","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"a04994b7-67ff-498c-9f10-70459b37fafb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aecd994d-d54b-4333-ae51-b2a2f859aba7","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aecd994d-d54b-4333-ae51-b2a2f859aba7","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"25a651f2-1356-41dc-ba75-1a9fb0920270","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"aecd994d-d54b-4333-ae51-b2a2f859aba7","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"e7b18980-8d97-400e-8804-622ce104ed3f","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"aecd994d-d54b-4333-ae51-b2a2f859aba7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"241949b1-5f85-4bf2-b482-296e20e5767c","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"241949b1-5f85-4bf2-b482-296e20e5767c","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"25a651f2-1356-41dc-ba75-1a9fb0920270","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"241949b1-5f85-4bf2-b482-296e20e5767c","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"e7b18980-8d97-400e-8804-622ce104ed3f","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"241949b1-5f85-4bf2-b482-296e20e5767c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c68d776-e0bf-45e6-956c-fcf9a3f6f86a","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c68d776-e0bf-45e6-956c-fcf9a3f6f86a","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"25a651f2-1356-41dc-ba75-1a9fb0920270","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"3c68d776-e0bf-45e6-956c-fcf9a3f6f86a","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"LayerId":{"name":"e7b18980-8d97-400e-8804-622ce104ed3f","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","name":"3c68d776-e0bf-45e6-956c-fcf9a3f6f86a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e15a0197-044f-4a9c-86e6-f9bfce42c4b2","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06f0960b-654e-4cbc-aeb8-ea380b223752","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f8fa84f-fbc4-44f0-8057-1cbb48851de7","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e0fb3e55-17ca-4e5e-ac81-7c6e326fcd47","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"47419772-75ed-44ef-9053-76b658ffea99","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a04994b7-67ff-498c-9f10-70459b37fafb","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"72b1a2ef-39d6-4af3-93d2-d8b4a4b7af6d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aecd994d-d54b-4333-ae51-b2a2f859aba7","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa69e347-feba-4817-8b8d-40cce5b92435","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"241949b1-5f85-4bf2-b482-296e20e5767c","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2c553079-9981-4162-ae35-27a9d05ceb4c","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c68d776-e0bf-45e6-956c-fcf9a3f6f86a","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_spikes_ice","path":"sprites/spr_spikes_ice/spr_spikes_ice.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"25a651f2-1356-41dc-ba75-1a9fb0920270","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 3","resourceVersion":"1.0","name":"e7b18980-8d97-400e-8804-622ce104ed3f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Spikes",
    "path": "folders/Sprites/Traps/Spikes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_spikes_ice",
  "tags": [],
  "resourceType": "GMSprite",
}