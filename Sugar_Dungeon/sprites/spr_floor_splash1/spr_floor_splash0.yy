{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 223,
  "bbox_top": 0,
  "bbox_bottom": 93,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 224,
  "height": 94,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":{"name":"ca019ff9-4527-4a7f-a0da-1137dc775dba","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":{"name":"048b3e51-4f7e-4e20-bd2d-1c739372b4c2","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":{"name":"36d9d0f0-f242-45de-95f4-a2b8f058f0fe","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":{"name":"78806c00-1f9f-40b9-9e2d-937c4369b07f","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":{"name":"4d21957b-ecd2-4e73-8261-ff957c1dc5e8","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":{"name":"06949493-27d7-4b9f-9a01-b04e3a71cc5c","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"LayerId":{"name":"cd2ee410-e096-4a39-b89a-2deaa403011d","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_floor_splash0","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","name":"1c6f1781-132b-4720-b52b-822a017a2a95","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_floor_splash0","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4a84e571-7e38-4826-ba8c-3631cf9e0dda","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1c6f1781-132b-4720-b52b-822a017a2a95","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 112,
    "yorigin": 47,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_floor_splash0","path":"sprites/spr_floor_splash0/spr_floor_splash0.yy",},
    "resourceVersion": "1.3",
    "name": "spr_floor_splash0",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cd2ee410-e096-4a39-b89a-2deaa403011d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Floor 0",
    "path": "folders/Sprites/Tiles, backgrounds and likes/Floor 0.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_floor_splash0",
  "tags": [],
  "resourceType": "GMSprite",
}