{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 639,
  "bbox_top": 0,
  "bbox_bottom": 319,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 640,
  "height": 320,
  "textureGroupId": {
    "name": "TilemapIce",
    "path": "texturegroups/TilemapIce",
  },
  "swatchColours": null,
  "gridX": 64,
  "gridY": 64,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3314e1fe-3cb1-4ff3-80cb-8dd7d59fd66c","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3314e1fe-3cb1-4ff3-80cb-8dd7d59fd66c","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},"LayerId":{"name":"6e7722c6-3fd8-432b-9ce2-d3f79285626a","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"3314e1fe-3cb1-4ff3-80cb-8dd7d59fd66c","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},"LayerId":{"name":"800eca05-d804-4c7e-9c0b-c3ab20316821","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_tileset_decor1","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},"resourceVersion":"1.0","name":"3314e1fe-3cb1-4ff3-80cb-8dd7d59fd66c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_tileset_decor1","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"86cfa59a-f6ba-44b8-a551-e59532bd579a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3314e1fe-3cb1-4ff3-80cb-8dd7d59fd66c","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_tileset_decor1","path":"sprites/spr_tileset_decor1/spr_tileset_decor1.yy",},
    "resourceVersion": "1.3",
    "name": "spr_tileset_decor1",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"6e7722c6-3fd8-432b-9ce2-d3f79285626a","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"800eca05-d804-4c7e-9c0b-c3ab20316821","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Floor 1",
    "path": "folders/Sprites/Tiles, backgrounds and likes/Floor 1.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_tileset_decor1",
  "tags": [],
  "resourceType": "GMSprite",
}