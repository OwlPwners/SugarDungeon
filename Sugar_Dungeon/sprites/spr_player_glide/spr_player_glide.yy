{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 51,
  "bbox_top": 0,
  "bbox_bottom": 51,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 52,
  "height": 52,
  "textureGroupId": {
    "name": "PlayerTexture",
    "path": "texturegroups/PlayerTexture",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"aee39ff7-c78c-4091-a40f-d34156dfb0ee","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aee39ff7-c78c-4091-a40f-d34156dfb0ee","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"aee39ff7-c78c-4091-a40f-d34156dfb0ee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f695419f-b4d8-40bd-9138-c15b31cf511f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f695419f-b4d8-40bd-9138-c15b31cf511f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"f695419f-b4d8-40bd-9138-c15b31cf511f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9e0403ee-387c-48f8-b9ad-718b355f7f7b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9e0403ee-387c-48f8-b9ad-718b355f7f7b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"9e0403ee-387c-48f8-b9ad-718b355f7f7b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1b2d05c9-88ff-4d24-8969-e505b1ba6379","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1b2d05c9-88ff-4d24-8969-e505b1ba6379","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"1b2d05c9-88ff-4d24-8969-e505b1ba6379","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8d9f71b9-d012-4849-82f1-6a9b9feec410","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8d9f71b9-d012-4849-82f1-6a9b9feec410","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"8d9f71b9-d012-4849-82f1-6a9b9feec410","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9f76e0e4-490a-4e43-b423-f097718e11cc","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9f76e0e4-490a-4e43-b423-f097718e11cc","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"9f76e0e4-490a-4e43-b423-f097718e11cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65692752-930d-493e-b77f-f49017a77042","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65692752-930d-493e-b77f-f49017a77042","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"65692752-930d-493e-b77f-f49017a77042","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8e53f32a-3f8a-43e8-bd93-4b77a60a2c33","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8e53f32a-3f8a-43e8-bd93-4b77a60a2c33","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"8e53f32a-3f8a-43e8-bd93-4b77a60a2c33","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"da6d80bf-ded8-4365-8ded-567b47e7db9f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"da6d80bf-ded8-4365-8ded-567b47e7db9f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"da6d80bf-ded8-4365-8ded-567b47e7db9f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"83b329be-b7f1-4ab7-bcc2-6746eef3315c","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"83b329be-b7f1-4ab7-bcc2-6746eef3315c","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"83b329be-b7f1-4ab7-bcc2-6746eef3315c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49824e64-83b8-4420-98c7-5f8111e3b423","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49824e64-83b8-4420-98c7-5f8111e3b423","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"49824e64-83b8-4420-98c7-5f8111e3b423","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"36117a27-67d1-4bdc-aa3f-fe54c1c4c53b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"36117a27-67d1-4bdc-aa3f-fe54c1c4c53b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"36117a27-67d1-4bdc-aa3f-fe54c1c4c53b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"de15db7f-01e6-4258-9fde-f6c17a7834cc","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"de15db7f-01e6-4258-9fde-f6c17a7834cc","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"de15db7f-01e6-4258-9fde-f6c17a7834cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"14e36f80-fa29-4ba1-a8a8-21b346c3cd3f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"14e36f80-fa29-4ba1-a8a8-21b346c3cd3f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"14e36f80-fa29-4ba1-a8a8-21b346c3cd3f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4bb5f085-e7fd-458b-90e1-5a995fe013f2","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4bb5f085-e7fd-458b-90e1-5a995fe013f2","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"4bb5f085-e7fd-458b-90e1-5a995fe013f2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cb70054a-16a6-498b-b6f7-e7c2322f638b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cb70054a-16a6-498b-b6f7-e7c2322f638b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"cb70054a-16a6-498b-b6f7-e7c2322f638b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3b31a4b1-949e-4bd5-95e7-d3214125bbfa","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3b31a4b1-949e-4bd5-95e7-d3214125bbfa","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"3b31a4b1-949e-4bd5-95e7-d3214125bbfa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"13a69753-9e50-46cf-a4d0-da9223a3f348","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"13a69753-9e50-46cf-a4d0-da9223a3f348","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"13a69753-9e50-46cf-a4d0-da9223a3f348","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b4a791de-605f-4332-93a2-0763b8d7396c","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b4a791de-605f-4332-93a2-0763b8d7396c","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"b4a791de-605f-4332-93a2-0763b8d7396c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d180ca3f-29da-4e2c-98af-164d961c5444","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d180ca3f-29da-4e2c-98af-164d961c5444","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"d180ca3f-29da-4e2c-98af-164d961c5444","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"67abd796-6bf2-46d5-bb94-43a1a8ee617b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"67abd796-6bf2-46d5-bb94-43a1a8ee617b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"67abd796-6bf2-46d5-bb94-43a1a8ee617b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70e19dba-cbdd-4343-88fd-dbbc3f2a845e","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70e19dba-cbdd-4343-88fd-dbbc3f2a845e","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"70e19dba-cbdd-4343-88fd-dbbc3f2a845e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c8cc7a1f-2139-4ee4-b792-0fbd237f7429","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c8cc7a1f-2139-4ee4-b792-0fbd237f7429","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"c8cc7a1f-2139-4ee4-b792-0fbd237f7429","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a9d47ab-b9fa-4898-a80c-3f66e5c7f28f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a9d47ab-b9fa-4898-a80c-3f66e5c7f28f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"7a9d47ab-b9fa-4898-a80c-3f66e5c7f28f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ccbe667e-37ca-48a4-a44b-95c3562dbd25","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ccbe667e-37ca-48a4-a44b-95c3562dbd25","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"ccbe667e-37ca-48a4-a44b-95c3562dbd25","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e70dddf4-03f7-474f-a261-26b77a8fca77","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e70dddf4-03f7-474f-a261-26b77a8fca77","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"e70dddf4-03f7-474f-a261-26b77a8fca77","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0786c02c-87cb-4372-9402-03ef7db648ca","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0786c02c-87cb-4372-9402-03ef7db648ca","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"0786c02c-87cb-4372-9402-03ef7db648ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ed1c9696-e0d5-4b12-a14b-e9e87585cf6e","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ed1c9696-e0d5-4b12-a14b-e9e87585cf6e","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"ed1c9696-e0d5-4b12-a14b-e9e87585cf6e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7de1f43b-2854-4063-8e50-760a76e71525","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7de1f43b-2854-4063-8e50-760a76e71525","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"7de1f43b-2854-4063-8e50-760a76e71525","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"59c92746-3384-4842-898e-619901bd1456","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"59c92746-3384-4842-898e-619901bd1456","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"59c92746-3384-4842-898e-619901bd1456","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00928f22-611f-46a9-8bc7-5aed57097f81","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00928f22-611f-46a9-8bc7-5aed57097f81","path":"sprites/spr_player_glide/spr_player_glide.yy",},"LayerId":{"name":"d49a39de-e20a-4aef-9413-7040413ba5f7","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","name":"00928f22-611f-46a9-8bc7-5aed57097f81","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 33.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 31.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"252767a4-a371-4dbe-a9fe-31c74146fb44","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aee39ff7-c78c-4091-a40f-d34156dfb0ee","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"19e3900f-bb35-40ef-914e-e047c71a5153","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f695419f-b4d8-40bd-9138-c15b31cf511f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8435ff37-4857-42c2-830d-dfd8f3f57e73","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9e0403ee-387c-48f8-b9ad-718b355f7f7b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b2e97fcd-f1a0-436e-ad63-ddd30222d38c","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b2d05c9-88ff-4d24-8969-e505b1ba6379","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9d48f5b4-eff2-47d3-ba86-89a314cf8fc8","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8d9f71b9-d012-4849-82f1-6a9b9feec410","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e7d7f33c-0061-4367-b7ab-c619df797c4b","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f76e0e4-490a-4e43-b423-f097718e11cc","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d2793ed4-834c-4013-b18a-47abb5d7826d","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65692752-930d-493e-b77f-f49017a77042","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9d12954-038d-4bc4-afa3-63b5c7a8e005","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8e53f32a-3f8a-43e8-bd93-4b77a60a2c33","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"96e08a0e-fe6a-441c-ad32-709843b663ce","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"da6d80bf-ded8-4365-8ded-567b47e7db9f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb74b2ca-96c0-4cdc-aa5a-49bc22bc2996","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"83b329be-b7f1-4ab7-bcc2-6746eef3315c","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"88a6719e-5278-45db-a27a-ac6e59c08e31","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49824e64-83b8-4420-98c7-5f8111e3b423","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9b491bab-816d-4c4e-bd87-2cbb6a1dabc5","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"36117a27-67d1-4bdc-aa3f-fe54c1c4c53b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"01b1efae-5418-46f1-9d7d-2e6ebf0c3ee6","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"de15db7f-01e6-4258-9fde-f6c17a7834cc","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ac82f47f-d612-4cb5-bc45-faf1ed42c38a","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"14e36f80-fa29-4ba1-a8a8-21b346c3cd3f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5b2eee1a-59a4-4650-8575-d8fd75100538","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4bb5f085-e7fd-458b-90e1-5a995fe013f2","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3d36a2e2-3650-4200-b403-318eb5985ce7","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb70054a-16a6-498b-b6f7-e7c2322f638b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7f65a7e6-86c4-4251-a08e-38a9bbadb989","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3b31a4b1-949e-4bd5-95e7-d3214125bbfa","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a495c6a-1906-4050-a504-e1d4eb306476","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"13a69753-9e50-46cf-a4d0-da9223a3f348","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4b040a58-528f-4987-ad6b-3f731e7ce27d","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b4a791de-605f-4332-93a2-0763b8d7396c","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"40456964-8481-43e5-8359-69d6ebf915cb","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d180ca3f-29da-4e2c-98af-164d961c5444","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d22b68b8-5258-4e5b-9f4b-3c641ba21401","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"67abd796-6bf2-46d5-bb94-43a1a8ee617b","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aa64c23c-4762-4690-9e06-435a4b8614ef","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70e19dba-cbdd-4343-88fd-dbbc3f2a845e","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"58019b11-b294-463e-9f0f-aad8682884e6","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c8cc7a1f-2139-4ee4-b792-0fbd237f7429","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"74e28716-04b1-4610-83cb-cf48a3f862ff","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a9d47ab-b9fa-4898-a80c-3f66e5c7f28f","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48a7e962-9f2b-4024-b563-d760310ead5b","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ccbe667e-37ca-48a4-a44b-95c3562dbd25","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9bd24dc4-a85a-4514-8704-a5fcc2c53290","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e70dddf4-03f7-474f-a261-26b77a8fca77","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9e93e701-e371-4348-97e2-f687afcb724f","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0786c02c-87cb-4372-9402-03ef7db648ca","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc6c9e17-6b9e-4f0a-ba83-d39916242544","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ed1c9696-e0d5-4b12-a14b-e9e87585cf6e","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"835139d5-0733-4bd1-9585-42f260cf812e","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7de1f43b-2854-4063-8e50-760a76e71525","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b6b78697-014b-407a-a414-d665e72defe1","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"59c92746-3384-4842-898e-619901bd1456","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef4493d1-35cc-475d-befb-de70d1107d9f","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00928f22-611f-46a9-8bc7-5aed57097f81","path":"sprites/spr_player_glide/spr_player_glide.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 33,
    "yorigin": 28,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_glide","path":"sprites/spr_player_glide/spr_player_glide.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d49a39de-e20a-4aef-9413-7040413ba5f7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_glide",
  "tags": [],
  "resourceType": "GMSprite",
}