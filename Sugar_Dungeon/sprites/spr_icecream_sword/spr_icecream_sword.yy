{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 25,
  "bbox_top": 1,
  "bbox_bottom": 25,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 27,
  "height": 27,
  "textureGroupId": {
    "name": "Items",
    "path": "texturegroups/Items",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a203db7a-66d3-4688-9441-5e3babc722d7","path":"sprites/spr_icecream_sword/spr_icecream_sword.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a203db7a-66d3-4688-9441-5e3babc722d7","path":"sprites/spr_icecream_sword/spr_icecream_sword.yy",},"LayerId":{"name":"3d21a6dd-5d06-49d2-a07b-8916eadd6212","path":"sprites/spr_icecream_sword/spr_icecream_sword.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icecream_sword","path":"sprites/spr_icecream_sword/spr_icecream_sword.yy",},"resourceVersion":"1.0","name":"a203db7a-66d3-4688-9441-5e3babc722d7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_icecream_sword","path":"sprites/spr_icecream_sword/spr_icecream_sword.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6627484a-a2b6-42f8-9496-d48b483171aa","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a203db7a-66d3-4688-9441-5e3babc722d7","path":"sprites/spr_icecream_sword/spr_icecream_sword.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_icecream_sword","path":"sprites/spr_icecream_sword/spr_icecream_sword.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3d21a6dd-5d06-49d2-a07b-8916eadd6212","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Icons",
    "path": "folders/Sprites/Items/Icons.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_icecream_sword",
  "tags": [],
  "resourceType": "GMSprite",
}