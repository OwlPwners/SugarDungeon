{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 19,
  "bbox_top": 0,
  "bbox_bottom": 47,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 20,
  "height": 48,
  "textureGroupId": {
    "name": "IcelandTexture",
    "path": "texturegroups/IcelandTexture",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"35a1de94-578e-4f70-bdd8-048d69dc0ee0","path":"sprites/spr_ice_golem_spikes/spr_ice_golem_spikes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"35a1de94-578e-4f70-bdd8-048d69dc0ee0","path":"sprites/spr_ice_golem_spikes/spr_ice_golem_spikes.yy",},"LayerId":{"name":"0840f18c-57eb-40bd-b4b6-420044114d55","path":"sprites/spr_ice_golem_spikes/spr_ice_golem_spikes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_spikes","path":"sprites/spr_ice_golem_spikes/spr_ice_golem_spikes.yy",},"resourceVersion":"1.0","name":"35a1de94-578e-4f70-bdd8-048d69dc0ee0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ice_golem_spikes","path":"sprites/spr_ice_golem_spikes/spr_ice_golem_spikes.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"03e462c3-e0e4-4d54-a817-5820eb912f9f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"35a1de94-578e-4f70-bdd8-048d69dc0ee0","path":"sprites/spr_ice_golem_spikes/spr_ice_golem_spikes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 10,
    "yorigin": 47,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ice_golem_spikes","path":"sprites/spr_ice_golem_spikes/spr_ice_golem_spikes.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"0840f18c-57eb-40bd-b4b6-420044114d55","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Ice golem",
    "path": "folders/Sprites/Enemies/Ice golem.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ice_golem_spikes",
  "tags": [],
  "resourceType": "GMSprite",
}