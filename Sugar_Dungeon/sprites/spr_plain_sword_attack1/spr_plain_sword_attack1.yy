{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 8,
  "bbox_right": 39,
  "bbox_top": 7,
  "bbox_bottom": 37,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 40,
  "height": 44,
  "textureGroupId": {
    "name": "Items",
    "path": "texturegroups/Items",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5c78e95b-4566-4d63-ac66-1a7bec9709df","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c78e95b-4566-4d63-ac66-1a7bec9709df","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"5c78e95b-4566-4d63-ac66-1a7bec9709df","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c7880e61-7b3f-43b8-bf6f-ed1eca42c4bf","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c7880e61-7b3f-43b8-bf6f-ed1eca42c4bf","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"c7880e61-7b3f-43b8-bf6f-ed1eca42c4bf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f103b61b-3317-425e-bd22-8bd9176da87c","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f103b61b-3317-425e-bd22-8bd9176da87c","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"f103b61b-3317-425e-bd22-8bd9176da87c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2996dd2c-c19b-4a7c-89e1-8f7b94298304","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2996dd2c-c19b-4a7c-89e1-8f7b94298304","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"2996dd2c-c19b-4a7c-89e1-8f7b94298304","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd6e6895-7f55-4e39-9537-7747927b3e3a","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd6e6895-7f55-4e39-9537-7747927b3e3a","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"dd6e6895-7f55-4e39-9537-7747927b3e3a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8f5159e1-ec7d-40d5-8d24-6b1c786ae218","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8f5159e1-ec7d-40d5-8d24-6b1c786ae218","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"8f5159e1-ec7d-40d5-8d24-6b1c786ae218","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"638d85fe-0701-4b3b-9268-79355dbfcd7c","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"638d85fe-0701-4b3b-9268-79355dbfcd7c","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"638d85fe-0701-4b3b-9268-79355dbfcd7c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b3b498ae-2445-42f5-b7b1-89d254761e43","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b3b498ae-2445-42f5-b7b1-89d254761e43","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"b3b498ae-2445-42f5-b7b1-89d254761e43","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4282f8b6-d7a6-413c-88c3-36672a239831","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4282f8b6-d7a6-413c-88c3-36672a239831","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"4282f8b6-d7a6-413c-88c3-36672a239831","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"660ef581-92f0-46a0-a492-2ca3227c2305","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"660ef581-92f0-46a0-a492-2ca3227c2305","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"660ef581-92f0-46a0-a492-2ca3227c2305","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"64ebaad6-73c2-4d4c-b876-dffabbad8ea1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"64ebaad6-73c2-4d4c-b876-dffabbad8ea1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"64ebaad6-73c2-4d4c-b876-dffabbad8ea1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"01ee1446-8c6d-4b2d-be4e-eb94a12cc5a9","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"01ee1446-8c6d-4b2d-be4e-eb94a12cc5a9","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"01ee1446-8c6d-4b2d-be4e-eb94a12cc5a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"970175e7-4a34-4f4e-bc9f-a80174180381","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"970175e7-4a34-4f4e-bc9f-a80174180381","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"970175e7-4a34-4f4e-bc9f-a80174180381","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"077ed7af-ec64-45d8-92c2-d18df78225d4","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"077ed7af-ec64-45d8-92c2-d18df78225d4","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"077ed7af-ec64-45d8-92c2-d18df78225d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b48aff19-dce0-4487-8b90-556af1783490","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b48aff19-dce0-4487-8b90-556af1783490","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"b48aff19-dce0-4487-8b90-556af1783490","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"883071e9-0714-4dae-87d7-8bc031a69444","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"883071e9-0714-4dae-87d7-8bc031a69444","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"883071e9-0714-4dae-87d7-8bc031a69444","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"35b22186-e6ac-472a-aa3f-e1da96caaee2","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"35b22186-e6ac-472a-aa3f-e1da96caaee2","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"35b22186-e6ac-472a-aa3f-e1da96caaee2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"328adfbb-150d-4cd8-99d0-caef12155e46","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"328adfbb-150d-4cd8-99d0-caef12155e46","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"328adfbb-150d-4cd8-99d0-caef12155e46","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06a187f4-d4a2-4b5c-bddc-b9928a440938","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06a187f4-d4a2-4b5c-bddc-b9928a440938","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"06a187f4-d4a2-4b5c-bddc-b9928a440938","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ddb02ea3-5fc2-4e7f-b028-0ace83868f87","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ddb02ea3-5fc2-4e7f-b028-0ace83868f87","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"ddb02ea3-5fc2-4e7f-b028-0ace83868f87","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"df7e4f3b-47f0-491f-855e-397271c3d696","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"df7e4f3b-47f0-491f-855e-397271c3d696","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"df7e4f3b-47f0-491f-855e-397271c3d696","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5bce8a57-c263-49d1-a82f-770229105538","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5bce8a57-c263-49d1-a82f-770229105538","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"5bce8a57-c263-49d1-a82f-770229105538","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0a2f5702-e1fb-4a86-a50d-266a947e2c18","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0a2f5702-e1fb-4a86-a50d-266a947e2c18","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"0a2f5702-e1fb-4a86-a50d-266a947e2c18","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cd483f8b-4018-44f4-8db8-18f532a84842","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cd483f8b-4018-44f4-8db8-18f532a84842","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"cd483f8b-4018-44f4-8db8-18f532a84842","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c6a722f7-3ab3-4458-af12-a677c2b9eae6","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6a722f7-3ab3-4458-af12-a677c2b9eae6","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"LayerId":{"name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","name":"c6a722f7-3ab3-4458-af12-a677c2b9eae6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 35.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 25.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"596360b9-ac41-47d6-848f-faba681c4e56","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c78e95b-4566-4d63-ac66-1a7bec9709df","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"086e1452-f3b3-437b-81d1-055d3073c2c9","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c7880e61-7b3f-43b8-bf6f-ed1eca42c4bf","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"39ab7cfc-5af5-4742-82cf-c7024d853476","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f103b61b-3317-425e-bd22-8bd9176da87c","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"890c9798-e6f5-4c2b-8ea6-457b4ace863b","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2996dd2c-c19b-4a7c-89e1-8f7b94298304","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ff662ca8-39cc-472e-83cb-eb43ccc9873e","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd6e6895-7f55-4e39-9537-7747927b3e3a","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e35b1aaa-7916-442b-a07f-b198b2e9cdeb","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8f5159e1-ec7d-40d5-8d24-6b1c786ae218","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7005edfb-a6c3-47df-a143-bce5465f9523","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"638d85fe-0701-4b3b-9268-79355dbfcd7c","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f489694b-1978-4e17-8906-c32e50c954e5","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b3b498ae-2445-42f5-b7b1-89d254761e43","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"27356f86-24f7-4038-8a75-7a37d2349d29","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4282f8b6-d7a6-413c-88c3-36672a239831","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"04dc85b4-f946-41b4-bbc3-4054b4112522","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"660ef581-92f0-46a0-a492-2ca3227c2305","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"affa2a8b-fcc3-403b-8893-3bc60a5558ca","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"64ebaad6-73c2-4d4c-b876-dffabbad8ea1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8e5c9f05-8fbb-40fa-88da-9f8e1d535e2d","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"01ee1446-8c6d-4b2d-be4e-eb94a12cc5a9","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b306ffa1-e04d-467a-95a3-5828384316ee","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"970175e7-4a34-4f4e-bc9f-a80174180381","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0863402-8035-487b-a518-a84f2508aaf1","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"077ed7af-ec64-45d8-92c2-d18df78225d4","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"94b90833-4197-4651-893a-43c6db77f5bd","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b48aff19-dce0-4487-8b90-556af1783490","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1e336cb-bca4-42ad-9b38-afc3a735ed2b","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"883071e9-0714-4dae-87d7-8bc031a69444","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"de462246-5eb2-4473-9f52-e6770dce5939","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"35b22186-e6ac-472a-aa3f-e1da96caaee2","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"516317ea-330b-49e5-ad2b-8e3a96f376f1","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"328adfbb-150d-4cd8-99d0-caef12155e46","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d7e40876-ad62-471c-bf78-568357a0db00","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06a187f4-d4a2-4b5c-bddc-b9928a440938","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"518abdd1-5cd1-43d8-bda3-2185e03cb12c","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ddb02ea3-5fc2-4e7f-b028-0ace83868f87","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fed269f2-d22a-4761-8958-4f50d5bd786c","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"df7e4f3b-47f0-491f-855e-397271c3d696","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8d1a0fc4-57f7-40c2-b6cd-56f5f552f409","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5bce8a57-c263-49d1-a82f-770229105538","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6e4620e9-8781-476f-89e8-02e1fa404d95","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0a2f5702-e1fb-4a86-a50d-266a947e2c18","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6769842-f585-4122-8ee9-dfb7ff0d8ab3","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cd483f8b-4018-44f4-8db8-18f532a84842","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0624213-1aed-48f5-b1f3-55a1834f16bb","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6a722f7-3ab3-4458-af12-a677c2b9eae6","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 13,
    "yorigin": 25,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_plain_sword_attack1","path":"sprites/spr_plain_sword_attack1/spr_plain_sword_attack1.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f529bcad-4b56-4619-8f29-cc2e2c87ec2e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Plain sword",
    "path": "folders/Sprites/Items/Sprites/Plain/Plain sword.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_plain_sword_attack1",
  "tags": [],
  "resourceType": "GMSprite",
}