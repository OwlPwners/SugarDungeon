{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 10,
  "bbox_right": 41,
  "bbox_top": 6,
  "bbox_bottom": 64,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 56,
  "height": 65,
  "textureGroupId": {
    "name": "IcelandTexture",
    "path": "texturegroups/IcelandTexture",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"61e9262c-9f7f-4122-8114-b6d9354379e7","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"61e9262c-9f7f-4122-8114-b6d9354379e7","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"61e9262c-9f7f-4122-8114-b6d9354379e7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0eaee54e-0ed1-44ce-83ff-c0374d744f72","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0eaee54e-0ed1-44ce-83ff-c0374d744f72","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"0eaee54e-0ed1-44ce-83ff-c0374d744f72","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3fee2f13-47e9-4dc0-8b90-7db3e75c31bc","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3fee2f13-47e9-4dc0-8b90-7db3e75c31bc","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"3fee2f13-47e9-4dc0-8b90-7db3e75c31bc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4ace8033-b5c7-400a-9c9d-ae20da7396a4","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4ace8033-b5c7-400a-9c9d-ae20da7396a4","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"4ace8033-b5c7-400a-9c9d-ae20da7396a4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eeebbd85-009e-46b6-8a6e-3fa4505c69d0","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eeebbd85-009e-46b6-8a6e-3fa4505c69d0","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"eeebbd85-009e-46b6-8a6e-3fa4505c69d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"25ed9701-b9b3-48b1-9833-06a741c4aa87","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25ed9701-b9b3-48b1-9833-06a741c4aa87","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"25ed9701-b9b3-48b1-9833-06a741c4aa87","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"27dad9bf-99bf-4291-9ef2-24880c7aac58","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"27dad9bf-99bf-4291-9ef2-24880c7aac58","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"27dad9bf-99bf-4291-9ef2-24880c7aac58","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6c92dd1f-7110-42fa-ae32-6ba9094f569a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6c92dd1f-7110-42fa-ae32-6ba9094f569a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"6c92dd1f-7110-42fa-ae32-6ba9094f569a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65246d08-e06d-4c44-b6a3-8ea1cfffe61c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65246d08-e06d-4c44-b6a3-8ea1cfffe61c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"65246d08-e06d-4c44-b6a3-8ea1cfffe61c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee70adfd-536a-49f0-b67c-f24c65f694f3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee70adfd-536a-49f0-b67c-f24c65f694f3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"ee70adfd-536a-49f0-b67c-f24c65f694f3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6e9d327-8b79-445c-b39c-affc5039a52f","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6e9d327-8b79-445c-b39c-affc5039a52f","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"e6e9d327-8b79-445c-b39c-affc5039a52f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ec79e2a7-6041-4890-a966-fed041fa96b3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ec79e2a7-6041-4890-a966-fed041fa96b3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"ec79e2a7-6041-4890-a966-fed041fa96b3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8162be72-a324-48b2-b8ac-21ea4a2161c3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8162be72-a324-48b2-b8ac-21ea4a2161c3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"8162be72-a324-48b2-b8ac-21ea4a2161c3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"78168523-1de1-4cf0-b10f-667501b3f2fe","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"78168523-1de1-4cf0-b10f-667501b3f2fe","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"78168523-1de1-4cf0-b10f-667501b3f2fe","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd348cf8-1b45-40dc-a947-b25d152dd36a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd348cf8-1b45-40dc-a947-b25d152dd36a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"dd348cf8-1b45-40dc-a947-b25d152dd36a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"323016ea-0056-4d02-ade6-02a8d8c07dbd","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"323016ea-0056-4d02-ade6-02a8d8c07dbd","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"323016ea-0056-4d02-ade6-02a8d8c07dbd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e12a9f92-34ab-4ba0-aa77-20caddbebceb","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e12a9f92-34ab-4ba0-aa77-20caddbebceb","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"e12a9f92-34ab-4ba0-aa77-20caddbebceb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ebb89a9d-970e-4995-b4ff-9e91107761cd","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ebb89a9d-970e-4995-b4ff-9e91107761cd","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"ebb89a9d-970e-4995-b4ff-9e91107761cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7d9fa16a-44e8-4c5e-9d71-3dc10d946a2a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7d9fa16a-44e8-4c5e-9d71-3dc10d946a2a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"7d9fa16a-44e8-4c5e-9d71-3dc10d946a2a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0ff81e84-5f6d-4847-b139-c3e5221df023","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0ff81e84-5f6d-4847-b139-c3e5221df023","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"0ff81e84-5f6d-4847-b139-c3e5221df023","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"25365541-6662-43b0-82f8-85b266b24111","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25365541-6662-43b0-82f8-85b266b24111","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"25365541-6662-43b0-82f8-85b266b24111","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"05a30d7b-c094-488e-9057-703755d34b6d","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05a30d7b-c094-488e-9057-703755d34b6d","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"05a30d7b-c094-488e-9057-703755d34b6d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"752cd19d-8cc0-4183-97b1-356397511ac9","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"752cd19d-8cc0-4183-97b1-356397511ac9","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"752cd19d-8cc0-4183-97b1-356397511ac9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"97ecd152-da79-443e-a856-7f90e76d457c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"97ecd152-da79-443e-a856-7f90e76d457c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"97ecd152-da79-443e-a856-7f90e76d457c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bdc16c81-9899-432f-b2e9-e8e9ada26ef7","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bdc16c81-9899-432f-b2e9-e8e9ada26ef7","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"bdc16c81-9899-432f-b2e9-e8e9ada26ef7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c085255-f30d-407b-bd58-2f7fd063ca6c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c085255-f30d-407b-bd58-2f7fd063ca6c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"5c085255-f30d-407b-bd58-2f7fd063ca6c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e867881c-65a7-4c2d-84b6-3fe953ab542c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e867881c-65a7-4c2d-84b6-3fe953ab542c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"e867881c-65a7-4c2d-84b6-3fe953ab542c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c1865201-5a12-45ca-b694-92796dfe9621","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c1865201-5a12-45ca-b694-92796dfe9621","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"c1865201-5a12-45ca-b694-92796dfe9621","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c5d63fd3-2bb7-416a-a9df-72d63d044323","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c5d63fd3-2bb7-416a-a9df-72d63d044323","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"c5d63fd3-2bb7-416a-a9df-72d63d044323","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"90623194-fdd5-47b2-a373-49078448463d","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90623194-fdd5-47b2-a373-49078448463d","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"LayerId":{"name":"1c763d6d-52d5-456c-8664-2cd61d486750","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","name":"90623194-fdd5-47b2-a373-49078448463d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 33.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 30.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"46c07a1e-6f0e-43c6-9fe1-71a9e28fd579","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"61e9262c-9f7f-4122-8114-b6d9354379e7","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"67c150d1-1a0e-425a-8bbd-6ccae5facfcc","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0eaee54e-0ed1-44ce-83ff-c0374d744f72","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"17537f8f-36e3-47de-add3-6d2f7893120a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3fee2f13-47e9-4dc0-8b90-7db3e75c31bc","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b68b8e40-2cae-4801-a797-595abfb53d9f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4ace8033-b5c7-400a-9c9d-ae20da7396a4","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea27f357-aa1f-4f03-9d1a-0c95c0e8a883","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eeebbd85-009e-46b6-8a6e-3fa4505c69d0","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7131622d-d04e-4100-8cdf-ff11ea2645dd","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25ed9701-b9b3-48b1-9833-06a741c4aa87","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a400a667-0e18-4f8c-a98d-3497e213ea81","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"27dad9bf-99bf-4291-9ef2-24880c7aac58","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef2b0636-4421-4fc1-b4b6-65eaa0db4d96","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6c92dd1f-7110-42fa-ae32-6ba9094f569a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa7d9fa7-8de2-489e-9c7d-5e6350872379","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65246d08-e06d-4c44-b6a3-8ea1cfffe61c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"045eec9d-4bed-42e7-a43e-27e788e61d09","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee70adfd-536a-49f0-b67c-f24c65f694f3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1de18934-dbaa-465f-944c-9829731cd8b7","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6e9d327-8b79-445c-b39c-affc5039a52f","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a8809ee-bba9-47d7-854f-4dae52879cca","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec79e2a7-6041-4890-a966-fed041fa96b3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b8f355b6-2f85-4867-9a9f-320633a3c48b","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8162be72-a324-48b2-b8ac-21ea4a2161c3","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3c42be77-9f33-408a-9ea6-cf96b96d13cd","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"78168523-1de1-4cf0-b10f-667501b3f2fe","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8b48612e-5351-4f53-8b76-2f15a21ee5b6","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd348cf8-1b45-40dc-a947-b25d152dd36a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4768ed0f-290d-433d-a29d-9f02fd1c3599","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"323016ea-0056-4d02-ade6-02a8d8c07dbd","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"747728e1-b672-45b7-9a2d-abf6dfdf3bac","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e12a9f92-34ab-4ba0-aa77-20caddbebceb","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1111f64e-9c96-4522-bbc1-d162ef2f6460","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ebb89a9d-970e-4995-b4ff-9e91107761cd","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8fbe12f9-96dc-41ed-b501-f006faf89bd7","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7d9fa16a-44e8-4c5e-9d71-3dc10d946a2a","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8ae674b-ed4d-431c-be9e-d5306cf27c5e","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0ff81e84-5f6d-4847-b139-c3e5221df023","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0457fab9-c331-4b18-8a7d-67540cf9f9cf","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25365541-6662-43b0-82f8-85b266b24111","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e932c8c0-260e-4f2e-8ebd-85ba339ede2a","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05a30d7b-c094-488e-9057-703755d34b6d","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ad1e585c-1040-4a2a-be7c-ae76a7124dc2","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"752cd19d-8cc0-4183-97b1-356397511ac9","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"78d3801a-aadf-4043-ab14-d7c10d8a77a5","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"97ecd152-da79-443e-a856-7f90e76d457c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ce026cf4-763f-4ac6-80a6-a667a6550e65","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bdc16c81-9899-432f-b2e9-e8e9ada26ef7","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8aaf87d0-2c7b-49d7-81d3-d555193bdbdf","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c085255-f30d-407b-bd58-2f7fd063ca6c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"825c8832-26de-4dac-a443-ab606935bf42","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e867881c-65a7-4c2d-84b6-3fe953ab542c","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"20bca4a8-4490-4be0-99f2-d01659107391","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1865201-5a12-45ca-b694-92796dfe9621","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d11dbf40-72c8-4db7-a085-3609f727e7e4","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c5d63fd3-2bb7-416a-a9df-72d63d044323","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b9a60e86-6aa6-4305-9104-e84c606b0904","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90623194-fdd5-47b2-a373-49078448463d","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 28,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ice_golem_run","path":"sprites/spr_ice_golem_run/spr_ice_golem_run.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1c763d6d-52d5-456c-8664-2cd61d486750","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Ice golem",
    "path": "folders/Sprites/Enemies/Ice golem.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ice_golem_run",
  "tags": [],
  "resourceType": "GMSprite",
}