{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 79,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 80,
  "height": 64,
  "textureGroupId": {
    "name": "GUI",
    "path": "texturegroups/GUI",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"51c13baf-caec-4e70-9a63-3bbc52bb5f65","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"51c13baf-caec-4e70-9a63-3bbc52bb5f65","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":{"name":"1016d071-7969-45b2-9fb0-10a70fd9face","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_languauges","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"51c13baf-caec-4e70-9a63-3bbc52bb5f65","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c17e2612-afc5-4a6a-81ab-98a10118d3d0","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c17e2612-afc5-4a6a-81ab-98a10118d3d0","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":{"name":"1016d071-7969-45b2-9fb0-10a70fd9face","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_languauges","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"c17e2612-afc5-4a6a-81ab-98a10118d3d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6bede9d6-7ee7-4db9-afe8-c2476f42c0a6","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6bede9d6-7ee7-4db9-afe8-c2476f42c0a6","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":{"name":"1016d071-7969-45b2-9fb0-10a70fd9face","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_languauges","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"6bede9d6-7ee7-4db9-afe8-c2476f42c0a6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"58f16d01-af08-4f45-86ef-de589b10e44f","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"58f16d01-af08-4f45-86ef-de589b10e44f","path":"sprites/spr_languauges/spr_languauges.yy",},"LayerId":{"name":"1016d071-7969-45b2-9fb0-10a70fd9face","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_languauges","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","name":"58f16d01-af08-4f45-86ef-de589b10e44f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_languauges","path":"sprites/spr_languauges/spr_languauges.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cb2beac2-45f9-4f03-a4f7-ef6935d37f03","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"51c13baf-caec-4e70-9a63-3bbc52bb5f65","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cdc3bbd5-b7fa-4d9a-b665-91895e15ac51","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c17e2612-afc5-4a6a-81ab-98a10118d3d0","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31acfdc8-6d35-4e4f-9654-85fa390dbc7f","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6bede9d6-7ee7-4db9-afe8-c2476f42c0a6","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7e038840-6b14-465e-b6ff-21da728501d7","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"58f16d01-af08-4f45-86ef-de589b10e44f","path":"sprites/spr_languauges/spr_languauges.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_languauges","path":"sprites/spr_languauges/spr_languauges.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1016d071-7969-45b2-9fb0-10a70fd9face","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Interfaces",
    "path": "folders/Sprites/Interfaces.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_languauges",
  "tags": [],
  "resourceType": "GMSprite",
}