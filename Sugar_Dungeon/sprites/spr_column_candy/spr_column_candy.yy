{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 20,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 21,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ccb46c9f-7b49-4359-bfe6-d4f07c5d75d3","path":"sprites/spr_column_candy/spr_column_candy.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ccb46c9f-7b49-4359-bfe6-d4f07c5d75d3","path":"sprites/spr_column_candy/spr_column_candy.yy",},"LayerId":{"name":"1d0c1a7e-5900-4c49-9d48-231bf7a8b897","path":"sprites/spr_column_candy/spr_column_candy.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_column_candy","path":"sprites/spr_column_candy/spr_column_candy.yy",},"resourceVersion":"1.0","name":"ccb46c9f-7b49-4359-bfe6-d4f07c5d75d3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_column_candy","path":"sprites/spr_column_candy/spr_column_candy.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1bc0b365-f361-4186-a581-aaf8271a9c5e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ccb46c9f-7b49-4359-bfe6-d4f07c5d75d3","path":"sprites/spr_column_candy/spr_column_candy.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 11,
    "yorigin": 29,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_column_candy","path":"sprites/spr_column_candy/spr_column_candy.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1d0c1a7e-5900-4c49-9d48-231bf7a8b897","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Candy",
    "path": "folders/Sprites/Decorations/Candy.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_column_candy",
  "tags": [],
  "resourceType": "GMSprite",
}