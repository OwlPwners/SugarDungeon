{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 12,
  "bbox_top": 0,
  "bbox_bottom": 35,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 13,
  "height": 36,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f94056fe-499d-45c3-8cf3-7f9729a5e937","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f94056fe-499d-45c3-8cf3-7f9729a5e937","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"f94056fe-499d-45c3-8cf3-7f9729a5e937","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cf002b4e-6974-4565-bbb9-9a30a4066186","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cf002b4e-6974-4565-bbb9-9a30a4066186","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"cf002b4e-6974-4565-bbb9-9a30a4066186","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aa42681d-aea1-45fa-a098-476152105901","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa42681d-aea1-45fa-a098-476152105901","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"aa42681d-aea1-45fa-a098-476152105901","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"97ffc5bc-6f58-4aa9-ba9b-38453c03e07a","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"97ffc5bc-6f58-4aa9-ba9b-38453c03e07a","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"97ffc5bc-6f58-4aa9-ba9b-38453c03e07a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8538a275-d233-4637-8ec7-c46c581eae25","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8538a275-d233-4637-8ec7-c46c581eae25","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"8538a275-d233-4637-8ec7-c46c581eae25","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"43368745-6901-4d3a-9bb2-c8aa3ae96836","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43368745-6901-4d3a-9bb2-c8aa3ae96836","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"43368745-6901-4d3a-9bb2-c8aa3ae96836","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"805829df-7c34-49a2-9a70-9df7b07a14be","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"805829df-7c34-49a2-9a70-9df7b07a14be","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"805829df-7c34-49a2-9a70-9df7b07a14be","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7d15445e-b36d-4c09-8ac9-d36b68af0f7c","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7d15445e-b36d-4c09-8ac9-d36b68af0f7c","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"LayerId":{"name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","name":"7d15445e-b36d-4c09-8ac9-d36b68af0f7c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9cff0070-2e5c-4296-a78d-201d2cfa4fea","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f94056fe-499d-45c3-8cf3-7f9729a5e937","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e2623375-19e1-4fbc-ad91-9cf0a9bdc7be","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cf002b4e-6974-4565-bbb9-9a30a4066186","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"27b42b7f-adc4-4674-994e-36bc8b687869","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa42681d-aea1-45fa-a098-476152105901","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6729d460-f249-4100-937f-eb9b90cbe3af","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"97ffc5bc-6f58-4aa9-ba9b-38453c03e07a","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ce4277e8-915b-422d-a2b8-b8db312344f9","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8538a275-d233-4637-8ec7-c46c581eae25","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8301a75a-7443-4501-befe-3dc182a4d5d1","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43368745-6901-4d3a-9bb2-c8aa3ae96836","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6d61836d-44f5-4c40-a584-c69afd1d3257","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"805829df-7c34-49a2-9a70-9df7b07a14be","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"92e7628d-ae80-4f4f-a4c7-c45921c5fb2c","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7d15445e-b36d-4c09-8ac9-d36b68af0f7c","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 6,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_torch_mine","path":"sprites/spr_torch_mine/spr_torch_mine.yy",},
    "resourceVersion": "1.3",
    "name": "spr_torch_mine",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9ecf5a39-c8a3-4ebd-ba62-dd44364eb13f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Lights",
    "path": "folders/Sprites/Decorations/Lights.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_torch_mine",
  "tags": [],
  "resourceType": "GMSprite",
}