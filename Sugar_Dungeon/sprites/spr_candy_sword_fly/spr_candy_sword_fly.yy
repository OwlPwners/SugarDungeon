{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 21,
  "bbox_top": 0,
  "bbox_bottom": 20,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 22,
  "height": 22,
  "textureGroupId": {
    "name": "Items",
    "path": "texturegroups/Items",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"db789ef2-c8f6-47c3-9631-bcdcd27c0f2f","path":"sprites/spr_candy_sword_fly/spr_candy_sword_fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"db789ef2-c8f6-47c3-9631-bcdcd27c0f2f","path":"sprites/spr_candy_sword_fly/spr_candy_sword_fly.yy",},"LayerId":{"name":"1c4c7288-c777-4594-9239-46ccc8ba9646","path":"sprites/spr_candy_sword_fly/spr_candy_sword_fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_candy_sword_fly","path":"sprites/spr_candy_sword_fly/spr_candy_sword_fly.yy",},"resourceVersion":"1.0","name":"db789ef2-c8f6-47c3-9631-bcdcd27c0f2f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_candy_sword_fly","path":"sprites/spr_candy_sword_fly/spr_candy_sword_fly.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7e4bf9ad-5497-4942-9377-dd8c7e744a2a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"db789ef2-c8f6-47c3-9631-bcdcd27c0f2f","path":"sprites/spr_candy_sword_fly/spr_candy_sword_fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 4,
    "yorigin": 17,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_candy_sword_fly","path":"sprites/spr_candy_sword_fly/spr_candy_sword_fly.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1c4c7288-c777-4594-9239-46ccc8ba9646","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Candy sword",
    "path": "folders/Sprites/Items/Sprites/Lollipop/Candy sword.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_candy_sword_fly",
  "tags": [],
  "resourceType": "GMSprite",
}