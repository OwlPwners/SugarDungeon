{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 69,
  "bbox_top": 0,
  "bbox_bottom": 79,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 70,
  "height": 80,
  "textureGroupId": {
    "name": "BossIcecream",
    "path": "texturegroups/BossIcecream",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7f4dfdd8-4fc0-489a-a8ea-2a52dc00c67f","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f4dfdd8-4fc0-489a-a8ea-2a52dc00c67f","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"7f4dfdd8-4fc0-489a-a8ea-2a52dc00c67f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e8b9c85d-285b-47e0-b41e-c3809266dbc6","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8b9c85d-285b-47e0-b41e-c3809266dbc6","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"e8b9c85d-285b-47e0-b41e-c3809266dbc6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2e67e1f8-49fe-499d-881a-df9fd0ca6ad1","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2e67e1f8-49fe-499d-881a-df9fd0ca6ad1","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"2e67e1f8-49fe-499d-881a-df9fd0ca6ad1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aa1ac32b-6986-4a41-b0a7-71c24bdea8f5","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa1ac32b-6986-4a41-b0a7-71c24bdea8f5","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"aa1ac32b-6986-4a41-b0a7-71c24bdea8f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ba13e034-cb52-40d7-a027-eb5c924ae656","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ba13e034-cb52-40d7-a027-eb5c924ae656","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"ba13e034-cb52-40d7-a027-eb5c924ae656","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bda71918-1402-4d72-9550-bc8ab8c4dbd0","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bda71918-1402-4d72-9550-bc8ab8c4dbd0","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"bda71918-1402-4d72-9550-bc8ab8c4dbd0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3661ec26-bb43-4896-944b-35a30bbb4e29","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3661ec26-bb43-4896-944b-35a30bbb4e29","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"3661ec26-bb43-4896-944b-35a30bbb4e29","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"39cd6560-d098-4b21-bfb8-dd0cc65fd0e1","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"39cd6560-d098-4b21-bfb8-dd0cc65fd0e1","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"39cd6560-d098-4b21-bfb8-dd0cc65fd0e1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1220821b-7f01-4fc0-9c42-1bcd3173ac0b","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1220821b-7f01-4fc0-9c42-1bcd3173ac0b","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"1220821b-7f01-4fc0-9c42-1bcd3173ac0b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a6ce85a0-6029-4774-80c4-fe3abff7d59b","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a6ce85a0-6029-4774-80c4-fe3abff7d59b","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"a6ce85a0-6029-4774-80c4-fe3abff7d59b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b08729e-427c-4fda-a649-aeaecbb2eaed","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b08729e-427c-4fda-a649-aeaecbb2eaed","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"9b08729e-427c-4fda-a649-aeaecbb2eaed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ec7074f5-b331-44b5-b046-7eeed8050496","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ec7074f5-b331-44b5-b046-7eeed8050496","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"ec7074f5-b331-44b5-b046-7eeed8050496","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"42525c21-a3c8-467b-a995-4e8c50824087","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"42525c21-a3c8-467b-a995-4e8c50824087","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"42525c21-a3c8-467b-a995-4e8c50824087","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0963e699-5570-4aa6-92a8-86c074557b60","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0963e699-5570-4aa6-92a8-86c074557b60","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"0963e699-5570-4aa6-92a8-86c074557b60","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"63e9eca3-3c94-413a-a69f-edeb0eb6bc48","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"63e9eca3-3c94-413a-a69f-edeb0eb6bc48","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"63e9eca3-3c94-413a-a69f-edeb0eb6bc48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a7f14759-6df8-41f2-bc15-ac383015143d","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a7f14759-6df8-41f2-bc15-ac383015143d","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"a7f14759-6df8-41f2-bc15-ac383015143d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"86b04380-a379-455a-834a-bc4fb4b91bd9","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"86b04380-a379-455a-834a-bc4fb4b91bd9","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"86b04380-a379-455a-834a-bc4fb4b91bd9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c31fe376-ad4e-4959-a3e2-f7b69ea286b3","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c31fe376-ad4e-4959-a3e2-f7b69ea286b3","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"c31fe376-ad4e-4959-a3e2-f7b69ea286b3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"858aa647-578b-43ef-b2a5-bbb8d0453e49","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"858aa647-578b-43ef-b2a5-bbb8d0453e49","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"858aa647-578b-43ef-b2a5-bbb8d0453e49","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1446c242-8c53-4aac-9a9b-ca12cabcac0d","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1446c242-8c53-4aac-9a9b-ca12cabcac0d","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"1446c242-8c53-4aac-9a9b-ca12cabcac0d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fa96af9e-9b35-4abb-a7af-9bf7b98abbf0","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fa96af9e-9b35-4abb-a7af-9bf7b98abbf0","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"fa96af9e-9b35-4abb-a7af-9bf7b98abbf0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d6b4c7e1-241f-4811-a159-e6b9bfc72e25","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d6b4c7e1-241f-4811-a159-e6b9bfc72e25","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"d6b4c7e1-241f-4811-a159-e6b9bfc72e25","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"14bd3233-7f7c-4436-84ab-99bccc8fce7a","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"14bd3233-7f7c-4436-84ab-99bccc8fce7a","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"14bd3233-7f7c-4436-84ab-99bccc8fce7a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca4fc98d-31d9-44a2-96a0-6a0f1c61cf59","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca4fc98d-31d9-44a2-96a0-6a0f1c61cf59","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"ca4fc98d-31d9-44a2-96a0-6a0f1c61cf59","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1dda7084-1444-4e19-9e31-71806e345e53","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1dda7084-1444-4e19-9e31-71806e345e53","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"LayerId":{"name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","name":"1dda7084-1444-4e19-9e31-71806e345e53","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 45.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 25.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"63a6cbf1-3b25-464d-b444-802c6d193201","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f4dfdd8-4fc0-489a-a8ea-2a52dc00c67f","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b2d823d7-e384-440c-a928-ed10874adbda","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8b9c85d-285b-47e0-b41e-c3809266dbc6","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"46a52049-2379-4fba-aeb3-d69569ec5b60","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2e67e1f8-49fe-499d-881a-df9fd0ca6ad1","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea7cca47-7e56-4adb-b3e4-a245a897f682","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa1ac32b-6986-4a41-b0a7-71c24bdea8f5","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48c4e4c9-070c-4a2c-903c-266f2d70da24","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ba13e034-cb52-40d7-a027-eb5c924ae656","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"295260db-da32-40a1-aab3-be1c4297ae13","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bda71918-1402-4d72-9550-bc8ab8c4dbd0","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6fc5913-5847-40f5-bc39-e0c89327bf53","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3661ec26-bb43-4896-944b-35a30bbb4e29","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"614f9c2d-3205-473a-87ae-9bca6ed0e05c","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"39cd6560-d098-4b21-bfb8-dd0cc65fd0e1","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b514147b-0429-4d94-a89d-69c61cc5c9d3","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1220821b-7f01-4fc0-9c42-1bcd3173ac0b","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"299294b5-fa99-4430-8896-7f06a7d79c4a","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a6ce85a0-6029-4774-80c4-fe3abff7d59b","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3a5567de-09f9-4ea6-a7b0-1bfc322d5535","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b08729e-427c-4fda-a649-aeaecbb2eaed","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2571f52e-8313-44f2-885e-3446baa7dbc0","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec7074f5-b331-44b5-b046-7eeed8050496","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d0fbad2d-341b-4b33-b8ae-ac24399c6dcc","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"42525c21-a3c8-467b-a995-4e8c50824087","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0d14eb71-ad45-4175-b73a-0db0c6514617","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0963e699-5570-4aa6-92a8-86c074557b60","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d2066578-bae9-4ce1-ad38-0460ef65182e","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63e9eca3-3c94-413a-a69f-edeb0eb6bc48","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"77f002a3-1b05-4a85-8ab1-09db3ce05c4a","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a7f14759-6df8-41f2-bc15-ac383015143d","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"52ee334a-d3b7-4f8d-a6c9-6e496f667503","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"86b04380-a379-455a-834a-bc4fb4b91bd9","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6b2cac3-cc06-4aee-b141-3eeafd72fad1","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c31fe376-ad4e-4959-a3e2-f7b69ea286b3","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5cc572a8-4b9e-4492-ad93-96f1dbc99a6d","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"858aa647-578b-43ef-b2a5-bbb8d0453e49","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c7ca4661-5d91-4dfa-a55a-719ad0a695d8","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1446c242-8c53-4aac-9a9b-ca12cabcac0d","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d2a1724-850c-4000-b7fb-17bff5edc526","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fa96af9e-9b35-4abb-a7af-9bf7b98abbf0","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"010f9ea4-f898-43d6-910e-0a35ac7b53f9","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6b4c7e1-241f-4811-a159-e6b9bfc72e25","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b7c52d81-0a72-4d5a-92ac-fbfcdc94caa2","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"14bd3233-7f7c-4436-84ab-99bccc8fce7a","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"290125f0-6a55-4feb-b150-b85a9c505b73","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca4fc98d-31d9-44a2-96a0-6a0f1c61cf59","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8993d629-48bb-40a6-8305-1635084b5d18","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1dda7084-1444-4e19-9e31-71806e345e53","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 19,
    "yorigin": 50,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_boss_icecream_run","path":"sprites/spr_boss_icecream_run/spr_boss_icecream_run.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e5b79a8c-1132-4c2d-b45c-bfe40c4d66ad","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Ice cream",
    "path": "folders/Sprites/Bosses/Ice cream.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_boss_icecream_run",
  "tags": [],
  "resourceType": "GMSprite",
}