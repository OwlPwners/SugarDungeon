{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 69,
  "bbox_top": 0,
  "bbox_bottom": 5,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 70,
  "height": 6,
  "textureGroupId": {
    "name": "Items",
    "path": "texturegroups/Items",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2c284eeb-c141-4588-9641-6f53b3e34e0e","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2c284eeb-c141-4588-9641-6f53b3e34e0e","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"2c284eeb-c141-4588-9641-6f53b3e34e0e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"72bb23b5-407a-4831-bdb4-e30b999f2f93","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"72bb23b5-407a-4831-bdb4-e30b999f2f93","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"72bb23b5-407a-4831-bdb4-e30b999f2f93","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9f18bd35-4600-4b43-a406-75c13a294025","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9f18bd35-4600-4b43-a406-75c13a294025","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"9f18bd35-4600-4b43-a406-75c13a294025","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"adaaa7f9-4143-44a8-8de4-15f4e757d3d6","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"adaaa7f9-4143-44a8-8de4-15f4e757d3d6","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"adaaa7f9-4143-44a8-8de4-15f4e757d3d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"63c389e2-0486-4699-8b46-fdbf2623e108","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"63c389e2-0486-4699-8b46-fdbf2623e108","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"63c389e2-0486-4699-8b46-fdbf2623e108","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8f4ed87a-417d-48b2-a369-3af5828823ea","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8f4ed87a-417d-48b2-a369-3af5828823ea","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"8f4ed87a-417d-48b2-a369-3af5828823ea","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e9396f42-cd19-4413-8866-e4972745ec02","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e9396f42-cd19-4413-8866-e4972745ec02","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"e9396f42-cd19-4413-8866-e4972745ec02","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ea044d3d-da6a-42dc-b612-3ad677909101","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ea044d3d-da6a-42dc-b612-3ad677909101","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"ea044d3d-da6a-42dc-b612-3ad677909101","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cd5a83dc-cc5d-48a9-9851-231898d780df","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cd5a83dc-cc5d-48a9-9851-231898d780df","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"cd5a83dc-cc5d-48a9-9851-231898d780df","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2b594a6-8d72-447b-b2bc-4c1339f38ada","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2b594a6-8d72-447b-b2bc-4c1339f38ada","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"e2b594a6-8d72-447b-b2bc-4c1339f38ada","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0af567fe-8e3f-4a14-9425-eb7573574183","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0af567fe-8e3f-4a14-9425-eb7573574183","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"0af567fe-8e3f-4a14-9425-eb7573574183","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f4e5a532-d76d-4046-80e1-d85040287e49","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f4e5a532-d76d-4046-80e1-d85040287e49","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"f4e5a532-d76d-4046-80e1-d85040287e49","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c5e5d531-05e6-4482-ad31-7e3378a34bea","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c5e5d531-05e6-4482-ad31-7e3378a34bea","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"c5e5d531-05e6-4482-ad31-7e3378a34bea","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d2f392fb-5f0c-466c-bc1c-9ed143ff9d01","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d2f392fb-5f0c-466c-bc1c-9ed143ff9d01","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"d2f392fb-5f0c-466c-bc1c-9ed143ff9d01","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"98eb3987-3500-4df7-8984-5689ceaba27f","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"98eb3987-3500-4df7-8984-5689ceaba27f","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"98eb3987-3500-4df7-8984-5689ceaba27f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65cfa5b8-273c-4983-9b00-20ac02e043fc","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65cfa5b8-273c-4983-9b00-20ac02e043fc","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"LayerId":{"name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","name":"65cfa5b8-273c-4983-9b00-20ac02e043fc","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 35.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 16.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2ec558a2-4a5f-4ace-910a-b9244646bbf6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2c284eeb-c141-4588-9641-6f53b3e34e0e","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0a31343e-2739-4972-a5e6-5972c0b96a7a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"72bb23b5-407a-4831-bdb4-e30b999f2f93","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"49b0f5a5-ecb7-4193-93ec-1b4f79c2b24e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f18bd35-4600-4b43-a406-75c13a294025","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b335a2b2-32bf-4331-97be-0d83250ca92f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"adaaa7f9-4143-44a8-8de4-15f4e757d3d6","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63713d20-4cb7-42ef-bc2a-2fb853fb8693","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63c389e2-0486-4699-8b46-fdbf2623e108","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"754ebbb2-76e0-47bf-9b77-91b9a004f1dd","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8f4ed87a-417d-48b2-a369-3af5828823ea","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"152bf82d-c182-45f3-9c6e-434a47554a61","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9396f42-cd19-4413-8866-e4972745ec02","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da0ad551-0af3-432a-8c23-6d6e260fd753","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ea044d3d-da6a-42dc-b612-3ad677909101","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4193abb3-0325-42a3-9063-458ecc9a329c","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cd5a83dc-cc5d-48a9-9851-231898d780df","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"517c02ef-1b14-4540-947e-d1291687717c","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2b594a6-8d72-447b-b2bc-4c1339f38ada","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"081701e6-8a2d-4e63-80b5-e81c9edc1810","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0af567fe-8e3f-4a14-9425-eb7573574183","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f47ed17b-1b77-4925-9556-cea9f0d542c0","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f4e5a532-d76d-4046-80e1-d85040287e49","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a465d85f-0ef1-47ff-b001-b062f9e30223","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c5e5d531-05e6-4482-ad31-7e3378a34bea","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e902f195-7134-48ab-b071-2837b4b7b6b3","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d2f392fb-5f0c-466c-bc1c-9ed143ff9d01","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"941d1cb7-113b-47fb-bc5b-5fb88412206a","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"98eb3987-3500-4df7-8984-5689ceaba27f","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ead143ae-6489-4e81-b1bb-122ad92605e1","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65cfa5b8-273c-4983-9b00-20ac02e043fc","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 2,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_plain_spear_attack","path":"sprites/spr_plain_spear_attack/spr_plain_spear_attack.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cc78ae41-f46d-4a77-aeb4-ab69edce8e79","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Plain spear",
    "path": "folders/Sprites/Items/Sprites/Plain/Plain spear.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_plain_spear_attack",
  "tags": [],
  "resourceType": "GMSprite",
}