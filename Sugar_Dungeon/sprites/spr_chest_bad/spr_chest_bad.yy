{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 26,
  "bbox_top": 1,
  "bbox_bottom": 24,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 28,
  "height": 26,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dc599a39-2e94-4d38-a1c6-b50269c79d81","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dc599a39-2e94-4d38-a1c6-b50269c79d81","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"dc599a39-2e94-4d38-a1c6-b50269c79d81","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d654aab7-36d1-4038-ad81-788ab286a0bd","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d654aab7-36d1-4038-ad81-788ab286a0bd","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"d654aab7-36d1-4038-ad81-788ab286a0bd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c84cf92c-74d3-430b-b4c6-f277cbb17835","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c84cf92c-74d3-430b-b4c6-f277cbb17835","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"c84cf92c-74d3-430b-b4c6-f277cbb17835","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b332af2-a4d1-42b2-9fa5-7eeb6525a800","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b332af2-a4d1-42b2-9fa5-7eeb6525a800","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"9b332af2-a4d1-42b2-9fa5-7eeb6525a800","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7db82582-50b1-430f-a6f9-9ed4ad28c280","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7db82582-50b1-430f-a6f9-9ed4ad28c280","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"7db82582-50b1-430f-a6f9-9ed4ad28c280","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3cab8e8c-5fca-4cf6-946e-f3c3cfacb965","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3cab8e8c-5fca-4cf6-946e-f3c3cfacb965","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"3cab8e8c-5fca-4cf6-946e-f3c3cfacb965","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"31497e2d-5ad3-49e5-937f-6332d470bd06","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"31497e2d-5ad3-49e5-937f-6332d470bd06","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"31497e2d-5ad3-49e5-937f-6332d470bd06","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4e093b03-9e9e-49fc-a37b-321f4cc6f5a9","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4e093b03-9e9e-49fc-a37b-321f4cc6f5a9","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"LayerId":{"name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","name":"4e093b03-9e9e-49fc-a37b-321f4cc6f5a9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a28fcda7-c68d-4c71-af74-fc751c221a40","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dc599a39-2e94-4d38-a1c6-b50269c79d81","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"193c6d89-9809-42c7-ab08-5356c04aafa1","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d654aab7-36d1-4038-ad81-788ab286a0bd","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"52a5e326-e326-4d73-aa57-66c23189eea0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c84cf92c-74d3-430b-b4c6-f277cbb17835","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13e6d176-72cf-4c92-8cc0-55cc01ff73e8","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b332af2-a4d1-42b2-9fa5-7eeb6525a800","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d3e985b5-cc17-4e10-86f7-9c397eb25119","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7db82582-50b1-430f-a6f9-9ed4ad28c280","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13d445db-f023-4876-bf06-e48f2995ec9a","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3cab8e8c-5fca-4cf6-946e-f3c3cfacb965","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"89956443-afae-4aff-bbc5-7e73f5da1bc9","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31497e2d-5ad3-49e5-937f-6332d470bd06","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ceeae362-ddb3-49df-89f6-03c458650e5f","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4e093b03-9e9e-49fc-a37b-321f4cc6f5a9","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 13,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_chest_bad","path":"sprites/spr_chest_bad/spr_chest_bad.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9d40fee5-d1ad-47a4-8b0b-c48464f58e6d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Chests",
    "path": "folders/Sprites/Chests.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_chest_bad",
  "tags": [],
  "resourceType": "GMSprite",
}