{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 36,
  "bbox_top": 0,
  "bbox_bottom": 104,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 37,
  "height": 105,
  "textureGroupId": {
    "name": "BossIcecream",
    "path": "texturegroups/BossIcecream",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"884420ad-1cb9-4454-b480-1d029808c6ee","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"884420ad-1cb9-4454-b480-1d029808c6ee","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"884420ad-1cb9-4454-b480-1d029808c6ee","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"884420ad-1cb9-4454-b480-1d029808c6ee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"45f938af-6e17-4813-914c-e0e83699ea09","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"45f938af-6e17-4813-914c-e0e83699ea09","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"45f938af-6e17-4813-914c-e0e83699ea09","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"45f938af-6e17-4813-914c-e0e83699ea09","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5a24719f-2be2-47bb-8006-4ed48f5566a6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5a24719f-2be2-47bb-8006-4ed48f5566a6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"5a24719f-2be2-47bb-8006-4ed48f5566a6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"5a24719f-2be2-47bb-8006-4ed48f5566a6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"68c30d45-afd9-4a4c-a952-c1ed18b61abd","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"68c30d45-afd9-4a4c-a952-c1ed18b61abd","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"68c30d45-afd9-4a4c-a952-c1ed18b61abd","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"68c30d45-afd9-4a4c-a952-c1ed18b61abd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"704f3e6f-d895-4b80-aefa-f51ee8ea8181","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"704f3e6f-d895-4b80-aefa-f51ee8ea8181","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"704f3e6f-d895-4b80-aefa-f51ee8ea8181","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"704f3e6f-d895-4b80-aefa-f51ee8ea8181","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2aa90634-6c35-42b7-a2a4-8ddc21a456d5","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2aa90634-6c35-42b7-a2a4-8ddc21a456d5","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"2aa90634-6c35-42b7-a2a4-8ddc21a456d5","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"2aa90634-6c35-42b7-a2a4-8ddc21a456d5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"80493c39-af08-476b-8a37-3430bf5c6cf6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"80493c39-af08-476b-8a37-3430bf5c6cf6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"80493c39-af08-476b-8a37-3430bf5c6cf6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"80493c39-af08-476b-8a37-3430bf5c6cf6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d9642fe-7255-4ec4-b011-331668d4e707","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d9642fe-7255-4ec4-b011-331668d4e707","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1d9642fe-7255-4ec4-b011-331668d4e707","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"1d9642fe-7255-4ec4-b011-331668d4e707","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"574da932-63f5-4f51-9322-81ed37f1970b","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"574da932-63f5-4f51-9322-81ed37f1970b","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"574da932-63f5-4f51-9322-81ed37f1970b","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"574da932-63f5-4f51-9322-81ed37f1970b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c00a64a-3566-488f-9b16-0e4aebbc3a29","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c00a64a-3566-488f-9b16-0e4aebbc3a29","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"3c00a64a-3566-488f-9b16-0e4aebbc3a29","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"3c00a64a-3566-488f-9b16-0e4aebbc3a29","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c4a7fa6b-d23c-4361-a861-d655de2e91c1","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c4a7fa6b-d23c-4361-a861-d655de2e91c1","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c4a7fa6b-d23c-4361-a861-d655de2e91c1","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"c4a7fa6b-d23c-4361-a861-d655de2e91c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e03024a1-eda3-42ab-8a42-42a677daf791","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e03024a1-eda3-42ab-8a42-42a677daf791","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e03024a1-eda3-42ab-8a42-42a677daf791","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"e03024a1-eda3-42ab-8a42-42a677daf791","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b265b233-9c87-4c19-baab-0a5c8b688a75","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b265b233-9c87-4c19-baab-0a5c8b688a75","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"b265b233-9c87-4c19-baab-0a5c8b688a75","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"LayerId":{"name":"a19c5962-2cd2-408b-9113-445cd1d80608","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","name":"b265b233-9c87-4c19-baab-0a5c8b688a75","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ba9f815f-6118-4e6c-b9be-eeb1d8d73bf5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"884420ad-1cb9-4454-b480-1d029808c6ee","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1dedbbf-934b-4426-9948-0c9b159134fd","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"45f938af-6e17-4813-914c-e0e83699ea09","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c32365b7-af4e-4ffe-843e-d3d951089d89","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5a24719f-2be2-47bb-8006-4ed48f5566a6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e40dd83d-c2ff-4a69-8f6b-4daa1b381c6d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"68c30d45-afd9-4a4c-a952-c1ed18b61abd","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"35355ad5-95ea-49b4-8015-c0e7b362beb9","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"704f3e6f-d895-4b80-aefa-f51ee8ea8181","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"957934be-7b61-458c-b0e9-57787528fabf","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2aa90634-6c35-42b7-a2a4-8ddc21a456d5","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aa4cdc94-fa71-44e2-8f89-cbe901c8f2a9","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"80493c39-af08-476b-8a37-3430bf5c6cf6","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f79beb8b-f327-4d6a-bace-d17b388e96ee","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d9642fe-7255-4ec4-b011-331668d4e707","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e3d4cef-21a8-4a93-94eb-a1dbc1fd261e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"574da932-63f5-4f51-9322-81ed37f1970b","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a7d1ad83-d059-44d6-abd2-62c9bfb9456b","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c00a64a-3566-488f-9b16-0e4aebbc3a29","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a9aee0b4-bd22-443e-b7aa-26a5c9f3126d","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c4a7fa6b-d23c-4361-a861-d655de2e91c1","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"18f417f7-141c-41c5-a06d-2731cf3913cd","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e03024a1-eda3-42ab-8a42-42a677daf791","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"67c19438-b902-4e81-9fc8-05adb9ff24a5","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b265b233-9c87-4c19-baab-0a5c8b688a75","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 18,
    "yorigin": 52,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_boss_icecream_mouth","path":"sprites/spr_boss_icecream_mouth/spr_boss_icecream_mouth.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"72ba331c-af65-41fe-9736-3cf3ebcc113e","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"a19c5962-2cd2-408b-9113-445cd1d80608","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Ice cream",
    "path": "folders/Sprites/Bosses/Ice cream.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_boss_icecream_mouth",
  "tags": [],
  "resourceType": "GMSprite",
}