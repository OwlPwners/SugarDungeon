{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "so_boss_soda",
  "duration": 84.0409,
  "parent": {
    "name": "Bosses",
    "path": "folders/Sounds/Music/Bosses.yy",
  },
  "resourceVersion": "1.0",
  "name": "so_boss_soda",
  "tags": [],
  "resourceType": "GMSound",
}