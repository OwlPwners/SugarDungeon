{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_menu_music",
    "path": "audiogroups/agp_menu_music",
  },
  "soundFile": "so_menu",
  "duration": 110.816422,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "so_menu",
  "tags": [],
  "resourceType": "GMSound",
}