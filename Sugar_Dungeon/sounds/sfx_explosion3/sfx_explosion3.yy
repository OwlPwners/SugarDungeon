{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_explosion3",
  "duration": 2.004887,
  "parent": {
    "name": "Explosion",
    "path": "folders/Sounds/SFX/Explosion.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_explosion3",
  "tags": [],
  "resourceType": "GMSound",
}