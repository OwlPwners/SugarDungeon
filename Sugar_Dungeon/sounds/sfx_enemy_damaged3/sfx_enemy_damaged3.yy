{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_enemy_damaged3",
  "duration": 1.155454,
  "parent": {
    "name": "Hit",
    "path": "folders/Sounds/SFX/Hit.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_enemy_damaged3",
  "tags": [],
  "resourceType": "GMSound",
}