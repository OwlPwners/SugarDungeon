{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "so_boss_icecream",
  "duration": 75.316,
  "parent": {
    "name": "Bosses",
    "path": "folders/Sounds/Music/Bosses.yy",
  },
  "resourceVersion": "1.0",
  "name": "so_boss_icecream",
  "tags": [],
  "resourceType": "GMSound",
}