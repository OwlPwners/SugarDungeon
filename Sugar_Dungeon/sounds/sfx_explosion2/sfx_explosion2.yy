{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_explosion2",
  "duration": 1.972891,
  "parent": {
    "name": "Explosion",
    "path": "folders/Sounds/SFX/Explosion.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_explosion2",
  "tags": [],
  "resourceType": "GMSound",
}