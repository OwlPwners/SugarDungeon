{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_game_message",
  "duration": 0.351145,
  "parent": {
    "name": "SFX",
    "path": "folders/Sounds/SFX.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_game_message",
  "tags": [],
  "resourceType": "GMSound",
}