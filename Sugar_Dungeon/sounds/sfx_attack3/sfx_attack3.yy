{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_attack3",
  "duration": 0.543753,
  "parent": {
    "name": "Weapons",
    "path": "folders/Sounds/SFX/Weapons.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_attack3",
  "tags": [],
  "resourceType": "GMSound",
}