{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_minimap_close",
  "duration": 0.463333,
  "parent": {
    "name": "Mini-map",
    "path": "folders/Sounds/SFX/Interfaces/Mini-map.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_minimap_close",
  "tags": [],
  "resourceType": "GMSound",
}