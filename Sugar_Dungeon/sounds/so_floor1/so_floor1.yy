{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "so_floor1",
  "duration": 85.00745,
  "parent": {
    "name": "Floors",
    "path": "folders/Sounds/Music/Floors.yy",
  },
  "resourceVersion": "1.0",
  "name": "so_floor1",
  "tags": [],
  "resourceType": "GMSound",
}