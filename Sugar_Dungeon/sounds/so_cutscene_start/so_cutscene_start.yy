{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_cutscene_start_music",
    "path": "audiogroups/agp_cutscene_start_music",
  },
  "soundFile": "so_cutscene_start",
  "duration": 72.5209,
  "parent": {
    "name": "Cutscenes",
    "path": "folders/Sounds/Music/Cutscenes.yy",
  },
  "resourceVersion": "1.0",
  "name": "so_cutscene_start",
  "tags": [],
  "resourceType": "GMSound",
}