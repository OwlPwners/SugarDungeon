{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_attack2",
  "duration": 0.635204,
  "parent": {
    "name": "Weapons",
    "path": "folders/Sounds/SFX/Weapons.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_attack2",
  "tags": [],
  "resourceType": "GMSound",
}