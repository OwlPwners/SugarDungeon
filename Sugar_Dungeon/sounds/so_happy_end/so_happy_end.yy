{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "so_happy_end.mp3",
  "duration": 195.8711,
  "parent": {
    "name": "Ends",
    "path": "folders/Sounds/Music/Ends.yy",
  },
  "resourceVersion": "1.0",
  "name": "so_happy_end",
  "tags": [],
  "resourceType": "GMSound",
}