{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "agp_sfx",
    "path": "audiogroups/agp_sfx",
  },
  "soundFile": "sfx_boss_soda_buble2",
  "duration": 0.442506,
  "parent": {
    "name": "Boss Soda",
    "path": "folders/Sounds/SFX/Boss Soda.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_boss_soda_buble2",
  "tags": [],
  "resourceType": "GMSound",
}