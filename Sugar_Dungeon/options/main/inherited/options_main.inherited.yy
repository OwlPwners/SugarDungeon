1.0.0←ed6a955d-5826-4f98-a450-10b414266c27←ed6a955d-5826-4f98-a450-10b414266c27|{
    "option_window_colour": {
        "Value": 4278190080
    },
    "option_gameguid": "f8d2f070-3ec8-403b-9f76-f379c09a7a08",
    "option_game_speed": 60,
    "option_lastchanged": "05 March 2020 12:55:58"
}←be5f1418-b31b-48af-a023-f04cdf6e5121|{
    "textureGroups": {
        "Additions": [
            {
                "Key": 1,
                "Value": {
                    "id": "79af880a-136e-4ca9-b366-de06078761af",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "GUI",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 2,
                "Value": {
                    "id": "af2fa1fd-62de-41a5-8d44-d337826160d0",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "MiniMap",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 3,
                "Value": {
                    "id": "ad13237d-c0ba-4c72-871e-9b147cb5dfab",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundMenu",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 4,
                "Value": {
                    "id": "9542f8bb-9157-4ee5-a2a0-92e7007b81ce",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "PlayerTexture",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 5,
                "Value": {
                    "id": "90d3d4f3-5416-4c5c-b3b4-4f7905f27f4e",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "PurpleSugarTextture",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 6,
                "Value": {
                    "id": "14725767-4315-41e4-b1c7-74d819c361f4",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "IcelandTexture",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 7,
                "Value": {
                    "id": "4dbb6eea-5141-49f4-844e-9be483dd02f0",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Logo",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 8,
                "Value": {
                    "id": "98595c65-3fbd-4984-a435-5f83fdad55c8",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BossSoda",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 9,
                "Value": {
                    "id": "20e50c7f-41e2-493e-9a37-7917689fc1c0",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Items",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 10,
                "Value": {
                    "id": "73afedf4-8c91-42f6-b355-c88746dad6c0",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "CutsceneStart",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 11,
                "Value": {
                    "id": "af207d0b-b626-4068-af7d-465345fb64c9",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "TilemapPurpleSugar",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 12,
                "Value": {
                    "id": "f265ca1d-36f4-4674-8ec4-86710cbd8cb4",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "TilemapIce",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 13,
                "Value": {
                    "id": "78f83c32-9938-4a45-aebb-6020a62a3dd3",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "TIleDestroyEffects",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 14,
                "Value": {
                    "id": "f842517a-eabf-4934-96fc-e26e0b8cc684",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BossIcecream",
                    "targets": 461609314234257646,
                    "autocrop": false,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            }
        ],
        "Checksum": "ࢺ懜璙ᓆ锯翋꤯맫",
        "Deletions": [
            
        ],
        "Ordering": [
            
        ]
    }
}←1225f6b0-ac20-43bd-a82e-be73fa0b6f4f|{
    "targets": 461609314234257646,
    "autocrop": false
}←7fa50043-cea6-4cd0-9521-a8ba8c6ea9f0|{
    "audioGroups": {
        "Additions": [
            {
                "Key": 1,
                "Value": {
                    "id": "2762adce-5e5d-4227-a41b-a032bbba6667",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "agp_menu_music",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 2,
                "Value": {
                    "id": "218018f2-c298-4d47-82f5-48ed74a6b916",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "agp_floor_music",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 3,
                "Value": {
                    "id": "a5b99df6-a229-470e-a7f7-46d934fe759a",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "agp_boss_soda_music",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 4,
                "Value": {
                    "id": "16fbc580-963c-43a8-afab-3dd95ae0eb1e",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "agp_cutscene_start_music",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 5,
                "Value": {
                    "id": "a8f80df1-792c-4083-86a0-51533e94e916",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "agp_boss_icecream_music",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 6,
                "Value": {
                    "id": "6ab6e5da-7923-4264-a274-3928c92200a6",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "agp_sfx",
                    "targets": 461609314234257646
                }
            }
        ],
        "Checksum": "濑Ⱥ朦씁ᬄ縈",
        "Deletions": [
            
        ],
        "Ordering": [
            
        ]
    }
}←7b2c4976-1e09-44e5-8256-c527145e03bb|{
    "targets": 461609314234257646
}