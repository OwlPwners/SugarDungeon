///@arg array_with_audiogriups
function audio_group_unload_num(argument0) {

	var arr = argument0, num = array_length(arr), i = 0;
	repeat(num) {
		var agp = arr[i];
		if (audio_group_is_loaded(agp)) audio_group_unload(agp);
	
		i ++;
	}


}
