///@arg file_name
///@arg selection
///@arg key
function ini_translate(argument0, argument1, argument2) {

	var file = argument0, selection = argument1, key = argument2, text;

	ini_open(file);

	text = ini_read_string(selection, key, "Default Text...");

	ini_close();

	return text;


}
