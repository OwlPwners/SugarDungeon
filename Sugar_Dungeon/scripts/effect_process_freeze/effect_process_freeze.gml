function effect_process_freeze() {
	timer_freeze --;
	if (timer_freeze > 0) {	
		image_blend = freeze_colour;
	
		spd_factor *= spd_freeze;
		img_spd_factor *= img_spd_freeze;
	
		part_snow_burst(x, y, irandom(1));
	} else {
		effect_delete(obj_effect.freeze);
	}
}