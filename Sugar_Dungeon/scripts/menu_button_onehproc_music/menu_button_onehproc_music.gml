function menu_button_onehproc_music() {
	global.music_gain = 1;
	audio_group_set_gain_all_music(global.music_gain);

	with(slider) {
		if (name == "music") {
			volume = x + width / 2;
		
			ini_save(file_settings, "Real", "SoundsAndMusic", "MusicSlider", volume);
		}
	}

	ini_save(file_settings, "Real", "SoundsAndMusic", "MusicVolume", global.music_gain);


}
