///@arg sprite_or_surface
///@arg image
///@arg x
///@arg y
///@arg width
///@arg color_or_array
///@arg alpha
function draw_sprite_outline(argument0, argument1, argument2, argument3, argument4, argument5, argument6) {

	var spr = argument0, img = argument1,
	xx = argument2, yy = argument3,
	width = argument4, col = argument5, alpha = argument6;

	var u_color = shader_get_uniform(shd_outline, "Color"),
	u_texel = shader_get_uniform(shd_outline, "Texel"),
	u_width = shader_get_uniform(shd_outline, "Width");

	var tex = 0;
	tex = sprite_get_texture(spr, img); 
	
	var tw = texture_get_texel_width(tex), th = texture_get_texel_height(tex);

	shader_set(shd_outline);

	if (is_array(col)) {
		shader_set_uniform_f(u_color, col[0], col[1], col[2], alpha);
	} else {
		shader_set_uniform_f(u_color, color_get_red(col) / 255, color_get_green(col) / 255, color_get_blue(col) / 255, alpha);
	}

	shader_set_uniform_f(u_texel, tw, th);
	shader_set_uniform_f(u_width, width);

	draw_sprite_ext(spr, img, xx, yy, image_xscale, image_yscale, image_angle, image_blend, image_alpha);

	shader_reset();


}
