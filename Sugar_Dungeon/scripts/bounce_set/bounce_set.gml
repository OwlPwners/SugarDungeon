///@arg x1
///@arg y1
///@arg x2
///@arg y2
///@arg force
///@arg object
function bounce_set(argument0, argument1, argument2, argument3, argument4, argument5) {

	var x1, y1, x2, y2, force, obj;
	x1 = argument0;
	y1 = argument1;
	x2 = argument2;
	y2 = argument3;
	force = argument4;
	obj = argument5;

	var dir = point_direction(x1, y1, x2, y2) - 180;
	obj.bounce_dir = dir;
	obj.bounce_force = force;


}
