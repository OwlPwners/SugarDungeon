///@arg obj

function instance_numer_deactivated(object) {
	var n = 0;
	
	with(chunk_controller) {
		for(var i = 0; i < array_length(list_chunk); i ++) {
			if (list_chunk[i][1] == object) n ++;
		}
	}
	
	return n;
	
	//var n = 0, i = 0, size = ds_list_size(global.deactivate_list);
	//repeat(size) {
	//	var index = global.deactivate_list[|i][3];
	//	if (index == object || object_is_ancestor(index, object)) n ++;
		
	//	i ++;
	//}
	
	//return n;
}