function enemy_draw() {
	draw_sprite_ext(sprite_index, image_index, x, y + stunned_y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);

	if (!stunned && flash != 0) flash_check();
}