///@arg file_name
///@arg type
///@arg selection
///@arg key
///@arg default
function ini_load(argument0, argument1, argument2, argument3, argument4) {

	var file = argument0, type = argument1,
	selection = argument2, key = argument3,
	default_ = argument4;

	var to_return;

	ini_open(file);

	switch(type) {
		case("String") :
		to_return = ini_read_string(selection, key, default_);
		break;
	
		case("Real") :
		to_return = ini_read_real(selection, key, default_);
		break;
	}

	ini_close();

	return to_return;


}
