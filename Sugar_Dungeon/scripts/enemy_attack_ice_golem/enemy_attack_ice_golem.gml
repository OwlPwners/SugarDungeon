function enemy_attack_ice_golem() {
	if (state_prev != state) image_index = 0;
		
	if (timer_attack <= 0) {
		if (hp > hp_max * 0.25) {	
			sprite_index = sprite_attack;
			mask_index = attack_hitbox;
	
			var player_inst = instance_place(x, y, player);
			if (player_inst != noone) {
				if (player_inst.war_timer <= 0 && player_inst.glide_spd == 0) {
					player_inst.flash = 1;
					player_inst.war_timer = player_inst.war_time;
				
					var inst_sh = player.shield_id;
					if (player.state == player_state.block && instance_exists(inst_sh) && place_meeting(x, y, inst_sh)) {
						var map = inst_sh.map,
						proc_hp = (100 - map[? "BLockHp"]) / 100,
						proc_bounce = (100 - map[? "BlockKnockback"]) / 100;
						
						player_inst.hp -= damage * proc_hp;
						bounce_set(player_inst.x, player_inst.y, x, y, force_bounce * proc_bounce, player_inst);
						with(player_inst) part_sparks_burst(x, y, 5);
					} else {
						player_inst.hp -= damage;
						bounce_set(player_inst.x, player_inst.y, x, y, force_bounce, player_inst);
					}
				
					var s = 7;
					camera.x += random_range(-s, s);
					camera.y += random_range(-s, s);
				}
			}
		} else {
			sprite_index = sprite_attack_spikes;
		}
	} else {
		sprite_index = sprite_stop;
			
		timer_attack --;
	}
	
	hsp = 0;
	vsp = 0;


}
