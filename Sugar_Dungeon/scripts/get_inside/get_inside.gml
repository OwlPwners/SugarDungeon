///@arg xx
///@arg yy
///@arg x1
///@arg y1
///@arg x2
///@arg y2

function get_inside(xx, yy, x1, y1, x2, y2) {
	if (x1 == undefined) x1 = camera.x - camera.cam_w / 2 - cam_dborder_w;
	if (y1 == undefined) y1 = camera.y - camera.cam_h / 2 - cam_dborder_h;
	if (x2 == undefined) x2 = camera.x + camera.cam_w / 2 + cam_dborder_w;
	if (y2 == undefined) y2 = camera.y + camera.cam_h / 2 + cam_dborder_h;
	
	var inside = (clamp(xx, x1, x2) == xx) 
	&& (clamp(yy, y1, y2) == yy);
		
	return inside;
}