///@arg filename
function buffer_load_string(file) {
	var buffer = buffer_load(file), str = buffer_read(buffer, buffer_string);
	buffer_delete(buffer);
	var expected_hash = string_copy(str, string_length(str) - 40, 40),
	hashless_string = string_copy(str, 1, string_length(str) - 42),
	new_hash = sha1_string_utf8_hmac("aS3diewD5jlfe28493CfoiafNeh878fejn29u9NUF", hashless_string);

	if (expected_hash == new_hash) {
		return hashless_string
	} else {
		file_delete(file);
	
		game_end();
	}
}