///@arg x1
///@arg y1
///@arg x2
///@arg y2
///@arg tile_layer
function tile_collision_line_point(argument0, argument1, argument2, argument3, argument4) {

	var x1 = argument0;
	var y1 = argument1;
	var x2 = argument2;
	var y2 = argument3;
	var tile_layer = argument4;

	var rr, rx, ry;
	rr = tile_collision_line(x1, y1, x2, y2, tile_layer, 1);
	rx = x2;
	ry = y2;

	if (rr != 0) { 
	    var point0 = 0;
	    var point1 = 1;
	
		var cout = ceil(log2(point_distance(x1, y1, x2, y2))) + 1;
		repeat(cout) { 
	        var npoint = point0 + (point1 - point0) * 0.5;
	        var nx = x1 + (x2 - x1) * npoint;
	        var ny = y1 + (y2 - y1) * npoint;
	        var pointx = x1 + (x2 - x1) * point0;
	        var pointy = y1 + (y2 - y1) * point0;
	        var nr = tile_collision_line(pointx, pointy, nx, ny, tile_layer, 1);
		
	        if (nr != 0) {
	            rr = nr;
	            rx = nx;
	            ry = ny;
	            point1 = npoint;
	        }  else point0 = npoint;
	    } 
	} 

	var r;
	r[tilepoint_index] = rr;
	r[tilepoint_x] = rx;
	r[tilepoint_y] = ry;

	return r;


}
