function part_snow_decor_destroy() {
	part_type_destroy(part_snow_decor_particle);
	part_emitter_destroy(global.particle_system, part_snow_decor_emitter);
}