///@arg x
///@arg y
///@arg text
///@arg color
///@arg scale_amount 
///@arg scale_target 
///@arg offset_target
///@arg alpha_spd
function text_indicator_create() {
	with(instance_create_depth(argument[0], argument[1], -999, text_indicator)) {
		text = argument[2];
		if (argument_count > 3) color = argument[3];
		if (argument_count > 4) scale_amount = argument[4];
		if (argument_count > 5) scale_target = argument[5];
		if (argument_count > 6) offset_target = argument[6];
		if (argument_count > 7) alpha_spd = argument[7];
	}
}