function wps_player_attack() {
	if (place_meeting(x, y, tnt)) {
		var inst = instance_place(x, y, tnt);
		inst.activated = true;
	
		statistic_manager.map[? "ExplodedTNT"] ++;
	}
			
	if (place_meeting(x, y, enemy)) {
		var enemy_list = ds_list_create(),
		enemy_inst = instance_place_list(x, y, enemy, enemy_list, false);
	
		if (!attacked && enemy_inst > 0) {
			for(var i = 0; i < enemy_inst; i ++) {
				var inst = enemy_list[| i];
				if (inst.state != "glide") {
					var player_damage = map[? "Damage"] * player.damage_factor;
					inst.hp -= player_damage;
				
					var so = -1;
					//if (inst.sound_damaged > -1) {
						if (is_array(inst.sound_damaged)) {
							var n = array_length(inst.sound_damaged);
							so = inst.sound_damaged[irandom(n - 1)];
						} else {
							so = inst.sound_damaged;
						}
					//}
	
					audio_play_sound(so, 99, false);
				
					statistic_manager.map[? "Damaged"] += player_damage;
				
					bounce_set(inst.x, inst.y, x, y, map[? "Knockback"], inst);
				
					inst.timer_attack = 0;
					inst.war_timer = inst.war_time;
					inst.flash = 1;
				}
			}
		
			attacked = true;
		}
	
		ds_list_destroy(enemy_list);
	}


}
