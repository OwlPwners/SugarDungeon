///@arg map
///@arg char_size
function button_check_released(argument0, argument1) {

	var xx = ds_map_find_value(argument0, "X"), yy = ds_map_find_value(argument0, "Y"),
	text = ds_map_find_value(argument0, "Text"), text_size = argument1, 
	text_length = string_length(text) * text_size, 
	sprw = ds_map_find_value(argument0, "SpriteWidth"), sprh = ds_map_find_value(argument0, "SpriteHeight")

	var between = 18;

	if (text != -1) {
		var in_rect = point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), xx - text_length / 2, yy - (text_size + between) / 2, xx + text_length / 2, yy + (text_size + between) / 2) 
		|| point_in_rectangle(view_xport[0] + cursor.x, view_yport[0] + cursor.y, xx - text_length / 2, yy - (text_size + between) / 2, xx + text_length / 2, yy + (text_size + between) / 2);

		if (in_rect == true) {
			if (mouse_check_button_released(mb_left) || cursor.click_released) return true;
		}
	} else {
		var in_rect = point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), xx - sprw / 2, yy - sprh / 2, xx + sprw / 2, yy + sprh / 2)
		|| point_in_rectangle(view_xport[0] + cursor.x, view_yport[0] + cursor.y, xx - sprw / 2, yy - sprh / 2, xx + sprw / 2, yy + sprh / 2);

		if (in_rect == true) {
			if (mouse_check_button_released(mb_left) || cursor.click_released) return true;
		}
	}


}
