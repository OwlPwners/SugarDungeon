function bounce_process() {
	bounce_force -= bounce_fric;
	bounce_force = max(0, bounce_force);

	x_bounce_f = lengthdir_x(bounce_force, bounce_dir);
	y_bounce_f = lengthdir_y(bounce_force, bounce_dir);


}
