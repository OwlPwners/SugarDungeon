///@arg instance
///@arg time
function effect_add_freeze(argument0, argument1) {

	var inst = argument0, time = argument1, absent = (effect_instance_find_effect(inst, obj_effect.freeze) == -1) && (effect_instance_find_effect(inst, obj_effect.notcold) == -1);
	if (absent) {
		ds_list_add(inst.eff_list, obj_effect.freeze);
	
		inst.timer_freeze = time;
	}
}