///@arg sprite_stop
///@arg sprite_run
///@arg sprite_attack
///@arg hitbox
///@arg hitbox_attack
///@arg speed
///@arg hp
///@arg attack_power
///@arg force_bounce
///@arg min_attack_dist
///@arg see_distance 
///@arg friction
///@arg money
///@arg sound_attack
///@arg sound_damaged
///@arg fight_weight
function enemy_create() {
	layer_changed = false;
	
	appearance = false;
	visible = false;

	stunned = false;
	stunned_prev = stunned;
	stunned_once = false;
	stunned_timer = 100;
	stunned_y = 0;

	healing = false;
	i_can_healing = true;
	time_healing = 120;
	timer_healing = time_healing;
	can_be_healed = true;

	time_search_wounded = 160;
	timer_search_wounded = time_search_wounded;

	enemy_wounded_id = -1;

	sprite_stop = argument[0];
	sprite_run = argument[1];
	sprite_attack = argument[2];
	hitbox = argument[3];
	attack_hitbox = argument[4];

	state = "stop";
	state_prev = "-1";

	hsp = 0;
	vsp = 0;
	spd = argument[5];

	grv = 0.25;
	jumpf = -7.25;

	part_step_create();
	part_step_timer = 0;

	glide_time = 105;
	glide_timer = glide_time;

	glide_spd = 0;
	glide_force = 7;
	glide_fric = 0.025;

	x_glide_dir = 0;
	y_glide_dir = 0;

	hp_max = argument[6];
	hp = hp_max;

	damage = argument[7];
	force_bounce = argument[8];

	flash = 0;
	scale_to = 1;

	time_attack = 18;
	timer_attack = time_attack;
	dist_y_attack = argument[9]

	war_time = 40;
	war_timer = 0;

	seen = false;
	see_dist = argument[10];

	save_object_default_variables();
	save_object_add_variables("hp", save_object_is_stunned_enemy);

	depth_variables();
	depth = -bbox_bottom;

	create_bounce_system(argument[11]);

	money = argument[12];

	sound_attack = argument[13]
	sound_damaged = argument[14];
	sound_attack_played = false;
	
	if (argument_count > 15) fight_weight = argument[15]; else fight_weight = 1;
	fight_with_player = false;

	clamp_box_id = -2;

	path = path_add();
	path_set_kind(path, true);
	path_set_precision(path, 6);
	
	path_pos = 1;
	path_xto = path_get_point_x(path, path_pos);
	path_yto = path_get_point_y(path, path_pos);
	
	state_controller = {
		choosing_process : enemy_choose_state,
		
		check_fight : enemy_choose_state_component_checkfight,
		
		check_attack : enemy_choose_state_component_checktosetattackstate,
		set_attack : enemy_choose_state_component_setattackstate,
		
		check_glide : enemy_choose_state_component_checktosetglidestate,
		set_glide : enemy_choose_state_component_setglidestate,
		
		check_imitation : enemy_choose_state_component_checktosetimitationstate,
		set_imitation : enemy_choose_state_component_setimitationstate,
		
		check_healing_other : enemy_choose_state_component_checktosethealingotherstate,
		set_healing_other : enemy_choose_state_component_sethealingotherstate,
	}
	
	script_stop = enemy_stop_standart;
	script_imitation = enemy_imitation_standart;
	script_run = enemy_run_standart;
	script_glide = enemy_glide;
	script_attack = enemy_attack_standart;
	script_run_to_wounded = enemy_run_to_wounded;
	script_healing_other = enemy_healing_other;
	script_comes_to_life = enemy_comes_to_life;
	
	state_map = ds_map_create();
	state_map[? "stop"] = script_stop;
	state_map[? "run"] = script_run;
	state_map[? "imitation"] = script_imitation;
	state_map[? "attack"] = script_attack;
	state_map[? "glide"] = script_glide;
	state_map[? "run to wounded"] = script_run_to_wounded;
	state_map[? "healing other"] = script_healing_other;
	state_map[? "comes to life"] = script_comes_to_life;
}