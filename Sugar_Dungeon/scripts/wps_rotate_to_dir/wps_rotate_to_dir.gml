function wps_rotate_to_dir() {
	image_xscale = 1;

	var gp = global.gp_connect_id, dirr;
	if (gp != -1) {
		var input_hor, input_ver;
		input_hor = gamepad_axis_value(gp, gp_axisrh);
		input_ver = gamepad_axis_value(gp, gp_axisrv);
	
		dirr = dir;
	} else dirr = point_direction(player.x, player.y, mouse_x, mouse_y);

	image_angle = dirr;


}
