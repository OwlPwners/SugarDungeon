function effect_system_process() {
	image_blend = c_white;
	image_speed = 1;

	var eff_list_size = ds_list_size(eff_list), i = 0;
	repeat(eff_list_size) {
		var eff = eff_list[| i];
		if (eff != undefined) {
			if (!eff_array[eff, eff_info.dy_immunity]) {
				script_execute(eff_array[eff, eff_info.script]);
			} else {
				effect_delete(eff);
			}
		}
	
		i ++;
	}
}