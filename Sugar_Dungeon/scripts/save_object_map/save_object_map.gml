///@arg array_with_variables
///@arg object
function save_object_map(argument0, argument1) {
	var map = ds_map_create(), arr = argument0, size = array_length(arr), i = 0, obj = argument1;
	with(obj) {
		repeat(size) {
			var name = arr[i], val = variable_instance_get(self, name);
			switch(name) {
				case(save_object_name) :
				val = object_get_name(object_index);
				ds_map_add(map, name, val);
				break;
		
				case(save_object_layer) :
				val = layer_get_name(layer);
				ds_map_add(map, name, val);
				break;
			
				case(save_object_is_merchant_array) :
				ds_map_add(map, name, true);
			
				for(var i = 0; i < product_number; i ++) {
					ds_map_add(map, "product" + string(i), product_array[i]);
				}
				break;
			
				case(save_object_is_stunned_enemy) :
				val = stunned;
				ds_map_add(map, name, val);
				break;
			
				default :
				ds_map_add(map, name, val);
				break;
			}
	
			i ++;
		}
	}

	return map


}
