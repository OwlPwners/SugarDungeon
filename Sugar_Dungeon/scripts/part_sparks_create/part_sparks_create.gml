function part_sparks_create() {
	part_sparks_particle = part_type_create();
	part_type_shape(part_sparks_particle, pt_shape_disk);
	part_type_size(part_sparks_particle, 0.05, 0.075, 0, 0);
	part_type_color2(part_sparks_particle, 16777215, 1218285);
	part_type_alpha2(part_sparks_particle, 1, 0);
	part_type_speed(part_sparks_particle, 1, 1, 0.4, 1);
	part_type_direction(part_sparks_particle, 0, 180, 0, 0);
	part_type_gravity(part_sparks_particle, 0.3, 270);
	part_type_orientation(part_sparks_particle, 0, 0, 0, 0, 1);
	part_type_blend(part_sparks_particle, 0);
	part_type_life(part_sparks_particle, 30, 45);

	part_sparks_emitter = part_emitter_create(global.particle_system);


}
