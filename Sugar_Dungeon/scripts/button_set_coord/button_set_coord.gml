///@arg map
///@arg x
///@arg y
function button_set_coord(argument0, argument1, argument2) {

	var xx = argument1, yy = argument2;
	ds_map_set(argument0, "X", xx);
	ds_map_set(argument0, "Y", yy);


}
