///@arg sprite
///@arg image
///@arg x1
///@arg y1
///@arg x2
///@arg y2
///@arg blend
///@arg alpha
function draw_gui_box(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {

	var spr = argument0, image = argument1,
	x1 = argument2, y1 = argument3,
	x2 = argument4, y2 = argument5,
	blend = argument6, alpha = argument7,

	var part_width = sprite_get_width(spr) / 3, part_height = sprite_get_height(spr) / 3;

	var xscale = 1 / part_width * (x2 - x1 - part_width),
	yscale = 1 / part_height * (y2 - y1 - part_height);

	draw_sprite_part_ext(spr, image, part_width, part_height, part_width, part_height, x1 + part_width, y1 + part_height, xscale, yscale, blend, alpha);

	draw_set_alpha(alpha);
	draw_sprite_part(spr, image, 0, 0, part_width, part_height, x1, y1);
	draw_sprite_part(spr, image, part_width * 2, 0, part_width, part_height, x2, y1);
	draw_sprite_part(spr, image, 0, part_width * 2, part_width, part_height, x1, y2);
	draw_sprite_part(spr, image, part_width * 2, part_width * 2, part_width, part_height, x2, y2);
	draw_set_alpha(1);

	draw_sprite_part_ext(spr, image, part_width, 0, part_width, part_height, x1 + part_width, y1, xscale, 1, blend, alpha);
	draw_sprite_part_ext(spr, image, part_width, part_height * 2, part_width, part_height, x1 + part_width, y2, xscale, 1, blend, alpha);

	draw_sprite_part_ext(spr, image, 0, part_height, part_width, part_height, x1, y1 + part_height, 1, yscale, blend, alpha);
	draw_sprite_part_ext(spr, image, part_width * 2, part_height, part_width, part_height, x2, y1 + part_height, 1, yscale, blend, alpha);


}
