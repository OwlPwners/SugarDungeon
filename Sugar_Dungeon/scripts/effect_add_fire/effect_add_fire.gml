///@arg instance
///@arg time
///@arg timer_fire_div
///@arg hp_minus
function effect_add_fire(argument0, argument1, argument2, argument3) {

	var inst = argument0, time = argument1, divv = argument2, hpp = argument3, absent = effect_instance_find_effect(inst, obj_effect.fire) == -1;
	if (absent) {
		ds_list_add(inst.eff_list, obj_effect.fire);
	
		inst.timer_fire = time;
		inst.hp_fire = hpp;
		inst.timer_fire_div = divv;
	}


}
