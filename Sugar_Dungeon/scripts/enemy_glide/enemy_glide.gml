function enemy_glide() {
	sprite_index = sprite_run;
	image_speed = 1.25;
		
	glide_timer = glide_time;

	part_step_burst(1);
		
	glide_spd = lerp(glide_spd, 0, glide_fric);
	hsp = x_glide_dir * glide_spd;
	vsp = y_glide_dir * glide_spd;
}