function part_bubble_create() {
	part_bubble_particle = part_type_create();
	part_type_sprite(part_bubble_particle, spr_soda_bullet, 0, 0, 0);
	part_type_size(part_bubble_particle, 1, 1, 0.01, 0);
	part_type_scale(part_bubble_particle, 0.50, 0.50);
	part_type_alpha3(part_bubble_particle, 0, 0.75, 0);
	part_type_speed(part_bubble_particle, 1, 1, 0, 0);
	part_type_direction(part_bubble_particle, 0, 359, 0, 0);
	part_type_blend(part_bubble_particle, 0);
	part_type_life(part_bubble_particle, 15, 45);

	part_bubble_emitter = part_emitter_create(global.particle_system);


}
