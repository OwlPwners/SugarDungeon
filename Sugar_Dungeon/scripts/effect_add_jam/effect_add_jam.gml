///@arg instance
///@arg time
function effect_add_jam(argument0, argument1) {

	var inst = argument0, time = argument1, absent = effect_instance_find_effect(inst, obj_effect.jam) == -1;
	if (absent) {
		ds_list_add(inst.eff_list, obj_effect.jam);
	
		inst.timer_jam = time;
	}


}
