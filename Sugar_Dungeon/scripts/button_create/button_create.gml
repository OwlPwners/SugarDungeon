///@arg sprite
///@arg image_down
///@arg image_up
///@arg x
///@arg y
///@arg blend
///@arg alpha
///@arg text
function button_create(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {

	var button = ds_map_create();
	ds_map_add(button, "Sprite", argument0);
	ds_map_add(button, "ImageDown", argument1)
	ds_map_add(button, "ImageUp", argument2);
	ds_map_add(button, "X", argument3);
	ds_map_add(button, "Y", argument4);
	ds_map_add(button, "Blend", argument5);
	ds_map_add(button, "Alpha", argument6);
	ds_map_add(button, "Text", argument7);

	ds_map_add(button, "SpriteWidth", sprite_get_width(ds_map_find_value(button, "Sprite")));
	ds_map_add(button, "SpriteHeight", sprite_get_height(ds_map_find_value(button, "Sprite")));

	return button;


}
