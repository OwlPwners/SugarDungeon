///@arg sprite
function part_crush_create(argument0) {

	part_crush_particle = part_type_create();
	part_type_sprite(part_crush_particle, argument0, false, 1, true);
	part_type_size(part_crush_particle, 1, 1.25, 0, 0);
	part_type_alpha2(part_crush_particle, 1, 0);
	part_type_speed(part_crush_particle, 1, 1, 0.4, 1);
	part_type_direction(part_crush_particle, 45, 135, 0, 0);
	part_type_gravity(part_crush_particle, 0.25, 270);
	part_type_orientation(part_crush_particle, 0, 0, 0, 0, 1);
	part_type_blend(part_crush_particle, 0);
	part_type_life(part_crush_particle, 15, 45);

	part_crush_emitter = part_emitter_create(global.particle_system);


}
