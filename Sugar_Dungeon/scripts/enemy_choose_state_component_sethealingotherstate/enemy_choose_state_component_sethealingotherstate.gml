function enemy_choose_state_component_sethealingotherstate() {
	timer_search_wounded = time_search_wounded;
	enemy_wounded_id = enemy_search_wounded();
			
	if (enemy_wounded_id != -1 && instance_exists(enemy_wounded_id) && enemy_wounded_id.healing == false) {
		state = "run to wounded";
				
		i_can_healing = false;
	}
}