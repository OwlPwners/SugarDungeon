///@arg macro_index
///@arg type
///@arg sprite_icon
///@arg sprite_attack
///@arg sprite_mask
///@arg sprite_block
///@arg rarity
///@arg knockback
///@arg damage
///@arg price
///@arg procent_block
///@arg script_begin
///@arg script_end
///@arg script_animend
function wp_add_weapon(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9, argument10, argument11, argument12, argument13) {

	var macro_index = argument0;

	global.map_items[macro_index] = ds_map_create();
	ds_map_add(global.map_items[macro_index], "Type", argument1);
	ds_map_add(global.map_items[macro_index], "Icon", argument2);

	var spr = argument3;
	if (is_array(spr)) {
		var list = ds_list_create(), len = array_length(spr);
	
		var i = 0;
		repeat(len) {
			list[| i] = spr[i];
		
			i ++;
		}
	
		ds_map_add(global.map_items[macro_index], "SpriteAnimated", true);
		ds_map_add(global.map_items[macro_index], "Sprite", ds_list_write(list));
		ds_list_destroy(list);
	} else {
		ds_map_add(global.map_items[macro_index], "SpriteAnimated", false);
		ds_map_add(global.map_items[macro_index], "Sprite", spr);
	}

	var mask = argument4;
	if (is_array(mask)) {
		var list = ds_list_create(), len = array_length(mask);
	
		var i = 0;
		repeat(len) {
			list[| i] = mask[i];
		
			i ++;
		}
	
		ds_map_add(global.map_items[macro_index], "Mask", ds_list_write(list));
		ds_list_destroy(list);
	} else {
		ds_map_add(global.map_items[macro_index], "Mask", mask);
	}

	ds_map_add(global.map_items[macro_index], "BlockSprite", argument5);
	ds_map_add(global.map_items[macro_index], "Rarity", argument6);
	ds_map_add(global.map_items[macro_index], "Knockback", argument7);
	ds_map_add(global.map_items[macro_index], "Damage", argument8);
	ds_map_add(global.map_items[macro_index], "Price", argument9);
	ds_map_add(global.map_items[macro_index], "ProcentBlock", argument10);
	ds_map_add(global.map_items[macro_index], "ScriptBegin", argument11);
	ds_map_add(global.map_items[macro_index], "ScriptEnd", argument12);
	ds_map_add(global.map_items[macro_index], "ScriptAnimEnd", argument13);


}
