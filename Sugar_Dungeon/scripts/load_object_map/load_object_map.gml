///@arg map
///@arg object
function load_object_map(argument0, argument1) {

	var map = argument0, size = ds_map_size(map), obj = argument1;
	with(obj) {
		var size = ds_map_size(map), key = ds_map_find_first(map);
		repeat(size) {
			var val = map[? key];
			switch(key) {
				case(save_object_name) :
				val = asset_get_index(val);
				variable_instance_set(self, key, val);
				break;
				
				case(save_object_layer) :
				val = layer_get_id(val);
				variable_instance_set(self, key, val);
				break;
			
				case(save_object_is_merchant_array) :
				for(var i = 0; i < product_number; i ++) {
					product_array[i] = map[? ("product" + string(i))];
				}
				break;
			
				case(save_object_is_stunned_enemy) :
				if (val) instance_destroy(self);
				break;
			
				default :
				variable_instance_set(self, key, val);
				break;
			}
		
			key = ds_map_find_next(map, key);
		}
	}
}