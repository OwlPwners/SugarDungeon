///@arg index
///@arg type
///@arg icon
///@arg rarity
///@arg price
///@arg color
///@arg script_animend
///@arg script_real
function potion_add(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {

	var index = argument0;
	global.map_items[index] = ds_map_create();

	ds_map_add(global.map_items[index], "ItemIndex", index);
	ds_map_add(global.map_items[index], "Type", argument1);
	ds_map_add(global.map_items[index], "Icon", argument2)
	ds_map_add(global.map_items[index], "Rarity", argument3);
	ds_map_add(global.map_items[index], "Price", argument4);;
	ds_map_add(global.map_items[index], "Color", argument5);
	ds_map_add(global.map_items[index], "Script", argument6);

	var scr = argument7, args = argument_count - 1;
	if (args >= 7) script_execute(scr);

	/*
	ds_map_add(global.map_items[item.potion_smallives], "Adding", 1);
	ds_map_add(global.map_items[item.potion_smallives], "Rarity", 55);
	ds_map_add(global.map_items[item.potion_smallives], "Price", 12);

/* end potion_add */
}
