function effect_system_destroy() {
	ds_list_destroy(eff_list);

	var len = array_length(eff_array), i = 0;
	repeat(len) {
		var scr_dest_part = eff_array[i, eff_info.scr_destr_part];
		if (!eff_array[i, eff_info.immunity] && scr_dest_part > -1) {
			script_execute(scr_dest_part);
		}
	}


}
