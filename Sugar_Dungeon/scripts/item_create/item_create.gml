///@arg x
///@arg y
///@arg player_seen
///@arg need_show
///@arg map_string
function item_create(argument0, argument1, argument2, argument3, argument4) {

	var xx, yy, seen, n_show, map;
	xx = argument0;
	yy = argument1;
	seen = argument2;
	n_show = argument3;
	map = argument4;

	with(instance_create_layer(xx, yy, layer_other, loot)) {
		player_seen = seen;
		need_show = n_show;
		to_add_index = map[? "ItemIndex"]; //array_find_map_index(global.map_items, map);
		to_add = global.map_items[to_add_index];
				
		sprite_index = to_add[? "Icon"];
	}


}
