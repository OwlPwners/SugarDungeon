///@arg x1
///@arg y1
///@arg x2
///@arg y2
///@arg tilemap_layer
///@arg pixel_distance
function tile_collision_line(argument0, argument1, argument2, argument3, argument4, argument5) {

	var x1, y1, x2, y2, _layer, len;
	x1 = argument0;
	y1 = argument1;
	x2 = argument2;
	y2 = argument3;
	_layer = argument4;
	len = argument5;

	var dist, dir, cout;
	dist = point_distance(x1, y1, x2, y2);
	dir = point_direction(x1, y1, x2, y2);
	cout = dist / len;

	var i = 0;
	repeat(cout) {
		var xx, yy;
		xx = x1 + lengthdir_x(i, dir);
		yy = y1 + lengthdir_y(i, dir);
	
		var tile = tilemap_get_at_pixel(_layer, xx, yy);
	
		if (tile != 0) {
			return tile;
		
			break;
		}
	
		i += len;
	}

	return false;


}
