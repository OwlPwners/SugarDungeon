///@arg index
///@arg map
function bitmask_normalized(argument0, argument1) {

	var index = argument0, map = argument1;

	index = ds_map_find_value(map, string(index));

	return index;


}
