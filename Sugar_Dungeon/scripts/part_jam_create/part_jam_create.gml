function part_jam_create() {
	part_jam_system = part_system_create();
	part_system_depth(part_jam_system, 599);

	part_jam_particles = part_type_create();
	part_type_sprite(part_jam_particles, spr_part_jam_steps_player, false, true, true);
	part_type_scale(part_jam_particles, 1, 1);
	part_type_color1(part_jam_particles, jam_blood_red_color);
	part_type_alpha2(part_jam_particles, 0.8, 0);
	part_type_blend(part_jam_particles, false);
	part_type_life(part_jam_particles, 40, 40);

	part_jam_emitter = part_emitter_create(part_jam_system);


}
