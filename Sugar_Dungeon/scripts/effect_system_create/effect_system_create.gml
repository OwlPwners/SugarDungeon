///@arg immunity_array
function effect_system_create() {

	eff_list = ds_list_create();

#region Creating effects.
	effect_add(obj_effect.freeze, effect_process_freeze, part_snow_create, part_snow_destroy);
	effect_add(obj_effect.jam, effect_process_jam, part_jam_create, part_jam_destroy);
	effect_add(obj_effect.fire, effect_process_fire, part_fire_create, part_fire_destroy);
	effect_add(obj_effect.notcold, effect_process_notcold, part_not_cold_create, part_not_cold_destroy);
	effect_add(obj_effect.icecream_lake, effect_process_icecream_lake, -1, -1);
#endregion

#region Adding immunity (if it exists) to array with effects.
	if (argument_count > 0) {
		var arr = argument[0], i = 0;
		repeat(array_length(arr)) {
			var val = arr[i];
			eff_array[val, eff_info.immunity] = true;
			script_execute(eff_array[val, eff_info.scr_destr_part]);
		
			i ++;
		}
	}
#endregion

	/// Timers initialization; 
	timer_freeze = 0;
	timer_jam = 0;
	timer_icecream_lake = 0;

	// Only for player.
	notcold = false;
	timer_not_cold = 0;
}