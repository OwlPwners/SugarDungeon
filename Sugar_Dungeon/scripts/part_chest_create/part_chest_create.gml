function part_chest_create() {
	part_chest_particle = part_type_create();
	part_type_shape(part_chest_particle, pt_shape_disk);
	part_type_size(part_chest_particle, 0.05, 0.10, 0, 0);
	part_type_scale(part_chest_particle, 1, 1);
	part_type_color1(part_chest_particle, 4227327);
	part_type_alpha2(part_chest_particle, 1, 0);
	part_type_speed(part_chest_particle, 1, 4, 0, 0);
	part_type_direction(part_chest_particle, 50, 130, 0, 0);
	part_type_gravity(part_chest_particle, 0.10, 270);
	part_type_blend(part_chest_particle, 1);
	part_type_life(part_chest_particle, 15, 30);

	part_chest_emitter = part_emitter_create(global.particle_system);


}
