function enemy_run_standart(){
	var player_behined_walls = tile_collision_line(x, y, player.x, player.bbox_bottom, layer_collision, 5),
	astar_enable;
		
	if (player_behined_walls) {
		astar_enable = true;
	} else {
		astar_enable = false;
	}
		
	enemy_run(player.x, player.y, astar_enable);
}