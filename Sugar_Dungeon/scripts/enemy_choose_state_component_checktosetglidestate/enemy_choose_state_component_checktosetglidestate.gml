function enemy_choose_state_component_checktosetglidestate() {
	var wp = player.wp_id, val = 
	(glide_timer <= 0 && instance_exists(wp) && wp.state == "attack" 
	&& state != "glide" && wp.image_index < (wp.image_number - 1) * 0.2 && wp.image_xscale == -image_xscale 
	&& !tile_collision_line(x, y, player.x, player.y, layer_collision, tile_size / 4)
	&& distance_to_object(wp) < sqrt(sqr(sprite_width) + sqr(sprite_height)));
	
	return val;
}