function part_not_cold_create() {
	part_not_cold_particle = part_type_create();
	part_type_shape(part_not_cold_particle, pt_shape_flare);
	part_type_size(part_not_cold_particle, 0.00, 1, 0, 0);
	part_type_scale(part_not_cold_particle, 0.50, 0.50);
	part_type_color3(part_not_cold_particle, 6029311, 2265599, 5987327);
	part_type_alpha3(part_not_cold_particle, 0, 0.30, 0);
	part_type_speed(part_not_cold_particle, 1, 1.25, 0, 0);
	part_type_direction(part_not_cold_particle, 0, 359, 0, 0);
	part_type_orientation(part_not_cold_particle, 0, 0, 0, 0, 1);
	part_type_blend(part_not_cold_particle, 1);
	part_type_life(part_not_cold_particle, 30, 70);

	part_not_cold_emitter = part_emitter_create(global.particle_system);


}
