///@arg sprite
///@arg image
///@arg steps_blur
///@arg intensity
///@arg x
///@arg y
function draw_sprite_glow(argument0, argument1, argument2, argument3, argument4, argument5) {

	var spr = argument0, img = argument1, steps = argument2, intens = argument3;

	var tex = sprite_get_texture(spr, img),
	texel_w = texture_get_texel_width(tex),
	texel_h = texture_get_texel_height(tex);

	var uni_texel = shader_get_uniform(shd_glowblur, "Texel"),
	uni_steps = shader_get_uniform(shd_glowblur, "Steps"),
	uni_intensity = shader_get_uniform(shd_glowblur, "Intensity");

	shader_set(shd_glowblur);
	shader_set_uniform_f(uni_texel, texel_w, texel_h);
	shader_set_uniform_f(uni_steps, round(steps));
	shader_set_uniform_f(uni_intensity, intens);

	draw_sprite(spr, img, argument4, argument5);

	shader_reset();


}
