///@arg sprite_slider
///@arg image_slider
///@arg sprite_circle
///@arg image_circle
///@arg xmin
///@arg xmax
///@arg ymin
///@arg ymax
///@arg xcircle
///@arg ycircle
///@arg alpha
function draw_slider(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9, argument10) {

	var spr_sl = argument0, img_sl = argument1, 
	spr_cl = argument2, img_cl = argument3, 
	xmin = argument4, xmax = argument5,
	ymin = argument6, ymax = argument7,
	xc = argument8, yc = argument9,
	alpha = argument10;

	var sld_size = xmax - xmin;

	draw_gui_box(spr_sl, img_sl, xmin, ymin, xmax, ymax, c_white, alpha);
	draw_sprite_ext(spr_cl, img_cl, xc, yc, 1, 1, 0, c_white, alpha);

	var volume = abs(xmin - xc) / sld_size;
	return volume;


}
