///@arg sprite_or_surface
///@arg x
///@arg y
///@arg width
///@arg color_or_array
///@arg alpha
function draw_surface_outline(argument0, argument1, argument2, argument3, argument4, argument5) {

	var surf = argument0,
	xx = argument1, yy = argument2,
	width = argument3, col = argument4, alpha = argument5;

	var u_color = shader_get_uniform(shd_outline, "Color"),
	u_texel = shader_get_uniform(shd_outline, "Texel"),
	u_width = shader_get_uniform(shd_outline, "Width");

	var tex = 0;
	tex = surface_get_texture(surf); 
	
	var tw = texture_get_texel_width(tex), th = texture_get_texel_height(tex);

	shader_set(shd_outline);

	if (is_array(col)) {
		shader_set_uniform_f(u_color, col[0], col[1], col[2], alpha);
	} else {
		shader_set_uniform_f(u_color, color_get_red(col) / 255, color_get_green(col) / 255, color_get_blue(col) / 255, alpha);
	}

	shader_set_uniform_f(u_texel, tw, th);
	shader_set_uniform_f(u_width, width);

	draw_surface(surf, xx, yy);

	shader_reset();


}
