function part_smoke_create() {
	part_smoke_particle = part_type_create();
	part_type_shape(part_smoke_particle, pt_shape_smoke);
	part_type_size(part_smoke_particle, 0.01, 0.50, 0, 0);
	part_type_scale(part_smoke_particle, 1.25, 1.25);
	part_type_color3(part_smoke_particle, 5460819, 3158064, 1381653);
	part_type_alpha3(part_smoke_particle, 0, 0.60, 0);
	part_type_gravity(part_smoke_particle, 0.30, 90);
	part_type_blend(part_smoke_particle, 0);
	part_type_life(part_smoke_particle, 15, 37);

	part_smoke_emitter = part_emitter_create(global.particle_system);


}
