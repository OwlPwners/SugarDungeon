///@arg x
///@arg y
///@arg radius
///@arg colour
///@arg alpha
///@arg surf_x
///@arg surf_y
///@arg surface
function draw_circle_shader(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {

	var xx = argument0, yy = argument1, rad = argument2,
	col = argument3, alpha = argument4;

	var u_vec = shader_get_uniform(shd_circle, "CirclePos"), 
	u_rad = shader_get_uniform(shd_circle, "CircleRadius"),
	u_col = shader_get_uniform(shd_circle, "CircleCol"), 
	u_alpha = shader_get_uniform(shd_circle, "AlphaStart");

	shader_set(shd_circle);

	shader_set_uniform_f(u_vec, xx, yy);
	shader_set_uniform_f(u_rad, rad);
	shader_set_uniform_f(u_col, color_get_red(col), color_get_green(col), color_get_blue(col));
	shader_set_uniform_f(u_alpha, alpha);

	draw_surface(argument7, argument5, argument6);

	shader_reset();


}
