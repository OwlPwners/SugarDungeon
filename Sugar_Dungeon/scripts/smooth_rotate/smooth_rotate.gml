///@arg angle_base
///@arg angle_target
///@arg spd
function smooth_rotate(argument0, argument1, argument2) {

	var ang_base = argument0, ang_to = argument1, spd = argument2, val;
	val = sin(degtorad(ang_to - ang_base)) * spd;

	return val;


}
