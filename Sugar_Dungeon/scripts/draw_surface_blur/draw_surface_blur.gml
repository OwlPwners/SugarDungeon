///@arg id
///@arg blur_quality
///@arg blur_amlifier
///@arg x 
///@arg y 
function draw_surface_blur(argument0, argument1, argument2, argument3, argument4) {

	var sprite = argument0, guality = argument1, amplifier = argument2,
	_x = argument3, _y = argument4;

	var i = 0, angle = 0;
	repeat(guality) {
		angle += 360 / guality;
	
		draw_set_alpha(1 / i);
	
		var xx = lengthdir_x(i * amplifier, angle) - lengthdir_x((i - 1) * amplifier, angle), yy = lengthdir_y(i * amplifier, angle) - lengthdir_y((i - 1) * amplifier, angle);
		draw_surface(sprite, _x + xx, _y + yy);
	
		draw_set_alpha(1);
	
		i ++;
	}


}
