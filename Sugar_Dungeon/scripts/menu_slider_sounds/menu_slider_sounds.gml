function menu_slider_sounds() {
	var slider_x1 = x - width / 2,
	slider_x2 = x + width / 2,
	slider_y1 = y - height / 2,
	slider_y2 = y + height / 2;

	var gain = draw_slider(sprite_rectangle, 0, sprite_button, 0, slider_x1, slider_x2, slider_y1, slider_y2, volume, y, alpha);
	audio_group_set_gain_all_sound(gain);

	ini_save(file_settings, "Real", "SoundsAndMusic", "SoundSlider", volume);
	ini_save(file_settings, "Real", "SoundsAndMusic", "SoundVolume", gain);


}
