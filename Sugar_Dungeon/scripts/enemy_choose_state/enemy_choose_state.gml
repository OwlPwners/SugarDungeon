function enemy_choose_state() {
	if ((player.war_slot < player.war_slot_max) && !fight_with_player && seen) {
		player.war_slot += fight_weight;
		player.war_slot = min(player.war_slot, player.war_slot_max);
				
		fight_with_player = true
	}
	
	var is_fighting = state_controller.check_fight;
	if (is_fighting()) {
		if (state != "attack" && state != "glide") {
			i_can_healing = true;

			if (seen) {
				state = "run";
				
				scale_to = sign(player.x - x + 0.1);
			} else {
				state = "stop";
			}
			
			var need_to_set_attack_state = state_controller.check_attack;
			if (need_to_set_attack_state()) {
				var scr = state_controller.set_attack;
				scr();
			}
		}
		
		var need_to_set_glide_state = state_controller.check_glide;
		if (need_to_set_glide_state()) {
			var scr = state_controller.set_glide;
			scr();
		}
	} else {
		var need_to_set_imitation_state = state_controller.check_imitation;
		if (need_to_set_imitation_state()) {
			var scr = state_controller.set_imitation;
			scr();
		}
		
		var need_to_set_healingother_state = state_controller.check_healing_other;
		if (need_to_set_healingother_state()) {
			var scr = state_controller.set_healing_other;
			scr();
		}
	}
}