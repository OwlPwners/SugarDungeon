///@arg filename
///@arg string
function buffer_save_string(file, str) {
	var hash = sha1_string_utf8_hmac("aS3diewD5jlfe28493CfoiafNeh878fejn29u9NUF", str);
	str += "#" + hash + "#";

	var buffer = buffer_create(string_byte_length(str) + 1, buffer_fixed, 1);
	buffer_write(buffer, buffer_string, str);
	buffer_save(buffer, file);
	buffer_delete(buffer);
}