///@arg x
///@arg y
///@arg depth
///@arg map
///@arg distx
///@arg disty
///@arg target
function weapon_create() {

	var xx = argument[0], yy = argument[1], depthh = argument[2], _map = argument[3],
	_distx = argument[4], _disty = argument[5], _target = argument[6];

	var idd = instance_create_depth(xx, yy, depthh, weapon);
	with(idd) {
		target = _target;
		map = _map;
		distx = _distx;
		disty = _disty;
	}

	return idd;


}
