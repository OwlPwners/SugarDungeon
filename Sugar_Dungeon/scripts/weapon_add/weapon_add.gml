///@arg item_index
///@arg type
///@arg sprite_icon
///@arg sprite
///@arg sprite_attack
///@arg sprite_mask
///@arg sound
///@arg rarity
///@arg knockback
///@arg damage
///@arg price
///@arg script_begin
///@arg script_end
///@arg script_animend
///@arg set
///@arg img_min
///@arg img_max
function weapon_add() {

	var item_index = argument[0], anim;
	global.map_items[item_index] = ds_map_create();
	ds_map_add(global.map_items[item_index], "ItemIndex", item_index);
	ds_map_add(global.map_items[item_index], "Type", argument[1]);

	ds_map_add(global.map_items[item_index], "Icon", argument[2]);
	ds_map_add(global.map_items[item_index], "Sprite", argument[3]);

	var spr = argument[4];
	if (is_array(spr)) {
		var list = ds_list_create(), len = array_length(spr);
	
		var i = 0;
		repeat(len) {
			list[| i] = spr[i];
		
			i ++;
		}
	
		anim = true;
	
		ds_map_add(global.map_items[item_index], "SpriteAnimated", true);
		ds_map_add(global.map_items[item_index], "SpriteAttack", ds_list_write(list));
		ds_list_destroy(list);
	} else {
		anim = false;
	
		ds_map_add(global.map_items[item_index], "SpriteAnimated", false);
		ds_map_add(global.map_items[item_index], "SpriteAttack", spr);
	}

	var mask = argument[5];
	if (is_array(mask)) {
		var list = ds_list_create(), len = array_length(mask);
	
		var i = 0;
		repeat(len) {
			list[| i] = mask[i];
		
			i ++;
		}
	
		ds_map_add(global.map_items[item_index], "Mask", ds_list_write(list));
		ds_list_destroy(list);
	} else {
		ds_map_add(global.map_items[item_index], "Mask", mask);
	}

	var sound = argument[6];
	if (is_array(sound)) {
		var list = ds_list_create(), len = array_length(sound);
	
		var i = 0;
		repeat(len) {
			list[| i] = sound[i];
		
			i ++;
		}
	
		ds_map_add(global.map_items[item_index], "Sound", ds_list_write(list));
		ds_list_destroy(list);
	} else {
		ds_map_add(global.map_items[item_index], "Sound", sound);
	}

	ds_map_add(global.map_items[item_index], "Rarity", argument[7]);
	ds_map_add(global.map_items[item_index], "Knockback", argument[8]);
	ds_map_add(global.map_items[item_index], "Damage", argument[9]);
	ds_map_add(global.map_items[item_index], "Price", argument[10]);

	ds_map_add(global.map_items[item_index], "ScriptBegin", argument[11]);
	ds_map_add(global.map_items[item_index], "ScriptEnd", argument[12]);
	ds_map_add(global.map_items[item_index], "ScriptAnimEnd", argument[13]);

	ds_map_add(global.map_items[item_index], "Set", argument[14]);

	var args = argument_count - 1;
	if (args > 14) {
		var img_min = argument[15], min_list = ds_list_create(), i = 0;
		repeat(array_length(img_min)) {
			min_list[| i] = img_min[i];
		
			i ++;
		}
	
		var val = ds_list_write(min_list);
		ds_map_add(global.map_items[item_index], "ImageMin", val);
		ds_list_destroy(min_list);
	}

	if (args > 15) {
		var img_max = argument[16], max_list = ds_list_create(), i = 0;
		repeat(array_length(img_max)) {
			max_list[| i] = img_max[i];
		
			i ++;
		}
	
		var val = ds_list_write(max_list);
		ds_map_add(global.map_items[item_index], "ImageMax", val);
		ds_list_destroy(max_list);
	}


}
