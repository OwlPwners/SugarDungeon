///@arg x
///@arg y
///@arg text
///@arg scale_x
///@arg scale_y
///@arg angle
///@arg color_outline
///@arg color_text

function draw_text_outline(x, y, text, scale_x, scale_y, angle, color_outline, color_text) {
	draw_set_color(color_outline);
	
	draw_text_transformed(x - scale_x, y - scale_y, text, scale_x, scale_y, angle);
	draw_text_transformed(x, y - scale_y, text, scale_x, scale_y, angle);
	draw_text_transformed(x + scale_x, y - scale_y, text, scale_y, scale_y, angle);
	draw_text_transformed(x + scale_x, y, text, scale_x, scale_y, angle);
	draw_text_transformed(x + scale_x, y + scale_y, text, scale_y, scale_y, angle);
	draw_text_transformed(x, y + scale_y, text, scale_y, scale_x, angle);
	draw_text_transformed(x - scale_x, y + scale_y, text, scale_x, scale_y, angle);
	draw_text_transformed(x - scale_x, y, text, scale_y, scale_x, angle);
	
	draw_set_color(color_text);
	
	draw_text_transformed(x, y, text, scale_x, scale_y, angle);
	
	draw_set_color(c_white);
}