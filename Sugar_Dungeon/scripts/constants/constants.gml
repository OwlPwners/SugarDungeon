function constants() {
#region Effect system.
	enum obj_effect {
		freeze,
		fire,
		jam, 
		notcold,
		icecream_lake
	}
	
	enum eff_info {
		script,
		immunity,
		dy_immunity,
		particles,
		scr_destr_part
	}

#macro spd_freeze 0.75
#macro img_spd_freeze 0.75

#macro spd_jam 0.67
#macro img_spd_jam 0.6

#macro spd_icecream_lake 0.5
#macro img_spd_icecream_lake 0.5

#endregion

#region Other.
#macro gui_width 640
#macro gui_height 360

#macro cam_dborder_w 32
#macro cam_dborder_h 18

#macro tile_size 64
#macro chunk_size 640

#macro flash_minus 0.05

#macro chapter_diving 3
#macro chapter_fragament floor((chapter / chapter_diving) - 0.1)
#endregion

#region Gamepad, keyboards key and likes.

#macro key_left_load ini_load(file_settings, "Real", "Control", "LeftKey", ord("A"))
#macro key_right_load ini_load(file_settings, "Real", "Control", "RightKey", ord("D"))
#macro key_up_load ini_load(file_settings, "Real", "Control", "UpKey", ord("W"))
#macro key_down_load ini_load(file_settings, "Real", "Control", "DownKey", ord("S"))
#macro key_glide_load ini_load(file_settings, "Real", "Control", "GlideKey", vk_space)
#macro key_interact_load ini_load(file_settings, "Real", "Control", "InteractKey", vk_shift)

#macro gp_deadzone 0.3

#endregion

#region Generation.

#macro wall 1
#macro _floor 0

#endregion

#region Menu. 

#macro menu_text_border_height 16
#macro menu_text_border_width 10

#macro menu_ui_border_w 64
#macro menu_ui_border_h 36
#macro menu_ui_size_aspect (gui_width - menu_ui_border_w * 2) / gui_width

#endregion

#region Scripts.
#macro tilepoint_index 0
#macro tilepoint_x 1
#macro tilepoint_y 2
#endregion

#region Save/Load system.

#macro save_object_name "name"
#macro save_object_layer "layer"
#macro save_object_is_merchant_array "merchantarray"
#macro save_object_is_stunned_enemy "enemystunned"

#endregion

#region Colors and alphas;
#macro light_alpha 0.2 //0.57
#macro light_colour_bg $666666

#macro light_colour_orange $4C94FF
#macro light_alpha_orange 0.887

#macro light_colour_blue $F4C092
#macro light_alpha_blue 0.92

#macro freeze_colour $E5E5A0
#macro notcold_colour $89BAE5

#macro player_hp_color $5A3CB2

#macro live_potion_color $3D3DE2

#macro mimic_color [1, 180 / 255, 180 / 255]

#macro jam_reflection_alpha 0.524
#macro jam_blood_red_color $3824BA

#macro merchant_color_cantbuy $3D3DCC

#macro text_addlive_color $4921ED
#macro text_currecny_color $0DADE8
#endregion

#region Files.
#macro file_settings "Settings.ini"
#macro file_translate_menu "TranslateMenu.ini"
#macro file_translate_merchant "TranslateMerchant.ini"
#macro file_room "GeneratedRoom.sav"
#macro file_rooms_list "GeneratedRoomTypes.sav"
#macro file_chapter "RoomChapter.sav"
#macro file_translate_cutscene_start "TranslateCutsceneStart.ini"
#macro file_translate_gamepad "TranslateGamepadInfo.ini"
#macro file_translate_gamestart "TranslateGameStart.ini"
#macro file_statistic_lasttime "StatisticLastTime.sav"
#macro file_statistic "Statistic.sav"
#macro file_translate_statistic "TranslateStatistic.ini"
#macro file_ui_coords "UI_Coords.ini"
#endregion

#region Slots.
#macro slot_width sprite_get_width(spr_slot)
#macro slot_height sprite_get_width(spr_slot)
#macro slot_between 12
#endregion

#region Items. 
	enum item {
		potion_smallives,
		potion_biglives,
	
		plainsword,
		plainspear,
		plainshield,
	
		candysword,
		candyspear,
		candyscythe,
		lollipopshield,
	
		gbreadsword,
		gbreadspear,
		gbreadman,
		gbreadshield,
	
		icecream_sword
	}

	enum item_set {
		plain,
		lollipop,
		gbread,
		icecream
	}
#endregion

#region Rooms.

#region Types.
	enum room_type {
		end_,
		ordinary1,
		loot1,
		miniboss1,
		boss1,
		ordinary2,
		loot2,
		miniboss2,
		boss2
	
	}
#endregion

#region Ordinary.
	enum room_vers_ordin1 {
		// First floor.
		smile,
		browed,
		tnt_,
		fire_cake_and_tnt,
		big_cookie
	}

	enum room_vers_ordin2 {
		// Second floor.
		badsmile,
		ice_cake_and_icecream
	}
#endregion

#region Loot.
	enum room_vers_loot1 {
		// First floor.
		chestwithrhombus,
		merchant_,
		chest_in_secret_region,
		soda_machine_
	}

	enum room_vers_loot2 {
		// Second floor.
		mimic_with_icecakes,
		icecake_ball_white_with_chest
	}
#endregion

#region Minibosses.
	enum room_vers_miniboss1 {
		// First floor.
		minichoco,
		arenawithsugars,
		bigjams,
		five_choco_and_sugar
	}

	enum room_vers_miniboss2 {
		// Second floor.
		many_icecreams,
		many_icecream_balls
	}
#endregion

#region Bosses. 

	enum room_vers_boss1 {
		soda
	}
	
	enum room_vers_boss2 {
		icecream
	}

#endregion

#endregion

#region Layers.

#region Tile.
#macro layer_collision layer_tilemap_get_id("Collision")
#macro layer_shadows layer_tilemap_get_id("Shadows")
#macro layer_rooms layer_tilemap_get_id("Rooms")
#macro layer_lm_fixgrid layer_tilemap_get_id("FixGrid")
#endregion

#region Backgrounds.
#macro layer_bg_floor layer_background_get_id("BackgroundFloor")
#endregion

#region Instance.
#macro layer_controllers layer_get_id("Controllers")
#macro layer_enemies layer_get_id("Enemies")
#macro layer_light layer_get_id("Light")
#macro layer_other layer_get_id("Other")
#endregion

#region Asset. 
#macro layer_decor_floor0 layer_get_id("DecorFloor0")
#endregion

#endregion
}