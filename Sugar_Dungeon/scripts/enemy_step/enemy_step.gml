function enemy_step() {
	if (!layer_changed) {
		layer = layer_enemies;
		layer_changed = true;
	}
	
	if (!appearance) {
		if (!instance_exists(player)) {
			appearance = true;
			visible = true;
		} else {
			if (player.clamp_box_id == clamp_box_id) {
				instance_create_depth(x, bbox_bottom, depth - 1, enemy_appearance);
				visible = true;
		
				appearance = true;
			}
		}
	}

	if (enemy_check_mode()) {
		image_speed = 1;
	
		set_depth();

		if (hp <= 0 || !instance_exists(player)) {
			stunned = true;
		}
	
		part_step_timer ++;
	
		flash -= flash_minus;
		glide_timer --;
		war_timer --;
		timer_search_wounded --;

		if (glide_spd <= 0 && state == "glide") {
			state = "stop";
			image_speed = 1;
		}
		
		var dist = distance_to_object(player);
		if (dist > see_dist) {
			seen = false; 
			
			if (fight_with_player) {
				player.war_slot -= fight_weight;
				player.war_slot = max(player.war_slot, 0);
			}
				
			fight_with_player = false;
		} else {
			if (!seen) seen = !tile_collision_line(x, y, player.x, player.y, layer_collision, tile_size / 2);
		}

		if (!stunned) {
			var scr = state_controller.choosing_process; //state_choosing_script();
			scr();
		
			image_xscale += (scale_to - image_xscale) / 3;
			
			var state_script = state_map[? state];
			state_script();
		
			if (!stunned) {
				mask_index = hitbox;

				bounce_process();
				enemy_collision();
			}
		
			mask_index = sprite_stop;
	
			} else {
			sprite_index = sprite_stop;
			mask_index = hitbox;
	
			if (!stunned_prev) {
				if (enemy_wounded_id != -1 && instance_exists(enemy_wounded_id)) enemy_wounded_id.healing = false;
			
				if (instance_exists(clamp_box_id)) clamp_box_id.enemy_num_lives --;
				
				if (fight_with_player) {
					player.war_slot -= fight_weight;
					player.war_slot = max(player.war_slot, 0);
				}
				
				fight_with_player = false;
			
				hsp = 0;
				vsp = jumpf;
			
				image_xscale = sign(player.x - x + 0.01);
			}
		
			vsp += grv;
			stunned_y += vsp;
		
			stunned_y = min(0, stunned_y);
		
			if (stunned_y == 0) {
				if (stunned_once) {
					stunned_timer -= 1.25;
				} else {
					stunned_timer --;
				}
			}
	
			if (stunned_timer <= 0) image_alpha -= 0.07;
		
			var ang = 90, ang_spd = 7 * image_xscale;
			image_angle += smooth_rotate(image_angle, ang, ang_spd);
		
			//image_angle += (90 - image_angle) / 15;
	
			if (image_alpha <= 0) {
				//player.money += money;
			
			#region Creating currency. 
			
				// I know, then I can write that system with arrays, but I don't want waste time on that. 
				var bag_c, bag
				bag_c = 10;
				bag = money div bag_c; money -= bag * bag_c;
			
				repeat(bag) {
					var bag_id = instance_create_depth(x, y, depth - 1, currency);
					with(bag_id) {
						image_index = 1;
					
						cost = bag_c;
					}
				}
			
				repeat(money) {
					var coin_id = instance_create_depth(x, y, depth - 1, currency);
					with(coin_id) {
						image_index = 0;
					
						cost = 1;
					}
				}
			#endregion
			
				player.hp += min(damage / 20, 2);
			
				statistic_manager.map[? "Kills"] ++;
			
				if (path_exists(path)) path_delete(path);
				part_step_destroy();
				
				ds_map_destroy(state_map);
				
				instance_destroy(self);
			}
		}

		state_prev = state;
		stunned_prev = stunned;
	} else image_speed = 0;


















	/*var elips_dist = 11;

	if (enemy_check_mode()) {
		image_speed = 1;
	
		set_depth();

		if (hp <= 0 || instance_exists(player) == false) {
			stunned = true;
		}

		flash -= flash_minus;
		glide_timer --;
		war_timer --;
		timer_search_wounded --;

		if (glide_spd <= 0 && state == "glide") {
			state = "stop";
			image_speed = 1;
		}

		if (stunned == false) {
			if (state != "attack" && state != "glide" && state != "healing other" 
			&& state != "run to wounded" && state != "comes to life") {
				i_can_healing = true;
		
				var dist = distance_to_object(player);
				if (dist > see_dist) {
					seen = false; 
				} else {
					if (seen == false) seen = (tile_collision_line(x, y, player.x, player.y, layer_collision, tile_size / 2) == false);
				}

				if (seen == true) state = "run"; else state = "stop";
	
				if (place_meeting(x, y, player)) {
					image_index = 0;
	
					state = "attack";
				}
		
				if (i_can_healing && timer_search_wounded <= 0 && hp > 0) {
					timer_search_wounded = time_search_wounded;
					enemy_wounded_id = enemy_search_wounded();
			
					if (enemy_wounded_id != -1 && instance_exists(enemy_wounded_id) && enemy_wounded_id.healing == false) {
						state = "run to wounded";
				
						i_can_healing = false;
					}
				}
			}
	
			if (glide_timer <= 0 && player.state == player_state_attack
			&& state != "glide" && player.image_index < 1.5 && player.image_xscale == -image_xscale 
			&& tile_collision_line(x, y, player.x, player.y, layer_collision, tile_size / 2) == false) {
				var xdir, ydir;
				xdir = player.x - x;
				ydir = player.y - y;
		
				if (collision_ellipse(bbox_left - elips_dist, bbox_top + elips_dist * 2, bbox_right + elips_dist, bbox_bottom - elips_dist * 2, player, false, true)) {
					image_index = 0;
					mask_index = -1;
			
					glide_spd = glide_force;
					x_glide_dir = xdir;
					y_glide_dir = ydir;
		
					var len = sqrt(x_glide_dir * x_glide_dir + y_glide_dir * y_glide_dir);
					if (len != 0) {
						x_glide_dir /= len;
						y_glide_dir /= len;
					}
		
					if (x_glide_dir != 0 || y_glide_dir != 0) state = "glide";
				}
			}
		
			if (state != "healing other") {
				if (enemy_wounded_id != -1 && instance_exists(enemy_wounded_id)) {
					enemy_wounded_id.healing = false;
					enemy_wounded_id = -1;
				}
			}

			switch(state) {
				case("stop") :
				enemy_stop();
				break;
		
				case("run") :
				var player_behined_walls = tile_collision_line(x, y, player.x, player.y, layer_collision, 5),
				astar_enable;
		
				if (player_behined_walls) {
					astar_enable = true;
				} else {
					astar_enable = false;
				}
		
				enemy_run(player, astar_enable);
				break;
		
				case("attack") :
				script_execute(script_attack);
				break;
		
				case("glide") :
				enemy_glide();
				break;
		
				case("run to wounded") :
				if (!instance_exists(enemy_wounded_id)) {
					state = "stop";
					break;
				}
			
				var player_behined_walls = tile_collision_line(x, y, player.x, player.y, layer_collision, 5),
				wounded_behined_walls = tile_collision_line(x, y, enemy_wounded_id.x, enemy_wounded_id.y, layer_collision, 5);
		
				if (player_behined_walls && wounded_behined_walls == false) {
					if (enemy_wounded_id.healing == false) {
						enemy_run(enemy_wounded_id, false);
					} else {
						state = "stop";
					}
				} else {
					state = "run";
				}
		
				if (place_meeting(x, y, enemy_wounded_id)) {
					state = "healing other";
				}
				break;
		
				case("healing other") :
				enemy_healing_other();
				break;
		
				case("comes to life") :
				enemy_comes_to_life();
				break;
			}

			mask_index = hitbox;

			bounce_process();
			enemy_collision();
		
			mask_index = sprite_stop;
	
			} else {
			sprite_index = sprite_stop;
			mask_index = -1;
	
			if (stunned_prev == false) {
				if (enemy_wounded_id != -1 && instance_exists(enemy_wounded_id)) enemy_wounded_id.healing = false;
		
				hsp = 0;
				vsp = jumpf;
				player.money += money;
		
				stunned_y_max = y;
			}
	
			vsp += grv;
			y += vsp;
	
			y = min(y, stunned_y_max);
	
			if (healing == false && (abs(y - stunned_y_max) < 5)) stunned_timer -= 1.25;
	
			if (stunned_timer <= 0) image_alpha -= 0.07;
			image_angle += (90 - image_angle) / 15;
	
			if (image_alpha <= 0) {
				if (path_exists(path)) path_delete(path);
				instance_destroy(self);
			}
		}

		state_prev = state;
		stunned_prev = stunned;
	} else image_speed = 0;

/* end enemy_step */
}