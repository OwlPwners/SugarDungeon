///@arg x
///@arg y
///@arg number
function part_bubble_burst(argument0, argument1, argument2) {

	var xx = argument0, yy = argument1, num = argument2;

	part_emitter_region(global.particle_system, part_bubble_emitter, xx, xx, yy, yy, ps_shape_rectangle, 1);
	part_emitter_burst(global.particle_system, part_bubble_emitter, part_bubble_particle, num);


}
