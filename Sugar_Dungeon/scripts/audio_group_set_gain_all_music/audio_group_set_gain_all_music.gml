///@arg value
function audio_group_set_gain_all_music(argument0) {

	audio_group_set_gain(agp_menu_music, argument0, 0);
	audio_group_set_gain(agp_floor_music, argument0, 0);
	audio_group_set_gain(agp_cutscene_start_music, argument0, 0);


}
