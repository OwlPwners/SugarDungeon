///@arg sprite
///@arg image
///@arg x
///@arg y
///@arg xscale
///@arg yscale
///@arg color
///@arg alpha
function draw_sprite_blend_ext(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) {

	var sprite = argument0, image = argument1,
	xx = argument2, yy = argument3, 
	xscale = argument4, yscale = argument5,
	color = argument6, alpha = argument7;
	shader_set(shd_blend);

	var u_col, red, green, blue;
	u_col = shader_get_uniform(shd_blend, "Color");
	red = color_get_red(color) / 255;
	green = color_get_green(color) / 255;
	blue = color_get_blue(color) / 255;

	shader_set_uniform_f(u_col, red, green, blue, alpha);

	draw_sprite_ext(sprite, image, xx, yy, xscale, yscale, 0, c_white, 1);

	shader_reset();


}
