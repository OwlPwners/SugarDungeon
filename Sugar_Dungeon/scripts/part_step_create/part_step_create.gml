function part_step_create() {
	part_step_particle = part_type_create();
	//part_type_shape(part_step_particle, pt_shape_explosion);
	part_type_size(part_step_particle, 0.10, 0.50, 0.01, 0.01);
	part_type_sprite(part_step_particle, spr_part_step, true, true, false);
	part_type_scale(part_step_particle, 1, 1);
	part_type_color2(part_step_particle, 16777215, 12632256);
	part_type_alpha2(part_step_particle, 1, 0);
	part_type_speed(part_step_particle, 0.5, 1.2, -0.03, 0);
	part_type_direction(part_step_particle, 0, 359, 0, 0);
	part_type_gravity(part_step_particle, 0, 270);
	part_type_orientation(part_step_particle, 0, 0, 0, 0, 1);
	part_type_blend(part_step_particle, 1);
	part_type_life(part_step_particle, 30, 60);

	part_step_emitter = part_emitter_create(global.particle_system);
}