///@arg sprite
///@arg hp
///@arg hp_max

function boss_ui_draw_healthbar(sprite, hp, hp_max) {
	var aspect = hp / hp_max, 
	x1 = gui_width / 2 - gui_width / 4 * aspect, x2 = gui_width / 2 + gui_width / 4 * aspect, 
	y1 = gui_height * 0.9 - gui_height * 0.03, y2 = gui_height * 0.9;
	
	draw_gui_box(sprite, 0, x1, y1, x2, y2, c_white, 1);
}