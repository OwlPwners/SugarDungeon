function flash_check() {
	if (flash > 0) {
		var u_alpha;
		u_alpha = shader_get_uniform(shd_flash, "Alpha");

		shader_set(shd_flash);
		shader_set_uniform_f(u_alpha, flash);
	
		draw_self();
	
		shader_reset();
	}


}
