///@arg id
///@arg blur_quality
///@arg blur_amlifier
///@arg x 
///@arg y 
function draw_sprite_blur(argument0, argument1, argument2, argument3, argument4, argument5) {

	var sprite = argument0, index = argument1, guality = argument2, amplifier = argument3,
	_x = argument4, _y = argument5;

	var i = 0, angle = 0;
	repeat(guality) {
		angle += 360 / guality;
	
		draw_set_alpha(1 / i);
	
		var xx = lengthdir_x(i * amplifier, angle) - lengthdir_x((i - 1) * amplifier, angle), yy = lengthdir_y(i * amplifier, angle) - lengthdir_y((i - 1) * amplifier, angle);
		draw_sprite(sprite, index, _x + xx, _y + yy);
	
		draw_set_alpha(1);
	
		i ++;
	}


}
