///@arg array
///@arg map_index
function array_find_map_index(argument0, argument1) {

	var array = argument0, size = array_length(array), map = ds_map_write(argument1), i = 0;
	repeat(size) {
		var check_map = ds_map_write(array[i]);
		if (check_map == map) {
			break;
		}
	
		i ++;
	}

	return i;


}
