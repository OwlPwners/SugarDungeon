function menu_button_newgame() {
	//if (file_exists(file_seed)) file_delete(file_seed);
	if (file_exists(file_room)) file_delete(file_room);
	if (file_exists(file_chapter)) file_delete(file_chapter);
	//if (file_exists(file_statistic_lasttime)) file_delete(file_statistic_lasttime);
	
	with(statistic_manager) {
		save_data_all();
		init_default_keys();
	}
	
	audio_stop_all();

	//if (audio_group_is_loaded(agp_menu_music)) audio_group_unload(agp_menu_music);

	with(room_loader) {
		rm_goto = rm_cutscene_start;
		transition = room_transition.def;
	}


}
