function enemy_healing_other() {
	sprite_index = sprite_stop;

	hsp = 0;
	vsp = 0;

	if (!instance_exists(enemy_wounded_id) || enemy_wounded_id.can_be_healed == false) {
		state = "stop";
	} else {
		enemy_wounded_id.healing = true;

		timer_healing --;
		if (timer_healing <= 0) {
			with(enemy_wounded_id) {
				hp = hp_max * 0.75;
		
				stunned = false;
				stunned_once = true;
				healing = false;
		
				state = "comes to life";
			}
	
			timer_healing = time_healing;
			i_can_healing = true;
			state = "stop";
			enemy_wounded_id = -1;
		}
	}


}
