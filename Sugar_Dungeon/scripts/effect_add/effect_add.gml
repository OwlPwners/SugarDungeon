///@arg index
///@arg scr_process
///@arg scr_part_create
///@arg scr_part_destroy
function effect_add(argument0, argument1, argument2, argument3) {

	var index = argument0, proc = argument1, part_create = argument2, part_destroy = argument3;
	eff_array[index, eff_info.script] = proc;
	eff_array[index, eff_info.particles] = part_create;
	eff_array[index, eff_info.scr_destr_part] = part_destroy;
	eff_array[index, eff_info.dy_immunity] = false;
	if (part_create > -1) script_execute(part_create);
}