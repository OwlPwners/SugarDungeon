function enemy_choose_state_component_setglidestate() {
	var xdir, ydir;
	xdir = player.x - x;
	ydir = player.y - y;
		
	image_index = 0;
	mask_index = -1;
			
	glide_spd = glide_force;
	x_glide_dir = xdir;
	y_glide_dir = ydir;
		
	var len = sqrt(x_glide_dir * x_glide_dir + y_glide_dir * y_glide_dir);
	if (len != 0) {
		x_glide_dir /= len;
		y_glide_dir /= len;
	}
		
	if (x_glide_dir != 0 || y_glide_dir != 0) state = "glide";
}