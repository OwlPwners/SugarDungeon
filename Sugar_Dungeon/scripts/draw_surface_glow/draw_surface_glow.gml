///@arg surface
///@arg steps_blur
///@arg intensity
///@arg x
///@arg y
function draw_surface_glow(argument0, argument1, argument2, argument3, argument4) {

	var surf = argument0, steps = argument1, intens = argument2;

	var tex = surface_get_texture(surf),
	texel_w = texture_get_texel_width(tex),
	texel_h = texture_get_texel_height(tex);

	var uni_texel = shader_get_uniform(shd_glowblur, "Texel"),
	uni_steps = shader_get_uniform(shd_glowblur, "Steps"),
	uni_intensity = shader_get_uniform(shd_glowblur, "Intensity");

	shader_set(shd_glowblur);
	shader_set_uniform_f(uni_texel, texel_w, texel_h);
	shader_set_uniform_f(uni_steps, round(steps));
	shader_set_uniform_f(uni_intensity, intens);

	draw_surface(surf, argument3, argument4);

	shader_reset();


}
