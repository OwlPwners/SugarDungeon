///@arg file_name
///@arg type
///@arg selection
///@arg key
///@arg value
function ini_save(argument0, argument1, argument2, argument3, argument4) {

	var file = argument0, type = argument1,
	selection = argument2, key = argument3,
	value = argument4;

	ini_open(file);

	switch(type) {
		case("String") :
		ini_write_string(selection, key, value);
		break;
	
		case("Real") :
		ini_write_real(selection, key, value);
		break;
	}

	ini_close();


}
