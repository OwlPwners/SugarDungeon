function effect_process_notcold() {
	timer_not_cold --;
	if (timer_not_cold > 0) {
		image_blend = notcold_colour;
	
		part_not_cold_burst(x, y, irandom(1));
	
		var val = effect_instance_find_effect(self, obj_effect.freeze);
		if (val != -1) {
			effect_delete(val);
		}
	} else {
		effect_delete(obj_effect.notcold);
	}


}
