///@arg layer
///@arg x
///@arg y
///@arg point_arrays
function tile_meeting_at_points() {

	var _layer = argument[0];

	var xprev, yprev;
	xprev = x;
	yprev = y;

	x = argument[1];
	y = argument[2];

	var tile = 0;
	for(var i = 3; i < argument_count; i ++) {
		var point = argument[i];
		tile = tile || tilemap_get_at_pixel(_layer, point[0], point[1]);
	}

	x = xprev;
	y = yprev;

	return tile;


}
