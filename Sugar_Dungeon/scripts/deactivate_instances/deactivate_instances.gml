///@arg x1
///@arg y1
///@arg x2
///@arg y2

function deactivate_instances(x1, y1, x2, y2) {
	var list_chunks = ds_list_create();
	collision_rectangle_list(x1, y1, x2, y2, chunk_controller, false, true, list_chunks, false);
	
	for(var i = 0; i < ds_list_size(list_chunks); i ++) {
		var idd = list_chunks[| i];
		with(idd) {
			deactivate();
		}
	}
	
	ds_list_destroy(list_chunks);
	
	//var xch, ych;
	//xch = xx div chunk_size;
	//ych = yy div chunk_size;
	
	//if (global.deactivate_grid[# xch, ych] == undefined) {
	//	var x1, y1, x2, y2;
	//	x1 = xch * chunk_size;
	//	y1 = ych * chunk_size;
	//	x2 = x1 + chunk_size;
	//	y2 = y1 + chunk_size;
	
	//	var list_inside = ds_list_create(), list_chunk = ds_list_create();
	//	collision_rectangle_list(x1, y2, x2, y2, i_must_save, false, true, list_inside, false);
			
	//	var list_size = ds_list_size(list_inside), id_index = 0;
	//	repeat(list_size) {
	//		var idd = list_inside[| id_index];
	//		ds_list_add(list_chunk, [idd.id, idd.x, idd.y, idd.object_index]);
	//		instance_deactivate_object(idd);
				
	//		id_index ++;
	//	}
			
	//	global.deactivate_grid[# xch, ych] = list_chunk;
		
	//	show_message("deactivated");
	//}
	
	//with(i_must_save) {
	//	var inside = get_inside(x, y);
	//	if (!inside) {
	//		ds_list_add(global.deactivate_list, [id, x, y, object_index]);
	//		instance_deactivate_object(id);
	//	}
	//}
}