function enemy_imitation_standart() {
	var point_behined_walls = tile_collision_line(x, y, player.x, player.y, layer_collision, 5),
	astar_enable;
		
	if (point_behined_walls) {
		astar_enable = true;
	} else {
		astar_enable = false;
	}
		
	enemy_run(player.x, player.y, astar_enable);
	
	scale_to = sign(hsp + 0.01);
}