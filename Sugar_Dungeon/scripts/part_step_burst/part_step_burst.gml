///@arg number
function part_step_burst(argument0) {

	var num = argument0;

	part_emitter_region(global.particle_system, part_step_emitter, bbox_left, bbox_right, bbox_bottom, bbox_bottom - 3, ps_shape_rectangle, 1);
	part_emitter_burst(global.particle_system, part_step_emitter, part_step_particle, num);


}
