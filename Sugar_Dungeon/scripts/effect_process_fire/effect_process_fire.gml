function effect_process_fire() {
	timer_fire --;
	if (timer_fire > 0) {
		if (timer_fire mod timer_fire_div == 0) hp -= hp_fire;
	
		part_fire_burst(x, y, irandom(1));
	} else {
		effect_delete(obj_effect.fire);
	}


}
