function wps_jerk() {
	if (state == "attack" && set_new_combo) {
		var gld, dir, dx, dy;
		gld = 1.3;
		dir = point_direction(target.x, target.y, mouse_x, mouse_y);
		dx = lengthdir_x(1, dir);
		dy = lengthdir_y(1, dir);
	
		bounce_set(dx, dy, 0, 0, gld / target.bounce_fric, target);
	}


}
