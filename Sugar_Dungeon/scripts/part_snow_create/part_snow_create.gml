function part_snow_create() {
	part_snow_particle = part_type_create();
	part_type_shape(part_snow_particle, pt_shape_snow);
	part_type_size(part_snow_particle, 0.1, 0.2, 0, 0);
	part_type_scale(part_snow_particle, 1, 1);
	part_type_color3(part_snow_particle, 16764874, 16710863, 16777215);
	part_type_alpha3(part_snow_particle, 0, 0.75, 0);
	part_type_speed(part_snow_particle, 0.5, 1, 0, 0);
	part_type_direction(part_snow_particle, 0, 359, 0, 0);
	part_type_orientation(part_snow_particle, 0, 0, 0, 0, 1);
	part_type_blend(part_snow_particle, 0);
	part_type_life(part_snow_particle, 15, 45);

	part_snow_emitter = part_emitter_create(global.particle_system);
}
