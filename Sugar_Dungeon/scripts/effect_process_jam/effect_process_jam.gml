function effect_process_jam() {
	timer_jam --;
	if (timer_jam > 0) {
		img_spd_factor *= img_spd_jam;
	
		var h, x1, y1, x2, y2;
		h = 5;
	
		x1 = x - sprite_get_xoffset(sprite_index);
		y1 = bbox_bottom;
		x2 = x1 + abs(sprite_width);
		y2 = y1 - h;
	
		part_jam_burst(x1, y1, x2, y2, choose(0, 0, 0, 2));
	
		spd_factor *= spd_jam;
	} else {
		effect_delete(obj_effect.jam);
	}


}
