///@arg instance
///@arg time
///@arg timer_fire_div
///@arg hp_minus
function effect_add_icecream_lake(argument0, argument1) {

	var inst = argument0, time = argument1, absent = effect_instance_find_effect(inst, obj_effect.icecream_lake) == -1;
	if (absent) {
		ds_list_add(inst.eff_list, obj_effect.icecream_lake);
	
		inst.timer_icecream_lake = time;
	}


}
