function enemy_search_wounded() {
	var enemy_list = ds_list_create(),
	enemy_num = collision_circle_list(x, y, 300, enemy, false, true, enemy_list, true),
	enemy_id_out = -1;

	if (i_can_healing) {
		var i = 0;
		repeat(enemy_num) {
			var enemy_id = enemy_list[| i];
			if (enemy_id.can_be_healed && enemy_id.healing == false && enemy_id.stunned && enemy_id.stunned_once = false) {
				enemy_id_out = enemy_id;
		
				break;
			}
	
			i ++;
		}
	}

	ds_list_destroy(enemy_list);

	return enemy_id_out;


}
