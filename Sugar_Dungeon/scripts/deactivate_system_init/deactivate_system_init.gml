function deactivate_system_init() {
	var w, h;
	w = room_width div chunk_size;
	h = room_height div chunk_size;
	//global.deactivate_grid = ds_grid_create(w, h);
	
	for(var i = 0; i < w; i ++) {
		for(var j = 0; j < h; j ++) {
			//var x1, y1, x2, y2;
			//x1 = i * chunk_size;
			//y1 = j * chunk_size;
			//x2 = x1 + chunk_size;
			//y2 = y1 + chunk_size;
			
			//var list_inside = ds_list_create(), list_chunk = ds_list_create();
			//collision_rectangle_list(x1, y1, x2, y2, i_must_save, false, true, list_inside, false);
			
			//var list_size = ds_list_size(list_inside), id_index = 0;
			//repeat(list_size) {
			//	var idd = list_inside[| id_index];
			//	ds_list_add(list_chunk, [idd.id, idd.x, idd.y, idd.object_index]);
			//	instance_deactivate_object(idd);
				
			//	id_index ++;
			//}
			
			var chunk_id = instance_create_layer(i * chunk_size, j * chunk_size, layer_controllers, chunk_controller);
			//global.deactivate_grid[# i, j] = chunk_id;
			
			with(chunk_id) deactivate();
		}
	}
}