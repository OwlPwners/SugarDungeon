function enemy_comes_to_life() {
	sprite_index = sprite_stop;

	hsp = 0;
	vsp = 0;

	x_bounce_f = 0
	y_bounce_f = 0;
	bounce_force = 0;

	image_angle = 0;
	image_xscale = 1;
	image_alpha = 1;

	clamp_box_id.enemy_num_lives ++;

	state = "stop";


}
