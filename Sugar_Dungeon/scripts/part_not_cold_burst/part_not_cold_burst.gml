///@arg x
///@arg y
///@arg number
function part_not_cold_burst(argument0, argument1, argument2) {

	var xx = argument0, yy = argument1, num = argument2;

	part_emitter_region(global.particle_system, part_not_cold_emitter, xx, xx, yy, yy, ps_shape_ellipse, 1);
	part_emitter_burst(global.particle_system, part_not_cold_emitter, part_not_cold_particle, num);


}
