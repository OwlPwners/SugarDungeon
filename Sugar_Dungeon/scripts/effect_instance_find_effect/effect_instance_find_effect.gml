///@arg instance
///@arg effect
function effect_instance_find_effect(argument0, argument1) {

	var inst = argument0, effect = argument1;

	var list = inst.eff_list, val = ds_list_find_index(list, effect);

	return val;


}
