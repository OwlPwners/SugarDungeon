function part_fire_create() {
	part_fire_particles = part_type_create();
	part_type_shape(part_fire_particles, pt_shape_explosion);
	//part_type_sprite(part_fire_particles, spr_part_fire, false, true, true);
	part_type_size(part_fire_particles, 0.01, 0.40, 0, 0);
	part_type_scale(part_fire_particles, 1, 1);
	part_type_color3(part_fire_particles, 5877241, 476408, 12632256);
	part_type_alpha2(part_fire_particles, 0.70, 0);
	part_type_speed(part_fire_particles, 0.10, 2, 0, 0);
	part_type_direction(part_fire_particles, 80, 100, 0, 0);
	part_type_orientation(part_fire_particles, 0, 0, 0, 0, 1);
	part_type_blend(part_fire_particles, 1);
	part_type_life(part_fire_particles, 60, 60);

	part_fire_emitter = part_emitter_create(global.particle_system);


}
