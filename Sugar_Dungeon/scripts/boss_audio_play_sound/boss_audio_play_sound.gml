///@arg sound
function boss_audio_play_sound(sound) {
	var type = async_load[? "type"];
	if (type == "audiogroup_load") {
		audio_stop_all();
		audio_play_sound(sound, 128, true);
		global.game_music = sound;
	}
}