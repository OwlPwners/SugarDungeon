///@arg px
///@arg py
///@arg enable_astar
function enemy_run(argument0, argument1, argument2) {

	var px = argument0, py = argument1, astar = argument2;
	sprite_index = sprite_run;
	mask_index = hitbox;

	var vecx = 0, vecy = 0;

	if (astar) {
		var mp_path = mp_grid_path(global.grid_enemy_ai, path, x, y, px, py, true);
		if (mp_path) {
			if (point_distance(x, y, path_xto, path_yto) < 8) path_pos ++;
			path_xto = path_get_point_x(path, path_pos);
			path_yto = path_get_point_y(path, path_pos);
	
			vecx = path_xto - x;
			vecy = path_yto - y;
		} else {
			vecx = px - x;
			vecy = py - y;
		}
	} else {
		vecx = px - x;
		vecy = py - y;
	}
	
	if (bounce_force != 0) {
		vecx = 0;
		vecy = 0;
	}

	var len = sqrt(sqr(vecx) + sqr(vecy));
	if (len != 0) {
		var p_step;
		p_step = (part_step_timer mod 8) == 0;
		if (p_step) part_step_burst(1);
	
		vecx /= len;
		vecy /= len;
	}
	
	hsp = vecx * spd;
	vsp = vecy * spd;

	mask_index = sprite_stop;
}
