function enemy_check_mode() {
	var mode = global.game_mod;

	if (instance_exists(player) == false || player.clamp_box_id != clamp_box_id || player.clamping == false || (mode != "game" && mode != "generating")) {
		return false;
	} else return true;


}
