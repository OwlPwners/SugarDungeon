///@arg ...
function set_depth() {

	var d = -bbox_bottom;
	if (argument_count > 0) d = argument[0];

	if (y != yprev_depth) {
		depth = d;
	}

	yprev_depth = y;


}
