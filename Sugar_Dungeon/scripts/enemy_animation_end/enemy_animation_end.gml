function enemy_animation_end() {
	if (state == "attack" || state == "glide") {
		if (sprite_index != sprite_stop) {
			timer_attack = time_attack;
			sound_attack_played = false;
		}
	
		state = "stop";
	}

	image_index = 0;
	image_speed = 1;

	glide_spd = 0;
	x_glide_dir = 0;
	y_glide_dir = 0;
}