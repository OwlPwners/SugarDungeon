///@arg surface
///@arg intensity
///@arg x
///@arg y
function draw_surface_remove_color(argument0, argument1, argument2, argument3) {

	var surf = argument0, intens = argument1;

	var uni_intensity = shader_get_uniform(shd_remove_color, "Intensity");

	shader_set(shd_remove_color);
	shader_set_uniform_f(uni_intensity, intens);

	draw_surface(surf, argument2, argument3);

	shader_reset();


}
