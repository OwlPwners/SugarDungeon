function enemy_run_to_wounded() {
	if (instance_exists(enemy_wounded_id)) {	
		var player_behined_walls = tile_collision_line(x, y, player.x, player.y, layer_collision, 5),
		wounded_behined_walls = tile_collision_line(x, y, enemy_wounded_id.x, enemy_wounded_id.y, layer_collision, 5);
		
		if (player_behined_walls && wounded_behined_walls == false) {
			if (enemy_wounded_id.healing == false) {
				enemy_run(enemy_wounded_id.x, enemy_wounded_id.y, false);
			} else {
				state = "stop";
			}
		} else {
			state = "run";
		}
		
		if (place_meeting(x, y, enemy_wounded_id)) {
			state = "healing other";
		}
	} else {
		state = "stop";
	}
}