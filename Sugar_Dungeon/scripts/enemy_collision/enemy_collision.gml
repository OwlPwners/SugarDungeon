function enemy_collision() {
	var xplus = hsp + x_bounce_f, yplus = vsp + y_bounce_f;
	
	var xcheck, ycheck;
	xcheck = xplus + sign(xplus);
	ycheck = yplus + sign(yplus);
	
	if (tile_meeting(layer_collision, x + xcheck, y)) {
		repeat(abs(xcheck)) {
			if (!tile_meeting(layer_collision, x + sign(xplus), y)) {
				x += sign(xplus);
			} else {
				break;
			}
		}
		
		xplus = 0;
		
		hsp = 0;
		x_bounce_f = 0;
	}
	
	x += xplus;
	
	if (tile_meeting(layer_collision, x, y + ycheck)) {
		repeat(abs(ycheck)) {
			if (!tile_meeting(layer_collision, x, y + sign(yplus))) {
				y += sign(yplus);
			} else {
				break;
			}
		}
		
		yplus = 0;
		
		vsp = 0;
		y_bounce_f = 0;
	}
	
	y += yplus;


}
