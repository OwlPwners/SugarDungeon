///@arg obj
///@arg x1
///@arg y1
///@arg x2
///@arg y2

function instance_numer_deactivated_region(object, x1, y1, x2, y2) {
	var n = 0;
	
	with(chunk_controller) {
		for(var i = 0; i < array_length(list_chunk); i ++) {
			if (list_chunk[i][1] == object && 
			get_inside(list_chunk[i][0].x, list_chunk[i][0].y, x1, y1, x2, y2)) n ++;
		}
	}
	
	return n;
	
	//var n = 0, i = 0, size = ds_list_size(global.deactivate_list);
	//repeat(size) {
	//	var index = global.deactivate_list[|i][3],
	//	xx = global.deactivate_list[|i][1],
	//	yy = global.deactivate_list[|i][2];
		
	//	if (index == object || object_is_ancestor(index, object) && get_inside(xx, yy, x1, y1, x2, y2)) n ++;
		
	//	i ++;
	//}
	
	//return n;
}