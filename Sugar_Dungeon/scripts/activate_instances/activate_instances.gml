///@arg x1
///@arg y1
///@arg x2
///@arg y2

function activate_instances(x1, y1, x2, y2) {
	var list_chunks = ds_list_create();
	collision_rectangle_list(x1, y1, x2, y2, chunk_controller, false, true, list_chunks, false);
	
	for(var i = 0; i < ds_list_size(list_chunks); i ++) {
		var idd = list_chunks[| i];
		with(idd) {
			activate();
		}
	}
	
	ds_list_destroy(list_chunks);
	
	//var xc1, yc1, xc2, yc2;
	//xc1 = x1 div chunk_size;
	//yc1 = y1 div chunk_size;
	//xc2 = x2 div chunk_size;
	//yc2 = y2 div chunk_size;
	
	//for(var i = xc1; i < xc2; i ++) {
	//	for(var j = yc1; i < yc2; j ++) {
	//		var list = global.deactivate_grid[# i, j];
	//		if (list != undefined) {
	//			for(var id_index = 0; id_index < ds_list_size(list); id_index ++) {
	//				var idd = list[| id_index][0];
	//				instance_activate_object(idd);
	//			}
	//		}
	//	}
	//}
	
	//global.deactivate_grid[# i, j] = undefined;
	
	//var i = 0, size = ds_list_size(global.deactivate_list);
	//repeat(size) {
	//	var inst = global.deactivate_list[| i], 
	//	idd = inst[0], xx = inst[1], yy = inst[2], 
	//	inside = get_inside(xx, yy, x1, y1, x2, y2);
		
	//	if (inside) {
	//		ds_list_delete(global.deactivate_list, i --);
	//		instance_activate_object(idd);
	//	}
		
	//	i ++;
	//}
}