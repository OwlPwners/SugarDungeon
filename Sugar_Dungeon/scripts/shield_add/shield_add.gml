///@arg item_index
///@arg type
///@arg icon
///@arg sprite
///@arg block_knockback
///@arg block_hp
///@arg price
///@arg rarity
///@arg set
///@arg scr_setcombo
function shield_add() {

	var item_index = argument[0];
	global.map_items[item_index] = ds_map_create();
	ds_map_add(global.map_items[item_index], "ItemIndex", item_index);
	ds_map_add(global.map_items[item_index], "Type", argument[1]);

	ds_map_add(global.map_items[item_index], "Icon", argument[2]);
	ds_map_add(global.map_items[item_index], "Sprite", argument[3]);

	ds_map_add(global.map_items[item_index], "BlockKnockback", argument[4]);
	ds_map_add(global.map_items[item_index], "BLockHp", argument[5]);

	ds_map_add(global.map_items[item_index], "Price", argument[6]);
	ds_map_add(global.map_items[item_index], "Rarity", argument[7]);

	ds_map_add(global.map_items[item_index], "Set", argument[8]);
	ds_map_add(global.map_items[item_index], "SetScript", argument[9]);


}
