///@arg list
function ds_list_convert_array(argument0) {

	var list = argument0, size = ds_list_size(list), arr = [0], i = 0;
	repeat(size) {
		arr[i] = list[| i];
	
		i ++;
	}

	ds_list_destroy(list);

	return arr;


}
