///@arg x1
///@arg y1
///@arg x2
///@arg y2
///@arg number
function part_jam_burst(argument0, argument1, argument2, argument3, argument4) {

	var x1 = argument0, y1 = argument1, x2 = argument2, y2 = argument3, num = argument4;

	part_emitter_region(part_jam_system, part_jam_emitter, x1, x2, y1, y2, ps_shape_ellipse, ps_distr_gaussian);
	part_emitter_burst(part_jam_system, part_jam_emitter, part_jam_particles, num);


}
