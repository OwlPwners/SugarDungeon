///@arg index
///@arg key
///@arg desc
function statistic_add_info(argument0, argument1, argument2) {

	var index = argument0, key = argument1, desc = argument2;

	stats_info_array[index, 0] = key;
	stats_info_array[index, 1] = desc;
}