///@arg x
///@arg y
///@arg number
function part_snow_decor_burst(argument0, argument1, argument2) {
	var xx = argument0, yy = argument1, num = argument2;
	
	var xoff, yoff;
	xoff = gui_width / 1.5;
	yoff = gui_height / 1.5;
	
	part_emitter_region(global.particle_system, part_snow_decor_emitter, xx - xoff, xx + xoff + gui_width, yy - yoff, yy + yoff + gui_height, ps_shape_rectangle, ps_distr_linear);
	part_emitter_burst(global.particle_system, part_snow_decor_emitter, part_snow_decor_particle, num);
}