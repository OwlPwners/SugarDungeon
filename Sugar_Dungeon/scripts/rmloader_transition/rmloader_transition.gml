function rmloader_transition() {
	if (rm_goto != -1) {
		global.game_mod = "game";
	
		switch(rm_goto) {
			case(rm_loader_spechialy.restart) :
			room_restart();
			break;
		
			default :
			room_goto(rm_goto);
			break;
		}
	
		rm_goto = -1;

		state = room_transition.st_end;
	}


}
