///@arg sprite
///@arg image
///@arg x
///@arg y
///@arg color
///@arg alpha
function draw_sprite_blend(argument0, argument1, argument2, argument3, argument4, argument5) {

	var sprite = argument0, image = argument1, xx = argument2, yy = argument3, color = argument4, alpha = argument5;
	shader_set(shd_blend);

	var u_col, red, green, blue;
	u_col = shader_get_uniform(shd_blend, "Color");
	red = color_get_red(color) / 255;
	green = color_get_green(color) / 255;
	blue = color_get_blue(color) / 255;

	shader_set_uniform_f(u_col, red, green, blue, alpha);

	draw_sprite(sprite, image, xx, yy);

	shader_reset();


}
