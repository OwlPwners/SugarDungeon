///@arg layer
///@arg x
///@arg y
function tile_meeting() {
	var tilemap, xx, yy, xp, yp, anglep, scxp, meeting;

	tilemap = argument[0];
	xx = argument[1];
	yy = argument[2];

	xp = x;
	yp = y;
	anglep = image_angle;
	scxp = image_xscale;

	x = xx;
	y = yy;
	image_angle = 0;
	image_xscale = 1;

	meeting = false;

	var division = ((bbox_right - bbox_left) div tile_size) + 1;

	var i = 0, xlen = bbox_right - bbox_left, xrepeats = round(xlen) / division;
	repeat(xrepeats) {
		var xcheck = i * division;
	
		if (tilemap_get_at_pixel(tilemap, bbox_left + xcheck, bbox_bottom) || tilemap_get_at_pixel(tilemap, bbox_left + xcheck, bbox_top)) {
			meeting = true;
		
			break;
		}
	
		i ++;
	}

	var division = ((bbox_bottom - bbox_top) div tile_size) + 1;

	if (!meeting) {
		var i = 0, ylen = bbox_bottom - bbox_top, yrepeats = round(ylen) / division;
		repeat(yrepeats) {
			var ycheck = i * division;
		
			if (tilemap_get_at_pixel(tilemap, bbox_left, bbox_top + ycheck) || tilemap_get_at_pixel(tilemap, bbox_right, bbox_top + ycheck)) {
				meeting = true;
		
				break;
			}
	
			i ++;
		}
	}

	x = xp;
	y = yp;
	image_angle = anglep;
	image_xscale = scxp;

	return(meeting);
}