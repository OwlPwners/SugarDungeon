///@arg map
///@arg char_size
function button_draw(argument0, argument1) {

	var is_down = false, image;

	var xx = ds_map_find_value(argument0, "X"), yy = ds_map_find_value(argument0, "Y"),
	width = ds_map_find_value(argument0, "SpriteWidth"), height = ds_map_find_value(argument0, "SpriteHeight"),
	sprite = ds_map_find_value(argument0, "Sprite"), text = ds_map_find_value(argument0, "Text"),
	text_size = argument1, text_length = string_length(text) * text_size, 
	blend = ds_map_find_value(argument0, "Blend"), alpha = ds_map_find_value(argument0, "Alpha"),
	sprw = ds_map_find_value(argument0, "SpriteWidth"), sprh = ds_map_find_value(argument0, "SpriteHeight");

	var between = font_get_size(draw_get_font()) / 2 * 1.75;

	draw_set_color(blend);
	draw_set_alpha(alpha);

	var part_width = width / 3, part_height = height / 3,
	button_x_min = xx - text_length / 2, button_x_max = xx + text_length / 2,
	button_y_min = yy - (text_size + between) / 2, button_y_max = yy + (text_size + between) / 2,
	xscale = 1 / part_width * (text_length - part_width), yscale = 1 / part_height * ((text_size + between) - part_height);

	if (text != -1) {
		var in_rect = point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), xx - text_length / 2, yy - (text_size + between) / 2, xx + text_length / 2, yy + (text_size + between) / 2);
		if (instance_exists(cursor)) in_rect = in_rect || point_in_rectangle(view_xport[0] + cursor.x, view_yport[0] + cursor.y, xx - text_length / 2, yy - (text_size + between) / 2, xx + text_length / 2, yy + (text_size + between) / 2);

		if (in_rect ) is_down = true;

		if (is_down == true) image = ds_map_find_value(argument0, "ImageDown") else image = ds_map_find_value(argument0, "ImageUp");

		draw_gui_box(sprite, image, button_x_min, button_y_min, button_x_max, button_y_max, blend, alpha);

		draw_set_halign(fa_middle);
		draw_set_valign(fa_center);

		draw_text_transformed_color(xx, yy, text, 0.5, 0.5, 0, c_white, c_white, c_white, c_white, alpha);

		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
	} else {
		var in_rect = point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), xx - sprw / 2, yy - sprh / 2, xx + sprw / 2, yy + sprh / 2)
		|| point_in_rectangle(view_xport[0] + cursor.x, view_yport[0] + cursor.y, xx - sprw / 2, yy - sprh / 2, xx + sprw / 2, yy + sprh / 2);

		if (in_rect ) is_down = true;

		if (is_down == true) image = ds_map_find_value(argument0, "ImageDown") else image = ds_map_find_value(argument0, "ImageUp");
	
		draw_sprite(sprite, image, xx, yy);
	}

	draw_set_color(c_white);
	draw_set_alpha(1);


}
