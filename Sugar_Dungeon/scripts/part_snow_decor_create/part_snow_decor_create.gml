function part_snow_decor_create() {
	part_snow_decor_particle = part_type_create();
	part_type_shape(part_snow_decor_particle, pt_shape_snow);
	part_type_size(part_snow_decor_particle, 0.10, 0.20, 0, 0);
	part_type_scale(part_snow_decor_particle, 1, 1);
	part_type_color1(part_snow_decor_particle, 16777215);
	part_type_alpha3(part_snow_decor_particle, 0, 1, 0);
	part_type_speed(part_snow_decor_particle, 0.05, 0.07, 0, 0);
	part_type_direction(part_snow_decor_particle, 0, 359, 0, 0);
	part_type_gravity(part_snow_decor_particle, 0, 270);
	part_type_orientation(part_snow_decor_particle, 0, 0, 0, 0, 1);
	part_type_blend(part_snow_decor_particle, 1);
	part_type_life(part_snow_decor_particle, 30, 120);
	
	part_snow_decor_emitter = part_emitter_create(global.particle_system);
}