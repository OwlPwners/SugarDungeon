///@arg obj
///@arg x1
///@arg y1
///@arg x2
///@arg y2

function instance_number_region(obj, x1, y1, x2, y2) {
	var i = 0; with(obj) {
		if (get_inside(x, y, x1, y1, x2, y2)) {
			i ++;
		}
	}
	
	return i;
}