///@arg instance
///@arg time
function effect_add_notcold(argument0, argument1) {

	var inst = argument0, time = argument1, absent = effect_instance_find_effect(inst, obj_effect.notcold) == -1;
	if (absent) {
		ds_list_add(inst.eff_list, obj_effect.notcold);
	
		inst.timer_not_cold = time;
	}


}
