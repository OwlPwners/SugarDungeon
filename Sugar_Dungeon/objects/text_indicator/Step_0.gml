scale_spd = lerp(scale_spd, (scale_target - scale) * 0.3, scale_amount);
scale += scale_spd;

offset ++;

if (offset >= offset_target) {
	offset = offset_target;
	
	alpha -= alpha_spd;
	if (alpha <= 0) instance_destroy(self);
}

//smooth_rotate(angle, angle_target, angle_spd);