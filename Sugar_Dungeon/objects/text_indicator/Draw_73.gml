var last_font = draw_get_font();
draw_set_font(global.font_game);
draw_set_alpha(alpha);
draw_set_halign(fa_center);

var yy = y - offset;
draw_text_outline(x, yy, text, scale, scale, 0, c_white, color);

draw_set_font(last_font);
draw_set_alpha(1);
draw_set_color(c_white);
draw_set_halign(fa_left);