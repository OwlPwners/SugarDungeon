draw_sprite_ext(sprite_index, image_index, x + xoff, y + yoff, image_xscale, image_yscale, image_angle, image_blend, image_alpha);

draw_set_color(c_black)
draw_set_alpha(0.05 * shadow_alpha_factor);

var yy = bbox_bottom + sprite_height / 2;
draw_ellipse(bbox_left + xoff, yy + yoff, bbox_right + xoff, yy + sprite_height / 3 + yoff, false);

draw_set_color(c_white)
draw_set_alpha(1);