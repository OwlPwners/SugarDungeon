if (!scale_setted) {
	scale = random_range(scale_min, scale_max);
	image_xscale = scale;
	image_yscale = scale;
	
	loff = scale * 0.1;
	
	shadow_alpha_factor = scale / 2;
	
	scale_setted = true;
}

if (instance_exists(camera)) {
	var xx, yy;
	if (clamp_box_id > -1) {
		xx = clamp_box_id.x + clamp_box_id.region_width / 2;
		yy = clamp_box_id.y + clamp_box_id.region_height / 2;
	} else {
		xx = -player.x;
		yy = -player.y;
	}
	
	xoff = (xx - camera.x) * loff * image_xscale;
	yoff = (yy - camera.y) * loff * image_yscale;
}