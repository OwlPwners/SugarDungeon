event_inherited();

depth = -room_height;

scale_min = 1;
scale_max = 2;
scale_setted = false;

shadow_alpha_factor = 1;

xoff = 0;
yoff = 0;
loff = 1;

clamp_box_id = -1;