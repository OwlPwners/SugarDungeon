part_bubble_create();

var so = choose(sfx_boss_soda_buble1, sfx_boss_soda_buble2, sfx_boss_soda_buble3);
audio_play_sound(so, 105, false);

ytimer = 90;
yspd = 0.5;
yspeed = 0;

xdir = irandom(359);
xspd = 0.75;

image_xscale = 0;
image_yscale = 0;
scale_spd = 0.025;

chase_timer = 30;
dir_to = 0;
dir_timer = 90;

speed_ = 0;
spd = 10;

damage = 2;
force_bounce = 2;

enum bubble_from_who {
	from_boss,
	from_player
}

from_who = bubble_from_who.from_boss;

exist_timer = 1440;

destroy_time = 60;
destroy_timer = destroy_time + 1;