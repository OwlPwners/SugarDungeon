if (boss_soda.boss_state == boss_soda_state.dead) instance_destroy(self);

exist_timer --;

if (visible == false) {
	if (destroy_timer > destroy_time) destroy_timer = destroy_time;
	destroy_timer --;
	
	if (destroy_timer <= 0) instance_destroy(self);
	
	exit;
}

if (ytimer > 0) {
	ytimer --;
	y -= yspd;
	yspeed = lerp(yspeed, yspd, 0.2);
	
	x += lengthdir_x(xspd, xdir);
	xdir += 5;
} else {
	part_bubble_burst(x, y, irandom(1));
	
	if (chase_timer > 0) {
		chase_timer --;
		
		dir_to = point_direction(x, y, player.x, player.y);
	}
	
	speed_ = lerp(speed_, spd, 0.05);

	x += lengthdir_x(speed_, dir_to);
	y += lengthdir_y(speed_, dir_to);
}

if (dir_timer > 0 && ytimer < 0) {
	dir_timer --;
	dir_to = point_direction(x, y, player.x, player.y);
}

image_xscale += scale_spd;
image_xscale = min(image_xscale, 1);

image_yscale = image_xscale;

var wp = player.wp_id;
if (instance_exists(wp) && place_meeting(x, y, wp) && wp.state == "attack") {
	from_who = bubble_from_who.from_player;
	dir_to -= 180;
}

if (place_meeting(x, y, player)) {
	if (from_who == bubble_from_who.from_boss) {
		var player_inst = instance_place(x, y, player);
		if (player_inst != noone) {
			if (player_inst.war_timer <= 0 && player_inst.glide_spd == 0) {
				player_inst.flash = 1;
				player_inst.war_timer = player_inst.war_time;
					
				var inst_sh = player.shield_id, s = 5;
				if (player.state == player_state.block && instance_exists(inst_sh) && place_meeting(x, y, inst_sh)) {
					var map = inst_sh.map,
					proc_hp = (100 - map[? "BLockHp"]) / 100,
					proc_bounce = (100 - map[? "BlockKnockback"]) / 100;
						
					player_inst.hp -= damage * proc_hp;
					bounce_set(player_inst.x, player_inst.y, x, y, force_bounce * proc_bounce, player_inst);
					with(player_inst) part_sparks_burst(x, y, 5);
				
					s *= proc_bounce;
				} else {
					player_inst.hp -= damage;
					bounce_set(player_inst.x, player_inst.y, x, y, force_bounce, player_inst);
				}
			
				camera.x += random_range(-s, s);
				camera.y += random_range(-s, s);
			}
		}
	}
}

if (from_who == bubble_from_who.from_player) {
	var boss_inst = instance_place(x, y, boss_soda);
	if (boss_inst != noone) {
		boss_inst.hp -= damage;
		boss_inst.flash = 1;
		visible = false;
	}
}

if (exist_timer <= 0) visible = false;