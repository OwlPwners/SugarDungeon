if (instance_exists(target)) {
	var xto, yto;
	xto = target.x;
	yto = target.y;
	
	x += (xto - x) / 25;
	y += (yto - y) / 25;
}

camera_set_view_pos(cam, x - cam_w / 2, y - cam_h / 2);
camera_set_view_size(cam, cam_w, cam_h);

if (shake) {
	x += random_range(-shake_x, shake_x);
	y += random_range(-shake_y, shake_y);
}
	
if (zoom) {
	cam_w_prev = cam_w;
	cam_h_prev = cam_h;
	
	cam_w += (zoom_w - cam_w) / zoom_spd;
	cam_h += (zoom_h - cam_h) / zoom_spd;
} else {
	cam_w += (cam_w_prev - cam_w) / 25;
	cam_h += (cam_h_prev - cam_h) / 25;
}