cam_w = gui_width;
cam_h = gui_height;
cam_w_prev = cam_w;
cam_h_prev = cam_h;

cam = view_camera[0];
target = player;
deactivate = true;

surface_resize(application_surface, gui_width, gui_height);
display_set_gui_size(gui_width, gui_height);

shake = false;
zoom = false;

shake_x = 0;
shake_y = 0;

zoom_w = 0;
zoom_h = 0;
zoom_spd = 0;