event_inherited();

/*
surface_set_target(surf_ref);
draw_clear_alpha(c_black, 0.0);

draw_sprite(sprite_index, image_index, sprite_xoffset, sprite_yoffset);

gpu_set_colorwriteenable(true, true, true, false);

with(all) {
	if (sprite_index > -1 && visible && image_alpha > 0) {
		var yy;
		yy = bbox_bottom + (sprite_height - sprite_get_yoffset(sprite_index));
		
		draw_sprite_ext(sprite_index, image_index, x - other.bbox_left, yy - other.bbox_top, image_xscale, -image_yscale, image_angle, image_blend, image_alpha * jam_reflection_alpha);
	}
}

gpu_set_colorwriteenable(true, true, true, true);

surface_reset_target();

//---------------------------
if (room == rm_redactor) exit;

var bbox = player.mask_index;
player.mask_index = spr_player_hitbox;

if (place_meeting(x, y, player)) {
	effect_add_jam(player, 10);
}

player.mask_index = bbox;