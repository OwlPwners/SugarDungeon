var ev_type = async_load[? "event_type"];
var pad_ind = async_load[? "pad_index"];

switch(ev_type) {
	case "gamepad discovered":
	global.gp_connect_id = pad_ind;
	
	gamepad_set_axis_deadzone(pad_ind, gp_deadzone);
	break;
	
	case "gamepad lost":
	global.gp_connect_id = -1;
	break;
}