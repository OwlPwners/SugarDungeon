txt_con = ini_translate(file_translate_gamepad, global.game_lang, "Connected");
txt_lost = ini_translate(file_translate_gamepad, global.game_lang, "Disconnected");

button_text = ini_translate(file_translate_gamepad, global.game_lang, "Connected");
button_x = gui_width / 2;
button_y = gui_height - string_height(button_text) / 2;
button_alpha = 0;

button = button_create(spr_pause_button, 0, 0, button_x, button_y, c_white, button_alpha, button_text);

gp_prev = global.gp_connect_id;
mode = -1;

for(var i = 0; i < gamepad_get_device_count(); i ++) {
	if (gamepad_is_connected(i)) {
		global.gp_connect_id = i;
		gamepad_set_axis_deadzone(i, gp_deadzone);
		
		break;
	}
}