var gp = global.gp_connect_id;
if (gp_prev != gp) {
	button_alpha = 10;
	
	mode = gp > -1;
	
	switch(mode) {
		case(0) :
		button_text = txt_lost;
		break;
		
		case(1) :
		button_text = txt_con;
		break;
	}
	
	button_y = gui_height + string_height(button_text);
	
	button_set_text(button, button_text);
	button_set_coord(button, button_x, button_y);
}

button_y += (gui_height - string_height(button_text) - button_y) / 15;
button_alpha -= 0.07;

button_set_coord(button, button_x, button_y);
button_set_alpha(button, button_alpha);

gp_prev = gp;