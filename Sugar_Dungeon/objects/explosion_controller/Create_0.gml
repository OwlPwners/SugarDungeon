surf = surface_create(gui_width, gui_height);

u_intens = shader_get_uniform(shd_normal_map, "Intensity");
u_aspect = shader_get_uniform(shd_normal_map, "Aspect");
u_sampler = shader_get_sampler_index(shd_normal_map, "NormalMap");