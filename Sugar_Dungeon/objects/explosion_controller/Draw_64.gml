if (!instance_exists(camera)) exit;

shader_set(shd_normal_map);

shader_set_uniform_f(u_intens, 0.06);
shader_set_uniform_f(u_aspect, camera.cam_w / camera.cam_h);
texture_set_stage(u_sampler, surface_get_texture(surf));

draw_surface(application_surface, 0, 0);

shader_reset();

var xx, yy;
xx = camera.x - gui_width / 2;
yy = camera.y - gui_height / 2;

with(explosion) {
	if (activated) {
		draw_set_alpha(exp_alpha);
		draw_circle(x - xx, y - yy, rad, true);
		draw_set_alpha(1);
	}
}