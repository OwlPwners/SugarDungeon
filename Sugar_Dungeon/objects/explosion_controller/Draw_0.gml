if (!instance_exists(camera)) exit;

if (surface_exists(surf) == false) surf = surface_create(gui_width, gui_height);

surface_set_target(surf);
gpu_set_texfilter(true);

var xx, yy;
xx = camera.x - gui_width / 2;
yy = camera.y - gui_height / 2;

draw_clear($FF7F7F);
with(explosion) {
	if (activated) draw_sprite_ext(sprite_normal_map, image_index, x - xx, y - yy, exp_scale, exp_scale, 0, c_white, exp_alpha);
}

gpu_set_texfilter(false);
surface_reset_target();