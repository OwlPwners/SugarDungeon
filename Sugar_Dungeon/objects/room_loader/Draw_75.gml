if (!surface_exists(surf)) surf = surface_create(gui_width, gui_height);

switch(transition) {
	case(room_transition.def) :
	draw_set_alpha(alpha);
	draw_set_color(c_black);
	
	draw_rectangle(0, 0, gui_width, gui_height, false);
	
	draw_set_alpha(1);
	draw_set_color(c_white);
	break;
	
	case(room_transition.lollipop) :
	surface_set_target(surf);
	draw_clear(c_white);
	
	draw_set_color(c_black);
	draw_circle(gui_width / 2, gui_height / 2, rad, false);
	draw_set_color(c_white);
	surface_reset_target();

	shader_set(shd_transition_lollipop);
	
	var lnum = 4, lspd = 0.002, lrot = 10 * sin(current_time / 800), lalph = alpha;
	shader_set_uniform_f(u_lollipop_time, current_time);
	shader_set_uniform_f(u_lollipop_resolution, 1, 1);
	shader_set_uniform_f(u_lollipop_settings, lnum, lspd, lrot, lalph);
	
	draw_surface(surf, 0, 0);

	shader_reset();
	break;
}