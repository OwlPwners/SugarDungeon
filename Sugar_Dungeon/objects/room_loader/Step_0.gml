if (transition == room_transition.noone_) {
	state = room_transition.st_start;
	alpha = 0;
	time = 0;
	spd = 0;
	rad = 0;
	
	exit;
}

switch(transition) {
	case(room_transition.def) :
	var alpha_spd = 0.07;
	if (state == room_transition.st_start) {
		alpha += alpha_spd;
		if (alpha >= 1.3) {
			rmloader_transition();
		}
	} else {
		alpha -= alpha_spd;
		if (alpha <= 0) transition = room_transition.noone_;
	}
	break;
	
	case(room_transition.lollipop) :
	var rspd, aspd, nrad;
	rspd = 20;
	aspd = 30;
	nrad = sqrt(sqr(gui_width) + sqr(gui_height));
	
	if (time == 0) rad = nrad;
	if (time > 160) rmloader_transition();
	time ++;
	
	if (state == room_transition.st_start) {
		rad += (0 - rad) / rspd * 2;
		alpha += (1 - alpha) / aspd;
	} else {
		rad += (nrad - rad) / rspd;
		alpha += (0 - alpha) / aspd;
		
		if (abs(rad - nrad) < 10) {
			transition = room_transition.noone_;
		}
	}
	break;
}