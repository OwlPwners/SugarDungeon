enum rm_loader_spechialy {
	restart
}

enum room_transition {
	st_start,
	st_end,
	noone_,
	def,
	lollipop
}

transition = room_transition.noone_;
state = room_transition.st_start;
rm_goto = -1;

surf = surface_create(gui_width, gui_height);

alpha = 0;
time = 0;
spd = 0;
rad = 0;

u_lollipop_time = shader_get_uniform(shd_transition_lollipop, "Time");
u_lollipop_resolution = shader_get_uniform(shd_transition_lollipop, "Resolution");
u_lollipop_settings = shader_get_uniform(shd_transition_lollipop, "Settings");