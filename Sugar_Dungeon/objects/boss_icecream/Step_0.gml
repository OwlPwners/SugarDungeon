if (room == rm_redactor || global.game_mod == "pause" || global.game_mod == "show_map") exit;

set_depth();

intro_timer --;
war_timer --;
flash -= flash_minus;
hp_draw = lerp(hp_draw, hp, 0.2);

if (hp > 0) {
	var scl = sign(player.x - x);
	if (scl != 0) xscale_to = scl;
	
	if (hp < hp_max * 0.5) aggressive = true;
	if (aggressive != aggressive_prev) boss_state = boss_icecream_state.transformation;
} else {
	boss_state = boss_icecream_state.dead;
}

image_xscale += (xscale_to - image_xscale) / 5;

switch(boss_state) {
	case(boss_icecream_state.intro) :
	if (intro_timer > 0) {
		player.draw = false;
	
		camera.target = boss_icecream;
	} else {
		player.draw = true;
	
		camera.target = player;
		
		boss_state = boss_icecream_state.attack_distant;
	}
	break;
	
	case(boss_icecream_state.stop) :
	sprite_index = spr_boss_icecream_stop;
	
	hsp = 0;
	vsp = 0;
	break;
	
	case(boss_icecream_state.transformation) :
	sprite_index = spr_boss_icecream_transformation;
	
	hsp = 0;
	vsp = 0;
	break;
	
	case(boss_icecream_state.run_to_player) :
	sprite_index = spr_boss_icecream_run;
	
	var dx, dy, len;
	dx = player.x - x;
	dy = player.y - y;
	len = sqrt(dx * dx + dy * dy);
	if (len != 0) {
		dx /= len;
		dy /= len;
	}
	
	hsp = dx * spd;
	vsp = dy * spd;
	
	run_to_player_timer --;
	if (run_to_player_timer <= 0) {
		run_to_player_timer = run_to_player_time;
		
		repeat(5) {
			var offd, offx, offy;
			offd = 12;
			offx = random_range(-offd, offd);
			offy = random_range(-offd, offd);
			with(instance_create_depth(x + offx, bbox_bottom + offy, depth, leaked_icecream)) {
				exists_timer = 60;
			}
		}
	}
	
	if (place_meeting(x + hsp, y + vsp, player)) {
		boss_state = boss_icecream_state.attack_melee; 
		image_index = 0;
	}
	break;
	
	case(boss_icecream_state.attack_melee) :
	sprite_index = spr_boss_icecream_attack;
	mask_index = spr_boss_icecream_mask_attack;
	
	hsp = 0;
	vsp = 0;
			
	var player_inst = instance_place(x, y, player);
	if ((player_inst != noone) && place_meeting(x, y, player) && can_attack) {
		if (player_inst.war_timer <= 0 && player_inst.glide_spd == 0) {
			player_inst.war_timer = player_inst.war_time;
			player_inst.flash = 1;
					
			var inst_sh = player.shield_id, s = 10;
			if (player.state == player_state.block && instance_exists(inst_sh) && place_meeting(x, y, inst_sh)) {
				var map = inst_sh.map,
				proc_hp = (100 - map[? "BLockHp"]) / 100,
				proc_bounce = (100 - map[? "BlockKnockback"]) / 100;
						
				player_inst.hp -= damage * proc_hp;
				bounce_set(player_inst.x, player_inst.y, x, y, force_bounce * proc_bounce, player_inst);
				with(player_inst) part_sparks_burst(x, y, 5);
				
				s *= proc_bounce;
				inst_sh.flash = 1;
			} else {
				player_inst.hp -= damage;
				bounce_set(player_inst.x, player_inst.y, x, y, force_bounce, player_inst);
			}
			
			camera.x += random_range(-s, s);
			camera.y += random_range(-s, s);
		}
		
		can_attack = false;
	}
	break;
	
	case(boss_icecream_state.run_to_random) :
	if (boss_state != boss_state_prev) {
		sprite_index = spr_boss_icecream_leaking;
	} else {
		var nimg = image_index + image_speed;
		if (nimg <= 0) {
			sprite_index = spr_boss_icecream_stop;
			image_speed = 1;
			
			boss_state = boss_icecream_state.attack_distant; 
		}
	}
	
	hsp = 0;
	vsp = 0;
	break;
	
	case(boss_icecream_state.attack_distant) :
	if (boss_state != boss_state_prev) sprite_index = spr_boss_icecream_stop;
	
	hsp = 0;
	vsp = 0;
	// 6 - 9
	
	attack_distant_timer --;
	if (attack_distant_timer < 0) {
		 sprite_index = spr_boss_icecream_mouth;
	}
	
	if (sprite_index == spr_boss_icecream_mouth) {
		var created_num = attack_distant_bullet_created_num < attack_distant_bullet_num;
		if (created_num) {
			var idd = instance_create_depth(x, y, depth + 1, boss_icecream_bullet);
			idd.image_index = attack_distant_bullet_created_num;
			
			attack_distant_bullet_created_num ++;
		}
	}
	
	break;
	
	case(boss_icecream_state.dead) :
	depth = player.depth - 1;
	
	if (boss_state_prev != boss_state) {
		player.money += 70;
		player.hp += 20;
	}
	
	sprite_index = spr_boss_icecream_stop_sword;
	
	if (draw) {
		with(camera) {
			target = boss_icecream;
			shake = true;
			shake_x = 3;
			shake_y = 3;
		}
		
		player.draw = false;
	} else {
		with(camera) {
			target = player;
			shake = false;
		}
		
		if (dead_glow_time > 200) dead_rectangle_alpha -= 0.025;
 		
		if (dead_rectangle_alpha <= 0) {
			game_end();
			
			with(instance_create_depth(x, y, depth, nextlevel_controller)) {
				chapter = chapter_diving * 2;
				sprite_index = asset_get_index("spr_next_level1");
			}
			
			item_create(player.x, player.y, true, false, global.map_items[item.icecream_sword]);
			
			audio_stop_all();
			global.game_music = -1;
			
			instance_destroy(self);
			player.draw = true;
		}
	}
	
	dead_glow_steps += 0.25;
	dead_glow_intens += 0.075;
	dead_glow_time ++;
	
	hsp = 0;
	vsp = 0;
	
	break;
}

mask_index = spr_boss_icecream_stop;

x += hsp;
y += vsp;

boss_state_prev = boss_state;
aggressive_prev = aggressive;