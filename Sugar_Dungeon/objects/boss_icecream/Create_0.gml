draw = true;

hsp = 0;
vsp = 0;
spd = 2.3;
xscale_to = 1;

hp_max = 10;
hp = hp_max;
hp_draw = 0;

enum boss_icecream_state {
	intro,
	stop,
	run_to_player,	
	transformation,
	run_to_random,
	attack_melee,
	attack_distant,
	dead
}

state = -1;
boss_state = boss_icecream_state.intro;
boss_state_prev = -1;

intro_timer = 180;

war_time = 8;
war_timer = war_time;
flash = 0;

can_attack = true;
damage = 2;
force_bounce = 5;

attack_distant_time = 150;
attack_distant_timer = attack_distant_time;
attack_distant_bullet_num = 3;
attack_distant_bullet_created_num = 0;
attack_distant_cycled_num = 0;
attack_distant_cycle_num = 5;

aggressive = false;
aggressive_prev = false;

run_to_player_time = 5;
run_to_player_timer = run_to_player_time;

dead_rectangle_alpha = 1;
dead_glow_steps = 0;
dead_glow_intens = 0;
dead_glow_time = 0;

save_object_default_variables();
save_object_add_variables("hp", "intro_timer", "boss_state");

audio_group_load(agp_boss_icecream_music);
global.game_music = so_boss_icecream;

sound_damaged = [sfx_enemy_damaged1, sfx_enemy_damaged2, sfx_enemy_damaged3];

depth_variables();
create_bounce_system(100);