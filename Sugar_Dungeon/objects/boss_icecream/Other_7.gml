if (room == rm_redactor) exit;

image_index = 0;

switch(boss_state) {
	case(boss_icecream_state.attack_distant) :
	if (sprite_index == spr_boss_icecream_mouth) {
		sprite_index = spr_boss_icecream_stop;
		
		attack_distant_timer = attack_distant_time;
		attack_distant_bullet_created_num = 0;
		
		attack_distant_cycled_num ++;
		
		if (attack_distant_cycled_num == attack_distant_cycle_num) {
			boss_state = boss_icecream_state.run_to_random;
			attack_distant_cycled_num = 0;
		}
	}
			
	break;
	
	case(boss_icecream_state.attack_melee) :
	boss_state = boss_icecream_state.run_to_player;
	can_attack = true;
	break;
	
	case(boss_icecream_state.transformation) :
	boss_state = boss_icecream_state.run_to_player;
	break;
	
	case(boss_icecream_state.run_to_random) :
	if (image_speed == -1) {
		boss_state = boss_icecream_state.attack_distant;
		
		break;
	}
	
	image_index = image_number - 1;
	image_speed = -1;
	
	var dx, dy, col;
	dx = random_range(clamp_box_id.x + sprite_width, clamp_box_id.x + clamp_box_id.region_width - sprite_width);
	dy = random_range(clamp_box_id.y + sprite_height, clamp_box_id.y + clamp_box_id.region_height - sprite_height);
	col = tile_meeting(layer_collision, dx, dy);
	while(col) {
		dx = random_range(clamp_box_id.x + sprite_width, clamp_box_id.x + clamp_box_id.region_width - sprite_width);
	dy = random_range(clamp_box_id.y + sprite_height, clamp_box_id.y + clamp_box_id.region_height - sprite_height);
		col = tile_meeting(layer_collision, dx, dy);
	}
	
	x = dx;
	y = dy;
	break;
}