if (!layer_changed) {
	layer = layer_other;
	layer_changed = true;
}

var game_mod_ = global.game_mod;
if (game_mod_ == "pause" || game_mod_ == "show_item" || !instance_exists(player)) exit;

image_index += sign((index_to - image_index));

if (can_attack == true) {
	var mask = player.mask_index;
	if (mask != -1) player.mask_index = spr_player_hitbox;
	
	if (place_meeting(x, y, player) && player.war_timer <= 0) {
		can_attack = false;
		if (player.state != player_state.glide) {
			player.hp -= damage;
			player.flash = 1;
			
			var p = 3;
			camera.x += random_range(-p, p);
			camera.y += random_range(-p, p);
			
			bounce_set(player.x, player.y, x, y, 3, player);
			
			if (attack_event != -1) event_user(attack_event);
		}
		
		player.war_timer = player.war_time;
	}
	
	player.mask_index = mask;
}