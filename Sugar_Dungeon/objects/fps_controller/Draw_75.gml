if (global.debug == false) exit;

var yy = display_get_gui_height() - 32;

draw_text(0, yy, "FPS: " + string(fps));
draw_text(0, yy - 32, "FPS Real: " + string(fps_now));
draw_text(0, yy - 64, "Active Instance Count: " + string(instance_count));
draw_text(0, yy - 96, "Mode: " + global.game_mod);