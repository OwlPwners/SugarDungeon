{
  "spriteId": {
    "name": "spr_pie_cherry_attack",
    "path": "sprites/spr_pie_cherry_attack/spr_pie_cherry_attack.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "enemy",
    "path": "objects/enemy/enemy.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"cookie_pie_cherry","path":"objects/cookie_pie_cherry/cookie_pie_cherry.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"cookie_pie_cherry","path":"objects/cookie_pie_cherry/cookie_pie_cherry.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":8,"collisionObjectId":null,"parent":{"name":"cookie_pie_cherry","path":"objects/cookie_pie_cherry/cookie_pie_cherry.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":7,"eventType":7,"collisionObjectId":null,"parent":{"name":"cookie_pie_cherry","path":"objects/cookie_pie_cherry/cookie_pie_cherry.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Pies",
    "path": "folders/Objects/Player and enemies/Pies.yy",
  },
  "resourceVersion": "1.0",
  "name": "cookie_pie_cherry",
  "tags": [],
  "resourceType": "GMObject",
}