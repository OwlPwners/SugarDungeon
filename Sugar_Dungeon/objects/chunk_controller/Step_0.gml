start_work_timer --;

if (start_work_timer > 0) exit; 

else if (start_work_timer == 0) {
	var list_inside = ds_list_create();
	collision_rectangle_list(x, y, x + chunk_size, y + chunk_size, i_must_save, false, true, list_inside, false);
	
	var n = ds_list_size(list_inside);
	if (n == 0) {
		instance_destroy(self);
		exit;
	}
}

//var mode = global.game_mod;
if (instance_exists(camera) /*&& mode != "pause"*/) {
	var dist = point_distance(x + chunk_size / 2, y + chunk_size / 2, camera.x, camera.y);
	if (dist < gui_width) {
		var x1, y1, x2, y2;
		x1 = camera.x - camera.cam_w / 2 - tile_size;
		y1 = camera.y - camera.cam_h / 2 - tile_size;
		x2 = camera.x + camera.cam_w / 2 + tile_size;
		y2 = camera.y + camera.cam_h / 2 + tile_size;

		if (rectangle_in_rectangle(x, y, x + chunk_size, y + chunk_size, x1, y1, x2, y2)) activate(); else deactivate();
	} else deactivate();
}

if (player.clamping) {
	var idd, x1, y1, x2, y2;
	idd = player.clamp_box_id;
	x1 = idd.x;
	y1 = idd.y;
	x2 = idd.x + idd.region_width;
	y2 = idd.y + idd.region_height;
	
	if (rectangle_in_rectangle(x, y, x + chunk_size, y + chunk_size, x1, y1, x2, y2)) activate();
}