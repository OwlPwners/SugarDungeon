start_work_timer = 3;

list_chunk = [];

is_active = true;

deactivate = function() {
	if (self.is_active) {
		var list_inside = ds_list_create();
		collision_rectangle_list(x, y, x + chunk_size, y + chunk_size, i_must_save, false, true, list_inside, false);
			
		var list_size = ds_list_size(list_inside), id_index = 0;
		repeat(list_size) {
			var idd = list_inside[| id_index];
			if (asset_has_tags(idd, "shouldnt_deactivate", asset_object)) {
				self.list_chunk[id_index] = [idd.id, idd.object_index];
				instance_deactivate_object(idd);
			}
		
			id_index ++;
		}
		
		ds_list_destroy(list_inside);
		
		self.is_active = false;
	}
}

activate = function() {
	if (!self.is_active) {
		var i = 0;
		repeat(array_length(self.list_chunk)) {
			instance_activate_object(self.list_chunk[i][0]);
			//ds_list_delete(self.list_chunk, i);
			
			i ++;
		}
		
		//ds_list_clear(self.list_chunk);
		
		self.is_active = true;
	}
}

image_alpha = 0;