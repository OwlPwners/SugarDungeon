if (!biome_setted) {
	switch(chapter_fragament) {
		case(0) :
		repeat(irandom_range(250, 300)) {
			var xx, yy, spr;
			xx = irandom(room_width);
			yy = irandom(room_height);
			spr = choose(spr_suspended_chain_chocolate, spr_cage_sugar);
			scale = random_range(1, 2);
			
			var idd = layer_sprite_create(layer_decor_floor0, xx, yy, spr);
			layer_sprite_xscale(idd, scale);
			layer_sprite_yscale(idd, scale);
			
			layer_depth(layer_decor_floor0, -room_height);
		}
		break;
		
		case(1) :
		part_snow_decor_create();
		break;
	}
	
	biome_setted = true;
}

switch(chapter_fragament) {
	case(0) :
	if (instance_exists(camera)) {
		layer_x(layer_decor_floor0, -camera.x / 10);
		layer_y(layer_decor_floor0, -camera.y / 10);
	}
	break;
	
	case(1) :
	if (instance_exists(camera)) {
		part_snow_decor_burst(camera.x - camera.cam_w / 2, camera.y - camera.cam_h / 2, irandom_range(0, 5));
	}
	break;
}