grid = ds_grid_create(0, 0);

input_left = key_left_load;
input_right = key_right_load;
input_up = key_up_load;
input_down = key_down_load;
input_show = ini_load(file_settings, "Real", "Control", "ShowMap", vk_tab);

xs = gui_width / 2;
ys = gui_height / 2;

xp = xs;
yp = ys;

xd = xs;
yd = ys;

minimap_width = sprite_get_width(spr_minimap_interface);
minimap_height = sprite_get_height(spr_minimap_interface);

minimap_x = ini_load(file_ui_coords, "Real", "MiniMap", "x", gui_width - minimap_width);
minimap_y = ini_load(file_ui_coords, "Real", "MiniMap", "y", gui_height - minimap_height);

spr_ico_size = 1;
spr_ico_size_prev = spr_ico_size;

path = path_add();

surf_bg = surface_create(gui_width, gui_height);

surface_set_target(surf_bg);
draw_clear($363636);

surface_reset_target();