var game_mod_ = global.game_mod;
if (game_mod_ == "pause" || game_mod_ == "show_item" || game_mod_ == "player_dead" || !instance_exists(player)) exit;

var enemies = instance_number(enemy) + instance_numer_deactivated(enemy);
if (enemies == 0 && instance_exists(nextlevel_controller)) {
	var mask = player.mask_index;
	player.mask_index = -1;
	
	mp_grid_path(grid, path, player.x, player.y, nextlevel_controller.x, nextlevel_controller.y, true);
	
	player.mask_index = mask;
}

spr_ico_size_prev = spr_ico_size;

var gp_id = global.gp_connect_id;
if (gp_id == -1) {
	spr_ico_size += (mouse_wheel_up() - mouse_wheel_down()) * 0.07;
	
	if (game_mod_ == "show_map") {
		var mx = device_mouse_x_to_gui(0), my = device_mouse_y_to_gui(0);

		if (mouse_check_button(mb_left)) {
			xd += mx - xp;
			yd += my - yp;
		}
	
		xp = mx;
		yp = my;
	
		xd += (keyboard_check(input_right) - keyboard_check(input_left)) * 3;
		yd += (keyboard_check(input_down) - keyboard_check(input_up)) * 3;
		
		cursor.draw = true;
	} else {
		cursor.draw = false;
		player.draw = true;
	}
	
	if (keyboard_check_released(input_show)) {
		if (game_mod_ == "game") {
			global.game_mod = "show_map"; 
			player.draw = false; 
			
			audio_play_sound(sfx_minimap_open, 80, false);
		} else if (game_mod_ == "show_map") {
			global.game_mod = "game";
			player.draw = true;
			
			audio_play_sound(sfx_minimap_close, 80, false);
		}
	}
} else {
	if (gamepad_button_check_pressed(gp_id, gp_start)) {
		if (game_mod_ == "game") {
			global.game_mod = "show_map"; 
			player.draw = false; 
			
			audio_play_sound(sfx_minimap_open, 80, false);
		} else if (game_mod_ == "show_map") {
			global.game_mod = "game";
			player.draw = true;
			
			audio_play_sound(sfx_minimap_close, 80, false);
		}
	}
	
	if (game_mod_ == "show_map") {
		xd -= gamepad_axis_value(gp_id, gp_axislh) * 3;
		yd -= gamepad_axis_value(gp_id, gp_axislv) * 3;
		spr_ico_size -= gamepad_axis_value(gp_id, gp_axisrv) * 0.07;
		
		cursor.draw = true;
	} else {
		player.draw = true;
		cursor.draw = false;
	}
}

spr_ico_size = clamp(spr_ico_size, 0.1, 2.5);