var game_mod_ = global.game_mod;
if (game_mod_ == "show_item" || game_mod_ == "player_dead" || !instance_exists(player)) exit;

var tile_s = sprite_get_width(spr_minimap_tiles);
if (game_mod_ == "show_map") {
	if (surface_exists(surf_bg)) {
		draw_surface(surf_bg, 0, 0);
	} else {
		surf_bg = surface_create(gui_width, gui_height);
		
		surface_set_target(surf_bg);
		draw_clear($363636);

		surface_reset_target();
	}
	
	var spr_w = sprite_get_width(spr_map), spr_h = sprite_get_height(spr_map);
	draw_sprite_ext(spr_map, 0, xd - (spr_w / 2) * spr_ico_size, yd - (spr_h / 2) * spr_ico_size, spr_ico_size, spr_ico_size, 0, c_white, 1);
	
	var xx, yy;
	xx = (player.x * (spr_w / room_width)) * spr_ico_size;
	yy = (player.y * (spr_h / room_height)) * spr_ico_size;
	
	draw_sprite_ext(spr_minimap_player, 0, xd - (spr_w / 2) * spr_ico_size + xx, yd - (spr_h / 2) * spr_ico_size + yy, spr_ico_size * player.image_xscale, spr_ico_size, 0, c_white, 1);
	
	with(bounding_controller) {
		if (enemy_num_lives == 0) {
			var xx, yy, ico_size, width, height, xc, yc;
			ico_size = other.spr_ico_size;
			xx = other.xd + (x * (spr_w / room_width) - spr_w / 2) * ico_size;
			yy = other.yd + (y * (spr_h / room_height) - spr_h / 2) * ico_size;
			width = region_width * (spr_w / room_width) * ico_size;
			height = region_height * (spr_h / room_height) * ico_size;
			xc = xx + width;
			yc = yy + height;
			
			draw_line_width_color(xx, yy, xc, yc, 3 * ico_size, c_red, c_red);
			draw_line_width_color(xc, yy, xx, yc, 3 * ico_size, c_red, c_red);
		}
	}
	
	var enemies = layer_get_all_elements(layer_enemies);
	if (array_length(enemies) == 0) {
		for(var i = 1; i < path_get_number(path); i ++) {
			var xx, yy;
			xx = xd + (path_get_point_x(path, i) * (spr_w / room_width) - spr_w / 2) * spr_ico_size;
			yy = yd + (path_get_point_y(path, i) * (spr_h / room_height) - spr_h / 2) * spr_ico_size;
			draw_sprite_ext(spr_minimap_path, 0, xx, yy, spr_ico_size, spr_ico_size, 0, c_white, 1);
		}
	}
} else {
	var map_height = minimap_height - 2, map_width = minimap_width - 2, map_border = 2,
	left = player.x / tile_size * tile_s - map_width / 2, top = player.y / tile_size * tile_s - map_height / 2,
	width = map_width, height = map_height, 
	xx = minimap_x, yy = minimap_y;

	draw_sprite_part(spr_map, 0, left, top, width, height, xx, yy);
	
	draw_sprite_ext(spr_minimap_player, 0, xx + map_width / 2, yy + map_height / 2, player.image_xscale, 1, 0, c_white, 1);
	
	var enemies = layer_get_all_elements(layer_enemies);
	if (array_length(enemies) == 0) {
		for(var i = 1; i < path_get_number(path); i ++) {
			var xx, yy;
			xx = (path_get_point_x(path, i) - player.x) / tile_size * tile_s + minimap_x + minimap_width / 2 - map_border;
			yy = (path_get_point_y(path, i) - player.y) / tile_size * tile_s + minimap_y + minimap_height / 2 - map_border;
			
			if (xx == clamp(xx, minimap_x - map_border, minimap_x + minimap_width - map_border) &&
				yy == clamp(yy, minimap_y - map_border, minimap_y + minimap_width - map_border)) {
			
				draw_sprite(spr_minimap_path, 0, xx, yy);
			}
		}
	}
	
	with(enemy) {
		if (player.clamp_box_id == clamp_box_id) {
			var xx, yy;
			xx = (x - player.x) / tile_size * tile_s + other.minimap_x + other.minimap_width / 2 + map_border;
			yy = (y - player.y) / tile_size * tile_s + other.minimap_y + other.minimap_height / 2 + map_border;
		
			draw_sprite(spr_minimap_enemy, 0, xx, yy);
		}
	}
	
	//with(bounding_controller) {
	//	if (enemy_num_lives == 0) {
	//		var xx, yy, width, height, xc, yc;
	//		xx = (x - player.x) / tile_size * tile_s + gui_width - (map_width / 2 + map_border);
	//		yy = (y - player.y) / tile_size * tile_s + gui_height - (map_height / 2 + map_border);
	//		width = region_width / tile_size * tile_s;
	//		height = region_height / tile_size * tile_s;
	//		xc = xx + width;
	//		yc = yy + height;
			
	//		if ((xx > gui_width - (map_width + map_border) && yy > gui_width - (map_width + map_border))
	//		|| (xc < gui_width - map_border && yc < gui_height - map_border)) {
	//			draw_line_width_color(xx, yy, xc, yc, 3, c_red, c_red);
	//			draw_line_width_color(xc, yy, xx, yc, 3, c_red, c_red);
	//		}
	//	}
	//}
}