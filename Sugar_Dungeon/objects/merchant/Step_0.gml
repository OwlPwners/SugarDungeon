event_inherited();

if (!instance_exists(player) || global.game_mod == "generating") exit;

var gp_id, axis_product;
gp_id = global.gp_connect_id;
if (gp_id == -1) {
	axis_product = mouse_wheel_down() - mouse_wheel_up();
	key_interact = keyboard_check_released(input_interact);
} else {
	axis_product = gamepad_button_check_released(gp_id, gp_shoulderr) - gamepad_button_check_released(gp_id, gp_shoulderl);
	key_interact = gamepad_button_check_pressed(gp_id, gp_face4);
}

product_alpha -= 0.03;
product_alpha = max(0, product_alpha);
text_xoff += (1 - text_xoff) / 10;
text_yoff += (1 - text_yoff) / 10;

if (product_index != product_index_prev) itime_product = 0;
itime_product ++;

if (selected) {
	//if (!surface_exists(outline_surf)) {
	//	outline_surf = surface_create(sprite_width + 3, sprite_height + 3);
	//	surface_set_target(outline_surf);
		
	//	var xx = sprite_get_xoffset(sprite_index) + 1, yy = sprite_get_yoffset(sprite_index) + 1;
	//	draw_sprite(sprite_index, image_index, xx, yy);
		
	//	surface_reset_target()
	//}
	
	//outline_alpha += (1 - outline_alpha) / 5;
	
	if (player.key_interact && !player_bought) {
		global.game_mod = "trading";
	}
}// else {
//	if (outline_surf != -1 && outline_alpha == 0) {
//		surface_free(outline_surf);
		
//		outline_surf = -1;
//	}
	
//	outline_alpha += (0 - outline_alpha) / 5;
//}

if (mod_prev == global.game_mod) {
	if (global.game_mod == "trading" && selected) {
		product_index += axis_product;
		product_index = clamp(product_index, 0, product_number - 1);
		
		if (key_interact && product_index != 0) {
			var prod_price = ds_map_find_value(global.map_items[product_array[product_index]], "Price");
			if (player.money >= prod_price) {
				global.game_mod = "game";
			
				var map = global.map_items[product_array[product_index]];
				item_create(player.x, player.y, true, false, map);
				
				//with(instance_create_layer(player.x, player.y, layer_other, loot)) {
				//	player_seen = true;
				//	need_show = false;
				//	to_add = player.slot[player.slot_index];
				
				//	sprite_index = to_add[? "Icon"];
				//}
			
				var map = global.map_items[product_array[product_index]];
				//player.slot[player.slot_index] = map;
				player.money -= map[? "Price"];
			
				player_bought = true;
			} else {
				product_alpha = 1.5;
				
				var off = 5;
				text_xoff += off * irandom_range(-1, 1);
				text_yoff += off * irandom_range(-1, 1);
			}
		}
		
		depth = -9999;
	
		window_x1 += (window_x1_to - window_x1) / window_spd;
		window_y1 += (window_y1_to - window_y1) / window_spd;
		window_x2 += (window_x2_to - window_x2) / window_spd;
		window_y2 += (window_y2_to - window_y2) / window_spd;
		
		window_alpha += (1 - window_alpha) / window_spd_alpha;
	
		product_y += (product_y_to - product_y) / product_spd;
		product_scale += (1 - product_scale) / product_spd;
	} else {
		depth = -bbox_bottom;
		
		window_x1 += (window_x1_start - window_x1) / window_spd;
		window_y1 += (window_y1_start - window_y1) / window_spd;
		window_x2 += (window_x2_start - window_x2) / window_spd;
		window_y2 += (window_y2_start - window_y2) / window_spd;
		
		window_alpha += (0 - window_alpha) / window_spd_alpha;
	
		product_y += (product_y_start - product_y) / product_spd;
		product_scale += (0 - product_scale) / product_spd;
		
		if (player_bought) {
			draw = false;
			image_alpha = 0;
			
			if (window_x1 - window_x1_start == 0) {
				//if (surface_exists(outline_surf)) surface_free(outline_surf);
			
				instance_destroy(self);
			}
		}
	}
}

mod_prev = global.game_mod;
product_index_prev = product_index;