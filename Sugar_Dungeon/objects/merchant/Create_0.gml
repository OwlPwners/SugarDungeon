event_inherited();
save_object_add_variables(save_object_is_merchant_array);

depth = -bbox_bottom;

clamp_box_id = -1000;

//input_plus = ini_load(file_settings, "Real", "Control", "PlusSlotKey", vk_right);
//input_minus = ini_load(file_settings, "Real", "Control", "MinusSlotKey", vk_left);
input_interact = key_interact_load;
key_interact = false;

text_close = ini_translate(file_translate_merchant, global.game_lang, "close");
text_xoff = 0;
text_yoff = 0;

//product_array = [global.map_items[item.plainspear], global.map_items[wp_lollipop], global.map_items[wp_candycane]];
var size = array_length(global.map_items), i = 1;
repeat(3) {
	var ind = irandom(size - 1), map = global.map_items[ind], col = map[? "Rarity"] > 100;
	while(col) {
		ind = irandom(size - 1);
		map = global.map_items[ind];
		col = map[? "Rarity"] > 100;
	}
	
	product_array[i] = ind;
	
	i ++;
}

product_array[0] = -1;

product_number = array_length(product_array);
product_index = 0;
product_index_prev = 0;

itime_product = 0;
product_alpha = 0;

window_x1 = gui_width / 2;
window_y1 = gui_height / 2;
window_x2 = gui_width / 2;
window_y2 = gui_height / 2;

window_x1_start = gui_width / 2;
window_y1_start = gui_height / 2;
window_x2_start = gui_width / 2;
window_y2_start = gui_height / 2;

window_x1_to = 64;
window_y1_to = 64;
window_x2_to = gui_width - 64;
window_y2_to = gui_height - 64;

window_spd = 15;

window_alpha = 0;
window_spd_alpha = 10;


product_y = -gui_height / 4;
product_y_start = -gui_height / 4;
product_y_to = gui_height / 2;
product_scale = 0;
product_spd = 15;

mod_prev = -1;

player_bought = false;

draw = true;

//outline_alpha = 0;
//outline_surf = -1;
//selected = false;