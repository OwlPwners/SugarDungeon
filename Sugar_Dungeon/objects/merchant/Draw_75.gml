if (window_x2 - window_x2_start < 5) exit;

var part_size, xscale, yscale;
part_size = sprite_get_width(spr_merch_window) / 3;
xscale = (1 / part_size) * (window_x2 - window_x1 - part_size);
yscale = (1 / part_size) * (window_y2 - window_y1 - part_size);

draw_gui_box(spr_merch_window, 0, window_x1, window_y1, window_x2, window_y2, c_white, window_alpha);

for(var i = 0; i < product_number; i ++) {
	if (product_array[i] != -1) {
		var map = global.map_items[product_array[i]], spr_width, spr_height, xscale, yscale, xoff, yoff, xx, yy, ys;
		spr_width = sprite_get_width(map[? "Icon"]);
		spr_height = sprite_get_height(map[? "Icon"]);
		xoff = sprite_get_xoffset(map[? "Icon"]);
		yoff = sprite_get_yoffset(map[? "Icon"]);
	
		xscale = (1 / spr_width) * 48 * product_scale;
		yscale = (1 / spr_height) * 48 * product_scale;
		ys = 0;
		
		if (i == product_index) {
			xscale *= 1.25;
			yscale *= 1.25;
			ys = dsin(itime_product) * 5;
		}
	
		xx = gui_width / 2 - spr_width * xscale / 2 + (i - (product_number - 1) / 2) * (spr_width * xscale + 48) + xoff * xscale;
		yy = product_y - spr_height * yscale / 2 + yoff * xscale + ys; 
	
		draw_sprite_ext(map[? "Icon"], 0, xx, yy, xscale, yscale, 0, c_white, 1);
		if (product_alpha > 0 && i == product_index) draw_sprite_blend_ext(map[? "Icon"], 0, xx, yy, xscale, yscale, merchant_color_cantbuy, product_alpha);
	
		if (i == product_index) {
	 		draw_set_font(global.font_game);
			draw_set_halign(fa_middle);
			draw_set_valign(fa_center);
			
			var tx, ty, tscl;
			tx = xx + (spr_width - xoff * 2) * xscale / 2 + text_xoff;
			ty = yy + (spr_height - yoff / 1.5) * yscale * 1.5 + text_yoff;
			tscl = 0.7
			
			draw_text_transformed(tx, ty, map[? "Price"], tscl, tscl, 0);
			
			draw_set_halign(fa_left);
			draw_set_valign(fa_top);
		}
	} else {
		var spr_width, spr_height, xscale, yscale, xx, yy;
		spr_width = sprite_get_width(spr_merch_exit);
		spr_height = sprite_get_height(spr_merch_exit);
	
		xscale = (1 / spr_width) * 48 * product_scale;
		yscale = (1 / spr_height) * 48 * product_scale;
	
		if (i == product_index) {
			xscale *= 1.25;
			yscale *= 1.25;
		}
	
		xx = gui_width / 2 - spr_width * xscale / 2 + (i - (product_number - 1) / 2) * (spr_width * xscale + 48);
		yy = product_y - spr_height * yscale / 2;
	
		draw_sprite_ext(spr_merch_exit, 0, xx, yy, xscale, yscale, 0, c_white, 1);
	
		if (i == product_index) {
	 		if (key_interact && global.game_mod == "trading") global.game_mod = "game";
			
			draw_set_font(global.font_game);
			draw_set_halign(fa_middle);
			draw_set_valign(fa_center);
		
			draw_text_transformed(xx + spr_width * xscale / 2, yy + spr_height * yscale * 1.5, text_close, 0.7, 0.7, 0);
		
			draw_set_halign(fa_left);
			draw_set_valign(fa_top);
		}
	}
}