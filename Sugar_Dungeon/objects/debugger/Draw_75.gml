if (!global.debug) exit;

var str = window_get_caption(), str_w = string_width(str), str_h = string_height(str), scl = 0.33;
draw_text_transformed(gui_width - str_w * scl, gui_height - str_h * scl, str, scl, scl, 0);

var gp = global.gp_connect_id > -1;
str = "Gp: " + string(gp);
str_w = string_width(str);
str_h = string_height(str);

draw_text_transformed(str_w * scl, gui_height - str_h * scl, str, scl, scl, 0);