if (!draw) exit;
 
var x1, y1, x2, y2;
x1 = x - txt_width * 0.5; 
y1 = y - txt_height * 0.5;
x2 = x + txt_width * 0.5; 
y2 = y + txt_height * 0.5;

draw_gui_box(sprite_text_box, image_text_box, x1, y1, x2, y2, c_white, alpha);

if (text != "") {
	rendering.text();
}

x2 += spr_w_dist + off_x;
y2 -= (txt_height + spr_h) / 2 - off_y;

draw_sprite_ext(sprite_circle_box, image_circle, x2, y2, 1, 1, 0, c_white, alpha);