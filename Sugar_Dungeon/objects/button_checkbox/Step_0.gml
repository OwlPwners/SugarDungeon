change.moving();
change.alphing();

txt_width = string_width(text) * 0.5 + menu_text_border_width;
txt_height = string_height(text) * 0.5 + menu_text_border_height;

spr_w = sprite_get_width(sprite_circle_box);
spr_h = sprite_get_height(sprite_circle_box);
off_x = sprite_get_xoffset(sprite_circle_box);
off_y = sprite_get_yoffset(sprite_circle_box);

spr_w_dist = spr_w;

var x1, y1, x2, y2;
x1 = x + txt_width * 0.5 + spr_w_dist;
y1 = y - spr_h / 2;
x2 = x1 + spr_w;
y2 = y1 + spr_h;

var col_cursor, mx, my;
if (instance_exists(cursor)) {
	mx = cursor.x;
	my = cursor.y;
} else {
	mx = device_mouse_x_to_gui(0);
	my = device_mouse_y_to_gui(0);
}

col_cursor = point_in_rectangle(mx, my, x1, y1, x2, y2);

var click;
click = check_mouse.released();

if (click && col_cursor && alpha > 0.8) {
	if (script > -1) script_execute(script);
	
	if (image_circle == image_circle_pressed) image_circle = image_circle_released; 
	else image_circle = image_circle_pressed;
}