event_inherited();

exists_timer --;
if (exists_timer <= 0) {
	image_alpha -= 0.08;
	if (image_alpha <= 0) instance_destroy(self);
} else {
	image_alpha += (1 - image_alpha) / 15;
}