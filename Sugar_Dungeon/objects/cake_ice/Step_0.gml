if (!enemy_check_mode()) exit;

set_depth();

z_max_height = 200;

part_snow_burst(x, bbox_top + z, irandom(2));

var dist = 128;
if (distance_to_object(player) <= dist && !tile_collision_line(x, y, player.x, player.y, layer_collision, 4)) {
	activated = true;
}

if (activated) {
	if (!activated_prev) {
		xto = player.x;
		yto = player.y;

		var f = point_distance(x, y, xto, yto) + point_distance(0, 0, sprite_width, sprite_height);
		zsp = -1 * f / grav / 60 / spd;
		
		//timer = 120;
	}
	
	//timer --;
	
	zsp += grav;
	
	var dx, dy, len;
	dx = xto - x;
	dy = yto - y;
	len = sqrt(dx * dx + dy * dy);
	if (len != 0) {
		dx /= len;
		dy /= len;
	}
	
	hsp = dx * spd;
	vsp = dy * spd;
	
	x += hsp;
	y += vsp;
	z += zsp;
	
	if (z + zsp > z_floor) {
		instance_create_depth(x, y, depth, cake_ice_circle);
		
		part_snow_destroy();
		instance_destroy(self);
		clamp_box_id.enemy_num_lives --;
	}
}

activated_prev = activated;