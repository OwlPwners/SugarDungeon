event_inherited();

keystring = "";
keyini = "";
def = 0;

text = keystring;

write_key = false;

init.moving();
init.alpha();
init.image();

width = string_width(text) * 0.5 + menu_text_border_width;
height = string_height(text) * 0.5 + menu_text_border_height;