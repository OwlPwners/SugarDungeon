if (!draw) exit;

if (check_mouse.collision()) {
	image = image_pressed;
} else {
	image = image_released;
}

if (text != "") {
	rendering.box();
	rendering.text();
} else {
	rendering.sprite();
}