change.moving();
change.alphing();

is_pressed = (image == image_pressed) && (mouse_check_button_pressed(mb_left) || cursor.click_released);
if (is_pressed && alpha > 0.8) {
	write_key = true;
}

if (write_key) {
	if (keyboard_check(vk_anykey)) {
		write_key = false;
		
		var key = keyboard_lastkey;
		ini_save(file_settings, "Real", "Control", keyini, key);
		
		var set_text = "";
				
		switch(keyboard_lastkey) {
			#region : - )
			case(vk_left) : 
			set_text = "LEFT"
			break;
					
			case(vk_right) : 
			set_text = "RIGHT"
			break;
					
			case(vk_up) : 
			set_text = "UP";
			break;
					
			case(vk_down) : 
			set_text = "DOWN";
			break;
					
			case(vk_tab) : 
			set_text = "TAB";
			break;
					
			case(vk_enter) : 
			set_text = "ENTER";
			break;
					
			case(vk_pagedown) : 
			set_text = "PAGEDOWN";
			break;
					
			case(vk_pageup) : 
			set_text = "PAGEUP";
			break;
					
			case(vk_pause) : 
			set_text = "PAUSE";
			break;
					
			case(vk_shift) : 
			set_text = "SHIFT";
			break;
					
			case(vk_lshift) : 
			set_text = "L-SHIFT";
			break;
					
			case(vk_rshift) : 
			set_text = "R-SHIFT";
			break;
					
			case(vk_alt) : 
			set_text = "ALT";
			break;
					
			case(vk_lalt) : 
			set_text = "L-ALT";
			break;
					
			case(vk_ralt) : 
			set_text = "R-ALT";
			break;
					
			case(vk_delete) : 
			set_text = "DELETE";
			break;
					
			case(vk_backspace) : 
			set_text = "BACKSPACE";
			break;
					
			case(vk_space) : 
			set_text = "SPACE";
			break;
					
			case(vk_control) : 
			set_text = "CTRL";
			break;
					
			case(vk_lcontrol) : 
			set_text = "L-CTRL";
			break;
					
			case(vk_rcontrol) : 
			set_text = "R-CTRL";
			break;
					
			default :
			set_text = chr(keyboard_lastkey);
			break;
			#endregion
		}
		
		text = keystring + set_text;
		
		width = string_width(text) * 0.5 + menu_text_border_width;
		height = string_height(text) * 0.5 + menu_text_border_height;
	}
}