enemy_create(spr_icecream_ball_blueberry_stop, spr_icecream_ball_blueberry_run, spr_icecream_ball_blueberry_attack, spr_icecream_ball_blueberry_hitbox, -1, 1.45, 18, 2.3, 6, tile_size * 3, 300, 0.6, 4, sfx_attack1, [sfx_enemy_damaged1, sfx_enemy_damaged2, sfx_enemy_damaged3], 3);

ball_created = false;

script_attack = function() {
	if (state_prev != state) {
		image_index = 0;
	}
	
	//if (sound_attack > -1 && !sound_attack_played) {
	//	audio_play_sound(sound_attack, 99, false);
	//	sound_attack_played = true;
	//}
	
	sprite_index = sprite_attack;
	
	if (image_index == clamp(image_index, 16, 19)) {
		if (!ball_created) {
			var id_ball = instance_create_depth(x, y, depth - 1, blueberry_ball);
			with(id_ball) {
				zsp = (point_distance(x, y, 
				lerp(player.x, player.x + player.hsp, 60), 
				lerp(player.y, player.y + player.vsp, 60)) + z_max) / grav / 60 / spd * -1;
				
				var dx, dy, len;
				dx = player.x - x;
				dy = player.y - y;
				
				len = sqrt(dx * dx + dy * dy);
				if (len != 0) {
					dx /= len;
					dy /= len;
				}
				
				x_dir = dx;
				y_dir = dy;
			}
		
			ball_created = true;
		}
	} else {
		ball_created = false;
	}
	
	hsp = 0;
	vsp = 0;
}
state_map[? "attack"] = script_attack;

state_controller.check_attack = function () {
	return (point_distance(x, y, player.x, player.y) < see_dist / 3);
}