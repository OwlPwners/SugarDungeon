x = target.x + lengthdir_x(distx, dir);
y = target.y + lengthdir_y(disty, dir);
image_xscale = target.image_xscale;
image_angle = 0;

if (!active || !ds_exists(map, ds_type_map)) exit;

set_depth();

if (state != "attack" && key_attack) {
	state = "attack";
}

switch(map[? "Type"]) {
	case("Melee") :
	var scr = map[? "ScriptBegin"];
	if (scr != -1) script_execute(scr);
		
	switch(state) {
		case("stop") :
		sprite_index = map[? "Sprite"];
		break;
	
		case("attack") :
		var spr = map[? "SpriteAttack"], mask = map[? "Mask"], sound = map[? "Sound"], animated = map[? "SpriteAnimated"];
		if (animated) {
			var sprite_list = ds_list_create(), sprite_string = string(spr), 
			mask_list = ds_list_create(), mask_string = string(mask),
			sound_list = ds_list_create(), sound_string = string(sound),
			min_list = ds_list_create(), min_string = map[? "ImageMin"], 
			max_list = ds_list_create(), max_string = map[? "ImageMax"], 
			list_len;
			
			ds_list_read(sprite_list, sprite_string)
			ds_list_read(mask_list, mask_string);
			ds_list_read(sound_list, sound_string);
			ds_list_read(min_list, min_string);
			ds_list_read(max_list, max_string);
			
			list_len = ds_list_size(sprite_list);
		
			if (state != state_prev) {
				sprite_index = sprite_list[| combo_index];
				mask_index = mask_list[| combo_index];
				
				audio_play_sound(sound_list[| combo_index], 100, false);
			}
		
			var image_posi = (min_list[| combo_index] > 0) && (image_index > min_list[| combo_index]) && (image_index < max_list[| combo_index]);
			if (set_new_combo && state == state_prev && image_posi) {
				combo_index ++;
				combo_index = min(combo_index, list_len - 1);
			
				set_new_combo = false;
			
				image_index = 0;
			
				sprite_index = sprite_list[| combo_index];
				mask_index = mask_list[| combo_index];
				
				audio_play_sound(sound_list[| combo_index], 100, false);
			}
			
			if (combo_index != combo_index_prev) attacked = false;
			
			ds_list_destroy(sprite_list);
			ds_list_destroy(mask_list);
			ds_list_destroy(min_list);
			ds_list_destroy(max_list);
		} else {
			sprite_index = spr;
			mask_index = mask;
			
			if (state != state_prev) audio_play_sound(sound, 100, false);
		}
	
		scr = map[? "ScriptEnd"];
		if (scr != -1) script_execute(scr);
		break
	}
	break;
	
	case("Potion") :	
	switch(state) {
		case("stop") :
		sprite_index = map[? "Icon"];
		break;
	
		case("attack") :
		if (state != state_prev) audio_play_sound(sfx_dringing_potion, 98, false);
		
		target.drinking = true;
	
		map = -1;
	
		state = "stop";
		break;
	}
	
	var xoff, yoff;
	xoff = sprite_get_xoffset(sprite_index);
	yoff = sprite_get_yoffset(sprite_index);
	
	x -= sprite_width / 2 + xoff;
	y -= sprite_height / 2 + yoff;
	break;
}

state_prev = state;
combo_index_prev = combo_index;