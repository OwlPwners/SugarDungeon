draw_set_alpha(alpha);

draw_circle(x, y, rad, true);

draw_set_alpha(1);

if (collision_circle(x, y, rad, player, false, true)) {
	effect_add_fire(player, 240, 1, 3 / 240);
}

var list = ds_list_create(), num = collision_circle_list(x, y, rad, tnt_parent, false, true, list, false);
if (num > 0) {
	var i = 0;
	repeat(num) {
		var inst = list[| i];
		inst.activated = true;
		
		i ++;
	}
}
ds_list_destroy(list);