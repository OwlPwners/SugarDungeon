if (!instance_exists(player)) exit;

var x1, y1, x2, y2;
x1 = x;
y1 = y;
x2 = x1 + region_width;
y2 = y1 + region_height;

enemy_number = instance_number_region(enemy, x1, y1, x2, y2);

//enemy_num_lives = 0;
//var elements = layer_get_all_elements(layer_enemies), elements_num = array_length(elements), i = 0;
//repeat(elements_num) {
//	var idd = elements[i];
//	if (idd.clamp_box_id == id && !idd.stunned) enemy_num_lives ++;
	
//	i ++;
//}

player_in_rect = (collision_rectangle(x1, y1, x2, y2, player, false, true) != noone);

if (player_in_rect) {
	//activate_instances(x1, y1, x2, y2);
	
	enemy_in_rect = enemy_number > 0;
	if (enemy_in_rect) {
		with(player) {
			//if (!tile_meeting(layer_collision, x, y)) {
				x = clamp(x, x1, x2);
				y = clamp(y, y1, y2);
			//}
			
			//if (clamp_box_id != other.id) map_controller.bound_contr_num ++;
			
			clamping = true;
			clamp_box_id = other.id;
		}
	} else {
		with(player) {
			clamping = false;
			clamp_box_id = -1;
		}
		
		surface_free(surf);
	}
}

with(enemy) {
	if (clamp_box_id == other.id) {
		x = clamp(x, x1, x2);
		y = clamp(y, y1, y2);
	}
}

enemy_num_lives_prev = enemy_num_lives;