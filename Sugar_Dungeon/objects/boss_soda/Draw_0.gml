if (draw) draw_self();

if (flash > 0) {
	flash_check();
}

if (room == rm_redactor || global.game_mod == "pause" || global.game_mod == "show_map") exit;

if (boss_state == boss_soda_state.attack_laser) {
	var xoff = x + xtube[index_of_image], yoff = y + ytube[index_of_image];
	var xx = lengthdir_x(line_size, dir_line), yy = lengthdir_y(line_size, dir_line);

	var laserx, lasery, tile_col;
	laserx = clamp(xoff + xx, tile_size * 1.25, room_width - tile_size * 1.25);
	lasery = clamp(yoff + yy, tile_size * 1.25, room_height - tile_size * 1.25);

	tile_col = tile_collision_line_point(xoff, yoff, laserx, lasery, layer_collision);

	laserx = tile_col[tilepoint_x];
	lasery = tile_col[tilepoint_y];

	if (collision_line(xoff, yoff, laserx, lasery, player, false, true)) player.hp -= 0.725;

	if (surface_exists(surf_laser)) {
		surface_set_target(surf_laser);
		draw_clear_alpha(c_white, 0.0);
		
		draw_line_width_color(surf_size / 2, surf_size / 2, laserx - xoff + surf_size / 2, lasery - yoff + surf_size / 2, line_width, c_white, c_red);
		
		gpu_set_colorwriteenable(true, true, true, false);
		draw_sprite_tiled(spr_candylaser_bg, 0, 0, 0);
		gpu_set_colorwriteenable(true, true, true, true);
		
		surface_reset_target();
	} else {
		surf_laser = surface_create(surf_size, surf_size);
	}
	draw_surface(surf_laser, xoff - surf_size / 2, yoff - surf_size / 2);
}