if (room == rm_redactor || player.clamp_box_id != clamp_box_id || player.state == player_state.dead || global.game_mod == "pause" || global.game_mod == "show_map") exit;

if (hp > 0) {
	boss_ui_draw_healthbar(spr_boss_soda_hp, hp_draw, hp_max);
}

if (boss_state == boss_soda_state.dead) {
	if (dead_glow_time < 300) {
		draw_surface_glow(application_surface, min(dead_glow_steps, 4), dead_glow_intens, 0, 0);
	} else {
		draw = false;
		
		draw_set_alpha(dead_rectangle_alpha);
		draw_rectangle(0, 0, gui_width, gui_height, false);
		draw_set_alpha(1);
	}
}