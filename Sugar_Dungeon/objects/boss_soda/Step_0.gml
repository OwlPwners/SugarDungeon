if (room == rm_redactor || global.game_mod == "pause" || global.game_mod == "show_map") exit;

set_depth();

intro_timer --;
war_timer --;
bubble_state_timer --;
flash -= flash_minus;

hp_draw = lerp(hp_draw, hp, 0.2);

var number = sprite_get_number(spr_boss_soda_bullet_attack) - 1;

index_of_image = floor(image_index) - 1;
index_of_image = clamp(index_of_image, 0, number);
if (index_of_image >= number) index_of_image = 0;

with(tile_destroy) {
	tile_draw = false;
}

if (hp > 0) {
	if (intro_timer <= 0 && boss_state == boss_soda_state.intro) {
		boss_state = boss_soda_state.stop;
		player.draw = true;
	
		with(camera) {
			target = player;
		}
	}

	if (bubble_state_timer <= 0) {
		boss_state = boss_soda_state.run_to_centr;
	
		bubble_state_timer = bubble_state_time;
	}
} else {
	boss_state = boss_soda_state.dead;
}

if (boss_state != boss_soda_state.attack_laser) {
	line_size = 0;
	dir_line = point_direction(x, y, player.x, player.y) + 45;
	line_spd_dir = choose(1, -1);
	line_circle_dir = 0;
} else {
	line_size += (line_size_to + irandom(line_size_plus) - line_size) / 50;
}

image_xscale += (xscale_to - image_xscale) / 5;

switch(boss_state) {
	case(boss_soda_state.intro) :
	player.draw = false;
	
	with(camera) {
		target = boss_soda;
	}
	break;
	
	case(boss_soda_state.stop) :
	sprite_index = spr_boss_soda_stop;
	
	hsp = 0;
	vsp = 0;
	break;
	
	case(boss_soda_state.run_to_player) :
	sprite_index = spr_boss_soda_run;
	
	var xdir, ydir;
	xdir = player.x - x;
	ydir = player.y - y;
	
	if (abs(xdir) < abs(sprite_width / 3.75)) xdir = 0; else xscale_to = sign(xdir);
	if (abs(ydir) < abs(sprite_height / 25)) ydir = 0;
	
	var lenght = sqrt(xdir * xdir + ydir * ydir);
	if (lenght != 0) {
		xdir /= lenght;
		ydir /= lenght;
	} else {
		if (boss_state != boss_soda_state.attack_bubble && boss_state != boss_soda_state.attack_laser) {
			boss_state = boss_soda_state.attack_fist;
			image_index = 0;
		}
	}
	
	hsp = xdir * spd;
	vsp = ydir * spd;
	break;
	
	case(boss_soda_state.run_to_centr) :
	sprite_index = spr_boss_soda_run;
	
	var xdir, ydir;
	xdir = clamp_box_id.x + clamp_box_id.region_width / 2 - x;
	ydir = clamp_box_id.y + clamp_box_id.region_height / 2 - y;
	
	if (abs(xdir) < abs(sprite_width / 3.75)) xdir = 0; else xscale_to = sign(xdir);
	if (abs(ydir) < abs(sprite_height / 25)) ydir = 0;
	
	var lenght = sqrt(xdir * xdir + ydir * ydir);
	if (lenght != 0) {
		xdir /= lenght;
		ydir /= lenght;
	} else {
		if (hp < hp_max / 2) boss_state = boss_soda_state.attack_laser;
		else boss_state = boss_soda_state.attack_bubble;
	}
	
	hsp = xdir * spd;
	vsp = ydir * spd;
	break;
	
	case(boss_soda_state.attack_fist) :
	sprite_index = spr_boss_soda_meele_attack;
	mask_index = spr_boss_soda_meele_mask_attack
	
	var player_inst = instance_place(x, y, player);
	if (player_inst != noone && can_attack) {
		if (player_inst.war_timer <= 0) {
			can_attack = false;
			
			player_inst.flash = 1;
			player_inst.war_timer = player_inst.war_time;
					
			player_inst.hp -= fist_damage;
			bounce_set(player_inst.x, player_inst.y, x, y, fist_bounce_force, player_inst);
			
			var s = 15;
			camera.x += random_range(-s, s);
			camera.y += random_range(-s, s);
		}
	}
	
	//if (place_meeting(x, y, player)) {
	//	var player_inst = instance_place(x, y, player);
	//	if (player_inst != noone && can_attack == true) {
	//		if (player_inst.war_timer <= 0) {
	//			can_attack = false;
				
	//			player_inst.hp -= fist_damage;
	//			player_inst.flash = 1;
	//			player_inst.war_timer = player_inst.war_time;
	//			bounce_set(player_inst.x, player_inst.y, x, y, fist_bounce_force, player_inst);
	//		}
	//	}
	//}
	
	hsp = 0;
	vsp = 0;
	break;
	
	case(boss_soda_state.attack_bubble) :
	var player_inst = instance_place(x, y, player);
	if (player_inst != noone && can_attack) {
		if (player_inst.war_timer <= 0) {
			can_attack = false;
			
			player_inst.flash = 1;
			player_inst.war_timer = player_inst.war_time;
					
			player_inst.hp -= fist_damage;
			bounce_set(player_inst.x, player_inst.y, x, y, fist_bounce_force, player_inst);
			
			var s = 15;
			camera.x += random_range(-s, s);
			camera.y += random_range(-s, s);
		}
	}
	
	sprite_index = spr_boss_soda_bullet_attack;
	xscale_to = 1;
	
	bubble_attack_timer --;
	
	if (bubble_attack_timer <= 0 && bubble_created < bubble_number) {
		instance_create_depth(x + xtube[index_of_image], y + ytube[index_of_image], depth - 1, soda_bullet);
		
		bubble_attack_timer = bubble_attack_time;
		bubble_created ++;
		
		bubble_state_timer = bubble_state_time;
		
		if (bubble_created == bubble_number) {
			boss_state = boss_soda_state.run_to_player;
			
			bubble_created = 0;
		}
	}
	
	hsp = 0;
	vsp = 0;
	break;
	
	case(boss_soda_state.attack_laser) :
	var player_inst = instance_place(x, y, player);
	if (player_inst != noone && can_attack) {
		if (player_inst.war_timer <= 0) {
			can_attack = false;
			
			player_inst.flash = 1;
			player_inst.war_timer = player_inst.war_time;
					
			player_inst.hp -= fist_damage;
			bounce_set(player_inst.x, player_inst.y, x, y, fist_bounce_force, player_inst);
			
			var s = 15;
			camera.x += random_range(-s, s);
			camera.y += random_range(-s, s);
		}
	}
	
	with(tile_destroy) {
		tile_draw = true;
	}
	
	sprite_index = spr_boss_soda_bullet_attack;
	xscale_to = 1;
	
	bubble_state_timer = bubble_state_time;
	
	if (line_circle_dir >= 1800) {
		boss_state = boss_soda_state.run_to_player;
	}
	
	dir_line += line_spd * line_spd_dir;
	line_circle_dir += abs(line_spd * line_spd_dir);
	
	hsp = 0;
	vsp = 0;
	break;
	
	case(boss_soda_state.dead) :
	depth = player.depth - 1;
	
	if (boss_state_prev != boss_state) {
		player.money += 30;
		player.hp += 15;
		audio_stop_all();
		
		global.game_music = -1;
	}
	
	sprite_index = spr_boss_soda_stop;
	
	if (draw) {
		with(camera) {
			target = boss_soda;
			shake = true;
			shake_x = 3;
			shake_y = 3;
		}
		
		player.draw = false;
	} else {
		with(camera) {
			target = player;
			shake = false;
		}
		
		if (dead_glow_time > 200) dead_rectangle_alpha -= 0.025;
		
		if (dead_rectangle_alpha <= 0) {
			with(instance_create_depth(x, y, depth, nextlevel_controller)) chapter = chapter_diving;
			audio_stop_all();
			instance_destroy(self);
			player.draw = true;
		}
	}
	
	dead_glow_steps += 0.25;
	dead_glow_intens += 0.075;
	dead_glow_time ++;
	
	hsp = 0;
	vsp = 0;
	break;
}

boss_state_prev = boss_state;

mask_index = spr_boss_soda_stop;
	
var xcheck, ycheck;
xcheck = hsp + sign(hsp);
ycheck = vsp + sign(vsp);
	
if (tile_meeting(layer_collision, x + xcheck, y)) {
	repeat(abs(xcheck)) {
		if (!tile_meeting(layer_collision, x + sign(hsp), y)) {
			x += sign(hsp);
		} else {
			break;
		}
	}
		
	hsp = 0;
}
	
x += hsp;
	
if (tile_meeting(layer_collision, x, y + ycheck)) {
	repeat(abs(ycheck)) {
		if (!tile_meeting(layer_collision, x, y + sign(vsp))) {
			y += sign(vsp);
		} else {
			break;
		}
	}
	
	vsp = 0;
}
	
y += vsp;