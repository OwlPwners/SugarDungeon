event_inherited();

sprite_index = spr_boss_soda_stop;
xscale_to = 1;

var xoff = -sprite_get_xoffset(spr_boss_soda_bullet_attack), yoff = -sprite_get_yoffset(spr_boss_soda_bullet_attack);
xtube = [xoff + 37, xoff + 38, xoff + 39, xoff + 40, xoff + 41, xoff + 41, xoff + 42, xoff + 43, xoff + 43, xoff + 44, xoff + 45, xoff + 46, xoff + 46, xoff + 47, xoff + 48, xoff + 49, xoff + 49, xoff + 50, xoff + 50, xoff + 51, xoff + 50, xoff + 50, xoff + 49, xoff + 48, xoff + 47, xoff + 47, xoff + 46, xoff + 45, xoff + 44, xoff + 44, xoff + 43, xoff + 42, xoff + 42, xoff + 40, xoff + 39];
ytube = [yoff + 29, yoff + 28, yoff + 28, yoff + 28, yoff + 27, yoff + 27, yoff + 27, yoff + 27, yoff + 26, yoff + 26, yoff + 26, yoff + 26, yoff + 26, yoff + 26, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 25, yoff + 26, yoff + 26, yoff + 26, yoff + 26, yoff + 26, yoff + 27, yoff + 27, yoff + 27, yoff + 28, yoff + 28];
index_of_image = 0;

hsp = 0;
vsp = 0;
spd = 2.1;

hp_max = 30; //220;
hp = hp_max;
hp_draw = 0;

can_attack = true;

enum boss_soda_state {
	intro,
	stop,
	run_to_player,
	run_to_centr,
	attack_fist,
	attack_bubble,
	attack_laser,
	dead
}

state = -1; // To prevent errors.
boss_state = boss_soda_state.intro;
boss_state_prev = boss_state;

intro_timer = 180;

war_time = 60;
war_timer = war_time;
flash = 0;

fist_damage = 2;
fist_bounce_force = 5.75;

bubble_attack_time = 45;
bubble_attack_timer = bubble_attack_time;
bubble_number = 3;
bubble_created = 0;
bubble_state_time = 450;
bubble_state_timer = bubble_state_time;

dir_line = 0;
line_spd = 2.25;
line_spd_dir = 1;
line_width = 3.25;
line_size = 0;
line_size_to = 320;
line_size_plus = 200;
surf_size = (line_size_to + line_size_plus) * 2;
surf_laser = surface_create(surf_size, surf_size);

dead_rectangle_alpha = 1;
dead_glow_steps = 0;
dead_glow_intens = 0;
dead_glow_time = 0;

draw = true;

sound_damaged = [sfx_enemy_damaged1, sfx_enemy_damaged2, sfx_enemy_damaged3];

depth_variables();
save_object_add_variables("hp", "intro_timer", "boss_state");

audio_group_load(agp_boss_soda_music);
global.game_music = so_boss_soda;

with(tile_destroy) {
	sprite_tile = spr_tile_destroy_effect1;
}