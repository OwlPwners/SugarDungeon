var map = slot[slot_index];
if (state == player_state.drinking) {
	shader_set(shd_player_drinking);
	var map = slot[slot_index], col = map[? "Color"], uni = shader_get_uniform(shd_player_drinking, "Color");
	shader_set_uniform_f(uni, color_get_red(col) / 255, color_get_green(col) / 255, color_get_blue(col) / 255);
	
	draw_self();
	
	shader_reset();
} else {
	draw_self();
}

if (state != player_state.dead) flash_check();

if (instance_exists(shield_id)) with(shield_id) {
	if (active) draw_self();
}