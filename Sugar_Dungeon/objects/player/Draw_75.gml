var mode = global.game_mod;
if (mode == "pause" || mode == "show_item" || !draw) exit;

if (state != player_state.dead) {
	var i = 0;
	repeat(slot_number) {
		var xx, yy;
		xx = ui.slot.x + (i - (slot_number - 1) / 2) * (slot_width + slot_between);
		yy = ui.slot.y;
	
		draw_sprite(spr_slot, i == slot_index, xx, yy);
	
		if (slot[i] != -1) {
			var sprite, spr_width, spr_height;
			sprite = ds_map_find_value(slot[i], "Icon");
			if (sprite != undefined) {
				spr_width = sprite_get_width(sprite);
				spr_height = sprite_get_height(sprite);
		
				draw_sprite_ext(sprite, 0, xx, yy, 1 / spr_width * slot_width, 1 / spr_height * slot_height, 0, c_white, 1.0);
			}
		}
	
		i ++;
	}

	var xx, yy;
	xx = ui.healtbar.x;
	yy = ui.healtbar.y;
	
	draw_sprite(spr_player_hp, 0, xx, yy);
	draw_set_color(player_hp_color);
	if (hp_bar > 0) draw_rectangle(xx, yy, xx + 83 * hp_bar / hp_max, yy + 7, false);
	draw_set_color(c_white);
	
	xx = ui.money.x;
	yy = ui.money.y;
	
	draw_sprite(spr_currency, 0, xx, yy);
	
	if (money_flash > 0) {
		var u_alpha;
		u_alpha = shader_get_uniform(shd_flash, "Alpha");

		shader_set(shd_flash);
		shader_set_uniform_f(u_alpha, money_flash);
	
		draw_sprite(spr_currency, 0, xx, yy);
	
		shader_reset();
	}

	var get_font = draw_get_font(), get_h = draw_get_halign(), get_v = draw_get_valign();

	draw_set_font(global.font_game);
	draw_set_halign(fa_right);
	draw_set_valign(fa_top);

	var text;
	text = string(round(money_draw)) + "/" + string(money_max);
	xx -= slot_between / 3;
	yy += sprite_get_height(spr_currency) / 2;

	draw_text(xx, yy, text);
 
	draw_set_font(get_font);
	draw_set_halign(get_h);
	draw_set_valign(get_v);
} else {
	draw_set_alpha(dead_rect_alpha);
	draw_rectangle(0, 0, gui_width, gui_height, false);
	draw_set_alpha(1);
	
	var cx, cy;
	cx = camera.x - gui_width / 2;
	cy = camera.y - gui_height / 2;
	draw_sprite_blend(sprite_index, image_index, x - cx, y - cy, c_black, dead_rect_alpha);
	
	with(enemy) draw_sprite_blend(sprite_index, image_index, x - cx, y - cy, c_black, other.dead_rect_alpha);
}