var mode = global.game_mod;
if (mode == "pause" || mode == "show_item" || mode == "show_map" || mode == "trading") {
	image_speed = 0;
	
	exit;
} else if (state != player_state.dead) image_speed = 1;

set_depth();

glide_timer --;
war_timer --;
part_step_timer ++;

block = false;

money_flash -= flash_minus;
flash -= flash_minus;

if (money_draw != money) money_flash = 1;

money_draw = lerp(money_draw, money, 0.2);
money = clamp(money, 0, money_max);

hp_bar = lerp(hp_bar, hp, 0.2);
hp = min(hp, hp_max);

if (hp <= 0) state = player_state.dead;

if (instance_exists(camera)) {
	//camera.zoom = true;
	
	var wt, ht;
	wt = gui_width;
	ht = gui_height;
	
	if (clamping) {
		cam_scl += (0.8 - cam_scl) / 10;
	
		camera.deactivate = false;
	} else {
		cam_scl += (1 - cam_scl) / 10;
		
		camera.deactivate = true;
	}
	
	with(camera) {
		var camspd = 10;
		cam_w += (wt * other.cam_scl - cam_w) / camspd;
		cam_h += (ht * other.cam_scl - cam_h) / camspd;
	}
}

var key_attack, key_block, slot_axis;

var gp_id = global.gp_connect_id;
if (gp_id == -1) {
	var key_left, key_right, key_up, key_down, key_glide;
	key_left = keyboard_check(input_left);
	key_right = keyboard_check(input_right);
	key_up = keyboard_check(input_up);
	key_down = keyboard_check(input_down);
	key_attack = mouse_check_button_pressed(mb_left); //keyboard_check_pressed(input_attack);
	key_block = mouse_check_button(mb_right); //keyboard_check(input_block);
	key_glide = keyboard_check_pressed(input_glide);
	key_interact = keyboard_check_released(input_interact);

	xaxis = key_right - key_left;
	yaxis = key_down - key_up;
	
	slot_axis = mouse_wheel_down() - mouse_wheel_up(); //keyboard_check_released(input_slotp) - keyboard_check_released(input_slotm);
	
	attack_dir = point_direction(x, y, mouse_x, mouse_y);
	
	var fast_slot_key = keyboard_key - 49;
	if (fast_slot_key == clamp(fast_slot_key, 0, 2)) slot_index = fast_slot_key;
	
	scl = sign(mouse_x - x);
} else {
	var input_hor, input_ver, input_hor_r, input_ver_r;
	input_hor = gamepad_axis_value(gp_id, gp_axislh);
	input_ver = gamepad_axis_value(gp_id, gp_axislv);
	input_hor_r = gamepad_axis_value(gp_id, gp_axisrh);
	input_ver_r = gamepad_axis_value(gp_id, gp_axisrv);
	
	xaxis = (abs(input_hor) > gp_deadzone) * sign(input_hor);
	yaxis = (abs(input_ver) > gp_deadzone) * sign(input_ver);
	
	key_attack = gamepad_button_check(gp_id, gp_face3);
	key_block = gamepad_button_check(gp_id, gp_face2);
	key_glide = gamepad_button_check_pressed(gp_id, gp_face1);
	
	key_interact = gamepad_button_check_pressed(gp_id, gp_face4);
	
	slot_axis = gamepad_button_check_released(gp_id, gp_shoulderr) - gamepad_button_check_released(gp_id, gp_shoulderl);
	
	var near = instance_nearest(x, y, enemy); if (near == noone) near = player;
	attack_dir = point_direction(x, y, near.x, near.y);
	
	var s = abs(sign(near.x - x));
	if (s != 0) scl = s;
}

if (instance_exists(inst_selected_prev)) {
	inst_selected_prev.selected = false;
}

var inst_selected = instance_nearest(x, y, interact_parent);
if (inst_selected != noone) {
	var xx = x - sprite_xoffset + sprite_width / 2, yy = y - sprite_yoffset + sprite_height / 2,
	inst_x = inst_selected.x - inst_selected.sprite_xoffset + inst_selected.sprite_width / 2, 
	inst_y = inst_selected.y - inst_selected.sprite_yoffset + inst_selected.sprite_height / 2;
	
	if (point_distance(xx, yy, inst_x, inst_y) < selected_dist) {
		inst_selected.selected = true;
	}
}

inst_selected_prev = inst_selected;

if (instance_exists(shield_id)) {
	var st = "stop";
	if (key_block) st = "block";
	shield_id.state = st;
	shield_id.active = true;
	shield_id.dir = attack_dir;
	shield_id.image_speed = img_spd_factor;
	
	shield_id.map = slot_shield;
}

if (instance_exists(wp_id)) {
	wp_id.set_new_combo = key_attack;
	wp_id.key_attack = key_attack;
	wp_id.active = true;
	wp_id.dir = attack_dir;
	wp_id.image_speed = img_spd_factor;
	
	wp_id.map = slot[slot_index];
}

if (state != player_state.drinking) {
	slot_index += slot_axis;
	slot_index = clamp(slot_index, 0, slot_number - 1);
}

if (state != player_state.dead) {
	if (state != player_state.drinking && state != player_state.glide) {
		if (xaxis == 0 && yaxis == 0) state = player_state.stop;
		else state = player_state.run;
	
		if (key_attack) {
			wp_id.state = "attack";
		}
	
		if (key_block && instance_exists(shield_id)) {
			state = player_state.block;
		}
	}
	
	if (key_glide && glide_timer <= 0) {
		image_index = 0;
		//image_xscale = sign(xaxis + 1);
		
		glide_spd = glide_force;
		x_glide_dir = xaxis;
		y_glide_dir = yaxis;
		
		var len = sqrt(x_glide_dir * x_glide_dir + y_glide_dir * y_glide_dir);
		if (len != 0) {
			x_glide_dir /= len;
			y_glide_dir /= len;
		}
		
		if (x_glide_dir != 0 || y_glide_dir != 0) state = player_state.glide;
	}
	
	if (drinking) {
		state = player_state.drinking;
		drinking = false;
	}
	
	spd /= spd_factor;
	glide_spd /= spd_factor;
	image_speed /= img_spd_factor;
	
	spd_factor = 1;
	img_spd_factor = 1;
	damage_factor = 1;
	
	effect_system_process();
	weapon_combo_process();
	
	spd *= spd_factor;
	glide_spd *= spd_factor;
	image_speed *= img_spd_factor;
}

if (state != player_state.glide && state != player_state.dead) {
	if (scl != 0) image_xscale += (scl - image_xscale) / 3;
}

switch(state) {
	case(player_state.stop) :
	sprite_index = spr_player_stop;
		
	hsp = 0;
	vsp = 0;
	break;
	
	case(player_state.run) :
	sprite_index = spr_player_run;
	
	//if (xaxis != 0) image_xscale = xaxis;
	
	var len = sqrt(sqr(xaxis) + sqr(yaxis));
	if (len != 0) {
		var p_step;
		p_step = (part_step_timer mod 8) == 0;
		if (p_step) part_step_burst(1);
		
		xaxis /= len;
		yaxis /= len;
	}
	
	if (bounce_force != 0) {
		xaxis = 0;
		yaxis = 0;
	}
	
	hsp = xaxis * spd;
	vsp = yaxis * spd;
	break;
	
	case(player_state.drinking) :
	if (instance_exists(shield_id)) shield_id.active = false;
	if (instance_exists(wp_id)) wp_id.active = false;
	
	sprite_index = spr_player_drinking_potion;
	if (state != state_prev) image_index = 0;
		
	hsp = 0;
	vsp = 0;
	break;
	
	#region Attack...
	case(player_state.attack) :
	var map = slot[slot_index];
	if (map == -1) break;
	
	switch(map[? "Type"]) {
		case("Melee") :
		
		
		#region Melee.
		/*
		var scr = map[? "ScriptBegin"];
		if (scr != -1) script_execute(scr);
		
		var spr = map[? "Sprite"], mask = map[? "Mask"], animated = map[? "SpriteAnimated"];
		if (animated) {
			var sprite_list = ds_list_create(), sprite_string = string(spr), 
			mask_list = ds_list_create(), mask_string = string(mask), list_len;
			
			ds_list_read(sprite_list, sprite_string)
			ds_list_read(mask_list, mask_string);
			
			list_len = ds_list_size(sprite_list);
			
			if (state != state_prev) {
				sprite_index = sprite_list[| combo_index];
				mask_index = mask_list[| combo_index];
				
				debug_attack_mask = mask_index;
			}
			
			ds_list_destroy(sprite_list);
			ds_list_destroy(mask_list);
		} else {
			sprite_index = spr;
			mask_index = mask;
		}
		
		var fric = 0.85;
		hsp *= fric;
		vsp *= fric;
		
		if (mask_index != spr_player_hitbox) {
			if (place_meeting(x, y, tnt)) {
				var inst = instance_place(x, y, tnt);
				inst.activated = true;
			}
			
			if (place_meeting(x, y, enemy)) {
				var enemy_list = ds_list_create(),
				enemy_inst = instance_place_list(x, y, enemy, enemy_list, false);
		
				if (enemy_inst > 0) {
					for(var i = 0; i < enemy_inst; i ++) {
						var inst = enemy_list[| i];
						if (inst.war_timer <= 0 && inst.state != "glide") {
							inst.hp -= map[? "Damage"];
							inst.flash = 1;
							inst.state = "attack";
							bounce_set(inst.x, inst.y, x, y, map[? "Knockback"], inst);
							inst.timer_attack = 0;
							inst.war_timer = inst.war_time;
						}
					}
				}
		
				ds_list_destroy(enemy_list);
			}
		}
		
		var animated = map[? "SpriteAnimated"];
		if (state == state_prev && key_attack && animated && image_index < image_number) {
			combo_index ++;
			combo_index = min(combo_index, list_len - 1);
		}
		
		scr = map[? "ScriptEnd"];
		if (scr != -1) script_execute(scr);
		*/
		#endregion
		break;
		
		case("Potion") :
		#region Potion.
		sprite_index = map[? "Sprite"];
		
		hsp = 0;
		vsp = 0;
		#endregion
		break;
	}
	break;
	#endregion
	
	case(player_state.block) :
	if (instance_exists(wp_id)) wp_id.active = false;
	
	var bspd = 0.67;
	image_speed = bspd;
	
	var len = sqrt(sqr(xaxis) + sqr(yaxis));
	if (len != 0) {
		xaxis /= len;
		yaxis /= len;
	}
	
	hsp = spd * bspd * xaxis;
	vsp = spd * bspd * yaxis;
	
	if (hsp != 0 || vsp != 0) {
		sprite_index = spr_player_run; 
	} else {
		sprite_index = spr_player_stop;
	}
	break;
	
	case(player_state.glide) :
	if (instance_exists(shield_id)) shield_id.active = false;
	if (instance_exists(wp_id)) wp_id.active = false;
	
	if (state != state_prev) {
		part_step_burst(5);
	} else {
		part_step_burst(1);
	}
	
	sprite_index = spr_player_glide;
	mask_index = -1;
	
	if (image_xscale != 0) image_xscale = sign(image_xscale); else image_xscale = 1; // This is need because scale can be not integer.
	
	glide_timer = glide_time;
	
	glide_spd = lerp(glide_spd, 0, glide_fric);
	hsp = x_glide_dir * glide_spd * spd_factor;
	vsp = y_glide_dir * glide_spd * spd_factor;
	
	if (hsp != 0) image_xscale = sign(hsp);
	break;
	
	case(player_state.dead) :
	audio_stop_all();
	
	flash = 0;
	
	if (instance_exists(shield_id)) shield_id.active = false;
	if (instance_exists(wp_id)) wp_id.active = false;
	
	statistic_manager.need_save = true;
	
	sprite_index = spr_player_dead;
	mask_index = -1;
	
	with(enemy) {
		if (!place_meeting(x, y, player)) instance_destroy(); else {
			image_index = 0;
			flash = 0;
		}
	}
	
	if (dead_img_spd) {
		image_index = image_number - 1;
		image_speed = 0;
	}
	
	dead_rect_alpha += (0 - dead_rect_alpha) / 25;
	
	if (image_xscale > 0) image_xscale = sign(image_xscale); else image_xscale = 1;
	
	hsp = 0;
	vsp = 0;
	
	global.game_mod = "player_dead";
	
	if (state != state_prev) {
		if (file_exists(file_room)) file_delete(file_room);
		if (file_exists(file_rooms_list)) file_delete(file_rooms_list);
		if (file_exists(file_chapter)) file_delete(file_chapter);
		
		with(statistic_manager) {
			save_data_all();
		}
	}
	
	dead_timer --;
	
	if (dead_timer > 0) {
		if (!statistic_manager.show_results) {
			with(camera) {
				// 27, 16/9;
			
				zoom_w = 432;
				zoom_h = 243;
				zoom_spd = 35;
		
				zoom = true;
			}
		}
	} else {
		dead_time += 0.07;
	}
	
	
	if (dead_time > 5.5) {
		draw = false;
		
		statistic_manager.show_results = true;
		statistic_manager.surf_blur = application_surface;
	}
	
	//image_angle += (90 - image_angle) / 25;
	
	break;
}

mask_index = spr_player_hitbox;
bounce_process();

if (state != player_state.dead) {
	var xplus = hsp + x_bounce_f, yplus = vsp + y_bounce_f;
	#region Collision.
	
	var xcheck, ycheck;
	xcheck = xplus + sign(xplus);
	ycheck = yplus + sign(yplus);
	
	if (tile_meeting(layer_collision, x + xcheck, y)) {
		repeat(abs(xcheck)) {
			if (!tile_meeting(layer_collision, x + sign(xplus), y)) {
				x += sign(xplus);
			} else {
				break;
			}
		}
		
		xplus = 0;
		
		hsp = 0;
		x_bounce_f = 0;
	}
	
	x += xplus;
	
	if (tile_meeting(layer_collision, x, y + ycheck)) {
		repeat(abs(ycheck)) {
			if (!tile_meeting(layer_collision, x, y + sign(yplus))) {
				y += sign(yplus);
			} else {
				break;
			}
		}
		
		yplus = 0;
		
		vsp = 0;
		y_bounce_f = 0;
	}
	
	y += yplus;
	
	#endregion 
}

mask_index = spr_player_stop;

state_prev = state;