hsp = 0;
vsp = 0;
spd = 3.5;

xaxis = 0;
yaxis = 0;

input_left = key_left_load;
input_right = key_right_load;
input_up = key_up_load;
input_down = key_down_load;
input_glide = key_glide_load;
input_interact = key_interact_load;
key_interact = false;

#region UI.

ui = {
	healtbar : {
		x : ini_load(file_ui_coords, "Real", "PlayerHealth", "x", sprite_get_xoffset(spr_player_hp)),
		y : ini_load(file_ui_coords, "Real", "PlayerHealth", "y", sprite_get_yoffset(spr_player_hp)),
	},
	
	money : {
		x : ini_load(file_ui_coords, "Real", "PlayerMoney", "x", gui_width - sprite_get_width(spr_currency)),
		y : ini_load(file_ui_coords, "Real", "PlayerMoney", "y", sprite_get_yoffset(spr_currency)),
	},
	
	slot : {
		x : ini_load(file_ui_coords, "Real", "PlayerSlot", "x", gui_width / 2 - slot_width / 2),
		y : ini_load(file_ui_coords, "Real", "PlayerSlot", "y", slot_between),
	}
}

#endregion

effect_system_create();
part_sparks_create();
part_step_create();

enum player_state {
	stop,
	run,
	attack,
	drinking,
	block,
	glide,
	dead
}

state = player_state.stop;

block = false;
drinking = false;

dead_timer = 180;
dead_time = 0;
dead_img_spd = false;
dead_rect_alpha = 1;

part_step_timer = 0;

money = 0;
money_max = 100;
money_draw = 0;
money_flash = 0;

glide_time = 105;
glide_timer = glide_time;

glide_spd = 0;
glide_force = 5.53;
glide_fric = 0.025;

x_glide_dir = 0;
y_glide_dir = 0;

hp_max = 22;
hp = hp_max;
hp_bar = 0;

can_attack = false;
attack_dir = 0;
cam_scl = 1; 

war_time = 45;
war_timer = 0;
war_slot_max = 10;
war_slot = 0;

damage_factor = 1;
spd_factor = 1;
img_spd_factor = 1;

combo_index = 0;
combo_index_prev = 0;

flash = 0;

clamping = false;
clamp_box_id = -3;

selected_dist = abs(sprite_width * 1.5);
inst_selected_prev = noone;

slot = [global.map_items[item.plainsword], global.map_items[item.plainspear], global.map_items[item.potion_smallives]];
slot_number = array_length(slot);
slot_index = 0;
slot_shield = global.map_items[item.plainshield];
draw = false;

depth_variables();
create_bounce_system(0.3);

wp_id = weapon_create(x, y, depth - 1, slot[slot_index], 14, 12, player);
shield_id = shield_create(x, y, depth - 1, slot_shield, -9, 1, player);