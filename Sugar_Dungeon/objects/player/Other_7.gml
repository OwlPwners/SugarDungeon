var map = slot[slot_index];
if (map != -1) {
	if (slot[slot_index] != -1) {
		switch(map[? "Type"]) {
			case("Potion") :
			if (sprite_index == map[? "Sprite"]) {
				slot[slot_index] = -1;
				
				script_execute(map[? "Script"], map);
			}
			break;
			
			//case("Melee") :
			//var scr = map[? "ScriptAnimEnd"];
			//if (scr != -1) script_execute(scr);
			//break;
		}
	}
}

if (state == player_state.drinking) {
	state = player_state.stop; 
	
	var scr = map[? "Script"];
	script_execute(scr, map);
	
	slot[slot_index] = -1;
}

if (state == player_state.dead) dead_img_spd = true; else {
	if (combo_index != combo_index_prev) {
		state = player_state.attack;
	
		state_prev = -1;
	} else {
		state = player_state.stop;
		combo_index = 0;
		can_attack = true;
	}
	
	image_index = 0;
}

glide_spd = 0;
x_glide_dir = 0;
y_glide_dir = 0;

combo_index_prev = combo_index;