if (!layer_changed) {
	layer = layer_other;
	layer_changed = true;
}

if (room == rm_redactor) exit;

if (activated) {
	with(instance_create_depth(x, y, depth, explosion)) {
		exp_scale = 0;
		exp_alpha = 1;

		exp_scale_spd = 0.2;
		exp_scale_max = 2.2;
		exp_alpha_spd = -0.025;
		exp_damage = 2;
		exp_force_bounce = 6.89;
		exp_sprite = spr_explosion;
		
		activated = true;
	}
	
	var shake_min = 5, shake_max = 8;
	camera.x += random_range(shake_min, shake_max) * random_range(-1, 1);
	camera.y += random_range(shake_min, shake_max) * random_range(-1, 1);
	
	instance_destroy(self);
}