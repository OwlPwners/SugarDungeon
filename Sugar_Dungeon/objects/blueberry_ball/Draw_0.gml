draw_set_color(c_black);
draw_set_alpha(0.5);

draw_circle(x, bbox_bottom, sprite_width / 2, false);

draw_set_color(c_white);
draw_set_alpha(1);

draw_sprite(sprite_index, image_index, x, y + z);