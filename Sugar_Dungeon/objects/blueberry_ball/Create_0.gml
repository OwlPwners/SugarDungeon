z = 0;
z_max = tile_size * 3;

x_dir = 0;
y_dir = 0;

hsp = 0;
vsp = 0;
zsp = 0;

grav = 0.3;
spd = 3.3;

damage = 1;
force_bounce = 2;