hsp = x_dir * spd;
vsp = y_dir * spd;
zsp += grav;

if ((z + zsp) >= 0) {
	z = 0;
	zsp = 0;
	
	instance_destroy(self);
}

if (place_meeting(x, y, player)) {
	if (player.war_timer <= 0 && player.glide_spd == 0) {
		player.war_timer = player.war_time;
		player.flash = 1;
					
		var inst_sh = player.shield_id, s = 10;
		if (player.state == player_state.block && instance_exists(inst_sh) && place_meeting(x, y, inst_sh)) {
			var map = inst_sh.map,
			proc_hp = (100 - map[? "BLockHp"]) / 100,
			proc_bounce = (100 - map[? "BlockKnockback"]) / 100;
						
			player.hp -= damage * proc_hp;
			bounce_set(player.x, player.y, x, y, force_bounce * proc_bounce, player);
			with(player) part_sparks_burst(x, y, 5);
				
			s *= proc_bounce;
			inst_sh.flash = 1;
		} else {
			player.hp -= damage;
			bounce_set(player.x, player.y, x, y, force_bounce, player);
		}
			
		camera.x += random_range(-s, s);
		camera.y += random_range(-s, s);
	}
}

x += hsp;
y += vsp;
z += zsp;