activated = false;
col = false;
rad = 0;

sprite_normal_map = spr_normalmap_explosion;

exp_scale = 0;
exp_alpha = 0;

exp_scale_spd = 0.07;
exp_scale_max = 2;
exp_alpha_spd = -0.03;
exp_damage = 2;
exp_force_bounce = 10;
exp_sprite = spr_explosion;

damaged = false;
animated = false;

var so = choose(sfx_explosion1, sfx_explosion2, sfx_explosion3);
audio_play_sound(so, 111, false);