rad = sprite_get_width(sprite_normal_map) / 2 * exp_scale

var list = ds_list_create(), col_tnt = collision_circle_list(x, y, rad, tnt_parent, false, true, list, false);

if (col_tnt) {
	var i = 0;
	repeat(ds_list_size(list)) {
		var inst = list[| i];
		inst.activated = true;
		
		i ++;
	}
}

ds_list_destroy(list);

col = collision_circle(x, y, rad, player, false, true);

if (col) {
	if (!activated) {
		exp_alpha = 1;
	}
	
	activated = true;
	
	if (player.glide_spd == 0 && !damaged) {
		player.flash = 1;
			
		bounce_set(player.x, player.y, x, y, exp_force_bounce, player);
					
		if (!player.block) {
			player.hp -= exp_damage;
		} else {
			var map = global.map_items[player.slot_index],
			procent_minus = (100 - map[? "ProcentBlock"]) / 100;
						
			player.hp -= damage * procent_minus;
				
			bounce_set(player.x, player.y, x, y, exp_force_bounce / 1.5, player);
		}
	}
		
	damaged = true;
}

if (activated) {
	sprite_index = exp_sprite;
	
	if (exp_alpha <= 0 && animated) {
		instance_destroy(self);
	}
	
	exp_scale += (exp_scale_max - exp_scale) * exp_scale_spd;
	exp_alpha += exp_alpha_spd;
}