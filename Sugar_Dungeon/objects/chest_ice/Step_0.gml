event_inherited();

/*
if (!instance_exists(player)) exit;

if (selected) {
	if (image_index == 0 && player.key_interact == true) {
		opened = true;
	}
	
	if (image_index != img_index_prev) outline_surf = -1;
	
	if (outline_surf == -1) {
		outline_surf = surface_create(sprite_width + 3, sprite_height + 3);
		surface_set_target(outline_surf);
		
		var xx = sprite_get_xoffset(sprite_index) + 1, yy = sprite_get_yoffset(sprite_index) + 1;
		draw_sprite(sprite_index, image_index, xx, yy);
		
		surface_reset_target()
	}
	
	outline_alpha += (1 - outline_alpha) / 5;
} else {
	if (outline_surf != -1 && outline_alpha == 0) {
		surface_free(outline_surf);
		
		outline_surf = -1;
	}
	
	outline_alpha += (0 - outline_alpha) / 5;
}

if (opened == true) {
	if (effect_instance_find_effect(player, obj_effect.notcold) == -1) {
		effect_add_freeze(player, 360);
	}
	
	opened = false;
	instance_create_layer(x, y, layer_other, loot);
	image_speed = 1;
}

img_index_prev = image_index;