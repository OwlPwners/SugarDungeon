draw = true;

init = {
	alpha : function() {
		with(other) {
			alpha_to_move = false;
	
			alpha_to = 1;
			alpha_start = 0;
			alpha_spd = 1;
			alpha = 0;
		}
	},
	
	moving : function() {
		with(other) {
			move_to_move = false;

			x_to = xstart;
			y_to = ystart;
			x_start = 0;
			y_start = 0;
			x_spd = 1;
			y_spd = 1;
		}
	},
	
	image : function() {
		with(other) {
			width = 0;
			height = 0;
		
			sprite = 0;
			image_pressed = 0;
			image_released = 0;
			image = 0;
		}
	}
}

change = {
	moving : function() {
		with(other) {
			if (move_to_move) {
				x += (x_to - x) / x_spd;
				y += (y_to - y) / y_spd;
			} else {
				x += (x_start - x) / x_spd;
				y += (y_start - y) / y_spd;
			}
		}
	},
	
	alphing : function() {
		with(other) {
			if (alpha_to_move) {
				alpha += (alpha_to - alpha) / alpha_spd;
			} else {
				alpha += (alpha_start - alpha) / alpha_spd;
			}
		}
	}
}

check_mouse = {
	released : function(button) {
		var val;
		if (instance_exists(cursor)) {
			val = cursor.click_released; 
		} else {
			val = mouse_check_button_released(button);
		}
		
		return val;
	},
	
	press : function(button) {
		var val;
		if (instance_exists(cursor)) {
			val = cursor.click; 
		} else {
			val = mouse_check_button=(button);
		}
		
		return val;
	},
	
	collision : function(x1, y1, x2, y2) {
		if (x1 == undefined) x1 = other.x - other.width / 2;
		if (y1 == undefined) y1 = other.y - other.height / 2;
		if (x2 == undefined) x2 = other.x + other.width / 2;
		if (y2 == undefined) y2 = other.y + other.height / 2;
		
		var col_cursor;
		if (instance_exists(cursor)) {
			col_cursor = point_in_rectangle(cursor.x, cursor.y, x1, y1, x2, y2);
		} else {
			col_cursor = point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), x1, y1, x2, y2);
		}
		
		return col_cursor;
	}
}
	
rendering = {
	box : function() {
		with(other) {
			draw_gui_box(sprite, image, x - width / 2, y - height / 2, x + width / 2, y + height / 2, c_white, alpha);
		}
	},
		
	sprite : function() {
		with(other) {
			draw_sprite_ext(sprite, image, x - width / 2, y - height / 2, 1 / sprite_get_width(sprite) * width, 1 / sprite_get_height(sprite) * height, 0, c_white, alpha);
		}
	},
	
	text : function() {
		with(other) {
			draw_set_halign(fa_center);
			draw_set_valign(fa_middle);

			draw_set_alpha(alpha);
			draw_text_transformed(x, y, text, 0.5, 0.5, 0);
			draw_set_alpha(1);

			draw_set_halign(fa_left);
			draw_set_valign(fa_top);
		}
	}
}