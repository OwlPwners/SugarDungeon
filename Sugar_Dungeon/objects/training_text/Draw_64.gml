draw_set_alpha(alpha);

draw_set_halign(fa_middle);
draw_set_valign(fa_center);

var font_size, scl, xx, yy;
font_size = font_get_size(global.font_game);
scl = 0.7;
xx = gui_width / 2;
yy = gui_height - font_size * 3;
draw_text_ext_transformed(xx, yy, text, font_size * 2, gui_width / scl, scl, scl, 0);

draw_set_halign(fa_left);
draw_set_valign(fa_top);

draw_set_alpha(1);