var dist = distance_to_object(player), alpha_to;
if (dist < min_dist) {
	alpha_to = 1;
} else {
	alpha_to = 0;
}

alpha += (alpha_to - alpha) / 15;