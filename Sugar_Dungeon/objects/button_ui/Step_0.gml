change.moving();
change.alphing();

x1 = x - offset_x;
x2 = x1 + width;
y1 = y - offset_y;
y2 = y1 + height;

save_x = lerp(-menu_ui_border_w, gui_width + menu_ui_border_w, x / gui_width);
save_y = lerp(-menu_ui_border_h, gui_height + menu_ui_border_h, y / gui_height);

is_pressed = ((image == image_pressed) || col_cursor_prev) && check_mouse.collision(x1, y1, x2, y2);
if ((is_pressed && alpha > 0.8) || button_ui_region.button_pressed_id == id) {
	var button_can_pressed = (button_ui_region.button_pressed_id == -1) || (button_ui_region.button_pressed_id == id);
	if (button_can_pressed) {
		if (check_mouse.press()) {
			towart_to_mouse = true;
			if (towart_to_mouse != towart_to_mouse_prev) {
				offset_toward_x = x - cursor.x;
				offset_toward_y = y - cursor.y;
			}
			
			button_ui_region.button_pressed_id = id;
		
			x_to = clamp(cursor.x + offset_toward_x, menu_ui_border_w, gui_width - menu_ui_border_w);
			y_to = clamp(cursor.y + offset_toward_y, menu_ui_border_h, gui_height - menu_ui_border_h);
		}
	
		if (check_mouse.released() && script > -1) {
			script(filename, selection);
			
			towart_to_mouse = false;
			
			button_ui_region.button_pressed_id = -1;
	
			audio_play_sound(sfx_menu_button_pressed, 83, false);
		}
	}
}

towart_to_mouse_prev = towart_to_mouse;