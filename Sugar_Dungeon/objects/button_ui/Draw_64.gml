if (!draw) exit;

var col_cursor = check_mouse.collision(x1, y1, x2, y2);
if (col_cursor) {
	image = image_pressed;
	
	if (col_cursor != col_cursor_prev && alpha > 0.8) audio_play_sound(sfx_menu_button_check, 82, false);
} else {
	image = image_released;
}

draw_sprite_ext(sprite, 0, x1 + offset_x, y1 + offset_y, menu_ui_size_aspect, menu_ui_size_aspect, 0, c_white, alpha);

if (global.debug) {
	draw_set_alpha(alpha);
	draw_rectangle(x1, y1, x2, y2, true);
	
	draw_circle(x, y, 5, true);
	
	draw_circle(save_x, save_y, 5, true);
	draw_set_alpha(1);
}

col_cursor_prev = col_cursor;