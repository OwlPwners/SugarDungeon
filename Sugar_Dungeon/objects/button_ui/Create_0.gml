event_inherited();

script = function(filename, selection) {
	ini_save(filename, "Real", selection, "x", save_x);
	ini_save(filename, "Real", selection, "y", save_y);
}

set_size = function(spr) {
	with(other) {
		sprite = spr;
		width = sprite_get_width(sprite) * menu_ui_size_aspect;
		height = sprite_get_height(sprite) * menu_ui_size_aspect;
		offset_x = sprite_get_xoffset(sprite) * menu_ui_size_aspect;
		offset_y = sprite_get_yoffset(sprite) * menu_ui_size_aspect;
		scale = menu_ui_size_aspect;
		
		offset_toward_x = 0;
		offset_toward_y = 0;
	}
}

filename = "";
selection = "";

save_x = 0;
save_y = 0;

init.moving();
init.alpha();
init.image();

col_cursor_prev = false;
towart_to_mouse = false;
towart_to_mouse_prev = false;