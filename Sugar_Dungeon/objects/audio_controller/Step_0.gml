if (!play_sound) exit;

if (play_sound && !audio_is_playing(sound)) {
	audio_play_sound(sound, priority, loops);
}

if (global.game_mod == "pause") {
	audio_volume_to = 0;
} else {
	audio_volume_to = global.music_gain;
}

audio_volume += (audio_volume_to - audio_volume) / 25;
audio_sound_gain(sound, audio_volume, 0);