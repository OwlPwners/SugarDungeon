if (room == rm_redactor) {
	draw_self();
	exit;
}

if (surface_exists(surf_tile)) {
	surface_set_target(surf_tile);

	draw_tile(tile_tileset, tile_index, 0, 0, 0);
	
	if (sprite_tile != -1) {
		gpu_set_blendmode(bm_subtract);
		draw_sprite(sprite_tile, image_tile_index, 0, 0);
		gpu_set_blendmode(bm_normal);
	}

	surface_reset_target();

	draw_surface(surf_tile, x, y);
} else {
	surf_tile = surface_create(tile_size, tile_size);
}