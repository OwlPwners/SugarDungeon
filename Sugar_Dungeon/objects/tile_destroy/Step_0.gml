if (room == rm_redactor) exit;

if (!init) {
	tile_index = tilemap_get_at_pixel(layer_rooms, x, y);
	tile_tileset = tilemap_get_tileset(layer_rooms);
	tilemap_set_at_pixel(layer_rooms, 0, x, y);
	if (tilemap_get_at_pixel(layer_shadows, x, y + tile_size + 1) == 49) tilemap_set_at_pixel(layer_shadows, 0, x, y + tile_size + 1);
	
	tile_draw = false;
	tile_draw_prev = false;

	surf_tile = surface_create(tile_size, tile_size);

	frame_speed = 1;
	spd = 0;

	image_tile_index = 0;

	init = true;
	exit; 
}

if (tile_draw) spd = -frame_speed; else spd = frame_speed;

image_tile_index += spd;
image_tile_index = clamp(image_tile_index, 0, sprite_get_number(sprite_tile) - 1);

if (tile_draw) {
	if (place_meeting(x, y, player)) {
		tile_draw = false;
		
		tile_draw_prev = tile_draw;
		exit;
	}
	
	tilemap_set_at_pixel(layer_collision, 1, x, y);
} else {
	tilemap_set_at_pixel(layer_collision, 0, x, y);
}

tile_draw_prev = tile_draw;