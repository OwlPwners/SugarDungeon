audio_group_load(agp_cutscene_start_music);
agp_is_load = false;

enum cs_start {
	image,
	show,
	text,
	time_str,
	time_image,
}

lang = global.game_lang;

cutscene[0, cs_start.image] = 0;
cutscene[0, cs_start.show] = true;
cutscene[0, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "zero");  // "Once upon a time...";
cutscene[0, cs_start.time_str] = 6;
cutscene[0, cs_start.time_image] = 90;



cutscene[1, cs_start.image] = 0;
cutscene[1, cs_start.show] = true;
cutscene[1, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "one");  // "The sugar kingdom flourished in the world.";
cutscene[1, cs_start.time_str] = 6;
cutscene[1, cs_start.time_image] = 100;



cutscene[2, cs_start.image] = 1;
cutscene[2, cs_start.show] = true;
cutscene[2, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "two");  // "The valiant and honest king ruled these lands.";
cutscene[2, cs_start.time_str] = 5;
cutscene[2, cs_start.time_image] = 110;



cutscene[3, cs_start.image] = 0;
cutscene[3, cs_start.show] = true;
cutscene[3, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "three");  // "It would seem that this will be forever.";
cutscene[3, cs_start.time_str] = 5;
cutscene[3, cs_start.time_image] = 110;



cutscene[4, cs_start.image] = 1;
cutscene[4, cs_start.show] = false;
cutscene[4, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "four");  // "But at one point...";
cutscene[4, cs_start.time_str] = 5;
cutscene[4, cs_start.time_image] = 90;



cutscene[5, cs_start.image] = 0;
cutscene[5, cs_start.show] = false;
cutscene[5, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "five");  // "Something attacked the king.";
cutscene[5, cs_start.time_str] = 5;
cutscene[5, cs_start.time_image] = 120;



cutscene[6, cs_start.image] = 2;
cutscene[6, cs_start.show] = true;
cutscene[6, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "six");  // "And killed him.";
cutscene[6, cs_start.time_str] = 15;
cutscene[6, cs_start.time_image] = 90;


cutscene[7, cs_start.image] = 3;
cutscene[7, cs_start.show] = true;
cutscene[7, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "seven");  // "The brave knight tried to stop him!";
cutscene[7, cs_start.time_str] = 5;
cutscene[7, cs_start.time_image] = 110;


cutscene[8, cs_start.image] = 3;
cutscene[8, cs_start.show] = true;
cutscene[8, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "eight");  // "But this creature opened a portal leading to the dungeon.";
cutscene[8, cs_start.time_str] = 5;
cutscene[8, cs_start.time_image] = 110;



cutscene[9, cs_start.image] = 0;
cutscene[9, cs_start.show] = false;
cutscene[9, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "nine");  // "After 6 seconds, the knight was sent to prison.";
cutscene[9, cs_start.time_str] = 4;
cutscene[9, cs_start.time_image] = 120;


cutscene[10, cs_start.image] = 0;
cutscene[10, cs_start.show] = false;
cutscene[10, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "ten");  // "From a strong blow, he remembers poorly.";
cutscene[10, cs_start.time_str] = 4;
cutscene[10, cs_start.time_image] = 120;



cutscene[11, cs_start.image] = 0;
cutscene[11, cs_start.show] = false;
cutscene[11, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "eleven");  // "Now the knight must kill all evil, so that the magician weakens.";
cutscene[11, cs_start.time_str] = 4;
cutscene[11, cs_start.time_image] = 120;



cutscene[12, cs_start.image] = 0;
cutscene[12, cs_start.show] = false;
cutscene[12, cs_start.text] = ini_translate(file_translate_cutscene_start, lang, "twelve");  // "And avenge the magician for the murder.";
cutscene[12, cs_start.time_str] = 4;
cutscene[12, cs_start.time_image] = 120;



cutscene[13, cs_start.image] = 0;
cutscene[13, cs_start.show] = false;
cutscene[13, cs_start.text] = ""; // Ass code.
cutscene[13, cs_start.time_str] = 4;
cutscene[13, cs_start.time_image] = 180;


scene_timer = -1;
scene_index = 0;
scene_prev = -1;

string_outside = "";
string_chars = 0;
string_timer = 1;

alpha = 0;
alpha_to = 1;