if (!agp_is_load) {
	if (audio_group_is_loaded(agp_cutscene_start_music)) {
		audio_play_sound(so_cutscene_start, 100, false);
		
		agp_is_load = true;
	} else exit;
}

if (keyboard_check_released(vk_space) || keyboard_check_released(vk_enter) || gamepad_button_check_released(global.gp_connect_id, gp_face1) || scene_index == array_height_2d(cutscene) - 1) {
	audio_group_unload(agp_cutscene_start_music);
	with(room_loader) {
		rm_goto = -1;
		transition = room_transition.noone_;
	}
	
	room_goto(rm_game);
}

var time_str = cutscene[scene_index, cs_start.time_str],
time_image = cutscene[scene_index, cs_start.time_image],
str = cutscene[scene_index, cs_start.text];

if (string_chars < string_length(str)) {
	string_timer --;
	
	if (string_timer == 0) {
		string_chars ++;
	
		string_outside += string_char_at(str, string_chars);

		string_timer = time_str;
	}
} else {
	if (scene_timer == -1) scene_timer = time_image; else scene_timer --;
	
	if (scene_timer == 0) {
		string_outside = "";
		string_chars = 0;
		alpha = 0;
		
		scene_index ++;
	}
}

if (abs(time_image * 0.2 - scene_timer) < 3) alpha_to = 0;
if (scene_prev != scene_index) alpha_to = 1;

alpha = lerp(alpha, alpha_to, 0.25);

scene_prev = scene_index;