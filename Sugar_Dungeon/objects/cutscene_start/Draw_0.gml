draw_set_alpha(alpha);
if (cutscene[scene_index, cs_start.show]) {
	draw_sprite(sprite_index, cutscene[scene_index, cs_start.image], x, y);
}
draw_set_alpha(1);

draw_set_halign(fa_center);
draw_set_valign(fa_middle);

var font_size = font_get_size(draw_get_font()) * 2;
draw_text_ext_transformed(gui_width / 2, gui_height * 0.9, string_outside, font_size, room_width / 0.5, 0.5, 0.5, 0);

draw_set_halign(fa_left);
draw_set_valign(fa_top);