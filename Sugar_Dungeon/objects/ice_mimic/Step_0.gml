enemy_step();

if (image_alpha < 0.7 && created == false) {
	with(instance_create_layer(x, y, layer_other, loot)) {
		var size = array_length(global.map_items);
		to_add = irandom_range(0, size - 1);
		
		need_show = false;
	}
	
	created = true;
}