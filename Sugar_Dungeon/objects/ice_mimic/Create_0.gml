enemy_create(spr_ice_mimic_opened, spr_ice_mimic_run, spr_ice_mimic_attack, spr_ice_mimic_hitbox, spr_ice_mimic_mask_attack, 2, 13, 1.2, 4, 1, 1000, 0.37, 5, sfx_attack1, [sfx_enemy_damaged1, sfx_enemy_damaged2, sfx_enemy_damaged3]);

created = false;

pscr = script_attack;
script_attack = function() {
	pscr();
	
	var player_inst = instance_place(x, y, player);
	if (player_inst != noone) effect_add_freeze(player_inst, 180);
}