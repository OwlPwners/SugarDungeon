{
  "spriteId": {
    "name": "spr_next_level0",
    "path": "sprites/spr_next_level0/spr_next_level0.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "i_must_save",
    "path": "objects/i_must_save/i_must_save.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"nextlevel_controller","path":"objects/nextlevel_controller/nextlevel_controller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"nextlevel_controller","path":"objects/nextlevel_controller/nextlevel_controller.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Controllers",
    "path": "folders/Objects/Controllers.yy",
  },
  "resourceVersion": "1.0",
  "name": "nextlevel_controller",
  "tags": [
    "shouldnt_deactivate",
  ],
  "resourceType": "GMObject",
}