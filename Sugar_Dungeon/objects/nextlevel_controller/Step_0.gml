var opened = false;

var enemies = layer_get_all_elements(layer_enemies);
if (array_length(enemies) <= 0 ) {
	opened = true;
	
	if (mat == false) {
		if (place_meeting(x, y, player)) {
			player.draw = false;
			player.hp += irandom_range(round(player.hp / 10), round(player.hp / 5));
			
			mat = true;
			
			if (file_exists(file_rooms_list)) file_delete(file_rooms_list);
			if (file_exists(file_chapter)) file_delete(file_chapter);
			
			var file = file_text_open_write(file_chapter);
			file_text_write_string(file, string(chapter + 1));
			file_text_close(file);
		}
	} else {
		if (room != rm_game) {
			with(room_loader) {
				rm_goto = rm_game;
				transition = room_transition.lollipop;
			}
		} else { 
			with(room_loader) {
				rm_goto = rm_loader_spechialy.restart;
				transition = room_transition.lollipop;
			}
		}
		
		if (!statistc_wrote) {
			with(statistic_manager) {
				map[? "CompletedLevelsRace"] ++;
				var maxx = map[? "CompletedLevelsMaxInRace"], now = map[? "CompletedLevelsRace"];
				map[? "CompletedLevelsMaxInRace"] = min(maxx, now);
			}
			
			statistc_wrote = true;
		}
	
		audio_stop_all();
	}
	
	depth = -room_height;
}

image_index = opened;