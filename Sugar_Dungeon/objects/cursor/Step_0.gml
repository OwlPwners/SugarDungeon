var gp = global.gp_connect_id;
if (gp != -1) {
	if (room != rm_menu && global.game_mod == "game") exit;
	
	var xx, yy, sp = 2.75;
	xx = device_mouse_x_to_gui(0) + gamepad_axis_value(gp, gp_axislh) * sp;
	yy = device_mouse_y_to_gui(0) + gamepad_axis_value(gp, gp_axislv) * sp;
	
	display_mouse_set(xx, yy);
	
	click = gamepad_button_check(0, gp_face1);
	click_released = gamepad_button_check_released(0, gp_face1);
	click_pressed = gamepad_button_check_pressed(0, gp_face1);
}

click = mouse_check_button(mb_left);
click_released = mouse_check_button_released(mb_left);
click_pressed = mouse_check_button_pressed(mb_left);

x = device_mouse_x_to_gui(0);
y = device_mouse_y_to_gui(0);