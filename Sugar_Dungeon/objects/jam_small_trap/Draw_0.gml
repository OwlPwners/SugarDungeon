if (surface_exists(surf_ref)) {
	draw_set_alpha(image_alpha);
	
	var xoff, yoff;
	xoff = sprite_get_xoffset(sprite_index);
	yoff = sprite_get_yoffset(sprite_index);
	
	draw_surface(surf_ref, x - xoff, y - yoff);
	
	draw_set_alpha(1);
} else {
	surf_ref = surface_create(sprite_width, sprite_height);
}