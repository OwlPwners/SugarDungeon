display_set_gui_size(gui_width, gui_height);
draw_set_font(global.font_game);
instance_destroy(player);

enum menu_page {
	start,
	options,
	audio,
	languauge,
	display,
	display_ui,
	control,
	control_gp,
	newgame,
	quit
}

with(gui_parent) {
	switch(layer_get_name(layer)) {
		case("MenuStart") :
		my_page = menu_page.start;
		break;
		
		case("MenuOptions") :
		my_page = menu_page.options;
		break;
		
		case("MenuAudio") :
		my_page = menu_page.audio;
		break;
		
		case("MenuDisplay") :
		my_page = menu_page.display;
		break;
		
		case("MenuDisplayUI") :
		my_page = menu_page.display_ui;
		break;
		
		case("MenuLanguauge") :
		my_page = menu_page.languauge;
		break;
		
		case("MenuControl") :
		my_page = menu_page.control;
		break;
		
		case("MenuControlGP") :
		my_page = menu_page.control_gp;
		break;
		
		case("MenuAYSnewgame") :
		my_page = menu_page.newgame;
		break;
		
		case("MenuAYSquit") :
		my_page = menu_page.quit;
		break;
	}
}

page_stack = ds_stack_create();
page_prev_is_downly = false;
page = menu_page.start;
page_prev = -1;

var music_volume_value, sfx_volume_value;
music_volume_value = ini_load(file_settings, "Real", "SoundsAndMusic", "MusicVolume", 1);
sfx_volume_value = ini_load(file_settings, "Real", "SoundsAndMusic", "SoundVolume", 1);
global.music_gain = music_volume_value;

audio_group_set_gain_all_music(global.music_gain);
audio_group_set_gain_all_sound(sfx_volume_value);

//with(room_loader) {
//	sound = so_menu;
//	priority = 100;
//	loops = true;
//	update_timer = 3;
//}