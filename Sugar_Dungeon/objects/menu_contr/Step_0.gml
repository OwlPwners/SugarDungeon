if (page_prev != page) {
	with(gui_parent) {
		if (menu_contr.page == my_page) {
			move_to_move = true;
			alpha_to_move = true;
		} else {
			move_to_move = false;
			alpha_to_move = false;
		}
	}
	
	var val;
	if (page_prev == -1) val = menu_page.start; else val = page_prev;
	if (page_prev_is_downly == false) ds_stack_push(page_stack, val);
}

page_prev = page;
page_prev_is_downly = false;