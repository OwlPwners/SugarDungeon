if (!layer_changed) {
	layer = layer_light;
	layer_changed = true;
}

if (!tile_meeting(layer_collision, x, y)) instance_destroy(self);