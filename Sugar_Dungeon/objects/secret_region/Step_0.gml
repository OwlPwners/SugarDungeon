if (room == rm_redactor || !instance_exists(clamp_box_id)) exit;

if (!alpha_setted) {
	set_alpha(0);
	alpha_setted = true;
}

var alpha_to = 1;
if (sat) {
	var col = collision_rectangle(x, y, x + sr_width * tile_size, y + sr_height * tile_size, player, false, true) != noone;
	if (col) alpha_to = 0.3;
}

alpha += (alpha_to - alpha) / 15;

if (clamp_box_id.enemy_num_lives <= 0) {
	if (!surface_exists(surf) || !sat) {
		set_alpha(1);
		
		sat = true;
	
		sr_width = max(1, sr_width);
		sr_height = max(1, sr_height);
		surf = surface_create(sr_width * tile_size, sr_height * tile_size);
		
		surface_set_target(surf);
		for(var i = 0; i < sr_width; i ++) {
			for(var j = 0; j < sr_height; j ++) {
				var xx, yy, tx, ty, tile, tileset;
				xx = i * tile_size;
				yy = j * tile_size;
				tx = x + xx;
				ty = y + yy;
				tile = tilemap_get_at_pixel(layer_rooms, tx, ty);
				tileset = tilemap_get_tileset(layer_rooms);
			
				draw_tile(tileset, tile, 0, xx, yy);
		
				tilemap_set_at_pixel(layer_rooms, 0, tx, ty);
				tilemap_set_at_pixel(layer_collision, 0, tx, ty);
			}
		}
		surface_reset_target();
	}
}







//var alpha_to = 1;
//if (!sat) {
//	if (clamp_box_id.enemy_num_lives == 0) {
//		sat = true;
	
//		for(var i = 0; i < sr_width; i ++) {
//			for(var j = 0; j < sr_height; j ++) {
//				var xx, yy, tx, ty, tile, tileset;
//				xx = i * tile_size;
//				yy = j * tile_size;
//				tx = x + xx;
//				ty = y + yy;
		
//				tilemap_set_at_pixel(layer_collision, 0, tx, ty);
//			}
//		}
//	}
//} else {
//	var col = (collision_rectangle(x, y, x + sr_width * tile_size, y + sr_height * tile_size, player, false, true) == noone);
//	if (!col) alpha_to = 0.3;
//}

//alpha += (alpha_to - alpha) / 15;

//if (!surface_exists(surf)) {
//	sr_width = max(1, sr_width);
//	sr_height = max(1, sr_height);
//	surf = surface_create(sr_width * tile_size, sr_height * tile_size);
		
//	surface_set_target(surf);
//	for(var i = 0; i < sr_width; i ++) {
//		for(var j = 0; j < sr_height; j ++) {
//			var xx, yy, tx, ty, tile, tileset;
//			xx = i * tile_size;
//			yy = j * tile_size;
//			tx = x + xx;
//			ty = y + yy;
//			tile = tilemap_get_at_pixel(layer_rooms, tx, ty);
//			tileset = tilemap_get_tileset(layer_rooms);
			
//			draw_tile(tileset, tile, 0, xx, yy);
		
//			tilemap_set_at_pixel(layer_rooms, 0, tx, ty);
//		}
//	}
//	surface_reset_target();
//}