if (room == rm_redactor) exit;

draw_set_alpha(alpha);

if (surface_exists(surf)) draw_surface(surf, x, y);

draw_set_alpha(1);