save_object_default_variables();
save_object_add_variables("sr_width", "sr_height");

sr_width = 1;
sr_height = 1;
alpha = 0;

surf = -1;

if (room == rm_redactor) {
	sprite_index = spr_question;
	
	sr_width = get_integer("width", "");
	sr_height = get_integer("height", "");
} else image_alpha = 0;

alpha_setted = false;
sat = false;

set_alpha = function(alpha) {
	var list_inside = ds_list_create();
	collision_rectangle_list(x, y, x + other.sr_width * tile_size, y + other.sr_height * tile_size, all, false, true, list_inside, false);
	
	var i, size;
	i = 0; size = ds_list_size(list_inside);
	repeat(size) {
		if (!tile_meeting(layer_collision, x, y)) {
			var idd = list_inside[| i];
			idd.image_alpha = alpha;
		}

		i ++;
	}
	
	ds_list_destroy(list_inside);
}

clamp_box_id = -1;