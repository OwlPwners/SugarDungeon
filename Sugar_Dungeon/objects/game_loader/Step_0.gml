scale += (1 - scale) / 15;
alpha += (1 - alpha) / 45;

image_xscale = scale;
image_yscale = scale;
image_alpha = alpha;