var screen = ini_load(file_settings, "Real", "Dsiplay", "Fullscreen", 1);
window_set_fullscreen(screen);

scale = 0;
alpha = 0;

audio_group_load(agp_sfx);
audio_group_load(agp_floor_music);

alarm[0] = 120;

#region Globalvars.
global.font_game = font_add_sprite_ext(spr_font, " %,+-.!?:/0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnmЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэячсмитьбю", false, 0);
global.game_mod = "game";
global.debug = false;
global.music_gain = 1;
global.game_lang = ini_load(file_settings, "String", "Languauge", "Lang", string_upper(os_get_language()));
global.gp_connect_id = -1;
global.particle_system = part_system_create();
global.grid_enemy_ai = -1;
global.game_music = so_floor0;
#endregion

#region Items Inithialization.

#region Small health potion.
potion_add(item.potion_smallives, "Potion", spr_potion_small_lives, 35, 15, live_potion_color, ptsr_addlive, ptsr_smalllives);
#endregion

#region Big health potion.
potion_add(item.potion_biglives, "Potion", spr_potion_big_lives, 35, 20, live_potion_color, ptsr_addlive, ptsr_biglives);
#endregion

#region Plain sword.
weapon_add(item.plainsword, "Melee", 
spr_plain_sword, 
spr_plain_sword_fly,
[spr_plain_sword_attack1, spr_plain_sword_attack2, spr_plain_sword_attack3],
[spr_plain_sword_attack1, spr_plain_sword_attack2, spr_plain_sword_attack3], 
[sfx_attack1, sfx_attack2, sfx_attack3],
3, 1.5, 1, 6, 
-1, wps_player_attack, -1, 
item_set.plain,
[3, 5, -1], [20, 17, -1]);
#endregion

#region Plain spear.
weapon_add(item.plainspear, "Melee", 
spr_plain_spear, 
spr_plain_spear_fly,
spr_plain_spear_attack,
spr_plain_spear_mask_attack,
sfx_attack1,
5, 3, 2, 11, 
wps_rotate_to_dir, wps_player_attack, -1, 
item_set.plain);
#endregion

#region Plain shield.
shield_add(item.plainshield, "Shield", spr_plain_shield_ico, spr_plain_shield, 10, 7, 8, 8, item_set.plain, weapon_combo_plain);
#endregion

#region Candy sword.
weapon_add(item.candysword, "Melee", 
spr_candy_sword, 
spr_candy_sword_fly,
[spr_candy_sword_attack1, spr_candy_sword_attack2],
[spr_candy_sword_attack1, spr_candy_sword_attack2],
[sfx_attack1, sfx_attack3],
30, 3.5, 3, 25, 
-1, wps_player_attack, -1, 
item_set.lollipop,
[3, -1], [14, -1]);
#endregion

#region Candy spear.
weapon_add(item.candyspear, "Melee", 
spr_candy_spear, 
spr_candy_spear_fly,
spr_candy_spear_attack,
spr_candy_spear_mask_attack,
sfx_attack1,
30, 3.2, 2, 23, 
wps_rotate_to_dir, wps_player_attack, -1, 
item_set.lollipop);
#endregion

#region Candy scyth.
weapon_add(item.candyscythe, "Melee", 
spr_candy_scythe, 
spr_candy_scythe_fly,
spr_candy_scythe_attack,
spr_candy_scythe_attack,
sfx_attack1,
35, -20.2, 2, 27, 
-1, wps_player_attack, -1, 
item_set.lollipop);
#endregion

#region Lollipop shield.
shield_add(item.lollipopshield, "Shield", spr_lollipop_shield_ico, spr_lollipop_shield, 20, 15, 22, 30, item_set.lollipop, weapon_combo_lollipop);
#endregion

#region Gingerbread sword.
weapon_add(item.gbreadsword, "Melee", 
spr_gingerbread_sword, 
spr_gingerbread_sword_fly,
[spr_gingerbread_sword_attack1, spr_gingerbread_sword_attack2, spr_gingerbread_sword_attack3],
[spr_gingerbread_sword_attack1, spr_gingerbread_sword_attack2, spr_gingerbread_sword_attack3],
[sfx_attack1, sfx_attack2, sfx_attack3],
45, 3.6, 3, 35, 
-1, wps_player_attack, -1, 
item_set.gbread,
[10, 11, -1], [24, 24, -1]);
#endregion

#region Gingelbread spear.
weapon_add(item.gbreadspear, "Melee", 
spr_gingerbread_spear, 
spr_gingerbread_spear_fly,
spr_gingerbread_spear_attack,
spr_gingerbread_spear_attack,
sfx_attack1,
40, 2.2, 2, 35, 
wps_rotate_to_dir, wps_player_attack, -1, 
item_set.gbread);
#endregion

#region Gingerbread Man.
weapon_add(item.gbreadman, "Melee", 
spr_gingerbread_man, 
spr_gingerbread_man_fly,
spr_gingerbread_man_attack,
spr_gingerbread_man_attack,
sfx_attack1,
40, 3.2, 2, 45, 
-1, wps_player_attack, -1, 
item_set.gbread);
#endregion

#region Gingerbread shield.
shield_add(item.gbreadshield, "Shield", spr_gingerbread_shield_ico, spr_gingerbread_shield, 25, 20, 30, 45, item_set.gbread, weapon_combo_gingerbread);
#endregion

#region Icecream boss sword.
weapon_add(item.icecream_sword, "Melee", 
spr_icecream_sword, 
spr_icecream_sword_fly,
[spr_icecream_sword_attack1, spr_icecream_sword_attack2],
[spr_icecream_sword_attack1, spr_icecream_sword_attack2], 
[sfx_attack1, sfx_attack3],
999, 4.2, 4, 999, 
-1, wps_player_attack, -1, 
item_set.icecream,
[15, -1], [26, -1]);
#endregion

#endregion
















/*

#region Plain sword.
wp_add_weapon(item.plainsword, "Melee", spr_plain_sword, 
[spr_player_attack_plain_sword1, spr_player_attack_plain_sword2], 
[spr_player_mask_attack_plain_sword1, spr_player_mask_attack_plain_sword2], 
spr_player_block_plain_sword, 1, 5, 2.25, 1, 3, -1, -1, wp_script_animend_glide);
#endregion

#region Plain spear.
wp_add_weapon(item.plainspear, "Melee", spr_plain_spear, 
spr_player_attack_plain_spear, spr_player_mask_attack_plain_spear, spr_player_block_plain_spear, 5, 5, 3.25, 5, 7, -1, -1, -1);
#endregion

#region Candy sword.
wp_add_weapon(wp_candysword, "Melee", spr_candy_sword, 
spr_player_attack_candy_sword, spr_player_mask_attack_candy_sword, spr_player_block_candy_sword, 95, 10, 10, 190, 40, -1, -1, -1);
#endregion

#region Lollipop.
wp_add_weapon(wp_lollipop, "Melee", spr_lollipop, 
spr_player_attack_lollipop, spr_player_mask_attack_lollipop, spr_player_block_lollipop, 50, 5, 3, 17, 19, -1, -1, -1);
#endregion

#region Candy cane.
wp_add_weapon(wp_candycane, "Melee", spr_candy_cane, 
spr_player_attack_candy_cane, spr_player_mask_attack_candy_cane, spr_player_block_candy_cane, 65, 5, 3.5, 25, 14, -1, -1, -1);
#endregion

#region Small live potion.
global.map_items[item.potion_smallives] = ds_map_create();

ds_map_add(global.map_items[item.potion_smallives], "Type", "Potion");
ds_map_add(global.map_items[item.potion_smallives], "Icon", spr_potion_small_lives);
ds_map_add(global.map_items[item.potion_smallives], "Sprite", spr_player_drinking_potion_smalllives);
ds_map_add(global.map_items[item.potion_smallives], "Script", pt_script_addlive);
ds_map_add(global.map_items[item.potion_smallives], "Adding", 1);
ds_map_add(global.map_items[item.potion_smallives], "Rarity", 55);
ds_map_add(global.map_items[item.potion_smallives], "Price", 12);
#endregion

#region Big live potion.
global.map_items[wp_potion_biglives] = ds_map_create();

ds_map_add(global.map_items[wp_potion_biglives], "Type", "Potion");
ds_map_add(global.map_items[wp_potion_biglives], "Icon", spr_potion_big_lives);
ds_map_add(global.map_items[wp_potion_biglives], "Sprite", spr_player_drinking_potion_biglives);
ds_map_add(global.map_items[wp_potion_biglives], "Script", pt_script_addlive);
ds_map_add(global.map_items[wp_potion_biglives], "Adding", 7);
ds_map_add(global.map_items[wp_potion_biglives], "Rarity", 60);
ds_map_add(global.map_items[wp_potion_biglives], "Price", 40);
#endregion

#region Watermelon sword.
wp_add_weapon(wp_watermelon_sword, "Melee", spr_watermelon_sword, 
spr_player_attack_watermelon_sword, spr_player_mask_attack_watermelon_sword, spr_player_block_watermelon_sword, 30, 5, 2, 20, 10, -1, -1, -1);
#endregion

#region Popsicle.
wp_add_weapon(wp_popsicle, "Melee", spr_popsicle, 
spr_player_attack_popsicle, spr_player_mask_attack_popsicle, spr_player_block_popsicle, 60, 5, 4, 31, 8, -1, -1, -1);
#endregion

#region Watermelon spear.
wp_add_weapon(wp_watermelon_spear, "Melee", spr_watermelon_spear, 
spr_player_attack_watermelon_spear, spr_player_mask_attack_watermelon_spear, spr_player_block_watermelon_spear, 60, 6, 4, 23, 20, -1, -1, -1);
#endregion

#region Big live potion.
global.map_items[wp_potion_notcold] = ds_map_create();

ds_map_add(global.map_items[wp_potion_notcold], "Type", "Potion");
ds_map_add(global.map_items[wp_potion_notcold], "Icon", spr_potion_not_cold);
ds_map_add(global.map_items[wp_potion_notcold], "Sprite", spr_player_drinking_potion_notcold);
ds_map_add(global.map_items[wp_potion_notcold], "Script", "NotCold");
ds_map_add(global.map_items[wp_potion_notcold], "Time", 360);
ds_map_add(global.map_items[wp_potion_notcold], "Rarity", 55);
ds_map_add(global.map_items[wp_potion_notcold], "Price", 30);
#endregion