draw_set_alpha(alpha);

draw_circle(x, y, rad, true);

draw_set_alpha(1);

if (collision_circle(x, y, rad, player, false, true)) {
	effect_add_freeze(player, 180);
}