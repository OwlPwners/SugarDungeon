// For checking collision you must: 
// 1) y += z;
// 2) checking_collision();
// 3) y -= z;

depth = -bbox_bottom;

scale += (1 - scale) / 10;
image_xscale = scale;
image_yscale = scale;

zsp += grav;

var dx, dy, len;
dx = xto - x;
dy = yto - y;
len = sqrt(dx * dx + dy * dy);
if (len != 0) {
	dx /= len;
	dy /= len;
}

hsp = dx * spd;
vsp = dy * spd;

if (z + zsp > z_floor) {
	z = z_floor;
	zsp = 0;
	
	var dist, xx, yy, idd;
	dist = 10;
	xx = irandom_range(-dist, dist);
	yy = irandom_range(-dist, dist);
	idd = instance_create_depth(x + xx, y + yy, depth, leaked_icecream);
	
	idd.image_speed = 0;
	idd.image_index = image_index;
	idd.alarm[0] = -1;
	
	if (place_meeting(x, y, player)) {
		var player_inst = instance_place(x, y, player);
		if (player_inst != noone) {
			if (player_inst.war_timer <= 0 && player_inst.glide_spd == 0) {
				player_inst.flash = 1;
				player_inst.war_timer = player_inst.war_time;
					
				var inst_sh = player.shield_id;
				if (player.state == player_state.block && instance_exists(inst_sh) && place_meeting(x, y, inst_sh)) {
					var map = inst_sh.map,
					proc_hp = (100 - map[? "BLockHp"]) / 100,
					proc_bounce = (100 - map[? "BlockKnockback"]) / 100;
				
					inst_sh.flash = 1;
						
					player_inst.hp -= damage * proc_hp;
					bounce_set(player_inst.x, player_inst.y, x, y, force_bounce * proc_bounce, player_inst);
					with(player_inst) part_sparks_burst(x, y, 5);
				} else {
					player_inst.hp -= damage;
					bounce_set(player_inst.x, player_inst.y, x, y, force_bounce, player_inst);
				}
			
				var s = 5;
				camera.x += random_range(-s, s);
				camera.y += random_range(-s, s);
			}
		}
	}
	
	instance_destroy(self);
}

x += hsp;
y += vsp;
z += zsp;