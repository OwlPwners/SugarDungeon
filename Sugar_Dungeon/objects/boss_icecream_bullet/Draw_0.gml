var shadow_k = abs(z / z_max_height), 
shadow_scale = max(0, 1 - shadow_k) * scale, 
shadow_alpha = max(0, 0.7 - shadow_k);

draw_set_alpha(shadow_alpha);
draw_set_color(c_black);

draw_circle(x, y, abs(sprite_width / 2) * shadow_scale, false);

draw_set_color(c_white);
draw_set_alpha(1);

draw_sprite_ext(sprite_index, image_index, x, y + z, image_xscale, image_yscale, image_angle, image_blend, image_alpha);