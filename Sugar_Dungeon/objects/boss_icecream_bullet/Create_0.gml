z = 0;
z_floor = 0;
z_max_height = 350;

hsp = 0;
vsp = 0;
zsp = 0;

spd = 3 + random(2);
grav = 0.2;

depth_variables();

var i = random_range(30, 60);
xto = lerp(player.x, player.x + player.hsp, i);
yto = lerp(player.y, player.y + player.vsp, i);

var f = point_distance(x, y, xto, yto) + point_distance(0, 0, sprite_width, sprite_height);
zsp = -1 * f / grav / 60 / spd;

image_speed = 0;
//image_index = irandom(image_number - 1);

image_xscale = 0;
image_yscale = 0;

scale = 0;

damage = 2;
force_bounce = 3;