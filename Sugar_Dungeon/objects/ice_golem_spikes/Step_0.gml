if (alpha < 0) instance_destroy(self);

alpha += (alpha_to - alpha) / 25;
scale += (1 - scale) / 15;

image_alpha = alpha;
image_yscale = scale;

if (place_meeting(x, y, player)) {	
	var player_inst = instance_place(x, y, player);
	if (player_inst != noone) {
		if (player_inst.war_timer <= 0 && player_inst.glide_spd == 0) {
			player_inst.flash = 1;
			player_inst.war_timer = player_inst.war_time;
					
			if (player_inst.block == false) {
				player_inst.hp -= damage;
				bounce_set(player_inst.x, player_inst.y, x, y, force_bounce, player_inst);
			} else {
				var map = global.map_items[player.slot_index],
				procent_minus = (100 - map[? "ProcentBlock"]) / 100;
						
				player_inst.hp -= damage * procent_minus;
				bounce_set(player_inst.x, player_inst.y, x, y, force_bounce / 1.5, player_inst);
				with(player_inst) part_sparks_burst(x, y, 5);
			}
		}
	}
}