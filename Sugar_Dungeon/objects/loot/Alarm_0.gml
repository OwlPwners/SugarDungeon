y = y_start_me;

if (to_add_index == -1) {
	var weap_cout;
	weap_cout = array_length(global.map_items);

	to_add_index = irandom(weap_cout - 1);
	to_add = global.map_items[to_add_index];

	var rarity, procent;
	rarity = to_add[? "Rarity"];
	procent = irandom_range(rar_min, rar_max);

	var col = procent < rarity;
	while(col) {
		to_add_index = irandom(weap_cout - 1);
		to_add = global.map_items[to_add_index];
		
		rarity = to_add[? "Rarity"];
		procent = random_range(rar_min, rar_max);
	
		col = procent < rarity;
	}
} else {
	to_add = global.map_items[to_add_index];
}

sprite_index = to_add[? "Icon"];

sprited = true;