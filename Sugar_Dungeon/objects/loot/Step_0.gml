if (!sprited) exit;

event_inherited();

if (global.game_mod == "show_map" || global.game_mod == "trading" || !ds_exists(to_add, ds_type_map)) exit;
_alarm --;

set_depth();

if (_alarm < 0) player_seen = true;

if (selected) {
	//if (outline_surf == -1) {
	//	outline_surf = surface_create(sprite_width + 3, sprite_height + 3);
	//	surface_set_target(outline_surf);
		
	//	draw_sprite(sprite_index, 0, 1 + sprite_get_xoffset(sprite_index), 1 + sprite_get_yoffset(sprite_index));
		
	//	surface_reset_target();
	//}
	
	//outline_alpha += (1 - outline_alpha) / 5;
	
	if (player.key_interact && global.game_mod != "show_item") {
		surface_free(outline_surf);
		outline_surf = -1;
		
		var my_item;
		switch(to_add[? "Type"]) {
			case("Melee") :
			case("Potion") :
			my_item = player.slot[player.slot_index];
			player.slot[player.slot_index] = to_add;
			break;
			
			case("Shield") :
			my_item = player.slot_shield;
			player.slot_shield = to_add;
			break;
		}
		
		if (my_item == -1) instance_destroy(self); else
		to_add = my_item;
		to_add_index = to_add[? "ItemIndex"];
	}
} //else {
//	if (outline_surf != -1 && outline_alpha == 0) {
//		surface_free(outline_surf);
		
//		outline_surf = -1;
//	}
	
//	outline_alpha += (0 - outline_alpha) / 5;
//}

if (need_show) {
	if (player_seen) {
		player.draw = true;
	
		var rect_yto1, rect_yto2;
		rect_yto1 = rect_ystart[0] - rectangle_y[0];
		rect_yto2 = rect_ystart[1] - rectangle_y[1];
	
		var xto;
		xto = -sprite_width * xscale - x_to;
	
		var scale_x, scale_y;
		scale_x = 1 - xscale;
		scale_y = 1 - yscale;
	
		//if (selected && player.key_interact == true) {
		//	var my_item = player.slot[player.slot_index];
		//	player.slot[player.slot_index] = to_add;
		//	if (my_item == -1) instance_destroy(self); else
		//	to_add = my_item;
		//}
	
		if (global.game_mod != "pause") global.game_mod = "game";
	} else {
		depth = player.depth - 1;
	
		var rect_yto1, rect_yto2;
		rect_yto1 = 0 - rectangle_y[0];
		rect_yto2 = disp_height - rectangle_height - rectangle_y[1];
	
		var xto;
		xto = disp_width / 2 - x_to;
	
		var sqrs, scale_x, scale_y;
		sqrs = sqrt(disp_width + disp_height) * 4;
		scale_x = sqrs / sprite_width * image_xscale - xscale;
		scale_y = sqrs / sprite_height * image_yscale - yscale;
	
		global.game_mod = "show_item";
	}

	if (ds_exists(to_add, ds_type_map) == false) {
		instance_destroy(self);
		exit;
	}
} else {
	var rect_yto1, rect_yto2;
	rect_yto1 = rect_ystart[0] - rectangle_y[0];
	rect_yto2 = rect_ystart[1] - rectangle_y[1];
	
	var xto;
	xto = -sprite_width * xscale - x_to;
	
	var scale_x, scale_y;
	scale_x = 1 - xscale;
	scale_y = 1 - yscale;
	
	if (prev_show == false) player.draw = true;
	
	prev_show = true;
}

sprite_index = to_add[? "Icon"];

rectangle_y[0] += rect_yto1 / spd;
rectangle_y[1] += rect_yto2 / spd;

if (rectangle_y[0] == rect_ystart[0] && player_seen) need_show = false;

x_to += xto / spd;
y += lengthdir_y(spd_float, rot);

xscale += scale_x / spd;
yscale += scale_y / spd;

rot ++;