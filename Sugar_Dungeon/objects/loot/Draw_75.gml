if (!sprited) exit;

if (global.game_mod == "show_map" || global.game_mod == "trading" || need_show == false) exit;

draw_set_color(c_black);
draw_set_alpha(0.9);

draw_rectangle(-1, rectangle_y[0], rectangle_width, rectangle_y[0] + rectangle_height, false);
draw_rectangle(-1, rectangle_y[1], rectangle_width, rectangle_y[1] + rectangle_height, false);

draw_set_alpha(1);
draw_set_color(c_white);

var xx = x_to - ((sprite_width - sprite_get_xoffset(sprite_index) * 2) * xscale) / 2,
yy = y_to - ((sprite_height - sprite_get_yoffset(sprite_index) * 2) * yscale) / 2;
draw_sprite_ext(sprite_index, 0, xx, yy, xscale, yscale, 0, c_white, 1.0);