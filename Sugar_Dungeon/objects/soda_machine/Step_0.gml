event_inherited();

text_alpha += (selected - text_alpha) / 10;

if (selected && player.key_interact && image_speed == 0) {
	if (player.money >= item_price) {	
		var map = global.map_items[item_index];
		item_create(player.x, player.y, true, false, map);
	
		player.money -= item_price;
	
		image_speed = 1;
	}
}