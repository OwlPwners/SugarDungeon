event_inherited();

draw_set_alpha(text_alpha);
draw_set_halign(fa_center);

var text_scale = 0.7, text_offset_y = sin(current_time / 1000) * 3 - sprite_height / 4;
draw_text_transformed(x, bbox_top + text_offset_y, string(item_price), text_scale, text_scale, 0);

draw_set_halign(fa_left);
draw_set_alpha(1);