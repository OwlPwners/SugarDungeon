event_inherited();

depth = -bbox_bottom;
image_speed = 0;
image_index = 0;

// In the future, it will be better to change this and use an array with indices: arr = [item.potion_smalllave, ...];
while(true) {
	var item_num = array_length(global.map_items) - 1;
	item_index = irandom(item_num);
	
	var item_type = global.map_items[item_index][? "Type"];
	if (item_type == "Potion") break;
}

item_price = 10;

text_alpha = 0;