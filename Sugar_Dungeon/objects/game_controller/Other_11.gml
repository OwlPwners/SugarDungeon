global.game_mod = "game";

var root_map = ds_map_create();
if (room == rm_game) {
	//with(audio_controller) {
	//	audio_group_unload_num(audiogroup);
	//}
	
	var list_instances = ds_list_create();
	
	instance_destroy(camera);
	instance_destroy(light_controller);
	
	instance_activate_all();
	with(chunk_controller) is_active = true;
	
	with(all) {
		if (variable_instance_exists(self, "save_array")) {
			var map = save_object_map(save_array, self);
			//ds_list_add(list_instances, ds_map_write(map));
			
			ds_list_add(list_instances, map);
			ds_list_mark_as_map(list_instances, ds_list_size(list_instances) - 1);
		}
	}
	
	with(player) {
		var player_map = ds_map_create();
		ds_map_add(player_map, "x", x);
		ds_map_add(player_map, "y", y);
		ds_map_add(player_map, "hp", hp);
		ds_map_add(player_map, "money", money);
		ds_map_add(player_map, "slot shield", slot_shield);
	
		for(var i = 0; i < slot_number; i ++) {
			ds_map_add(player_map, "slot" + string(i), slot[i]);
		}
		
		//ds_map_add(root_map, "player", ds_map_write(player_map));
		ds_map_add_map(root_map, "player", player_map);
		
		//ds_map_destroy(player_map);
	}
	
	ds_map_add_list(root_map, "instances", list_instances);
	
	ds_map_add(root_map, "boss_fight", false);
	ds_map_add(root_map, "boss_room", -1);

	//ds_list_destroy(list_instances);
} else {
	var player_map = ds_map_create();
	ds_map_add(player_map, "hp", player.hp);
	ds_map_add(player_map, "money", player.money);
	ds_map_add(player_map, "slot shield", player.slot_shield);
	
	for(var i = 0; i < player.slot_number; i ++) {
		ds_map_add(player_map, "slot" + string(i), player.slot[i]);
	}
		
	//ds_map_add(root_map, "player", ds_map_write(player_map));
	ds_map_add_map(root_map, "player", player_map);
	//ds_map_destroy(player_map);
		
	ds_map_add(root_map, "boss_fight", true);
	ds_map_add(root_map, "boss_room", room);
}

if (file_exists(file_room)) file_delete(file_room);
//var file = file_text_open_write(file_room);
//file_text_write_string(file, ds_map_write(root_map));
//file_text_close(file);

var str = json_encode(root_map);
buffer_save_string(file_room, str);

ds_map_destroy(root_map);

audio_stop_all();

with(room_loader) {
	rm_goto = rm_menu;
	transition = room_transition.lollipop;
}