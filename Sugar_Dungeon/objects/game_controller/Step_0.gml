var mode = global.game_mod;
if (mode == "trading" || mode == "show_map") exit;

if (instance_exists(camera)) {
	x = camera.x;
	y = camera.y;
}

if (keyboard_check_released(vk_f2)) {	
	var file = get_save_filename("PNG | * .png", "");
	if (file != "") {
		screen_save(file);
	}
}

var gp = global.gp_connect_id, key_pause;
if (gp == -1) {
	key_pause = keyboard_check_released(vk_escape);
} else {
	key_pause = gamepad_button_check_released(gp, gp_select);
}

if (key_pause) pressed = !pressed;
if (pressed) {
	global.game_mod = "pause";
}

var audio_volume_to = global.music_gain;
if (global.game_mod == "pause") {
	instance_activate_all();
	with(chunk_controller) is_active = true;
	
	audio_volume_to = 0;
	
	var i = 0;
	repeat(buttons) {
		button_y_now[i] += (button_y_to[i] - button_y_now[i]) / button_spd;
		button_alpha_now[i] += (button_alpha_to[i] - button_alpha_now[i]) / alpha_spd;
		
		button_set_coord(button_array[i], button_x, button_y_now[i]);
		button_set_alpha(button_array[i], button_alpha_now[i]);
		
		if (button_check_released(button_array[i], char_size)) event_user(i);
		
		i ++;
	}
	
	blur += blur_spd;
	
	if (pause_prev == false) surf = application_surface;
	
	with(camera) deactivate = false;
} else {
	blur -= blur_spd;
	
	var i = 0;
	repeat(buttons) {
		button_y_now[i] += (button_y_start[i] - button_y_now[i]) / button_spd;
		button_alpha_now[i] += (button_alpha_start[i] - button_alpha_now[i]) / alpha_spd;
		
		button_set_coord(button_array[i], button_x, button_y_now[i]);
		button_set_alpha(button_array[i], button_alpha_now[i]);
		
		i ++;
	}
	
	with(camera) deactivate = true;
}

if (room == rm_game) {
	if (audio_exists(global.game_music) && !audio_is_playing(global.game_music)) {
		audio_stop_all();
		audio_play_sound(global.game_music, 999, true);
	}

	audio_volume += (audio_volume_to - audio_volume) / 25;
	audio_sound_gain(global.game_music, audio_volume, 0);
}

blur = clamp(blur, 0, blur_max);

pause_prev = (global.game_mod == "pause");