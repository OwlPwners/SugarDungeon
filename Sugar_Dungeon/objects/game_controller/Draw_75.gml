if (global.game_mod == "pause" || blur > 0) {
	if (surface_exists(surf)) {
		draw_surface_blur(surf, blur, blur / 7.5, 0, 0);
	
		var font = draw_get_font();
		draw_set_font(global.font_game);
	
		var i = 0;
		repeat(buttons) {
			button_draw(button_array[i], char_size);
		
			i ++;
		}
	
		draw_set_font(font);
	
		with(cursor) draw_self();
	} else {
		global.game_mod = "game";
	}
}