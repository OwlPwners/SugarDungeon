pressed = false;

lang = global.game_lang;

blur = 0;
blur_spd = 0.5;
blur_max = 20;

char_size = 16;

surf = -1;
pause_prev = false;

var centerx = display_get_gui_width() / 2, centery = display_get_gui_height() / 2, spr_h = sprite_get_height(spr_pause_button);

button_x = centerx;
button_y_start = [centery - spr_h * 5, centery + spr_h * 5];
button_y_to = [centery - spr_h * 2, centery + spr_h * 2];

button_alpha_start = [0, 0];
button_alpha_to = [1, 1];

button_spd = 12.25;
alpha_spd = 5.5;

button_array = [
	button_create(spr_pause_button, 1, 0, button_x, button_y_start[0], c_white, button_alpha_start[0], ini_translate(file_translate_menu, lang, "play")), 
	button_create(spr_pause_button, 1, 0, button_x, button_y_start[1], c_white, button_alpha_start[1], ini_translate(file_translate_menu, lang, "quit"))
];

buttons = array_length(button_array);

var i = 0;
repeat(buttons) {
	button_y_now[i] = ds_map_find_value(button_array[i], "Y");
	button_alpha_now[i] = ds_map_find_value(button_array[i], "Alpha");
	
	i ++;
}

audio_volume = 0;