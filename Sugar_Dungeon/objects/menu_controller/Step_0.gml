switch(page) {
	case(menu_pages.main) :
	var i = 0;
	
	repeat(main_buttons_count) {
		main_button_x_now[i] += (main_button_x_to[i] - main_button_x_now[i]) / button_coord_spd;
		main_button_y_now[i] += (main_button_y_to[i] - main_button_y_now[i] ) / button_coord_spd;
		main_button_alpha_now[i] += (main_button_alpha_to[i] - main_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(main_button_map[i], main_button_x_now[i], main_button_y_now[i]);
		button_set_alpha(main_button_map[i], main_button_alpha_now[i]);
		
		if (button_check_released(main_button_map[i], font_size)) event_user(main_buttons_events[i]);
		
		i ++;
	}
	
	var i = 0; 
	repeat(settings_buttons_count) {
		settings_button_x_now[i] += (settings_button_x_start[i] - settings_button_x_now[i]) / button_coord_spd;
		settings_button_y_now[i] += (settings_button_y_start[i] - settings_button_y_now[i] ) / button_coord_spd;
		settings_button_alpha_now[i] += (settings_button_alpha_start[i] - settings_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(settings_button_map[i], settings_button_x_now[i], settings_button_y_now[i]);
		button_set_alpha(settings_button_map[i], settings_button_alpha_now[i]);
		
		i ++;
	}
	break;
	
	case(menu_pages.settings) :
	var i = 0;
	
	repeat(main_buttons_count) {
		main_button_x_now[i] += (main_button_x_start[i] - main_button_x_now[i]) / button_coord_spd;
		main_button_y_now[i] += (main_button_y_start[i] - main_button_y_now[i] ) / button_coord_spd;
		main_button_alpha_now[i] += (main_button_alpha_start[i] - main_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(main_button_map[i], main_button_x_now[i], main_button_y_now[i]);
		button_set_alpha(main_button_map[i], main_button_alpha_now[i]);
		
		i ++;
	}
		
	var i = 0;
		
	repeat(settings_buttons_count) {
		settings_button_x_now[i] += (settings_button_x_to[i] - settings_button_x_now[i]) / button_coord_spd;
		settings_button_y_now[i] += (settings_button_y_to[i] - settings_button_y_now[i] ) / button_coord_spd;
		settings_button_alpha_now[i] += (settings_button_alpha_to[i] - settings_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(settings_button_map[i], settings_button_x_now[i], settings_button_y_now[i]);
		button_set_alpha(settings_button_map[i], settings_button_alpha_now[i]);
		
		if (button_check_released(settings_button_map[i], font_size)) event_user(settings_buttons_events[i]);
			
		i ++;
	}
	
	var i = 0;
	
	repeat(control_buttons_count) {
		control_button_x_now[i] += (control_button_x_start[i] - control_button_x_now[i]) / button_coord_spd;
		control_button_y_now[i] += (control_button_y_start[i] - control_button_y_now[i] ) / button_coord_spd;
		control_button_alpha_now[i] += (control_button_alpha_start[i] - control_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(control_button_map[i], control_button_x_now[i], control_button_y_now[i]);
		button_set_alpha(control_button_map[i], control_button_alpha_now[i]);
		
		i ++;
	}
	
	var i = 0;
		
	repeat(lang_buttons_count) {
		lang_button_x_now[i] += (lang_button_x_start[i] - lang_button_x_now[i]) / button_coord_spd;
		lang_button_y_now[i] += (lang_button_y_start[i] - lang_button_y_now[i] ) / button_coord_spd;
		lang_button_alpha_now[i] += (lang_button_alpha_start[i] - lang_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(lang_button_map[i], lang_button_x_now[i], lang_button_y_now[i]);
		button_set_alpha(lang_button_map[i], lang_button_alpha_now[i]);
			
		i ++;
	}
	break;
		
	case(menu_pages.audio) :
	var i = 0;
	
	repeat(settings_buttons_count) {
		settings_button_x_now[i] += (settings_button_x_start[i] - settings_button_x_now[i]) / button_coord_spd;
		settings_button_y_now[i] += (settings_button_y_start[i] - settings_button_y_now[i] ) / button_coord_spd;
		settings_button_alpha_now[i] += (settings_button_alpha_start[i] - settings_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(settings_button_map[i], settings_button_x_now[i], settings_button_y_now[i]);
		button_set_alpha(settings_button_map[i], settings_button_alpha_now[i]);
		
		i ++;
	}
	break;
	
	case(menu_pages.control) :
	var i = 0;
	
	repeat(settings_buttons_count) {
		settings_button_x_now[i] += (settings_button_x_start[i] - settings_button_x_now[i]) / button_coord_spd;
		settings_button_y_now[i] += (settings_button_y_start[i] - settings_button_y_now[i] ) / button_coord_spd;
		settings_button_alpha_now[i] += (settings_button_alpha_start[i] - settings_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(settings_button_map[i], settings_button_x_now[i], settings_button_y_now[i]);
		button_set_alpha(settings_button_map[i], settings_button_alpha_now[i]);
		
		i ++;
	}
	
	var i = 0;
	
	repeat(control_buttons_count) {
		control_button_x_now[i] += (control_button_x_to[i] - control_button_x_now[i]) / button_coord_spd;
		control_button_y_now[i] += (control_button_y_to[i] - control_button_y_now[i] ) / button_coord_spd;
		control_button_alpha_now[i] += (control_button_alpha_to[i] - control_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(control_button_map[i], control_button_x_now[i], control_button_y_now[i]);
		button_set_alpha(control_button_map[i], control_button_alpha_now[i]);
		
		if (control_check_key[i] == false) {
			control_last_key[i] = control_buttons_keys[i];
			
			if (control_can_other_check == true) {
				control_check_key[i] = button_check_pressed(control_button_map[i], font_size);
			
				if (control_check_key[i] == true) control_can_other_check = false;
			}
		} else {
			control_buttons_keys[i] = keyboard_lastkey;
			
			button_set_text(control_button_map[i], control_buttons_keys_strings[i]);
			
			if (keyboard_check_released(vk_anykey)) {
				var set_text = "";
				
				switch(keyboard_lastkey) {
					#region : - )
					case(vk_left) : 
					set_text = "LEFT"
					break;
					
					case(vk_right) : 
					set_text = "RIGHT"
					break;
					
					case(vk_up) : 
					set_text = "UP";
					break;
					
					case(vk_down) : 
					set_text = "DOWN";
					break;
					
					case(vk_tab) : 
					set_text = "TAB";
					break;
					
					case(vk_enter) : 
					set_text = "ENTER";
					break;
					
					case(vk_pagedown) : 
					set_text = "PAGEDOWN";
					break;
					
					case(vk_pageup) : 
					set_text = "PAGEUP";
					break;
					
					case(vk_pause) : 
					set_text = "PAUSE";
					break;
					
					case(vk_shift) : 
					set_text = "SHIFT";
					break;
					
					case(vk_lshift) : 
					set_text = "L-SHIFT";
					break;
					
					case(vk_rshift) : 
					set_text = "R-SHIFT";
					break;
					
					case(vk_alt) : 
					set_text = "ALT";
					break;
					
					case(vk_lalt) : 
					set_text = "L-ALT";
					break;
					
					case(vk_ralt) : 
					set_text = "R-ALT";
					break;
					
					case(vk_delete) : 
					set_text = "DELETE";
					break;
					
					case(vk_backspace) : 
					set_text = "BACKSPACE";
					break;
					
					case(vk_space) : 
					set_text = "SPACE";
					break;
					
					case(vk_control) : 
					set_text = "CTRL";
					break;
					
					case(vk_lcontrol) : 
					set_text = "L-CTRL";
					break;
					
					case(vk_rcontrol) : 
					set_text = "R-CTRL";
					break;
					
					default :
					set_text = chr(keyboard_lastkey);
					break;
					#endregion
				}
				
				button_set_text(control_button_map[i], control_buttons_keys_strings[i] + set_text);
				
				control_check_key[i] = false;
				control_can_other_check = true;
			}
		}
		
		i ++;
	}
	break;
	
	case(menu_pages.languauge) :
	var i = 0;
	
	repeat(settings_buttons_count) {
		settings_button_x_now[i] += (settings_button_x_start[i] - settings_button_x_now[i]) / button_coord_spd;
		settings_button_y_now[i] += (settings_button_y_start[i] - settings_button_y_now[i] ) / button_coord_spd;
		settings_button_alpha_now[i] += (settings_button_alpha_start[i] - settings_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(settings_button_map[i], settings_button_x_now[i], settings_button_y_now[i]);
		button_set_alpha(settings_button_map[i], settings_button_alpha_now[i]);
		
		i ++;
	}
	
	var i = 0;
		
	repeat(lang_buttons_count) {
		lang_button_x_now[i] += (lang_button_x_to[i] - lang_button_x_now[i]) / button_coord_spd;
		lang_button_y_now[i] += (lang_button_y_to[i] - lang_button_y_now[i] ) / button_coord_spd;
		lang_button_alpha_now[i] += (lang_button_alpha_to[i] - lang_button_alpha_now[i]) / button_alpha_spd;
		
		button_set_coord(lang_button_map[i], lang_button_x_now[i], lang_button_y_now[i]);
		button_set_alpha(lang_button_map[i], lang_button_alpha_now[i]);
		
		if (button_check_released(lang_button_map[i], font_size)) {
			event_user(lang_buttons_events[i]);
			
			ini_save(file_settings, "Real", "Languauge", "Lang", lang_index);
			
			audio_stop_all();
			
			room_restart();
		}
			
		i ++;
	}
	break;
}

if (page != menu_pages.main) {
	back_button_x_now += (back_button_x_to - back_button_x_now) / button_coord_spd;
	back_button_y_now += (back_button_y_to - back_button_y_now ) / button_coord_spd;
	back_button_alpha_now += (back_button_alpha_to - back_button_alpha_now) / button_alpha_spd;
		
	button_set_coord(back_button_map, back_button_x_now, back_button_y_now);
	button_set_alpha(back_button_map, back_button_alpha_now);
		
	if (button_check_released(back_button_map, font_size)) event_user(back_buttons_event);
} else {
	back_button_x_now += (back_button_x_start - back_button_x_now) / button_coord_spd;
	back_button_y_now += (back_button_y_start - back_button_y_now ) / button_coord_spd;
	back_button_alpha_now += (back_button_alpha_to - back_button_alpha_now) / button_alpha_spd;
		
	button_set_coord(back_button_map, back_button_x_now, back_button_y_now);
	button_set_alpha(back_button_map, back_button_alpha_now);
}

if (page == menu_pages.audio) {
	slider_alpha += (1 - slider_alpha) / slider_alpha_spd;
} else {
	slider_alpha += (0 - slider_alpha) / slider_alpha_spd;
	
	if (page_prev == menu_pages.audio) {
		ini_save(file_settings, "Real", "SoundsAndMusic", "MusicSlider", music_volume);
		ini_save(file_settings, "Real", "SoundsAndMusic", "MusicVolume", global.music_gain);
	}
}

if (page != menu_pages.control && page_prev == menu_pages.control) {
	var i = 0;
	repeat(control_buttons_count) {
		ini_save(file_settings, "Real", "Control", control_buttons_keys_ini[i], control_buttons_keys[i]);
		
		i ++;
	}
}

slider_alpha = clamp(slider_alpha, 0, 1);

page_prev = page;