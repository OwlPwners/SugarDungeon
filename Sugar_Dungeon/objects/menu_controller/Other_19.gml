///@desc New game.
//if (file_exists(file_seed)) file_delete(file_seed);
if (file_exists(file_room)) file_delete(file_room);
if (file_exists(file_chapter)) file_delete(file_chapter);

audio_stop_all();

if (audio_group_is_loaded(agp_menu_music)) audio_group_unload(agp_menu_music);

with(room_loader) {
	rm_goto = rm_cutscene_start;
}