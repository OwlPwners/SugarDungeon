var i = 0;
repeat(max(main_buttons_count, settings_buttons_count, control_buttons_count, lang_buttons_count)) {
	var main_index = min(i, main_buttons_count - 1), settings_index = min(i, settings_buttons_count - 1), control_index = min(i, control_buttons_count - 1), _lang_index = min(i, lang_buttons_count - 1);
	
	button_destroy(main_button_map[main_index]);
	button_destroy(settings_button_map[settings_index]);
	button_destroy(control_button_map[control_index]);
	button_destroy(lang_button_map[_lang_index]);
	
	i ++;
}

if (audio_group_is_loaded(agp_menu_music)) audio_group_unload(agp_menu_music);