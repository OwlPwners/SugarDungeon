var font_prev = draw_get_font();
draw_set_font(global.font_game);

switch(page) {
	case(menu_pages.main) :
	var i = 0;
	
	repeat(max(main_buttons_count, settings_buttons_count)) {
		var main_index = min(i, main_buttons_count - 1), settings_index = min(i, settings_buttons_count - 1);
		
		button_draw(main_button_map[main_index], font_size);
		button_draw(settings_button_map[settings_index], font_size);
		
		i ++;
	}
	break;
	
	case(menu_pages.settings) :
	var i = 0; 
	
	repeat(max(control_buttons_count, settings_buttons_count, lang_buttons_count)) {
		var control_index = min(i, control_buttons_count - 1), settings_index = min(i, settings_buttons_count - 1), _lang_index = min(i, lang_buttons_count - 1);
		
		button_draw(control_button_map[control_index], font_size);
		button_draw(settings_button_map[settings_index], font_size);
		button_draw(lang_button_map[_lang_index], font_size);
		
		i ++;
	}
	break;
		
	case(menu_pages.audio) :
	var i = 0; 
	
	repeat(settings_buttons_count) {
		
		button_draw(settings_button_map[i], font_size);
		
		i ++;
	}
	break;
	
	case(menu_pages.control) :
	var i = 0; 
	
	repeat(max(control_buttons_count, settings_buttons_count)) {
		var control_index = min(i, control_buttons_count - 1), settings_index = min(i, settings_buttons_count - 1);
		
		button_draw(control_button_map[control_index], font_size);
		button_draw(settings_button_map[settings_index], font_size);
		
		i ++;
	}
	break;
	
	case(menu_pages.languauge) :
	var i = 0; 
	
	repeat(max(lang_buttons_count, settings_buttons_count)) {
		var _lang_index = min(i, lang_buttons_count - 1), settings_index = min(i, settings_buttons_count - 1);
		
		button_draw(lang_button_map[_lang_index], font_size);
		button_draw(settings_button_map[settings_index], font_size);
		
		i ++;
	}
	break;
}

button_draw(back_button_map, font_size);

if (page == menu_pages.audio) {
	draw_set_alpha(slider_alpha);
	
	var spr_width = sprite_get_width(spr_menu_sounds);
	
	var slider_x1 = centr_x - slider_width / 2,
	slider_x2 = centr_x + slider_width / 2,
	slider_y1 = centr_y - slider_height / 2,
	slider_y2 = centr_y + slider_height / 2;
	
	var press_in_rect = point_in_rectangle(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), slider_x1, slider_y1, slider_x2, slider_x1) && (mouse_check_button(mb_left) || cursor.click);
	
	if (press_in_rect && alpha > 0.8) {
		music_volume = device_mouse_x_to_gui(0); 
		global.music_gain = draw_slider(spr_menu_button, 0, spr_menu_slider, 0, slider_x1, slider_x2, slider_y1, slider_y2, music_volume, centr_y, 0);
		audio_group_set_gain(agp_menu_music, global.music_gain, 0);
		audio_group_set_gain(agp_floor_music, global.music_gain, 0);
	}
	
	draw_sprite(spr_menu_sounds, 0, slider_x1 - spr_width, centr_y);
	draw_slider(spr_menu_button, 0, spr_menu_slider, press_in_rect, slider_x1, slider_x2, slider_y1, slider_y2, music_volume, centr_y, slider_alpha);
	
	draw_set_alpha(1);
}

draw_set_font(font_prev);