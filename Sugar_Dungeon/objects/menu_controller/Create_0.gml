draw_set_font(global.font_game);

audio_stop_all();

instance_destroy(player);

display_set_gui_size(gui_width, gui_height);

enum menu_pages {
	main,
	settings,
	audio,
	control,
	languauge,
	play
}

page_prev = -1;
page = menu_pages.main;

centr_x = gui_width / 2;
centr_y = gui_height / 2;

button_width = sprite_get_width(spr_menu_button);
button_height = sprite_get_height(spr_menu_button);

button_coord_spd = 5;
button_alpha_spd = 3;

font_size = 12;

lang = global.game_lang;
lang_index = ini_load(file_settings, "Real", "Languauge", "Lang", 0);



back_button_x_start = -button_width * 3;
back_button_y_start = button_height * 3;
back_button_alpha_start = 0;

back_button_x_to = button_width * 3;
back_button_y_to = button_height * 3;
back_button_alpha_to = 1;

back_buttons_event = 3;

back_button_map = button_create(spr_menu_button, 1, 0, back_button_x_start, back_button_y_start, c_white, back_button_alpha_start, ini_translate(file_translate_menu, lang[lang_index], "back"));

back_button_x_now = ds_map_find_value(back_button_map, "X");
back_button_y_now = ds_map_find_value(back_button_map, "Y");
back_button_alpha_now = ds_map_find_value(back_button_map, "Alpha");



main_button_x_start = [centr_x, centr_x, centr_x, centr_x];
main_button_y_start = [centr_y, centr_y, centr_y, centr_y];
main_button_alpha_start = [0, 0, 0, 0];

main_button_x_to = [centr_x, centr_x, centr_x, centr_x];
main_button_y_to = [centr_y - button_height * 5, centr_y + button_height * 1.75, centr_y + button_height * 5, centr_y - button_height * 1.75];
main_button_alpha_to = [1, 1, 1, 1];

main_buttons_events = [0, 1, 2, 9];

main_button_map = [
	button_create(spr_menu_button, 1, 0, main_button_x_start[0], main_button_y_start[0], c_white, main_button_alpha_start[0], ini_translate(file_translate_menu, lang[lang_index], "play")),
	button_create(spr_menu_button, 1, 0, main_button_x_start[1], main_button_y_start[1], c_white, main_button_alpha_start[1], ini_translate(file_translate_menu, lang[lang_index], "settings")),
	button_create(spr_menu_button, 1, 0, main_button_x_start[2], main_button_y_start[2], c_white, main_button_alpha_start[2], ini_translate(file_translate_menu, lang[lang_index], "quit")),
	button_create(spr_menu_button, 1, 0, main_button_x_start[3], main_button_y_start[3], c_white, main_button_alpha_start[3], ini_translate(file_translate_menu, lang[lang_index], "newgame"))
]

main_buttons_count = array_length(main_button_map);

var i = 0;
repeat(main_buttons_count) {
	main_button_x_now[i] = ds_map_find_value(main_button_map[i], "X");
	main_button_y_now[i] = ds_map_find_value(main_button_map[i], "Y");
	main_button_alpha_now[i] = ds_map_find_value(main_button_map[i], "Alpha");
	
	i ++;
}



settings_button_x_start = [centr_x, centr_x, centr_x];
settings_button_y_start = [centr_y - button_height * 5, centr_y, centr_y + button_height * 5];
settings_button_alpha_start = [0, 0, 0];

settings_button_x_to = [centr_x, centr_x, centr_x];
settings_button_y_to = [centr_y - button_height * 3, centr_y, centr_y + button_height * 3];
settings_button_alpha_to = [1, 1, 1];

settings_buttons_events = [4, 5, 6];

settings_button_map = [
	button_create(spr_menu_button, 1, 0, settings_button_x_start[0], settings_button_y_start[0], c_white, settings_button_alpha_start[0], ini_translate(file_translate_menu, lang[lang_index], "audio")),
	button_create(spr_menu_button, 1, 0, settings_button_x_start[1], settings_button_y_start[1], c_white, settings_button_alpha_start[1], ini_translate(file_translate_menu, lang[lang_index], "control")),
	button_create(spr_menu_button, 1, 0, settings_button_x_start[2], settings_button_y_start[2], c_white, settings_button_alpha_start[2], ini_translate(file_translate_menu, lang[lang_index], "languauge"))
]

settings_buttons_count = array_length(settings_button_map);

var i = 0;
repeat(settings_buttons_count) {
	settings_button_x_now[i] = ds_map_find_value(settings_button_map[i], "X");
	settings_button_y_now[i] = ds_map_find_value(settings_button_map[i], "Y");
	settings_button_alpha_now[i] = ds_map_find_value(settings_button_map[i], "Alpha");
	
	i ++;
}



control_button_x_start = [centr_x, centr_x, centr_x, centr_x, centr_x, centr_x, centr_x, centr_x, centr_x, centr_x, centr_x];
control_button_y_start = [centr_y, centr_y, centr_y, centr_y, centr_y, centr_y, centr_y, centr_y, -button_height, gui_height + button_height, centr_y];
control_button_alpha_start = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

control_button_x_to = [centr_x - button_width * 5 - button_width / 2, centr_x + button_width * 5 - button_width / 2, centr_x - button_width * 5 - button_width / 2, centr_x + button_width * 5 - button_width / 2, centr_x - button_width * 12 - button_width / 2, centr_x + button_width * 12 - button_width / 2, centr_x - button_width * 12 - button_width / 2, centr_x + button_width * 12 - button_width / 2, centr_x, centr_x, centr_x];
control_button_y_to = [centr_y + button_height * 3 - button_height / 2, centr_y - button_height * 3 - button_height / 2, centr_y - button_height * 3 - button_height / 2, centr_y + button_height * 3 - button_height / 2, centr_y + button_height * 7 - button_height / 2, centr_y + button_height * 7 - button_height / 2, centr_y - button_height * 7 - button_height / 2, centr_y - button_height * 7 - button_height / 2, (button_height + font_size) * 2, gui_height - (button_height + font_size) * 2, centr_y];
control_button_alpha_to = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

control_buttons_keys_ini = ["LeftKey", "RightKey", "UpKey", "DownKey", "PlusSlotKey", "MinusSlotKey", "AttackKey", "BlockKey", "InteractKey", "GlideKey", "ShowMap"];

control_buttons_keys_default = [ord("A"), ord("D"), ord("W"), ord("S"), vk_right, vk_left, vk_down, vk_up, vk_shift, vk_space, vk_tab];

control_buttons_keys = [ord("A"), ord("D"), ord("W"), ord("S"), vk_right, vk_left, vk_down, vk_up, vk_shift, vk_space, vk_tab];
control_buttons_keys_strings = [ini_translate(file_translate_menu, lang[lang_index], "left"), ini_translate(file_translate_menu, lang[lang_index], "right"), ini_translate(file_translate_menu, lang[lang_index], "up"), ini_translate(file_translate_menu, lang[lang_index], "down"), ini_translate(file_translate_menu, lang[lang_index], "slotplus"),ini_translate(file_translate_menu, lang[lang_index], "slotminus"), ini_translate(file_translate_menu, lang[lang_index], "attack"), ini_translate(file_translate_menu, lang[lang_index], "block"), ini_translate(file_translate_menu, lang[lang_index], "interact"), ini_translate(file_translate_menu, lang[lang_index], "glide"), ini_translate(file_translate_menu, lang[lang_index], "showmap")];
control_check_key = [false, false, false, false, false, false, false, false, false, false, false];
control_last_key = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
control_can_other_check = true;

control_button_map = [
	button_create(spr_menu_button, 1, 0, control_button_x_start[0], control_button_y_start[0], c_white, control_button_alpha_start[0], control_buttons_keys_strings[0] + chr(control_buttons_keys_default[0])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[1], control_button_y_start[1], c_white, control_button_alpha_start[1], control_buttons_keys_strings[1] + chr(control_buttons_keys_default[1])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[2], control_button_y_start[2], c_white, control_button_alpha_start[2], control_buttons_keys_strings[2] + chr(control_buttons_keys_default[2])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[3], control_button_y_start[3], c_white, control_button_alpha_start[3], control_buttons_keys_strings[3] + chr(control_buttons_keys_default[3])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[4], control_button_y_start[4], c_white, control_button_alpha_start[4], control_buttons_keys_strings[4] + chr(control_buttons_keys_default[4])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[5], control_button_y_start[5], c_white, control_button_alpha_start[5], control_buttons_keys_strings[5] + chr(control_buttons_keys_default[5])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[6], control_button_y_start[6], c_white, control_button_alpha_start[6], control_buttons_keys_strings[6] + chr(control_buttons_keys_default[6])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[7], control_button_y_start[7], c_white, control_button_alpha_start[7], control_buttons_keys_strings[7] + chr(control_buttons_keys_default[7])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[8], control_button_y_start[8], c_white, control_button_alpha_start[8], control_buttons_keys_strings[8] + chr(control_buttons_keys_default[8])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[9], control_button_y_start[8], c_white, control_button_alpha_start[9], control_buttons_keys_strings[9] + chr(control_buttons_keys_default[9])),
	button_create(spr_menu_button, 1, 0, control_button_x_start[10], control_button_y_start[10], c_white, control_button_alpha_start[10], control_buttons_keys_strings[10] + chr(control_buttons_keys_default[10])),
]

control_buttons_count = array_length(control_button_map);

var i = 0;
repeat(control_buttons_count) {
	control_button_x_now[i] = ds_map_find_value(control_button_map[i], "X");
	control_button_y_now[i] = ds_map_find_value(control_button_map[i], "Y");
	control_button_alpha_now[i] = ds_map_find_value(control_button_map[i], "Alpha");
	
	button_set_text(control_button_map[i], control_buttons_keys_strings[i]);
		
		var set_text = "", ini = ini_load(file_settings, "Real", "Control", control_buttons_keys_ini[i], control_buttons_keys_default[i]);
			
		switch(ini) {
			#region : - )
				case(vk_left) : 
				set_text = "LEFT"
				break;
					
				case(vk_right) : 
				set_text = "RIGHT"
				break;
					
				case(vk_up) : 
				set_text = "UP";
				break;
					
				case(vk_down) : 
				set_text = "DOWN";
				break;
					
				case(vk_tab) : 
				set_text = "TAB";
				break;
					
				case(vk_enter) : 
				set_text = "ENTER";
				break;
					
				case(vk_pagedown) : 
				set_text = "PAGEDOWN";
				break;
					
				case(vk_pageup) : 
				set_text = "PAGEUP";
				break;
					
				case(vk_pause) : 
				set_text = "PAUSE";
				break;
					
				case(vk_shift) : 
				set_text = "SHIFT";
				break;
					
				case(vk_lshift) : 
				set_text = "L-SHIFT";
				break;
					
				case(vk_rshift) : 
				set_text = "R-SHIFT";
				break;
					
				case(vk_alt) : 
				set_text = "ALT";
				break;
					
				case(vk_lalt) : 
				set_text = "L-ALT";
				break;
					
				case(vk_ralt) : 
				set_text = "R-ALT";
				break;
					
				case(vk_delete) : 
				set_text = "DELETE";
				break;
					
				case(vk_backspace) : 
				set_text = "BACKSPACE";
				break;
					
				case(vk_space) : 
				set_text = "SPACE";
				break;
					
				case(vk_control) : 
				set_text = "CTRL";
				break;
					
				case(vk_lcontrol) : 
				set_text = "L-CTRL";
				break;
					
				case(vk_rcontrol) : 
				set_text = "R-CTRL";
				break;
					
				default :
				set_text = chr(ini);
				break;
				#endregion
		}
				
	button_set_text(control_button_map[i], control_buttons_keys_strings[i] + set_text);
	control_buttons_keys[i] = ini;
	
	i ++;
}



var lang_button_width = sprite_get_width(spr_languauges);

lang_button_x_start = [centr_x - lang_button_width / 2 * 10, centr_x + lang_button_width / 2 * 10];
lang_button_y_start = [centr_y, centr_y];
lang_button_alpha_start = [0, 0];

lang_button_x_to = [centr_x - lang_button_width / 2 * 3, centr_x + lang_button_width / 2 * 3];
lang_button_y_to = [centr_y, centr_y];
lang_button_alpha_to = [1, 1];

lang_buttons_events = [7, 8];

lang_button_map = [
	button_create(spr_languauges, 0, 1, lang_button_x_start[0], lang_button_y_start[0], c_white, lang_button_alpha_start[0], -1),
	button_create(spr_languauges, 2, 3, lang_button_x_start[1], lang_button_y_start[1], c_white, lang_button_alpha_start[1], -1),
]

lang_buttons_count = array_length(lang_button_map);

var i = 0;
repeat(lang_buttons_count) {
	lang_button_x_now[i] = ds_map_find_value(lang_button_map[i], "X");
	lang_button_y_now[i] = ds_map_find_value(lang_button_map[i], "Y");
	lang_button_alpha_now[i] = ds_map_find_value(lang_button_map[i], "Alpha");
	
	i ++;
}



slider_width = centr_x * 2 / 5;
slider_height = centr_y * 2 / 25;

slider_alpha = 0;
slider_alpha_spd = 3;

var music_slider_value = ini_load(file_settings, "Real", "SoundsAndMusic", "MusicSlider", gui_width),
music_volume_value = ini_load(file_settings, "Real", "SoundsAndMusic", "MusicVolume", 1);

music_volume = music_slider_value;
global.music_gain = music_volume_value;

audio_group_set_gain(agp_menu_music, global.music_gain, 0);
audio_group_set_gain(agp_floor_music, global.music_gain, 0);