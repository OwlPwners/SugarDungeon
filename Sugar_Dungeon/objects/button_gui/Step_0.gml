change.moving();
change.alphing();

is_pressed = (image == image_pressed) && check_mouse.released(mb_left);
if (is_pressed && alpha > 0.8 && script > -1) {
	script();
	
	audio_play_sound(sfx_menu_button_pressed, 81, false);
}