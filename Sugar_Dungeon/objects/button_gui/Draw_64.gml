if (!draw) exit;
var col_cursor = check_mouse.collision();

if (col_cursor) {
	image = image_pressed;
	
	if (col_cursor != col_cursor_prev && alpha > 0.8) audio_play_sound(sfx_menu_button_check, 82, false);
} else {
	image = image_released;
}

if (text != "") {
	rendering.box();
	rendering.text();
} else {
	rendering.sprite();
}

if (global.debug) {
	draw_set_alpha(alpha);
	draw_rectangle(x - width / 2, y - height / 2, x + width / 2, y + height / 2, true);
	draw_set_alpha(1);
}

col_cursor_prev = col_cursor;