event_inherited();
//save_object_add_variables("activated");
create_bounce_system(1);
depth_variables();

enemy_default_variables();
save_object_add_variables("xto", "yto", "activated", "activated_prev");

activated = false;
activated_prev = false;

z = 0;
z_floor = 0;

grav = 0.2;
spd = 1.8;

hsp = 0;
vsp = 0;
zsp = 0;

part_fire_create();