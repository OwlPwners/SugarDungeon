save_object_default_variables();
save_object_add_variables("toward");

image_speed = 0;

z = 0;

hsp = 0;
vsp = 0;

spd = 6.4;
zgrav = 0.24;

var d = random(360), l = 30;
xto = x + lengthdir_x(l, d);
yto = y + lengthdir_y(l, d);

var f = point_distance(x, y, xto, yto) + point_distance(0, 0, sprite_width, sprite_height);
zsp = -1 * f / zgrav / 60 / spd * 3;

toward = false;

timer_toward = 150;

cost = 0;