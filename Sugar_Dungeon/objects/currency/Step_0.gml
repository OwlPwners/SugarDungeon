timer_toward --;

if (timer_toward <= 0) toward = true;

if (toward) {
	var dx, dy, len;
	dx = (player.x - x);
	dy = (player.y - y);
	len = sqrt(dx * dx + dy * dy);
	if (len != 0) {
		dx /= len;
		dy /= len;
	}
	
	hsp = dx * spd;
	vsp = dy * spd;
	
	x += hsp;
	y += vsp;

	if (place_meeting(x, y, player)) {
		instance_destroy(self);
		
		text_indicator_create(x, y, "+" + string(cost), text_currecny_color);
		
		player.money += cost;
	}
} else {
	zsp += zgrav;
	
	var dx, dy, len;
	dx = (xto - x) * spd;
	dy = (yto - y) * spd;
	
	len = sqrt(dx * dx + dy * dy);
	if (len != 0) {
		dx /= len;
		dy /= len;
	}

	hsp = dx;
	vsp = dy;
	
	if (z + zsp >= 0) {
		zsp = 0;
		z = 0;
		
		if (point_distance(x, y, player.x, player.y) < tile_size) toward = true;
	}
	
	y -= z; // Set 2D position. 
	
	if (tile_meeting(layer_collision, x + hsp, y)) {
		repeat(abs(hsp)) {
			var xcheck = floor(hsp) + sign(hsp);
			if (!tile_meeting(layer_collision, x + xcheck, y)) {
				x += sign(hsp);
			} else {
				break;
			}
		}
	}
	
	x += hsp;
	
	if (tile_meeting(layer_collision, x, y + vsp)) {
		repeat(abs(vsp)) {
			var ycheck = floor(vsp) + sign(vsp);
			if (!tile_meeting(layer_collision, x, y + ycheck)) {
				y += sign(vsp);
			} else {
				break;
			}
		}
	}
	
	y += vsp;
	
	y += z; // Remove position to 2.5D.
	
	z += zsp;
}