if (global.game_mod != "generating") instance_destroy(self);

var spd_show = 0.0175, spd_timer = 1, alpha_factor = 1;
if (instance_exists(player) && player.clamping) {
	spd_show *= 2;
	spd_timer *= 2;
	alpha_factor = 0.75;
}

time_show -= spd_timer;

if (!showed) {
	text_x = lerp(text_x, gui_width / 2, spd_show);
	text_alpha = lerp(text_alpha * alpha_factor, 0.5 * alpha_factor, spd_show * 2);
		
	if (time_show < 0) showed = true;
} else {
	text_x = lerp(text_x, gui_width, spd_show);
	text_alpha = lerp(text_alpha, 0, spd_show * 2);
	
	if (text_alpha == 0) {
		global.game_mod = "game";
			
		instance_destroy(self);
	}
}