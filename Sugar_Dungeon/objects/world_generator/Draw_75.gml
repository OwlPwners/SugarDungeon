draw_set_halign(fa_middle);
draw_set_valign(fa_center);
draw_set_alpha(text_alpha);

draw_sprite(sprite_splash, 0, gui_width / 2, gui_height / 2);
draw_text(text_x, gui_height / 2, text_level + " " + string(chapter));

draw_set_alpha(1);
draw_set_valign(fa_top);
draw_set_halign(fa_left);