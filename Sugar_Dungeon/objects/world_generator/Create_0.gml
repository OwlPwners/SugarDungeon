draw_texture_flush();

global.game_mod = "generating";

if (instance_exists(player)) player.draw = false;

text_alpha = 0;
text_x = 0;
text_level = ini_translate(file_translate_gamestart, global.game_lang, "level");
showed = false;
time_show = 300;

alarm[0] = 1;

if (file_exists(file_room)) {
	saved = true;
	
	//var file = file_text_open_read(file_room);
	//load_string = file_text_read_string(file);
	//file_text_close(file);
	
	load_string = buffer_load_string(file_room);
} else {
	saved = false;
	
	load_string = -1;
}

if (file_exists(file_chapter)) {
	saved_chapter = false;
	
	var file = file_text_open_read(file_chapter);
	chapter = real(file_text_read_string(file));
	file_text_close(file);
} else {
	saved_chapter = false;
	chapter = 1;
}

if (!file_exists(file_rooms_list)) saved = false;

//chapter = chapter_diving + 1;

var chap = chapter_fragament;
layer_background_sprite(layer_bg_floor, asset_get_index("spr_floor_background" + string(chap)));
tilemap_tileset(layer_rooms, asset_get_index("tst_decor" + string(chap)));

sprite_splash = asset_get_index("spr_floor_splash" + string(chap));

global.game_music = asset_get_index("so_floor" + string(chap));
//with(instance_create_layer(x, y, layer_controllers, audio_controller)) {
//	audiogroup[0] = agp_floor_music;
//	sound = global.game_music;
//	priority = 100;
//	loops = true;
//	update_timer = 3;
//	must_unloading = false;
//}