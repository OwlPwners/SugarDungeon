if (!draw) exit;
	
var slider_x1 = x - width / 2,
slider_x2 = x + width / 2,
slider_y1 = y - height / 2,
slider_y2 = y + height / 2;

var col = point_in_rectangle(cursor.x, cursor.y, slider_x1, slider_y1, slider_x2, slider_y2),
press = cursor.click || mouse_check_button(mb_left);
if (col) {
	image = image_pressed;
	
	if (press) {
		volume = cursor.x;
		
		if (script > -1) {
			script_execute(script);
		}
	}
} else {
	image = image_released;
}

var spr_width = sprite_get_width(icons_sprite);

draw_slider(sprite_rectangle, 0, sprite_button, image, slider_x1, slider_x2, slider_y1, slider_y2, volume, y, alpha);
draw_sprite_ext(icons_sprite, icons_image, slider_x1 - spr_width * 1.5, y, 1, 1, 0, c_white, alpha);