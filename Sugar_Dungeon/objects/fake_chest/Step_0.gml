if (!layer_changed) {
	layer = layer_other;
	layer_changed = true;
}

if (!instance_exists(player)) exit;
selected = point_distance(player.x, player.y, x, y) < player.selected_dist;

if (selected) {
	if (!surface_exists(outline_surf)) {
		outline_surf = surface_create(sprite_width + 3, sprite_height + 3);
		surface_set_target(outline_surf);
		
		var xx = sprite_get_xoffset(sprite_index) + 1, yy = sprite_get_yoffset(sprite_index) + 1;
		draw_sprite(sprite_index, image_index, xx, yy);
		
		surface_reset_target()
	}
	
	outline_alpha += (1 - outline_alpha) / 5;
	
	if (player.key_interact) {
		with(instance_create_layer(x, y, layer_enemies, obj)) {
			clamp_box_id = other.clamp_box_id;
			clamp_box_id.enemy_num_lives ++;
		}
		
		surface_free(outline_surf);
		instance_destroy(self);
		
		if (interact_event != -1) event_user(interact_event);
	}
} else {
	//if (outline_surf != -1 && outline_alpha == 0) {
	//	surface_free(outline_surf);
		
	//	outline_surf = -1;
	//}
	
	outline_alpha += (0 - outline_alpha) / 5;
}