event_inherited();

image_speed = 0;
image_index = 0;

depth = -bbox_bottom

outline_alpha = 0;
outline_surf = surface_create(sprite_width + 3, sprite_height + 3);
surface_set_target(outline_surf);
		
var xx = sprite_get_xoffset(sprite_index) + 1, yy = sprite_get_yoffset(sprite_index) + 1;
draw_sprite(sprite_index, image_index, xx, yy);
		
surface_reset_target()
selected = false;

interact_event = 0;

obj = purple_mimic;