if (global.game_mod != "player_dead") show_results = false;

if (show_results) {
	with(camera) {
		zoom = false;
	}
	
	blur += (blur_max - blur) / 25;
	if (blur == blur_max) button_alpha += (0.7 - button_alpha) / 10;
	button_set_alpha(button, button_alpha);
	
	if (button_alpha > 0.5 && button_check_released(button, 16)) {
		with(room_loader) {
			rm_goto = rm_menu;
			transition = room_transition.lollipop;
		}
		
		//with(audio_controller) audio_group_unload_num(audiogroup);
		audio_stop_all();
		
		global.game_mod = "game";
	}
	
	depth = light_controller.depth - 1;
}