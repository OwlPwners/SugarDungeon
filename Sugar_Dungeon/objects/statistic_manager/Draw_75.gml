if (show_results) {
	if (!surface_exists(surf_blur)) surf_blur = application_surface;
	
	draw_surface_blur(surf_blur, blur, blur / 5, 0, 0);
	
	var alpha = (blur / blur_max) * 0.6;
	draw_set_alpha(alpha);
	draw_set_color(c_black);
	
	draw_rectangle(0, 0, gui_width, gui_height, false);
	
	draw_set_color(c_white);
	
	draw_set_halign(fa_middle);
	draw_text_transformed(gui_width / 2, gui_height / 10, dead_text, 1.5, 1.5, 0);
	draw_set_halign(fa_left);
	
	for(var i = 0; i < stats_size; i ++) {
		var key, val, desc, str;
		key = statistic_get_key(i);
		val = ds_map_find_value(map, key);
		desc = statistic_get_description(i);
		str = string(desc) + string(val);
		
		var scl, size, xx, yy;
		scl = 0.5;
		size = 24;
		xx = size; //+ string_width(str) * scl;
		yy = gui_height / 2 - (i - (stats_size - 1) / 2) * size;
		
		draw_text_transformed(xx, yy, str, scl, scl, 0);
	}
	
	draw_set_alpha(1);
	
	button_draw(button, 16);
	
	with(cursor) draw_self();
}