init_default_keys = function() {
	if (!ds_exists(map, ds_type_map)) map = ds_map_create();
	
	map[? "ExplodedTNT"] = 0;
	map[? "Kills"] = 0;
	map[? "Damaged"] = 0;
	map[? "CompletedLevelsRace"] = 0;
	map[? "CompletedLevelsMaxInRace"] = 0;
}

save_data_last = function() {
	var str = json_encode(map);
	buffer_save_string(file_statistic_lasttime, str);

	ds_map_destroy(map);
}

save_data_all = function() {
	var map_sav = ds_map_create();
	
	if (file_exists(file_statistic)) map_sav = json_decode(buffer_load_string(file_statistic));
	
	for(var i = 0; i < array_length(stats_info_array); i ++) {
		var key = statistic_get_key(i), val = map[? key];
		if (map_sav[? key] == undefined) map_sav[? key] = val; else map_sav[? key] += val;
	}
		
	buffer_save_string(file_statistic, json_encode(map_sav));
	
	//init_default_keys();
}

need_save = false;

if (file_exists(file_statistic_lasttime)) {
	var str = buffer_load_string(file_statistic_lasttime);
	map = json_decode(str);
} else {
	map = ds_map_create();
	init_default_keys();
}

var lang = global.game_lang;
statistic_add_info(0, "Kills", ini_translate(file_translate_statistic, lang, "kills"));
statistic_add_info(1, "CompletedLevelsRace", ini_translate(file_translate_statistic, lang, "levels_completed"));
statistic_add_info(2, "Damaged", ini_translate(file_translate_statistic, lang, "damaged"));
statistic_add_info(3, "ExplodedTNT", ini_translate(file_translate_statistic, lang, "exploded_tnt"));

stats_size = 4;

show_results = false;

blur = 0;
blur_max = 25;
surf_blur = application_surface;

dead_text = ini_translate(file_translate_statistic, lang, "you_dead");

button_text = ini_translate(file_translate_statistic, lang, "button_text");
button_alpha = 0;
button = button_create(spr_menu_button, 1, 0, gui_width / 2, gui_height * 0.9, c_white, button_alpha, button_text);