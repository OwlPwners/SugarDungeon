draw_rectangle(0, 0, room_width, room_height, true);

var draw_tile_rect = function() {
	var xx, yy;
	xx = (mouse_x div tile_size) * tile_size;
	yy = (mouse_y div tile_size) * tile_size;
	draw_rectangle(xx, yy, xx + tile_size, yy + tile_size, true);
}

switch(lmmode) {
	case("Room") :
	draw_tile_rect();
	break;
	
	case("FixGrid") :
	draw_tile_rect();
	break;
}

with(all) {
	if (sprite_exists(sprite_index)) draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, max(0.5, image_alpha));
}