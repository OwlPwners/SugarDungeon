if (room == rm_gameload) {
	if (room_next(room) == rm_redactor) {
		global.lmst = show_question("Хотите загрузить комнату?");
		
		if (!global.lmst) {
			room_set_width(rm_redactor, get_integer("Введите ширину.", "") * tile_size);
			room_set_height(rm_redactor, get_integer("Введите высоту.", "") * tile_size);
		} else {
			var map = ds_map_create(); ds_map_read(map, clipboard_get_text());
			var grid_room = ds_grid_create(0, 0);
			ds_grid_read(grid_room, map[? "room"]);
			 
			rm_w = ds_grid_width(grid_room);
			rm_h = ds_grid_height(grid_room);
			
			ds_grid_destroy(grid_room);
			
			room_set_width(rm_redactor, rm_w * tile_size);
			room_set_height(rm_redactor, rm_h * tile_size);
		}
	}
	
	instance_destroy(self);
	
	exit;
}

if (global.lmst) {
	var map = ds_map_create(), grid_room = ds_grid_create(0, 0), grid_fix = ds_grid_create(0, 0);
	ds_map_read(map, clipboard_get_text());
	ds_grid_read(grid_room, map[? "room"]);
	ds_grid_read(grid_fix, map[? "fix"]);
			
	rm_w = ds_grid_width(grid_room);
	rm_h = ds_grid_height(grid_room);
	
	for(var i = 0; i < rm_w; i ++) {
		for(var j = 0; j < rm_h; j ++) {
			tilemap_set(layer_collision, grid_room[# i, j], i, j);
		}
	}
	
	for(var i = 0; i < rm_w; i ++) {
		for(var j = 0; j < rm_h; j ++) {
			tilemap_set(layer_lm_fixgrid, grid_fix[# i, j], i, j);
		}
	}
	
	ds_grid_destroy(grid_room);
	ds_grid_destroy(grid_fix);
	
	var list = ds_list_create(); ds_list_read(list, map[? "objects"]);
	for(i = 0; i < ds_list_size(list); i ++) {
		var lmap = ds_map_create(); ds_map_read(lmap, list[| i]);
		instance_create_depth(lmap[? "x"], lmap[? "y"], 100 + i, asset_get_index(lmap[? "name"]));
	}
	ds_list_destroy(list);
	
	ds_map_destroy(map);
}

tilemap_set_width(layer_collision, room_width div tile_size);
tilemap_set_width(layer_lm_fixgrid, room_width div tile_size);
tilemap_set_height(layer_collision, room_height div tile_size);
tilemap_set_height(layer_lm_fixgrid, room_height div tile_size);

display_set_gui_size(gui_width, gui_height);

game_object = ds_list_create();

var i = 0, obj = 0;
while(object_exists(obj)) {
	//var parent = object_get_parent(obj);
	if (ds_list_find_index(game_object, obj) == -1 
	&& object_is_ancestor(obj, i_must_save) && obj != i_must_save 
	/* && sprite_exists(object_get_sprite(obj))*/) {
		game_object[| i] = obj;
		
		i ++;
	}
	
	//var parent = object_get_parent(obj);
	//if (object_get_parent(parent) == i_must_save || parent == i_must_save) {
	//	game_object[i] = obj;
		
	//	i ++;
	//}
	
	obj ++;
}

obj_index = 0;

selected_object = -1;

grid_enable = true;

cam_w = gui_width;
cam_h = gui_height;
cam_scale = 1;

lmmode = "Objects";























//grid = ds_grid_create(room_width div tile_size, room_height div tile_size);
//for(var i = 0; i < (room_width div tile_size); i ++) {
//	for(var j = 0; j < (room_height div tile_size); j ++) {
//		grid[# i, j] = tilemap_get(layer_collision, i, j);
//	}
//}

//list_enemy = ds_list_create();
//with(i_must_save) {
//	var map = ds_map_create();
//	map[? "name"] = object_get_name(object_index);
//	map[? "x"] = x;
//	map[? "y"] = y;
		
//	ds_list_add(level_maker.list_enemy, ds_map_write(map));
//}

//map_box = ds_map_create();
//with(bounding_controller) {
//	ds_map_add(level_maker.map_box, "x", x);
//	ds_map_add(level_maker.map_box, "y", y);
	
//	ds_map_add(level_maker.map_box, "width", room_width - tile_size * 2);
//	ds_map_add(level_maker.map_box, "height", room_height - tile_size * 2);
//}