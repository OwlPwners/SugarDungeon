if (keyboard_check_released(ord("R"))) lmmode = "Room";
if (keyboard_check_released(ord("O"))) lmmode = "Objects";
if (keyboard_check_released(ord("F"))) lmmode = "FixGrid";

switch(lmmode) {
	case("Objects") :
	var mouse_axis = mouse_wheel_down() - mouse_wheel_up(), key_axis = keyboard_check_released(vk_right) - keyboard_check_released(vk_left);
	if (!keyboard_check(vk_control)) obj_index += mouse_axis + key_axis;

	obj_index = clamp(obj_index, 0, ds_list_size(game_object) - 1);

	if (keyboard_check_released(ord("G"))) {
		grid_enable = !grid_enable;
	}

	if (mouse_check_button_released(mb_left)) {
		instance_create_depth(mouse_x, mouse_y, 100, game_object[| obj_index]);
	}

	if (mouse_check_button_released(mb_right)) {
		selected_object = -1;
	}

	if (selected_object == -1) {
		if (mouse_check_button(mb_right)) {
			var list = ds_list_create();
			instance_position_list(mouse_x, mouse_y, i_must_save, list, false);
		
			var pdepth = 9999;
			for(var i = 0; i < ds_list_size(list); i ++) {
				var ndepth = min(pdepth, list[| i].depth);
				if (ndepth != pdepth) {
					selected_object = list[| i];
				
					pdepth = ndepth;
				}
			}
		
			ds_list_destroy(list);
		}
	} else {
		var enable = grid_enable;
		with(selected_object) {
			var xto, yto;
			if (enable) {
				xto = mouse_x - (mouse_x mod 32);
				yto = mouse_y - (mouse_y mod 16);
			} else {
				xto = mouse_x;
				yto = mouse_y;
			}
		
			x += (xto - x) / 2;
			y += (yto - y) / 2;
		
			depth = -bbox_bottom;
		}
	
		if (keyboard_check_released(ord("D"))) {
			instance_destroy(selected_object);
			selected_object = -1;
		}
	}
	break;
	
	case("Room") :
	var xx = mouse_x div tile_size, yy = mouse_y div tile_size;
	if (mouse_check_button(mb_left)) tilemap_set(layer_collision, 1, xx, yy);
	if (mouse_check_button(mb_right)) tilemap_set(layer_collision, 0, xx, yy);
	break;
	
	case("FixGrid") :
	var xx = mouse_x div tile_size, yy = mouse_y div tile_size;
	if (mouse_check_button(mb_left)) tilemap_set(layer_lm_fixgrid, 2, xx, yy);
	if (mouse_check_button(mb_right)) tilemap_set(layer_lm_fixgrid, 0, xx, yy);
	break;
}

var cam_axis_x = keyboard_check(ord("D")) - keyboard_check(ord("A")),
cam_axis_y = keyboard_check(ord("S")) -	keyboard_check(ord("W"));
x += cam_axis_x * 3 * cam_scale;
y += cam_axis_y * 3 * cam_scale;

if (keyboard_check(vk_control)) {
	var mouse_axis = mouse_wheel_down() - mouse_wheel_up();
	cam_scale += mouse_axis * 0.2;
	cam_scale = clamp(cam_scale, 0.3, 2.7);
	
	camera_set_view_size(view_camera[0], cam_w * cam_scale, cam_h * cam_scale);
}

camera_set_view_pos(view_camera[0], x - cam_w * cam_scale / 2, y - cam_h * cam_scale / 2); 

if (keyboard_check_released(ord("S"))) {
	var grid_col = ds_grid_create(room_width div tile_size, room_height div tile_size);
	for(var i = 0; i < (room_width div tile_size); i ++) {
		for(var j = 0; j < (room_height div tile_size); j ++) {
			grid_col[# i, j] = tilemap_get(layer_collision, i, j);
		}
	}
	
	var grid_fix = ds_grid_create(room_width div tile_size, room_height div tile_size);
	for(var i = 0; i < (room_width div tile_size); i ++) {
		for(var j = 0; j < (room_height div tile_size); j ++) {
			grid_fix[# i, j] = tilemap_get(layer_lm_fixgrid, i, j);
		}
	}
	
	list_obj = ds_list_create();
	with(i_must_save) {
		var map = ds_map_create();
		map[? "name"] = object_get_name(object_index);
		map[? "x"] = x;
		map[? "y"] = y;
		
		var map_special = undefined;
		
		#region Specials... 
		switch(object_index) {
			case(secret_region) :
			map_special = ds_map_create();
			
			map_special[? "sr_width"] = sr_width;
			map_special[? "sr_height"] = sr_height;
			break;
			
			case(boss_background) :
			map_special = ds_map_create();
			
			map_special[? "sprite"] = sprite;
			break;
		}
		#endregion
		
		if (map_special != undefined) {
			map[? "specialmap"] = ds_map_write(map_special);
		} else {
			map[? "specialmap"] = undefined;
		}
		
		ds_list_add(level_maker.list_obj, ds_map_write(map));
	}
	
	map_box = ds_map_create();
	with(bounding_controller) {
		ds_map_add(level_maker.map_box, "x", x);
		ds_map_add(level_maker.map_box, "y", y);
		
		ds_map_add(level_maker.map_box, "width", room_width - tile_size * 2);
		ds_map_add(level_maker.map_box, "height", room_height - tile_size * 2);
	}
	
	var to_save;
	to_save = ds_map_create();
	ds_map_add(to_save, "room", ds_grid_write(grid_col));
	ds_map_add(to_save, "fix", ds_grid_write(grid_fix));
	ds_map_add(to_save, "objects", ds_list_write(list_obj));
	ds_map_add(to_save, "bounding box", ds_map_write(map_box));
	
	clipboard_set_text(ds_map_write(to_save));
	
	ds_map_destroy(to_save);
	ds_grid_destroy(grid_col);
	ds_grid_destroy(grid_fix);
	ds_list_destroy(list_obj);
	ds_map_destroy(map_box);
}













//if (keyboard_check_released(ord("S"))) {
//	var to_save;
//	to_save = ds_map_create();
//	ds_map_add(to_save, "room", ds_grid_write(grid));
//	ds_map_add(to_save, "objects", ds_list_write(list_enemy));
//	ds_map_add(to_save, "bounding box", ds_map_write(map_box));
	
//	clipboard_set_text(ds_map_write(to_save));
	
//	ds_map_destroy(to_save);
//}