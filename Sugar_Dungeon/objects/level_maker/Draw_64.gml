switch(lmmode) {
	case("Objects") :
	if (!mouse_check_button(mb_right)) {
		var sprite = object_get_sprite(game_object[| obj_index]), name = object_get_name(game_object[| obj_index]);
		if (!sprite_exists(sprite)) sprite = spr_question;
		draw_sprite_ext(sprite, 0, device_mouse_x_to_gui(0), device_mouse_y_to_gui(0), 1 / cam_scale, 1 / cam_scale, 0, c_white, 1);
		draw_text_transformed(device_mouse_x_to_gui(0) - string_width(name) / 2 * (1 / cam_scale), device_mouse_y_to_gui(0) + sprite_get_height(sprite) * (1 / cam_scale), name, 1 / cam_scale, 1 / cam_scale, 0);
		
		draw_text_transformed(4, gui_height - 10, "Grid: " + string(grid_enable), 0.5, 0.5, 0);

		var str = "X: " + string(mouse_x) + " Y: " + string(mouse_y);
		draw_text_transformed(gui_width - string_width(str) * 0.5 - 10, gui_height - 10, str, 0.5, 0.5, 0);
	}
	break;
	
	case("Room") :
	var str = "X: " + string(mouse_x div tile_size) + " Y: " + string(mouse_y div tile_size);
	draw_text_transformed(gui_width - string_width(str) * 0.5 - 10, gui_height - 10, str, 0.5, 0.5, 0);
	break;
	
	case("FixGrid") :
	var str = "X: " + string(mouse_x div tile_size) + " Y: " + string(mouse_y div tile_size);
	draw_text_transformed(gui_width - string_width(str) * 0.5 - 10, gui_height - 10, str, 0.5, 0.5, 0);
	break;
}

draw_text_transformed(gui_width / 2 - string_width(lmmode) * 0.25, gui_height - 10, lmmode, 0.5, 0.5, 0);