if (!instance_exists(player)) exit;

if (surface_exists(surf_light)) {
	surface_set_target(surf_light);
	draw_clear_alpha(light_colour_bg, light_alpha);
	
	gpu_set_blendmode(bm_add);
	
	var xx, yy;
	xx = camera.x - gui_width / 2;
	yy = camera.y - gui_height / 2;
	
	draw_sprite_ext(spr_light, 0, player.x - xx, player.y - yy, 0.6, 0.6, 0, c_white, 1);
	
	with(light) {
		var scale = random_range(1, 1.3);
		draw_sprite_ext(spr_light, 0, x - xx, y - yy, scale * (gui_width / camera.cam_w), scale * (gui_height / camera.cam_h), 0, color_light, alpha_light);
	}
	
	var arr = tag_get_assets("light"), i = 0, l = array_length(arr);
	repeat(l) {
		var idd = asset_get_index(arr[i]), scale = random_range(1, 1.3);
		with(idd) draw_sprite_ext(spr_light, 0, x - xx, y - yy, scale * (gui_width / camera.cam_w), scale * (gui_height / camera.cam_h), 0, color_light, alpha_light);
		
		i ++;
	}
	
	with(soda_bullet) {
		draw_sprite(spr_light, 0, x - xx, y - yy);
	}
	
	with(boss_soda) {
		draw_sprite_ext(spr_light, 0, x - xx, y - yy, 2.5, 2.5, 0, c_white, 1);
	}
	
	with(boss_icecream) {
		draw_sprite_ext(spr_light, 0, x - xx, y - yy, 2.5, 2.5, 0, c_white, 1);
	}
	
	gpu_set_blendmode(bm_normal);
	
	surface_reset_target();
} else {
	surf_light = surface_create(gui_width, gui_height);
}