if (!instance_exists(player) || global.game_mod == "show_map") exit;

gpu_set_blendmode_ext(bm_dest_color, bm_zero);

draw_surface(surf_light, 0, 0);

gpu_set_blendmode(bm_normal);