event_inherited();

if (!instance_exists(player)) exit;

if (selected) {
	if (image_index == 0 && player.key_interact == true) {
		opened = true;
	}
	
	if (image_index != img_index_prev) outline_surf = -1;
	
	//if (outline_surf == -1) {
	//	outline_surf = surface_create(sprite_width + 3, sprite_height + 3);
	//	surface_set_target(outline_surf);
		
	//	var xx = sprite_get_xoffset(sprite_index) + 1, yy = sprite_get_yoffset(sprite_index) + 1;
	//	draw_sprite(sprite_index, image_index, xx, yy);
		
	//	surface_reset_target()
	//}
	
	//outline_alpha += (1 - outline_alpha) / 5;
} //else {
	//if (outline_surf != -1 && outline_alpha == 0) {
	//	surface_free(outline_surf);
		
	//	outline_surf = -1;
	//}
	
	//outline_alpha += (0 - outline_alpha) / 5;
//}

if (opened) {
	if (interact_event != -1) event_user(interact_event);
	
	opened = false;
	
	audio_play_sound(sfx_chest_open, 110, false)
	
	with(instance_create_layer(x, y, layer_other, loot)) {
		rar_min = other.rar_min;
		rar_max = other.rar_max;
	}
	
	alarm[0] = 30;
	
	image_speed = 1;
}

img_index_prev = image_index;