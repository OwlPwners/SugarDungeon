filename = file_ui_coords;
selection = "PlayerSlot";

set_size(spr_slot);

x_to = ini_load(file_ui_coords, "Real", "PlayerSlot", "x", gui_width / 2 - (menu_ui_border_w + width / 2)) + menu_ui_border_w;
y_to = ini_load(file_ui_coords, "Real", "PlayerSlot", "y", offset_y) + menu_ui_border_h;
x_start = gui_width / 2;
y_start = gui_height / 2;
x_spd = 1;
y_spd = 1;

alpha_to = 1;
alpha_spd = 4;