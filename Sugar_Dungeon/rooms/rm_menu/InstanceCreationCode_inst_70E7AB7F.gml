filename = file_ui_coords;
selection = "PlayerHealth";

set_size(spr_player_hp);

x_to = ini_load(file_ui_coords, "Real", "PlayerHealth", "x", offset_x) + menu_ui_border_w;
y_to = ini_load(file_ui_coords, "Real", "PlayerHealth", "y", offset_y) + menu_ui_border_h;
x_start = gui_width / 2;
y_start = gui_height / 2;
x_spd = 1;
y_spd = 1;

alpha_to = 1;
alpha_spd = 4;