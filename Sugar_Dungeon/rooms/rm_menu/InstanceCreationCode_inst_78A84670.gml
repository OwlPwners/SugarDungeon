script = menu_button_back;

text = "";

x_to = xstart;
y_to = ystart;
x_start = xstart - 128;
y_start = ystart;
x_spd = 5;
y_spd = 5;

alpha_to = 1;
alpha_start = 0;
alpha_spd = 2;
alpha = 0;

width = 54;
height = 46;

sprite = spr_menu_button_back;
image_pressed = 3;
image_released = 4;
image = 0;