script = menu_button_fullscreen;

text = ini_translate(file_translate_menu, global.game_lang, "fullscreen");

sprite_text_box = spr_menu_button;
image_text_box = 0;
sprite_circle_box = spr_menu_button_circle;
image_circle_pressed = 1;
image_circle_released = 0;
image_circle = window_get_fullscreen();

move_to_move = true;

x_start = xstart;
y_start = ystart - 50;

y_spd = 7;

alpha_to_move = true;

alpha_spd = 5;

draw = true;