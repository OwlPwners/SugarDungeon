lang = global.game_lang

keystring = ini_translate(file_translate_menu, lang, "right");
keyini = "RightKey";
def = ord("D");

var set_text = "", str = ini_load(file_settings, "String", "Control", keyini, def);

switch(str) {
	#region : - )
		case(vk_left) : 
		set_text = "LEFT"
		break;
					
		case(vk_right) : 
		set_text = "RIGHT"
		break;
					
		case(vk_up) : 
		set_text = "UP";
		break;
					
		case(vk_down) : 
		set_text = "DOWN";
		break;
					
		case(vk_tab) : 
		set_text = "TAB";
		break;
					
		case(vk_enter) : 
		set_text = "ENTER";
		break;
					
		case(vk_pagedown) : 
		set_text = "PAGEDOWN";
		break;
					
		case(vk_pageup) : 
		set_text = "PAGEUP";
		break;
					
		case(vk_pause) : 
		set_text = "PAUSE";
		break;
					
		case(vk_shift) : 
		set_text = "SHIFT";
		break;
					
		case(vk_lshift) : 
		set_text = "L-SHIFT";
		break;
					
		case(vk_rshift) : 
		set_text = "R-SHIFT";
		break;
					
		case(vk_alt) : 
		set_text = "ALT";
		break;
					
		case(vk_lalt) : 
		set_text = "L-ALT";
		break;
					
		case(vk_ralt) : 
		set_text = "R-ALT";
		break;
					
		case(vk_delete) : 
		set_text = "DELETE";
		break;
					
		case(vk_backspace) : 
		set_text = "BACKSPACE";
		break;
					
		case(vk_space) : 
		set_text = "SPACE";
		break;
					
		case(vk_control) : 
		set_text = "CTRL";
		break;
					
		case(vk_lcontrol) : 
		set_text = "L-CTRL";
		break;
					
		case(vk_rcontrol) : 
		set_text = "R-CTRL";
		break;
					
		default :
		set_text = chr(str);
		break;
		#endregion
}

text = keystring + set_text;

write_key = false;

x_to = xstart;
y_to = ystart;
x_start = gui_width / 2;
y_start = gui_height / 2;
x_spd = 5;
y_spd = 5;

alpha_to = 1;
alpha_start = 0;
alpha_spd = 3;
alpha = 0;

width = string_width(text) * 0.5 + menu_text_border_width;
height = string_height(text) * 0.5 + menu_text_border_height;

sprite = spr_menu_button;
image_pressed = 1;
image_released = 0;
image = 0;