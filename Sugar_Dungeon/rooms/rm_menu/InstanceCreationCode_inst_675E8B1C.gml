filename = file_ui_coords;
selection = "PlayerMoney";

set_size(spr_currency);

x_to = ini_load(file_ui_coords, "Real", "PlayerMoney", "x", gui_width - (menu_ui_border_w * 2 + width)) + menu_ui_border_w
y_to = ini_load(file_ui_coords, "Real", "PlayerMoney", "y", offset_y) + menu_ui_border_h
x_start = gui_width / 2;
y_start = gui_height / 2;
x_spd = 1;
y_spd = 1;

alpha_to = 1;
alpha_spd = 4;