script = menu_slider_sounds;

name = "sfx";
alpha_to_move = false;

alpha_to = 1;
alpha_start = 0;
alpha_spd = 3;
alpha = 0;

sprite_button = spr_menu_slider;
image_pressed = 1;
image_released = 0;
image = 0;

sprite_rectangle = spr_menu_button;

icons_sprite = spr_menu_sounds;
icons_image = 1;

width = 200;
height = 18;

volume = ini_load(file_settings, "Real", "SoundsAndMusic", "SoundSlider", gui_width * 0.5);