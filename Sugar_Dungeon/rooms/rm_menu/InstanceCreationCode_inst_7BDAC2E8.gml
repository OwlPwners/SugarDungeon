script = menu_button_back;

text = ini_translate(file_translate_menu, global.game_lang, "no");

x_to = xstart;
y_to = ystart;
x_start = gui_width + 1;
y_start = ystart;
x_spd = 7;
y_spd = 7;

alpha_to = 1;
alpha_start = 0;
alpha_spd = 5;
alpha = 0;

width = string_width(text) * 0.5 + menu_text_border_width;
height = string_height(text) * 0.5 + menu_text_border_height;

sprite = spr_menu_button;
image_pressed = 1;
image_released = 0;
image = 0;