varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform sampler2D NormalMap;
uniform float Intensity;
uniform float Aspect;

void main()  {
	vec4 EndColor;
	
	vec2 Offset = (texture2D(NormalMap, v_vTexcoord).xy - 0.5) * 2.0 * Intensity;
	Offset.x /= Aspect;
	
	EndColor = texture2D(gm_BaseTexture, v_vTexcoord + Offset);
	
    gl_FragColor = EndColor;
}