varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float Alpha;

void main() {
    float TexAlpha = texture2D(gm_BaseTexture, v_vTexcoord).a;
	gl_FragColor = vec4(1.0, 1.0, 1.0, Alpha * TexAlpha);
}