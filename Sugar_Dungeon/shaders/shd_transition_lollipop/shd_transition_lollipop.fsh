varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float Time;
uniform vec2 Resolution;
uniform vec4 Settings;

void main() {
	float aspectRatio = Resolution.x / Resolution.y;
	vec2 n = 2.0 * v_vTexcoord.xy / Resolution.y - vec2(aspectRatio, 1.0);
	vec2 uv = 0.8 * n;
	float distSqr = dot(uv, uv);
	//float vignette = 1.0 - distSqr;
	float angle = atan(n.y, n.x);
	float shear = sqrt(distSqr);
	
	float stripes = cos(Settings.x * angle + Settings.y * Time - Settings.z * shear);
	vec3 lightred = vec3(0.7, 0.01, 0.09);
	vec3 red = vec3(1.0, 1.0, 1.0);
	
	vec3 col = texture2D(gm_BaseTexture, v_vTexcoord).rgb;
	vec3 white = vec3(1.0, 1.0, 1.0);
	
    if (col == white) {
		gl_FragColor = vec4(/*vignette * */mix(lightred, red, stripes), Settings.a);
	} else {
		discard;
	}
}