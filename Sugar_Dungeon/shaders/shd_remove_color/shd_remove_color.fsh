varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float Intensity;

void main() {
	vec4 Col = texture2D(gm_BaseTexture, v_vTexcoord);
	float ColOut = (Col.r + Col.g + Col.b) / Intensity;
	
    gl_FragColor = v_vColour * vec4(ColOut, ColOut, ColOut, Col.a);
}