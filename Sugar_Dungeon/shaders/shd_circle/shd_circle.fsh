varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec2 v_vLeftTopPos;

uniform vec2 CirclePos;
uniform float CircleRadius;

uniform vec3 CircleCol;
uniform float AlphaStart;

void main() {
	vec2 Vector = v_vLeftTopPos.xy - CirclePos.xy;
	float Distance = sqrt(Vector.x * Vector.x + Vector.y * Vector.y);
	
	if (Distance < CircleRadius) {
		gl_FragColor = vec4(CircleCol, AlphaStart);
		
		float Alpha = 1.0 - Distance / CircleRadius;
		gl_FragColor = mix(gl_FragColor, vec4(CircleCol, Alpha), vec4(CircleCol, AlphaStart));
	} else {
		discard;
	}
}