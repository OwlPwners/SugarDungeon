varying vec2 v_vTexcoord;
varying vec4 v_vColour;

const vec3 PixelWater = vec3(0, 0, 0);

uniform vec3 Color;

void main() {
	vec4 Pixel = texture2D(gm_BaseTexture, v_vTexcoord);
	if (Pixel.rgb == PixelWater) Pixel = vec4(Color, Pixel.a);
	
    gl_FragColor = v_vColour * Pixel;
}