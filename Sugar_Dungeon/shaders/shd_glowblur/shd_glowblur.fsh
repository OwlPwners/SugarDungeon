varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec2 Texel;
uniform float Intensity;
uniform float Steps;

void main() {
	vec4 BlurCol = vec4(0.0);
	
	vec2 Offset, Sample;
	for(Offset.x = -Steps; Offset.x <= Steps; Offset.x ++) {
		for(Offset.y = -Steps; Offset.y <= Steps; Offset.y ++) {
			Sample = v_vTexcoord + Offset * Texel;
			BlurCol += texture2D(gm_BaseTexture, Sample);
		}
	}
	
	float KernelSqr = (Steps * 2.0 + 1.0) * (Steps * 2.0 + 1.0);
	float EndIntensity = max(1.0, Intensity);
	
	gl_FragColor = v_vColour * BlurCol * (1.0 / KernelSqr) * EndIntensity;
}