varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec4 Color;
uniform vec2 Texel;
uniform float Width;

void main() {
	vec2 OffsetX;
	OffsetX.x = Texel.x * Width;
	vec2 OffsetY;
	OffsetY.y = Texel.y * Width;
	
	float Alpha = texture2D(gm_BaseTexture, v_vTexcoord).a;
	float AlphaOrig = Alpha;
	
	Alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord - OffsetX).a); // LeftMiddle.
	Alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord + OffsetX).a); // RightMiddle.
	Alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord - OffsetY).a); // TopMiddle.
	Alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord + OffsetY).a); // BottomMiddle.
	
	if (AlphaOrig != Alpha && AlphaOrig == 0.0) gl_FragColor = Color; else gl_FragColor = v_vColour * texture2D(gm_BaseTexture, v_vTexcoord);
}