varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec4 Color;

void main() {
	float Pixel = texture2D(gm_BaseTexture, v_vTexcoord).a;
	if (Pixel > 0.0) {
		gl_FragColor = Color;
	} else {
		discard;
	}
}